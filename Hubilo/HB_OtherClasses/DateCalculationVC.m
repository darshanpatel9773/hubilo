//
//  DateCalculationVC.m
//  Hubilo
//
//  Created by Priyank Gandhi on 17/08/16.
//  Copyright ©2016 nirav. All rights reserved.
//

#import "DateCalculationVC.h"

@interface DateCalculationVC ()

@end

@implementation DateCalculationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (NSString *)getDateCaption:(NSString *)strDate {
    
    
    
    //NSDate *today = [NSDate date];
    NSDateFormatter *dtFormatter  = [[NSDateFormatter alloc] init];
    [dtFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *toDate = [dtFormatter dateFromString:strDate];
//
//    unsigned flags = NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour |  NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear;
//    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    NSDateComponents *components = [calendar components:flags fromDate:toDate toDate:today options:0];
    
    double deltaSeconds = [[NSDate date] timeIntervalSinceDate:toDate];
    double deltaMinutes = deltaSeconds / 60.0f;

    
//    if([components day] > 7) {
//        return [NSString stringWithFormat:@"%ld week ago",(long)[components day]/7];
//    }
//    
//    if([components day] >= 1) {
//        return [NSString stringWithFormat:@"%ld day ago",(long)[components day]];
//    }
//    
//    if([components hour] >= 1) {
//        return [NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
//    }
//    
//    if([components minute] >= 1) {
//        return [NSString stringWithFormat:@"%ld minutes ago",(long)[components minute]];
//    }
//    
//    if([components second] >= 1) {
//        return [NSString stringWithFormat:@"%ld seconds ago",(long)[components second]];
//    }
//    
//    NSLog(@"%ld",(long)[components second]);
//    NSLog(@"%ld",(long)[components minute]);
//    NSLog(@"%ld",(long)[components hour]);
//    NSLog(@"%ld",(long)[components day]);
//    NSLog(@"%ld",(long)[components month]);
    int minutes;
    
    if(deltaSeconds < 5) {
        return [NSString stringWithFormat:@"Just Now"];
    }
    else if(deltaSeconds < 60)
    {
        return [NSString stringWithFormat:@"%ld seconds ago",(long)deltaSeconds];
    }
    else if(deltaSeconds < 120)
    {
        return [NSString stringWithFormat:@"A minute ago"];
    }
    else if (deltaMinutes < 60)
    {
        return [NSString stringWithFormat:@"%ld minutes ago",(long)deltaMinutes];
    }
    else if (deltaMinutes < 120)
    {
        return [NSString stringWithFormat:@"An hour ago"];
    }
    else if (deltaMinutes < (24 * 60))
    {
        minutes = (int)floor(deltaMinutes/60);
        return [NSString stringWithFormat:@"%d hours ago",minutes];
    }
    else if (deltaMinutes < (24 * 60 * 2))
    {
        return [NSString stringWithFormat:@"Yesterday"];
    }
    else if (deltaMinutes < (24 * 60 * 7))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24));
        return [NSString stringWithFormat:@"%d days ago",minutes];
    }
    else if (deltaMinutes < (24 * 60 * 14))
    {
        return [NSString stringWithFormat:@"Last week"];
    }
    else if (deltaMinutes < (24 * 60 * 31))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24 * 7));
        return [NSString stringWithFormat:@"%d weeks ago",minutes];
    }
    else if (deltaMinutes < (24 * 60 * 61))
    {
        return [NSString stringWithFormat:@"Last month"];
    }
    else if (deltaMinutes < (24 * 60 * 365.25))
    {
        minutes = (int)floor(deltaMinutes/(60 * 24 * 30));
        return [NSString stringWithFormat:@"%d months ago",minutes];
    }
    else if (deltaMinutes < (24 * 60 * 731))
    {
        return [NSString stringWithFormat:@"Last year"];
    }
    
    minutes = (int)floor(deltaMinutes/(60 * 24 * 365));
    return [NSString stringWithFormat:@"%d years ago",minutes];
    

    return strDate;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
