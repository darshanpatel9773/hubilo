//
//  Constants.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 17/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import Foundation
struct Constants {
    
    //Segue Constants
    
    struct eventInfo {
        static let kEventId = "512"
    }
    
    struct SegueConstant {
        static let kShowExibitorsInfoVC = "ShowExibitorsInfoVC"
        static let kShowSponserInfoVC = "ShowHB_SponserInfoVC"
        static let kShowSpeakerInfoVC = "ShowSpeakerInfoVC"
        static let kShowScheduleInfoVC = "ShowScheduleInfoVC"
        static let kShowAttendeeInfoVC = "ShowAttendeeInfoVC"
        static let kShowEventInfoFromSignupVC = "ShowEventInfoFromSignupVC"
        
    }
    struct URL {
        static let BASE_URL = "http://www.hubilo.in/eventApp/ws/customAppEngine/"
        
        //User JOIN
        static let JOIN_COMMUNITY = BASE_URL + "userAddCommunity.php?Add_Request="
        
        // New User Registartion 
        static let USER_SIGNUP = BASE_URL + "signup.php?Register_Request="
        static let SOCIAL_LOGIN = BASE_URL + "sociallogin.php?SocialLogin_Request="
        
        //Upload Image 
        static let UPLOAD_IMAGE = BASE_URL + "uploadimage.php"
        
        //USER 
        static let GET_ALL_DETAILS_SINGLE_USER = BASE_URL + "useralldetails.php?Useralldetails_Request="
        
        // Side bar color
        static let APPLICATION_DATA = BASE_URL + "sideBar.php?SideBar_Request="
        
        //Get all Event details
        static let EVENT_ALL_DETAILS = BASE_URL + "eventDetails.php?Eventalldetails_Request="
        
        //Get All Schedule LIST
        static let SCHEDULE_ALL_DETAILS = BASE_URL + "agendaList.php?Agendaeventdetails_Request="
        static let QUESTION_LIST = BASE_URL + "agendaQuestionList.php?AgendaquestionList_Request="
        static let POST_QUESTION = BASE_URL + "agendaQuestionCreate.php?AgendaquestionCreate_Request="
        static let AGENDA_LIKE = BASE_URL + "agendaLike.php?Agendalike_Request="
        static let QUESTION_LIKE = BASE_URL + "agendaQuestionLike.php?Agendalike_Request="
        static let AGENDA_MY_SCHEDULE = BASE_URL + "agendaMySchedule.php?Agendaeventdetails_Request="
        
        //Community
        static let GET_ALL_ATENDEE = BASE_URL + "communityList.php?Communitylist_Request="
        static let VIEW_ATENDEE_PROFILE = BASE_URL + "viewProfileData.php?viewUserData="
        
        //Book Mark
        static let GET_ALL_BOOKMARK = BASE_URL + "getBookmarkList.php?BookmarkData="
        
        //Note 
        static let GET_ALL_NOTE = BASE_URL + "fetchAllNote.php?NoteData="
        static let SAVE_NOTE = BASE_URL + "saveUserNote.php?NoteData="
        static let SINGLE_USER_NOTE = BASE_URL + "getUserNote.php?NoteData="
        
        //Speaker 
        static let SPEAKER_LIST = BASE_URL + "speakerList.php?Speakerlist_Request="
        static let SPEAKER_AGENDA = BASE_URL + "speakerAgenda.php?SpeakerAgendalist_Request="
        static let SPEAKER_RATING = BASE_URL + "speakerRating.php?speakerRating_Request="
        static let SPEAKER_RATING_UPDATE = BASE_URL + "speakerRatingUpdate.php?speakerRating_Request="
        
        //Exhibitor
        static let EXHIBITOR_LIST = BASE_URL + "exhibitorList.php?Exhibitorslist_Request="
        
        //Sponsor
        static let SPONSOR_LIST = BASE_URL + "sponsorList.php?Sponsorslist_Request="
        
        //Edit_Profile
        static let EDIT_PROFILE_UPDATE = BASE_URL + "editprofile.php?Profileedit_Request="
        static let INDUSTRY_LIST = BASE_URL + "industrieslist.php"
        static let INTEREST_LIST = BASE_URL + "interestlist.php"
        
        //Log out
        static let USER_LOGOUT = BASE_URL + "logout.php?Logout_Request="
        
        //Message
        static let MESSAGES = BASE_URL + "recentmessage.php?Recentmessage_Request="
        static let USER_MESSAGE = BASE_URL + "getmessage.php?Getmessage_Request="
        static let SEND_MESSAGE = BASE_URL + "sendmessage.php?Sendmessage_Request="
        static let READ_MESSAGE = BASE_URL + "markMessageRead.php?read_request="
        
        //discussion forum
        static let DISCUSSION_FORUM = BASE_URL + "discussionForum.php?DiscussionForumDataRequest="
        static let DISCUSSION_COMMENTS = BASE_URL + "getDiscussionForumComments.php?GetDiscussionForumCommentsData="
        static let DISCUSSION_POST_COMMENT = BASE_URL + "postCommentDiscussionForum.php"
        static let DISCUSSION_LIKE = BASE_URL + "discussionForumLike.php?Discussionlike_Request="
        static let DISCUSSION_FOUM_VIEW = BASE_URL + "discussionForumView.php?Discussion_forum_view_Request="
        static let DISCUSSION_POST_CCREATE = BASE_URL +  "createDiscussionForumTopic.php?"
        
        //Notification List
        static let GET_ALL_NOTIFICATION = BASE_URL + "notificationList_ios.php?request="
        
        //Polls
        static let GET_ALL_POLL = BASE_URL + "getPolls.php?GetPollsData="
        static let CREATE_POLL = BASE_URL + "conductPoll.php"
        static let LIKE_POLL = BASE_URL + "voteForPoll.php?voteForPoll_Request="
        static let VOTE_POLL = BASE_URL + "recordResponseForPoll.php?RecordResponseForPoll_Request="
        
        
        static let USER_REGISTER = BASE_URL + "registration.php"
        static let USER_LOGIN = BASE_URL + "login.php?Login_Request="
        
        
        static let FORGET_PASSWORD = BASE_URL + "forgetpassword_new.php?Forgetpassword_Request="
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        static let BOOKMARK_ADD = BASE_URL + "setBookmark.php?BookmarkData="
        static let BOOKMARK_LIST = BASE_URL + "getBookmarkList.php?BookmarkData="
    }
    struct WSColorsCodes {
        
    }
    struct ImagePaths {
        
        static let IMAGE_BASIC_URL = "http://hubilo.com/eventApp/ws/images/"
        static let kAttendeeImagePath = IMAGE_BASIC_URL + "profile/thumb/"
        static let kSpeakerImagePath =  IMAGE_BASIC_URL + "speaker/profile/thumb/"
        static let kSponserImagePath =  IMAGE_BASIC_URL + "sponsor/logo/thumb/"
        static let kEventCoverImagePath = IMAGE_BASIC_URL + "event/cover/mobile/thumb/"
        static let kExhibitorImagePath = IMAGE_BASIC_URL + "exhibitor/logo/thumb/"
        static let BROCHURE_PATH =  "http://hubilo.com/eventApp/ws/brochure/"
        static let kExhibitorBrochure = BROCHURE_PATH + "exhibitor/"
        static let kSponsorBrochure = BROCHURE_PATH + "sponsor/"

        
        
    }
}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
}
