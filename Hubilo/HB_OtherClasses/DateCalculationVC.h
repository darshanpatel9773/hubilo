//
//  DateCalculationVC.h
//  Hubilo
//
//  Created by Priyank Gandhi on 17/08/16.
//  Copyright ©2016 nirav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateCalculationVC : UIViewController

+ (NSString *)getDateCaption:(NSString *)strDate;

@end
