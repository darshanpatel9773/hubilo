//
//  Globals.swift
//  Hubilo
//
//  Created by Aadil on 7/8/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
    class Globals {
        var currentUserID : String
        var currentUser : HB_ModelUser!
        var fbPic : String!
        var backgroundImg : String!
        var isUserLoggedIn : Bool
        var userCommunityExist : Int = 0
        var color : String!
        
        static let sharedInstance = Globals()
        
        private init() {
            currentUserID=""
            isUserLoggedIn=false
        } //This prevents others from using the default '()' initializer for this class.
    }
