//
//  Messages.swift
//  Hubilo
//
//  Created by Aadil on 7/28/16.
//  Copyright © 2016 nirav. All rights reserved.
//

import UIKit
import ObjectMapper
class Messages: NSObject, Mappable {
    var status = ""
    var msg = ""
    var recenthistorylist : [HistoryList]!
    override init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    
     func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        recenthistorylist <- map["recenthistorylist"]
    }
}

class HistoryList : NSObject, Mappable {
    var user_id = ""
    var message_text = ""
    var time : Double = 0
    var state = 0
    var firstname = ""
    var lastname = ""
    var profile_img = ""
    override init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    func mapping(map: Map) {
        user_id <- map ["user_id"]
        message_text <- map ["message_text"]
        time <- map ["time"]
        state <- map ["state"]
        firstname <- map ["firstname"]
        lastname <- map ["lastname"]
        profile_img <- map ["profile_img"]
    }
}
