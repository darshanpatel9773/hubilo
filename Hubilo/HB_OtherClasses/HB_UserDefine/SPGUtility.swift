//
//  SPGUtility.swift
//  RE_RepairEstimation
//
//  Created by Priyank Gandhi on 15/05/16.
//  Copyright © 2016 Priyank Gandhi. All rights reserved.
//

import UIKit
import ReachabilitySwift
class SPGUtility: NSObject {
    
    static var instance: SPGUtility!
    
    // SHARED INSTANCE
    class func sharedInstance() -> SPGUtility {
        self.instance = (self.instance ?? SPGUtility())
        return self.instance
    }
    
    func showAlert(message :NSString , titleMessage: NSString,viewController:UIViewController)
    {
        let alert = UIAlertController(title: titleMessage as String, message: message as String, preferredStyle: .Alert)
        let ok = UIAlertAction(title: "OK", style:.Default, handler: nil)
        
        alert.addAction(ok)
               viewController.presentViewController(alert, animated: true, completion: nil)
    }
    
    class func hasConnectivity() -> Int {
        do
        {
            let reachability: Reachability = try Reachability.reachabilityForInternetConnection()
            let networkStatus: Int = reachability.currentReachabilityStatus.hashValue
            return networkStatus
        }catch
        {
            
        }
        return 0
    }
    func Is_Internet_Connection_Available()->Bool
    {
        return true
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func isPasswordLength(password: String , confirmPassword : String) -> Bool {
        if password.characters.count <= 7 && confirmPassword.characters.count <= 7{
            return true
        }
        else{
            return false
        }
    }
    
    func isValidPincode(value: String) -> Bool {
        if value.characters.count <= 4{
            return true
        }
        else{
            return false
        }
    }
    
    func isPasswordSame(password: String , confirmPassword : String) -> Bool {
        if password == confirmPassword{
            return true
        }
        else{
            return false
        }
    }

    func validatePhone(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluateWithObject(value)
        return result
    }
    
}
