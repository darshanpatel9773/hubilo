//
//  SPGWebService.swift
//  RE_RepairEstimation
//
//  Created by Priyank Gandhi on 06/05/16.
//  Copyright © 2016 Priyank Gandhi. All rights reserved.
//

import UIKit
import Alamofire

class SPGWebService: NSObject {

    
    class func callPostWebService(WSUrl:String,WSParams:Dictionary<String, AnyObject>?,StringReturnType:Bool, WSCompletionBlock:(data:AnyObject?,error:NSError?) -> ())
    {
        
        //let url = NSURL(string: WSUrl)
        //let aStrDomain = url?.host
        
        Alamofire.request(.POST, WSUrl,parameters: WSParams)
            .response { request, response, data, error in
                
                WSCompletionBlock(data:data!,error:nil)
                
                
                print(data)
                //                if(StringReturnType)
                //                {
                //                    let iStrResult:NSString = NSString(data:data!, encoding:NSUTF8StringEncoding)!
                //
                //                    WSCompletionBlock(data:iStrResult as String,error:nil)
                //
                //                }
                //                else
                //                {
                //                    do
                //                    {
                //                        let object:AnyObject? = try NSJSONSerialization.JSONObjectWithData(data!, options:[])
                //
                //
                //
                //                        WSCompletionBlock(data:object!,error:nil)
                //                    }
                //                    catch let caught as NSError
                //                    {
                //                        WSCompletionBlock(data:nil,error: caught)
                //                    }
                //                    catch
                //                    {
                //
                //                        let error: NSError = NSError(domain: aStrDomain!, code: 1, userInfo: nil)
                //
                //                        WSCompletionBlock(data:nil,error: error)
                //                    }
                //
                //                }
                
        }
    }
    
    
    
    class func callGETWebService(WSUrl:NSURL,WSCompletionBlock:(data:AnyObject?,error:NSError?) -> ())
    {
        
        //let url = NSURL(string: WSUrl)
        let aStrDomain = WSUrl.host
        
        Alamofire.request(.GET, WSUrl.absoluteString!)
            .response { request, response, data, error in
                
                do
                {
                    let object:AnyObject? = try NSJSONSerialization.JSONObjectWithData(data!, options:[])
                    WSCompletionBlock(data:object!,error:nil)
                }
                catch let caught as NSError
                {
                    WSCompletionBlock(data:nil,error: caught)
                }
                catch
                {
                    
                    let error: NSError = NSError(domain: aStrDomain!, code: 1, userInfo: nil)
                    
                    WSCompletionBlock(data:nil,error: error)
                }
        }
    }

    
}
