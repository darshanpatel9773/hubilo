//
//	HB_SpeakerList.swift
//
//	Create by Priyank Gandhi on 27/6/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_SpeakerList : NSObject, NSCoding{
    
    var speakerCategory : String!
    var speakerDescription : String!
    var speakerId : String!
    var speakerLinkedinLink : String!
    var speakerLongDescription : String!
    var speakerName : String!
    var speakerPosition : String!
    var speakerProfileImg : String!
    var speakerRating : String!
    var speakerTitle : String!
    var speakerTwitterFollow : String!
    var speakerUserRating : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        speakerCategory = dictionary["speaker_category"] as? String
        speakerDescription = dictionary["speaker_description"] as? String
        speakerId = dictionary["speaker_id"] as? String
        speakerLinkedinLink = dictionary["speaker_linkedin_link"] as? String
        speakerLongDescription = dictionary["speaker_long_description"] as? String
        speakerName = dictionary["speaker_name"] as? String
        speakerPosition = dictionary["speaker_position"] as? String
        speakerProfileImg = dictionary["speaker_profile_img"] as? String
        speakerRating = dictionary["speaker_rating"] as? String
        speakerTitle = dictionary["speaker_title"] as? String
        speakerTwitterFollow = dictionary["speaker_twitter_follow"] as? String
        speakerUserRating = dictionary["speaker_user_rating"] as? String
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if speakerCategory != nil{
            dictionary["speaker_category"] = speakerCategory
        }
        if speakerDescription != nil{
            dictionary["speaker_description"] = speakerDescription
        }
        if speakerId != nil{
            dictionary["speaker_id"] = speakerId
        }
        if speakerLinkedinLink != nil{
            dictionary["speaker_linkedin_link"] = speakerLinkedinLink
        }
        if speakerLongDescription != nil{
            dictionary["speaker_long_description"] = speakerLongDescription
        }
        if speakerName != nil{
            dictionary["speaker_name"] = speakerName
        }
        if speakerPosition != nil{
            dictionary["speaker_position"] = speakerPosition
        }
        if speakerProfileImg != nil{
            dictionary["speaker_profile_img"] = speakerProfileImg
        }
        if speakerRating != nil{
            dictionary["speaker_rating"] = speakerRating
        }
        if speakerTitle != nil{
            dictionary["speaker_title"] = speakerTitle
        }
        if speakerTwitterFollow != nil{
            dictionary["speaker_twitter_follow"] = speakerTwitterFollow
        }
        if speakerUserRating != nil{
            dictionary["speaker_user_rating"] = speakerUserRating
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        speakerCategory = aDecoder.decodeObjectForKey("speaker_category") as? String
        speakerDescription = aDecoder.decodeObjectForKey("speaker_description") as? String
        speakerId = aDecoder.decodeObjectForKey("speaker_id") as? String
        speakerLinkedinLink = aDecoder.decodeObjectForKey("speaker_linkedin_link") as? String
        speakerLongDescription = aDecoder.decodeObjectForKey("speaker_long_description") as? String
        speakerName = aDecoder.decodeObjectForKey("speaker_name") as? String
        speakerPosition = aDecoder.decodeObjectForKey("speaker_position") as? String
        speakerProfileImg = aDecoder.decodeObjectForKey("speaker_profile_img") as? String
        speakerRating = aDecoder.decodeObjectForKey("speaker_rating") as? String
        speakerTitle = aDecoder.decodeObjectForKey("speaker_title") as? String
        speakerTwitterFollow = aDecoder.decodeObjectForKey("speaker_twitter_follow") as? String
        speakerUserRating = aDecoder.decodeObjectForKey("speaker_user_rating") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encodeWithCoder(aCoder: NSCoder)
    {
        if speakerCategory != nil{
            aCoder.encodeObject(speakerCategory, forKey: "speaker_category")
        }
        if speakerDescription != nil{
            aCoder.encodeObject(speakerDescription, forKey: "speaker_description")
        }
        if speakerId != nil{
            aCoder.encodeObject(speakerId, forKey: "speaker_id")
        }
        if speakerLinkedinLink != nil{
            aCoder.encodeObject(speakerLinkedinLink, forKey: "speaker_linkedin_link")
        }
        if speakerLongDescription != nil{
            aCoder.encodeObject(speakerLongDescription, forKey: "speaker_long_description")
        }
        if speakerName != nil{
            aCoder.encodeObject(speakerName, forKey: "speaker_name")
        }
        if speakerPosition != nil{
            aCoder.encodeObject(speakerPosition, forKey: "speaker_position")
        }
        if speakerProfileImg != nil{
            aCoder.encodeObject(speakerProfileImg, forKey: "speaker_profile_img")
        }
        if speakerRating != nil{
            aCoder.encodeObject(speakerRating, forKey: "speaker_rating")
        }
        if speakerTitle != nil{
            aCoder.encodeObject(speakerTitle, forKey: "speaker_title")
        }
        if speakerTwitterFollow != nil{
            aCoder.encodeObject(speakerTwitterFollow, forKey: "speaker_twitter_follow")
        }
        if speakerUserRating != nil{
            aCoder.encodeObject(speakerUserRating, forKey: "speaker_user_rating")
        }
        
    }
    
}