//
//  Questions.swift
//  Hubilo
//
//  Created by Aadil on 7/12/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class Questions: NSObject {
    var user_id : String = ""
    var question : String = ""
    var likes : String = ""
    var question_id : String = ""
    var firstname : String = ""
    var lastname : String = ""
    var profile_img : String = ""
    var designation : String = ""
    var aboutme : String = ""
    var linkedin_public_url : String = ""
    var fb_id : String = ""
    var twitter_public_url : String = ""
    var create_time_mili : String = ""
    var organization : String = ""
    var is_like : Int = 0
    var is_bookmark : Int = 0
}
