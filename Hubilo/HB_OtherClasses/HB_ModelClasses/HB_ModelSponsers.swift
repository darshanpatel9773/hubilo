//
//	HB_ModelSponsers.swift
//
//	Create by Pawan jaiswal on 28/6/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_ModelSponsers : NSObject, NSCoding{

	var sponsorBrochure : String!
	var sponsorCategory : String!
	var sponsorDescription : String!
	var sponsorEmailid : String!
	var sponsorFb : String!
	var sponsorId : String!
	var sponsorLogoImg : String!
	var sponsorName : String!
	var sponsorPosition : String!
	var sponsorTitle : String!
	var sponsorTwitter : String!
	var sponsorWebsite : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		sponsorBrochure = dictionary["sponsor_brochure"] as? String
		sponsorCategory = dictionary["sponsor_category"] as? String
		sponsorDescription = dictionary["sponsor_description"] as? String
		sponsorEmailid = dictionary["sponsor_emailid"] as? String
		sponsorFb = dictionary["sponsor_fb"] as? String
		sponsorId = dictionary["sponsor_id"] as? String
		sponsorLogoImg = dictionary["sponsor_logo_img"] as? String
		sponsorName = dictionary["sponsor_name"] as? String
		sponsorPosition = dictionary["sponsor_position"] as? String
		sponsorTitle = dictionary["sponsor_title"] as? String
		sponsorTwitter = dictionary["sponsor_twitter"] as? String
		sponsorWebsite = dictionary["sponsor_website"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if sponsorBrochure != nil{
			dictionary["sponsor_brochure"] = sponsorBrochure
		}
		if sponsorCategory != nil{
			dictionary["sponsor_category"] = sponsorCategory
		}
		if sponsorDescription != nil{
			dictionary["sponsor_description"] = sponsorDescription
		}
		if sponsorEmailid != nil{
			dictionary["sponsor_emailid"] = sponsorEmailid
		}
		if sponsorFb != nil{
			dictionary["sponsor_fb"] = sponsorFb
		}
		if sponsorId != nil{
			dictionary["sponsor_id"] = sponsorId
		}
		if sponsorLogoImg != nil{
			dictionary["sponsor_logo_img"] = sponsorLogoImg
		}
		if sponsorName != nil{
			dictionary["sponsor_name"] = sponsorName
		}
		if sponsorPosition != nil{
			dictionary["sponsor_position"] = sponsorPosition
		}
		if sponsorTitle != nil{
			dictionary["sponsor_title"] = sponsorTitle
		}
		if sponsorTwitter != nil{
			dictionary["sponsor_twitter"] = sponsorTwitter
		}
		if sponsorWebsite != nil{
			dictionary["sponsor_website"] = sponsorWebsite
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         sponsorBrochure = aDecoder.decodeObjectForKey("sponsor_brochure") as? String
         sponsorCategory = aDecoder.decodeObjectForKey("sponsor_category") as? String
         sponsorDescription = aDecoder.decodeObjectForKey("sponsor_description") as? String
         sponsorEmailid = aDecoder.decodeObjectForKey("sponsor_emailid") as? String
         sponsorFb = aDecoder.decodeObjectForKey("sponsor_fb") as? String
         sponsorId = aDecoder.decodeObjectForKey("sponsor_id") as? String
         sponsorLogoImg = aDecoder.decodeObjectForKey("sponsor_logo_img") as? String
         sponsorName = aDecoder.decodeObjectForKey("sponsor_name") as? String
         sponsorPosition = aDecoder.decodeObjectForKey("sponsor_position") as? String
         sponsorTitle = aDecoder.decodeObjectForKey("sponsor_title") as? String
         sponsorTwitter = aDecoder.decodeObjectForKey("sponsor_twitter") as? String
         sponsorWebsite = aDecoder.decodeObjectForKey("sponsor_website") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if sponsorBrochure != nil{
			aCoder.encodeObject(sponsorBrochure, forKey: "sponsor_brochure")
		}
		if sponsorCategory != nil{
			aCoder.encodeObject(sponsorCategory, forKey: "sponsor_category")
		}
		if sponsorDescription != nil{
			aCoder.encodeObject(sponsorDescription, forKey: "sponsor_description")
		}
		if sponsorEmailid != nil{
			aCoder.encodeObject(sponsorEmailid, forKey: "sponsor_emailid")
		}
		if sponsorFb != nil{
			aCoder.encodeObject(sponsorFb, forKey: "sponsor_fb")
		}
		if sponsorId != nil{
			aCoder.encodeObject(sponsorId, forKey: "sponsor_id")
		}
		if sponsorLogoImg != nil{
			aCoder.encodeObject(sponsorLogoImg, forKey: "sponsor_logo_img")
		}
		if sponsorName != nil{
			aCoder.encodeObject(sponsorName, forKey: "sponsor_name")
		}
		if sponsorPosition != nil{
			aCoder.encodeObject(sponsorPosition, forKey: "sponsor_position")
		}
		if sponsorTitle != nil{
			aCoder.encodeObject(sponsorTitle, forKey: "sponsor_title")
		}
		if sponsorTwitter != nil{
			aCoder.encodeObject(sponsorTwitter, forKey: "sponsor_twitter")
		}
		if sponsorWebsite != nil{
			aCoder.encodeObject(sponsorWebsite, forKey: "sponsor_website")
		}

	}

}