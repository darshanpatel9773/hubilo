//
//  HBUserDetail.swift
//
//  Created by Priyank Gandhi on 31/07/16
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

public class HBUserDetail: NSObject, Mappable, NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kHBUserDetailOrganizationKey: String = "organization"
	internal let kHBUserDetailAddressKey: String = "address"
	internal let kHBUserDetailCreateTimeMiliKey: String = "create_time_mili"
	internal let kHBUserDetailConnectedFbKey: String = "connected_fb"
	internal let kHBUserDetailEmailidKey: String = "emailid"
	internal let kHBUserDetailActiveKey: String = "active"
	internal let kHBUserDetailConnectedTwitterKey: String = "connected_twitter"
	internal let kHBUserDetailDesignationKey: String = "designation"
	internal let kHBUserDetailFbSecretKeyKey: String = "fb_secret_key"
	internal let kHBUserDetailIndustrynameKey: String = "industryname"
	internal let kHBUserDetailLastnameKey: String = "lastname"
	internal let kHBUserDetailAboutmeKey: String = "aboutme"
	internal let kHBUserDetailWebsiteKey: String = "website"
	internal let kHBUserDetailAgeKey: String = "age"
	internal let kHBUserDetailUpdateTimeMiliKey: String = "update_time_mili"
	internal let kHBUserDetailSyncLinkedinKey: String = "sync_linkedin"
	internal let kHBUserDetailTwitterSecretKeyKey: String = "twitter_secret_key"
	internal let kHBUserDetailStateKey: String = "state"
	internal let kHBUserDetailLinkedinPublicUrlKey: String = "linkedin_public_url"
	internal let kHBUserDetailFbIdKey: String = "fb_id"
	internal let kHBUserDetailPhoneKey: String = "phone"
	internal let kHBUserDetailInterestnameKey: String = "interestname"
	internal let kHBUserDetailStatusKey: String = "status"
	internal let kHBUserDetailMsgKey: String = "msg"
	internal let kHBUserDetailCityKey: String = "city"
	internal let kHBUserDetailCreateTimeKey: String = "create_time"
	internal let kHBUserDetailCountryKey: String = "country"
	internal let kHBUserDetailLastLoginTypeKey: String = "last_login_type"
	internal let kHBUserDetailSyncFbKey: String = "sync_fb"
	internal let kHBUserDetailTwitterIdKey: String = "twitter_id"
	internal let kHBUserDetailLatKey: String = "lat"
	internal let kHBUserDetailFirstnameKey: String = "firstname"
	internal let kHBUserDetailInterestKey: String = "interest"
	internal let kHBUserDetailConnectedLinkedinKey: String = "connected_linkedin"
	internal let kHBUserDetailTwitterPublicUrlKey: String = "twitter_public_url"
	internal let kHBUserDetailProfileImgKey: String = "profile_img"
	internal let kHBUserDetailLngKey: String = "lng"
	internal let kHBUserDetailLinkedinIdKey: String = "linkedin_id"
	internal let kHBUserDetailGenderKey: String = "gender"
	internal let kHBUserDetailLinkedinSecretKeyKey: String = "linkedin_secret_key"
	internal let kHBUserDetailIndustryKey: String = "industry"
	internal let kHBUserDetailPasswordKey: String = "password"
	internal let kHBUserDetailUserIdKey: String = "user_id"
	internal let kHBUserDetailUpdateTimeKey: String = "update_time"
	internal let kHBUserDetailLocationIdKey: String = "location_id"
	internal let kHBUserDetailDobKey: String = "dob"


    // MARK: Properties
	public var organization: String?
	public var address: String?
	public var createTimeMili: String?
	public var connectedFb: String?
	public var emailid: String?
	public var active: String?
	public var connectedTwitter: String?
	public var designation: String?
	public var fbSecretKey: String?
	public var industryname: String?
	public var lastname: String?
	public var aboutme: String?
	public var website: String?
	public var age: String?
	public var updateTimeMili: String?
	public var syncLinkedin: String?
	public var twitterSecretKey: String?
	public var state: String?
	public var linkedinPublicUrl: String?
	public var fbId: String?
	public var phone: String?
	public var interestname: String?
	public var status: String?
	public var msg: String?
	public var city: String?
	public var createTime: String?
	public var country: String?
	public var lastLoginType: String?
	public var syncFb: String?
	public var twitterId: String?
	public var lat: String?
	public var firstname: String?
	public var interest: String?
	public var connectedLinkedin: String?
	public var twitterPublicUrl: String?
	public var profileImg: String?
	public var lng: String?
	public var linkedinId: String?
	public var gender: String?
	public var linkedinSecretKey: String?
	public var industry: String?
	public var password: String?
	public var userId: String?
	public var updateTime: String?
	public var locationId: String?
	public var dob: String?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		organization = json[kHBUserDetailOrganizationKey].string
		address = json[kHBUserDetailAddressKey].string
		createTimeMili = json[kHBUserDetailCreateTimeMiliKey].string
		connectedFb = json[kHBUserDetailConnectedFbKey].string
		emailid = json[kHBUserDetailEmailidKey].string
		active = json[kHBUserDetailActiveKey].string
		connectedTwitter = json[kHBUserDetailConnectedTwitterKey].string
		designation = json[kHBUserDetailDesignationKey].string
		fbSecretKey = json[kHBUserDetailFbSecretKeyKey].string
		industryname = json[kHBUserDetailIndustrynameKey].string
		lastname = json[kHBUserDetailLastnameKey].string
		aboutme = json[kHBUserDetailAboutmeKey].string
		website = json[kHBUserDetailWebsiteKey].string
		age = json[kHBUserDetailAgeKey].string
		updateTimeMili = json[kHBUserDetailUpdateTimeMiliKey].string
		syncLinkedin = json[kHBUserDetailSyncLinkedinKey].string
		twitterSecretKey = json[kHBUserDetailTwitterSecretKeyKey].string
		state = json[kHBUserDetailStateKey].string
		linkedinPublicUrl = json[kHBUserDetailLinkedinPublicUrlKey].string
		fbId = json[kHBUserDetailFbIdKey].string
		phone = json[kHBUserDetailPhoneKey].string
		interestname = json[kHBUserDetailInterestnameKey].string
		status = json[kHBUserDetailStatusKey].string
		msg = json[kHBUserDetailMsgKey].string
		city = json[kHBUserDetailCityKey].string
		createTime = json[kHBUserDetailCreateTimeKey].string
		country = json[kHBUserDetailCountryKey].string
		lastLoginType = json[kHBUserDetailLastLoginTypeKey].string
		syncFb = json[kHBUserDetailSyncFbKey].string
		twitterId = json[kHBUserDetailTwitterIdKey].string
		lat = json[kHBUserDetailLatKey].string
		firstname = json[kHBUserDetailFirstnameKey].string
		interest = json[kHBUserDetailInterestKey].string
		connectedLinkedin = json[kHBUserDetailConnectedLinkedinKey].string
		twitterPublicUrl = json[kHBUserDetailTwitterPublicUrlKey].string
		profileImg = json[kHBUserDetailProfileImgKey].string
		lng = json[kHBUserDetailLngKey].string
		linkedinId = json[kHBUserDetailLinkedinIdKey].string
		gender = json[kHBUserDetailGenderKey].string
		linkedinSecretKey = json[kHBUserDetailLinkedinSecretKeyKey].string
		industry = json[kHBUserDetailIndustryKey].string
		password = json[kHBUserDetailPasswordKey].string
		userId = json[kHBUserDetailUserIdKey].string
		updateTime = json[kHBUserDetailUpdateTimeKey].string
		locationId = json[kHBUserDetailLocationIdKey].string
		dob = json[kHBUserDetailDobKey].string

    }

    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    required public init?(_ map: Map){

    }

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
		organization <- map[kHBUserDetailOrganizationKey]
		address <- map[kHBUserDetailAddressKey]
		createTimeMili <- map[kHBUserDetailCreateTimeMiliKey]
		connectedFb <- map[kHBUserDetailConnectedFbKey]
		emailid <- map[kHBUserDetailEmailidKey]
		active <- map[kHBUserDetailActiveKey]
		connectedTwitter <- map[kHBUserDetailConnectedTwitterKey]
		designation <- map[kHBUserDetailDesignationKey]
		fbSecretKey <- map[kHBUserDetailFbSecretKeyKey]
		industryname <- map[kHBUserDetailIndustrynameKey]
		lastname <- map[kHBUserDetailLastnameKey]
		aboutme <- map[kHBUserDetailAboutmeKey]
		website <- map[kHBUserDetailWebsiteKey]
		age <- map[kHBUserDetailAgeKey]
		updateTimeMili <- map[kHBUserDetailUpdateTimeMiliKey]
		syncLinkedin <- map[kHBUserDetailSyncLinkedinKey]
		twitterSecretKey <- map[kHBUserDetailTwitterSecretKeyKey]
		state <- map[kHBUserDetailStateKey]
		linkedinPublicUrl <- map[kHBUserDetailLinkedinPublicUrlKey]
		fbId <- map[kHBUserDetailFbIdKey]
		phone <- map[kHBUserDetailPhoneKey]
		interestname <- map[kHBUserDetailInterestnameKey]
		status <- map[kHBUserDetailStatusKey]
		msg <- map[kHBUserDetailMsgKey]
		city <- map[kHBUserDetailCityKey]
		createTime <- map[kHBUserDetailCreateTimeKey]
		country <- map[kHBUserDetailCountryKey]
		lastLoginType <- map[kHBUserDetailLastLoginTypeKey]
		syncFb <- map[kHBUserDetailSyncFbKey]
		twitterId <- map[kHBUserDetailTwitterIdKey]
		lat <- map[kHBUserDetailLatKey]
		firstname <- map[kHBUserDetailFirstnameKey]
		interest <- map[kHBUserDetailInterestKey]
		connectedLinkedin <- map[kHBUserDetailConnectedLinkedinKey]
		twitterPublicUrl <- map[kHBUserDetailTwitterPublicUrlKey]
		profileImg <- map[kHBUserDetailProfileImgKey]
		lng <- map[kHBUserDetailLngKey]
		linkedinId <- map[kHBUserDetailLinkedinIdKey]
		gender <- map[kHBUserDetailGenderKey]
		linkedinSecretKey <- map[kHBUserDetailLinkedinSecretKeyKey]
		industry <- map[kHBUserDetailIndustryKey]
		password <- map[kHBUserDetailPasswordKey]
		userId <- map[kHBUserDetailUserIdKey]
		updateTime <- map[kHBUserDetailUpdateTimeKey]
		locationId <- map[kHBUserDetailLocationIdKey]
		dob <- map[kHBUserDetailDobKey]

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    public func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if organization != nil {
			dictionary.updateValue(organization!, forKey: kHBUserDetailOrganizationKey)
		}
		if address != nil {
			dictionary.updateValue(address!, forKey: kHBUserDetailAddressKey)
		}
		if createTimeMili != nil {
			dictionary.updateValue(createTimeMili!, forKey: kHBUserDetailCreateTimeMiliKey)
		}
		if connectedFb != nil {
			dictionary.updateValue(connectedFb!, forKey: kHBUserDetailConnectedFbKey)
		}
		if emailid != nil {
			dictionary.updateValue(emailid!, forKey: kHBUserDetailEmailidKey)
		}
		if active != nil {
			dictionary.updateValue(active!, forKey: kHBUserDetailActiveKey)
		}
		if connectedTwitter != nil {
			dictionary.updateValue(connectedTwitter!, forKey: kHBUserDetailConnectedTwitterKey)
		}
		if designation != nil {
			dictionary.updateValue(designation!, forKey: kHBUserDetailDesignationKey)
		}
		if fbSecretKey != nil {
			dictionary.updateValue(fbSecretKey!, forKey: kHBUserDetailFbSecretKeyKey)
		}
		if industryname != nil {
			dictionary.updateValue(industryname!, forKey: kHBUserDetailIndustrynameKey)
		}
		if lastname != nil {
			dictionary.updateValue(lastname!, forKey: kHBUserDetailLastnameKey)
		}
		if aboutme != nil {
			dictionary.updateValue(aboutme!, forKey: kHBUserDetailAboutmeKey)
		}
		if website != nil {
			dictionary.updateValue(website!, forKey: kHBUserDetailWebsiteKey)
		}
		if age != nil {
			dictionary.updateValue(age!, forKey: kHBUserDetailAgeKey)
		}
		if updateTimeMili != nil {
			dictionary.updateValue(updateTimeMili!, forKey: kHBUserDetailUpdateTimeMiliKey)
		}
		if syncLinkedin != nil {
			dictionary.updateValue(syncLinkedin!, forKey: kHBUserDetailSyncLinkedinKey)
		}
		if twitterSecretKey != nil {
			dictionary.updateValue(twitterSecretKey!, forKey: kHBUserDetailTwitterSecretKeyKey)
		}
		if state != nil {
			dictionary.updateValue(state!, forKey: kHBUserDetailStateKey)
		}
		if linkedinPublicUrl != nil {
			dictionary.updateValue(linkedinPublicUrl!, forKey: kHBUserDetailLinkedinPublicUrlKey)
		}
		if fbId != nil {
			dictionary.updateValue(fbId!, forKey: kHBUserDetailFbIdKey)
		}
		if phone != nil {
			dictionary.updateValue(phone!, forKey: kHBUserDetailPhoneKey)
		}
		if interestname != nil {
			dictionary.updateValue(interestname!, forKey: kHBUserDetailInterestnameKey)
		}
		if status != nil {
			dictionary.updateValue(status!, forKey: kHBUserDetailStatusKey)
		}
		if msg != nil {
			dictionary.updateValue(msg!, forKey: kHBUserDetailMsgKey)
		}
		if city != nil {
			dictionary.updateValue(city!, forKey: kHBUserDetailCityKey)
		}
		if createTime != nil {
			dictionary.updateValue(createTime!, forKey: kHBUserDetailCreateTimeKey)
		}
		if country != nil {
			dictionary.updateValue(country!, forKey: kHBUserDetailCountryKey)
		}
		if lastLoginType != nil {
			dictionary.updateValue(lastLoginType!, forKey: kHBUserDetailLastLoginTypeKey)
		}
		if syncFb != nil {
			dictionary.updateValue(syncFb!, forKey: kHBUserDetailSyncFbKey)
		}
		if twitterId != nil {
			dictionary.updateValue(twitterId!, forKey: kHBUserDetailTwitterIdKey)
		}
		if lat != nil {
			dictionary.updateValue(lat!, forKey: kHBUserDetailLatKey)
		}
		if firstname != nil {
			dictionary.updateValue(firstname!, forKey: kHBUserDetailFirstnameKey)
		}
		if interest != nil {
			dictionary.updateValue(interest!, forKey: kHBUserDetailInterestKey)
		}
		if connectedLinkedin != nil {
			dictionary.updateValue(connectedLinkedin!, forKey: kHBUserDetailConnectedLinkedinKey)
		}
		if twitterPublicUrl != nil {
			dictionary.updateValue(twitterPublicUrl!, forKey: kHBUserDetailTwitterPublicUrlKey)
		}
		if profileImg != nil {
			dictionary.updateValue(profileImg!, forKey: kHBUserDetailProfileImgKey)
		}
		if lng != nil {
			dictionary.updateValue(lng!, forKey: kHBUserDetailLngKey)
		}
		if linkedinId != nil {
			dictionary.updateValue(linkedinId!, forKey: kHBUserDetailLinkedinIdKey)
		}
		if gender != nil {
			dictionary.updateValue(gender!, forKey: kHBUserDetailGenderKey)
		}
		if linkedinSecretKey != nil {
			dictionary.updateValue(linkedinSecretKey!, forKey: kHBUserDetailLinkedinSecretKeyKey)
		}
		if industry != nil {
			dictionary.updateValue(industry!, forKey: kHBUserDetailIndustryKey)
		}
		if password != nil {
			dictionary.updateValue(password!, forKey: kHBUserDetailPasswordKey)
		}
		if userId != nil {
			dictionary.updateValue(userId!, forKey: kHBUserDetailUserIdKey)
		}
		if updateTime != nil {
			dictionary.updateValue(updateTime!, forKey: kHBUserDetailUpdateTimeKey)
		}
		if locationId != nil {
			dictionary.updateValue(locationId!, forKey: kHBUserDetailLocationIdKey)
		}
		if dob != nil {
			dictionary.updateValue(dob!, forKey: kHBUserDetailDobKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
		self.organization = aDecoder.decodeObjectForKey(kHBUserDetailOrganizationKey) as? String
		self.address = aDecoder.decodeObjectForKey(kHBUserDetailAddressKey) as? String
		self.createTimeMili = aDecoder.decodeObjectForKey(kHBUserDetailCreateTimeMiliKey) as? String
		self.connectedFb = aDecoder.decodeObjectForKey(kHBUserDetailConnectedFbKey) as? String
		self.emailid = aDecoder.decodeObjectForKey(kHBUserDetailEmailidKey) as? String
		self.active = aDecoder.decodeObjectForKey(kHBUserDetailActiveKey) as? String
		self.connectedTwitter = aDecoder.decodeObjectForKey(kHBUserDetailConnectedTwitterKey) as? String
		self.designation = aDecoder.decodeObjectForKey(kHBUserDetailDesignationKey) as? String
		self.fbSecretKey = aDecoder.decodeObjectForKey(kHBUserDetailFbSecretKeyKey) as? String
		self.industryname = aDecoder.decodeObjectForKey(kHBUserDetailIndustrynameKey) as? String
		self.lastname = aDecoder.decodeObjectForKey(kHBUserDetailLastnameKey) as? String
		self.aboutme = aDecoder.decodeObjectForKey(kHBUserDetailAboutmeKey) as? String
		self.website = aDecoder.decodeObjectForKey(kHBUserDetailWebsiteKey) as? String
		self.age = aDecoder.decodeObjectForKey(kHBUserDetailAgeKey) as? String
		self.updateTimeMili = aDecoder.decodeObjectForKey(kHBUserDetailUpdateTimeMiliKey) as? String
		self.syncLinkedin = aDecoder.decodeObjectForKey(kHBUserDetailSyncLinkedinKey) as? String
		self.twitterSecretKey = aDecoder.decodeObjectForKey(kHBUserDetailTwitterSecretKeyKey) as? String
		self.state = aDecoder.decodeObjectForKey(kHBUserDetailStateKey) as? String
		self.linkedinPublicUrl = aDecoder.decodeObjectForKey(kHBUserDetailLinkedinPublicUrlKey) as? String
		self.fbId = aDecoder.decodeObjectForKey(kHBUserDetailFbIdKey) as? String
		self.phone = aDecoder.decodeObjectForKey(kHBUserDetailPhoneKey) as? String
		self.interestname = aDecoder.decodeObjectForKey(kHBUserDetailInterestnameKey) as? String
		self.status = aDecoder.decodeObjectForKey(kHBUserDetailStatusKey) as? String
		self.msg = aDecoder.decodeObjectForKey(kHBUserDetailMsgKey) as? String
		self.city = aDecoder.decodeObjectForKey(kHBUserDetailCityKey) as? String
		self.createTime = aDecoder.decodeObjectForKey(kHBUserDetailCreateTimeKey) as? String
		self.country = aDecoder.decodeObjectForKey(kHBUserDetailCountryKey) as? String
		self.lastLoginType = aDecoder.decodeObjectForKey(kHBUserDetailLastLoginTypeKey) as? String
		self.syncFb = aDecoder.decodeObjectForKey(kHBUserDetailSyncFbKey) as? String
		self.twitterId = aDecoder.decodeObjectForKey(kHBUserDetailTwitterIdKey) as? String
		self.lat = aDecoder.decodeObjectForKey(kHBUserDetailLatKey) as? String
		self.firstname = aDecoder.decodeObjectForKey(kHBUserDetailFirstnameKey) as? String
		self.interest = aDecoder.decodeObjectForKey(kHBUserDetailInterestKey) as? String
		self.connectedLinkedin = aDecoder.decodeObjectForKey(kHBUserDetailConnectedLinkedinKey) as? String
		self.twitterPublicUrl = aDecoder.decodeObjectForKey(kHBUserDetailTwitterPublicUrlKey) as? String
		self.profileImg = aDecoder.decodeObjectForKey(kHBUserDetailProfileImgKey) as? String
		self.lng = aDecoder.decodeObjectForKey(kHBUserDetailLngKey) as? String
		self.linkedinId = aDecoder.decodeObjectForKey(kHBUserDetailLinkedinIdKey) as? String
		self.gender = aDecoder.decodeObjectForKey(kHBUserDetailGenderKey) as? String
		self.linkedinSecretKey = aDecoder.decodeObjectForKey(kHBUserDetailLinkedinSecretKeyKey) as? String
		self.industry = aDecoder.decodeObjectForKey(kHBUserDetailIndustryKey) as? String
		self.password = aDecoder.decodeObjectForKey(kHBUserDetailPasswordKey) as? String
		self.userId = aDecoder.decodeObjectForKey(kHBUserDetailUserIdKey) as? String
		self.updateTime = aDecoder.decodeObjectForKey(kHBUserDetailUpdateTimeKey) as? String
		self.locationId = aDecoder.decodeObjectForKey(kHBUserDetailLocationIdKey) as? String
		self.dob = aDecoder.decodeObjectForKey(kHBUserDetailDobKey) as? String

    }

    public func encodeWithCoder(aCoder: NSCoder) {
		aCoder.encodeObject(organization, forKey: kHBUserDetailOrganizationKey)
		aCoder.encodeObject(address, forKey: kHBUserDetailAddressKey)
		aCoder.encodeObject(createTimeMili, forKey: kHBUserDetailCreateTimeMiliKey)
		aCoder.encodeObject(connectedFb, forKey: kHBUserDetailConnectedFbKey)
		aCoder.encodeObject(emailid, forKey: kHBUserDetailEmailidKey)
		aCoder.encodeObject(active, forKey: kHBUserDetailActiveKey)
		aCoder.encodeObject(connectedTwitter, forKey: kHBUserDetailConnectedTwitterKey)
		aCoder.encodeObject(designation, forKey: kHBUserDetailDesignationKey)
		aCoder.encodeObject(fbSecretKey, forKey: kHBUserDetailFbSecretKeyKey)
		aCoder.encodeObject(industryname, forKey: kHBUserDetailIndustrynameKey)
		aCoder.encodeObject(lastname, forKey: kHBUserDetailLastnameKey)
		aCoder.encodeObject(aboutme, forKey: kHBUserDetailAboutmeKey)
		aCoder.encodeObject(website, forKey: kHBUserDetailWebsiteKey)
		aCoder.encodeObject(age, forKey: kHBUserDetailAgeKey)
		aCoder.encodeObject(updateTimeMili, forKey: kHBUserDetailUpdateTimeMiliKey)
		aCoder.encodeObject(syncLinkedin, forKey: kHBUserDetailSyncLinkedinKey)
		aCoder.encodeObject(twitterSecretKey, forKey: kHBUserDetailTwitterSecretKeyKey)
		aCoder.encodeObject(state, forKey: kHBUserDetailStateKey)
		aCoder.encodeObject(linkedinPublicUrl, forKey: kHBUserDetailLinkedinPublicUrlKey)
		aCoder.encodeObject(fbId, forKey: kHBUserDetailFbIdKey)
		aCoder.encodeObject(phone, forKey: kHBUserDetailPhoneKey)
		aCoder.encodeObject(interestname, forKey: kHBUserDetailInterestnameKey)
		aCoder.encodeObject(status, forKey: kHBUserDetailStatusKey)
		aCoder.encodeObject(msg, forKey: kHBUserDetailMsgKey)
		aCoder.encodeObject(city, forKey: kHBUserDetailCityKey)
		aCoder.encodeObject(createTime, forKey: kHBUserDetailCreateTimeKey)
		aCoder.encodeObject(country, forKey: kHBUserDetailCountryKey)
		aCoder.encodeObject(lastLoginType, forKey: kHBUserDetailLastLoginTypeKey)
		aCoder.encodeObject(syncFb, forKey: kHBUserDetailSyncFbKey)
		aCoder.encodeObject(twitterId, forKey: kHBUserDetailTwitterIdKey)
		aCoder.encodeObject(lat, forKey: kHBUserDetailLatKey)
		aCoder.encodeObject(firstname, forKey: kHBUserDetailFirstnameKey)
		aCoder.encodeObject(interest, forKey: kHBUserDetailInterestKey)
		aCoder.encodeObject(connectedLinkedin, forKey: kHBUserDetailConnectedLinkedinKey)
		aCoder.encodeObject(twitterPublicUrl, forKey: kHBUserDetailTwitterPublicUrlKey)
		aCoder.encodeObject(profileImg, forKey: kHBUserDetailProfileImgKey)
		aCoder.encodeObject(lng, forKey: kHBUserDetailLngKey)
		aCoder.encodeObject(linkedinId, forKey: kHBUserDetailLinkedinIdKey)
		aCoder.encodeObject(gender, forKey: kHBUserDetailGenderKey)
		aCoder.encodeObject(linkedinSecretKey, forKey: kHBUserDetailLinkedinSecretKeyKey)
		aCoder.encodeObject(industry, forKey: kHBUserDetailIndustryKey)
		aCoder.encodeObject(password, forKey: kHBUserDetailPasswordKey)
		aCoder.encodeObject(userId, forKey: kHBUserDetailUserIdKey)
		aCoder.encodeObject(updateTime, forKey: kHBUserDetailUpdateTimeKey)
		aCoder.encodeObject(locationId, forKey: kHBUserDetailLocationIdKey)
		aCoder.encodeObject(dob, forKey: kHBUserDetailDobKey)

    }

}
