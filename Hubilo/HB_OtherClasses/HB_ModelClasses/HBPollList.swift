//
//	HBPollList.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HBPollList : NSObject, NSCoding{

	var aboutme : String!
	var answer : String!
	var createTimeMili : String!
	var designation : String!
	var didVote : String!
	var endTime : String!
	var fbId : String!
	var firstname : String!
	var instruction : String!
	var isBookmark : Int!
	var lastname : String!
	var linkedinPublicUrl : String!
	var organization : String!
	var pollId : String!
	var pollType : String!
	var profileImg : String!
	var question : String!
	var startTime : String!
	var totalAnswers : Int!
	var twitterPublicUrl : String!
	var userId : String!
	var voteCount : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		aboutme = dictionary["aboutme"] as? String
		answer = dictionary["answer"] as? String
		createTimeMili = dictionary["create_time_mili"] as? String
		designation = dictionary["designation"] as? String
		didVote = dictionary["did_vote"] as? String
		endTime = dictionary["end_time"] as? String
		fbId = dictionary["fb_id"] as? String
		firstname = dictionary["firstname"] as? String
		instruction = dictionary["instruction"] as? String
		isBookmark = dictionary["is_bookmark"] as? Int
		lastname = dictionary["lastname"] as? String
		linkedinPublicUrl = dictionary["linkedin_public_url"] as? String
		organization = dictionary["organization"] as? String
		pollId = dictionary["poll_id"] as? String
		pollType = dictionary["poll_type"] as? String
		profileImg = dictionary["profile_img"] as? String
		question = dictionary["question"] as? String
		startTime = dictionary["start_time"] as? String
		totalAnswers = dictionary["totalAnswers"] as? Int
		twitterPublicUrl = dictionary["twitter_public_url"] as? String
		userId = dictionary["user_id"] as? String
		voteCount = dictionary["vote_count"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if aboutme != nil{
			dictionary["aboutme"] = aboutme
		}
		if answer != nil{
			dictionary["answer"] = answer
		}
		if createTimeMili != nil{
			dictionary["create_time_mili"] = createTimeMili
		}
		if designation != nil{
			dictionary["designation"] = designation
		}
		if didVote != nil{
			dictionary["did_vote"] = didVote
		}
		if endTime != nil{
			dictionary["end_time"] = endTime
		}
		if fbId != nil{
			dictionary["fb_id"] = fbId
		}
		if firstname != nil{
			dictionary["firstname"] = firstname
		}
		if instruction != nil{
			dictionary["instruction"] = instruction
		}
		if isBookmark != nil{
			dictionary["is_bookmark"] = isBookmark
		}
		if lastname != nil{
			dictionary["lastname"] = lastname
		}
		if linkedinPublicUrl != nil{
			dictionary["linkedin_public_url"] = linkedinPublicUrl
		}
		if organization != nil{
			dictionary["organization"] = organization
		}
		if pollId != nil{
			dictionary["poll_id"] = pollId
		}
		if pollType != nil{
			dictionary["poll_type"] = pollType
		}
		if profileImg != nil{
			dictionary["profile_img"] = profileImg
		}
		if question != nil{
			dictionary["question"] = question
		}
		if startTime != nil{
			dictionary["start_time"] = startTime
		}
		if totalAnswers != nil{
			dictionary["totalAnswers"] = totalAnswers
		}
		if twitterPublicUrl != nil{
			dictionary["twitter_public_url"] = twitterPublicUrl
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if voteCount != nil{
			dictionary["vote_count"] = voteCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         aboutme = aDecoder.decodeObjectForKey("aboutme") as? String
         answer = aDecoder.decodeObjectForKey("answer") as? String
         createTimeMili = aDecoder.decodeObjectForKey("create_time_mili") as? String
         designation = aDecoder.decodeObjectForKey("designation") as? String
         didVote = aDecoder.decodeObjectForKey("did_vote") as? String
         endTime = aDecoder.decodeObjectForKey("end_time") as? String
         fbId = aDecoder.decodeObjectForKey("fb_id") as? String
         firstname = aDecoder.decodeObjectForKey("firstname") as? String
         instruction = aDecoder.decodeObjectForKey("instruction") as? String
         isBookmark = aDecoder.decodeObjectForKey("is_bookmark") as? Int
         lastname = aDecoder.decodeObjectForKey("lastname") as? String
         linkedinPublicUrl = aDecoder.decodeObjectForKey("linkedin_public_url") as? String
         organization = aDecoder.decodeObjectForKey("organization") as? String
         pollId = aDecoder.decodeObjectForKey("poll_id") as? String
         pollType = aDecoder.decodeObjectForKey("poll_type") as? String
         profileImg = aDecoder.decodeObjectForKey("profile_img") as? String
         question = aDecoder.decodeObjectForKey("question") as? String
         startTime = aDecoder.decodeObjectForKey("start_time") as? String
         totalAnswers = aDecoder.decodeObjectForKey("totalAnswers") as? Int
         twitterPublicUrl = aDecoder.decodeObjectForKey("twitter_public_url") as? String
         userId = aDecoder.decodeObjectForKey("user_id") as? String
         voteCount = aDecoder.decodeObjectForKey("vote_count") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if aboutme != nil{
			aCoder.encodeObject(aboutme, forKey: "aboutme")
		}
		if answer != nil{
			aCoder.encodeObject(answer, forKey: "answer")
		}
		if createTimeMili != nil{
			aCoder.encodeObject(createTimeMili, forKey: "create_time_mili")
		}
		if designation != nil{
			aCoder.encodeObject(designation, forKey: "designation")
		}
		if didVote != nil{
			aCoder.encodeObject(didVote, forKey: "did_vote")
		}
		if endTime != nil{
			aCoder.encodeObject(endTime, forKey: "end_time")
		}
		if fbId != nil{
			aCoder.encodeObject(fbId, forKey: "fb_id")
		}
		if firstname != nil{
			aCoder.encodeObject(firstname, forKey: "firstname")
		}
		if instruction != nil{
			aCoder.encodeObject(instruction, forKey: "instruction")
		}
		if isBookmark != nil{
			aCoder.encodeObject(isBookmark, forKey: "is_bookmark")
		}
		if lastname != nil{
			aCoder.encodeObject(lastname, forKey: "lastname")
		}
		if linkedinPublicUrl != nil{
			aCoder.encodeObject(linkedinPublicUrl, forKey: "linkedin_public_url")
		}
		if organization != nil{
			aCoder.encodeObject(organization, forKey: "organization")
		}
		if pollId != nil{
			aCoder.encodeObject(pollId, forKey: "poll_id")
		}
		if pollType != nil{
			aCoder.encodeObject(pollType, forKey: "poll_type")
		}
		if profileImg != nil{
			aCoder.encodeObject(profileImg, forKey: "profile_img")
		}
		if question != nil{
			aCoder.encodeObject(question, forKey: "question")
		}
		if startTime != nil{
			aCoder.encodeObject(startTime, forKey: "start_time")
		}
		if totalAnswers != nil{
			aCoder.encodeObject(totalAnswers, forKey: "totalAnswers")
		}
		if twitterPublicUrl != nil{
			aCoder.encodeObject(twitterPublicUrl, forKey: "twitter_public_url")
		}
		if userId != nil{
			aCoder.encodeObject(userId, forKey: "user_id")
		}
		if voteCount != nil{
			aCoder.encodeObject(voteCount, forKey: "vote_count")
		}

	}

}