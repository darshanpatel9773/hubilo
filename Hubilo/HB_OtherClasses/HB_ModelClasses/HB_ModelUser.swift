//
//	HB_ModelUser.swift
//
//	Create by Pawan jaiswal on 21/6/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_ModelUser : NSObject, NSCoding{

	var aboutme : String!
	var active : String!
	var address : String!
	var age : String!
	var city : String!
	var connectedFb : String!
	var connectedLinkedin : String!
	var connectedTwitter : String!
	var country : String!
	var createTime : String!
	var createTimeMili : String!
	var designation : String!
	var dob : String!
	var emailid : String!
	var fbId : String!
	var fbSecretKey : String!
	var firstname : String!
	var gender : String!
	var industry : String!
	var industryname : String!
	var interest : String!
	var interestname : String!
	var lastLoginType : String!
	var lastname : String!
	var lat : String!
	var linkedinId : String!
	var linkedinPublicUrl : String!
	var linkedinSecretKey : String!
	var lng : String!
	var locationId : String!
	var msg : String!
	var organization : String!
	var password : String!
	var phone : String!
	var profileImg : String!
	var state : String!
	var status : String!
	var syncFb : String!
	var syncLinkedin : String!
	var twitterId : String!
	var twitterPublicUrl : String!
	var twitterSecretKey : String!
	var updateTime : String!
	var updateTimeMili : String!
	var userId : String!
	var website : String!
    

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		aboutme = dictionary["aboutme"] as? String
		active = dictionary["active"] as? String
		address = dictionary["address"] as? String
		age = dictionary["age"] as? String
		city = dictionary["city"] as? String
		connectedFb = dictionary["connected_fb"] as? String
		connectedLinkedin = dictionary["connected_linkedin"] as? String
		connectedTwitter = dictionary["connected_twitter"] as? String
		country = dictionary["country"] as? String
		createTime = dictionary["create_time"] as? String
		createTimeMili = dictionary["create_time_mili"] as? String
		designation = dictionary["designation"] as? String
		dob = dictionary["dob"] as? String
		emailid = dictionary["emailid"] as? String
		fbId = dictionary["fb_id"] as? String
		fbSecretKey = dictionary["fb_secret_key"] as? String
		firstname = dictionary["firstname"] as? String
		gender = dictionary["gender"] as? String
		industry = dictionary["industry"] as? String
		industryname = dictionary["industryname"] as? String
		interest = dictionary["interest"] as? String
		interestname = dictionary["interestname"] as? String
		lastLoginType = dictionary["last_login_type"] as? String
		lastname = dictionary["lastname"] as? String
		lat = dictionary["lat"] as? String
		linkedinId = dictionary["linkedin_id"] as? String
		linkedinPublicUrl = dictionary["linkedin_public_url"] as? String
		linkedinSecretKey = dictionary["linkedin_secret_key"] as? String
		lng = dictionary["lng"] as? String
		locationId = dictionary["location_id"] as? String
		msg = dictionary["msg"] as? String
		organization = dictionary["organization"] as? String
		password = dictionary["password"] as? String
		phone = dictionary["phone"] as? String
		profileImg = dictionary["profile_img"] as? String
		state = dictionary["state"] as? String
		status = dictionary["status"] as? String
		syncFb = dictionary["sync_fb"] as? String
		syncLinkedin = dictionary["sync_linkedin"] as? String
		twitterId = dictionary["twitter_id"] as? String
		twitterPublicUrl = dictionary["twitter_public_url"] as? String
		twitterSecretKey = dictionary["twitter_secret_key"] as? String
		updateTime = dictionary["update_time"] as? String
		updateTimeMili = dictionary["update_time_mili"] as? String
		userId = dictionary["user_id"] as? String
		website = dictionary["website"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if aboutme != nil{
			dictionary["aboutme"] = aboutme
		}
		if active != nil{
			dictionary["active"] = active
		}
		if address != nil{
			dictionary["address"] = address
		}
		if age != nil{
			dictionary["age"] = age
		}
		if city != nil{
			dictionary["city"] = city
		}
		if connectedFb != nil{
			dictionary["connected_fb"] = connectedFb
		}
		if connectedLinkedin != nil{
			dictionary["connected_linkedin"] = connectedLinkedin
		}
		if connectedTwitter != nil{
			dictionary["connected_twitter"] = connectedTwitter
		}
		if country != nil{
			dictionary["country"] = country
		}
		if createTime != nil{
			dictionary["create_time"] = createTime
		}
		if createTimeMili != nil{
			dictionary["create_time_mili"] = createTimeMili
		}
		if designation != nil{
			dictionary["designation"] = designation
		}
		if dob != nil{
			dictionary["dob"] = dob
		}
		if emailid != nil{
			dictionary["emailid"] = emailid
		}
		if fbId != nil{
			dictionary["fb_id"] = fbId
		}
		if fbSecretKey != nil{
			dictionary["fb_secret_key"] = fbSecretKey
		}
		if firstname != nil{
			dictionary["firstname"] = firstname
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if industry != nil{
			dictionary["industry"] = industry
		}
		if industryname != nil{
			dictionary["industryname"] = industryname
		}
		if interest != nil{
			dictionary["interest"] = interest
		}
		if interestname != nil{
			dictionary["interestname"] = interestname
		}
		if lastLoginType != nil{
			dictionary["last_login_type"] = lastLoginType
		}
		if lastname != nil{
			dictionary["lastname"] = lastname
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if linkedinId != nil{
			dictionary["linkedin_id"] = linkedinId
		}
		if linkedinPublicUrl != nil{
			dictionary["linkedin_public_url"] = linkedinPublicUrl
		}
		if linkedinSecretKey != nil{
			dictionary["linkedin_secret_key"] = linkedinSecretKey
		}
		if lng != nil{
			dictionary["lng"] = lng
		}
		if locationId != nil{
			dictionary["location_id"] = locationId
		}
		if msg != nil{
			dictionary["msg"] = msg
		}
		if organization != nil{
			dictionary["organization"] = organization
		}
		if password != nil{
			dictionary["password"] = password
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if profileImg != nil{
			dictionary["profile_img"] = profileImg
		}
		if state != nil{
			dictionary["state"] = state
		}
		if status != nil{
			dictionary["status"] = status
		}
		if syncFb != nil{
			dictionary["sync_fb"] = syncFb
		}
		if syncLinkedin != nil{
			dictionary["sync_linkedin"] = syncLinkedin
		}
		if twitterId != nil{
			dictionary["twitter_id"] = twitterId
		}
		if twitterPublicUrl != nil{
			dictionary["twitter_public_url"] = twitterPublicUrl
		}
		if twitterSecretKey != nil{
			dictionary["twitter_secret_key"] = twitterSecretKey
		}
		if updateTime != nil{
			dictionary["update_time"] = updateTime
		}
		if updateTimeMili != nil{
			dictionary["update_time_mili"] = updateTimeMili
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if website != nil{
			dictionary["website"] = website
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         aboutme = aDecoder.decodeObjectForKey("aboutme") as? String
         active = aDecoder.decodeObjectForKey("active") as? String
         address = aDecoder.decodeObjectForKey("address") as? String
         age = aDecoder.decodeObjectForKey("age") as? String
         city = aDecoder.decodeObjectForKey("city") as? String
         connectedFb = aDecoder.decodeObjectForKey("connected_fb") as? String
         connectedLinkedin = aDecoder.decodeObjectForKey("connected_linkedin") as? String
         connectedTwitter = aDecoder.decodeObjectForKey("connected_twitter") as? String
         country = aDecoder.decodeObjectForKey("country") as? String
         createTime = aDecoder.decodeObjectForKey("create_time") as? String
         createTimeMili = aDecoder.decodeObjectForKey("create_time_mili") as? String
         designation = aDecoder.decodeObjectForKey("designation") as? String
         dob = aDecoder.decodeObjectForKey("dob") as? String
         emailid = aDecoder.decodeObjectForKey("emailid") as? String
         fbId = aDecoder.decodeObjectForKey("fb_id") as? String
         fbSecretKey = aDecoder.decodeObjectForKey("fb_secret_key") as? String
         firstname = aDecoder.decodeObjectForKey("firstname") as? String
         gender = aDecoder.decodeObjectForKey("gender") as? String
         industry = aDecoder.decodeObjectForKey("industry") as? String
         industryname = aDecoder.decodeObjectForKey("industryname") as? String
         interest = aDecoder.decodeObjectForKey("interest") as? String
         interestname = aDecoder.decodeObjectForKey("interestname") as? String
         lastLoginType = aDecoder.decodeObjectForKey("last_login_type") as? String
         lastname = aDecoder.decodeObjectForKey("lastname") as? String
         lat = aDecoder.decodeObjectForKey("lat") as? String
         linkedinId = aDecoder.decodeObjectForKey("linkedin_id") as? String
         linkedinPublicUrl = aDecoder.decodeObjectForKey("linkedin_public_url") as? String
         linkedinSecretKey = aDecoder.decodeObjectForKey("linkedin_secret_key") as? String
         lng = aDecoder.decodeObjectForKey("lng") as? String
         locationId = aDecoder.decodeObjectForKey("location_id") as? String
         msg = aDecoder.decodeObjectForKey("msg") as? String
         organization = aDecoder.decodeObjectForKey("organization") as? String
         password = aDecoder.decodeObjectForKey("password") as? String
         phone = aDecoder.decodeObjectForKey("phone") as? String
         profileImg = aDecoder.decodeObjectForKey("profile_img") as? String
         state = aDecoder.decodeObjectForKey("state") as? String
         status = aDecoder.decodeObjectForKey("status") as? String
         syncFb = aDecoder.decodeObjectForKey("sync_fb") as? String
         syncLinkedin = aDecoder.decodeObjectForKey("sync_linkedin") as? String
         twitterId = aDecoder.decodeObjectForKey("twitter_id") as? String
         twitterPublicUrl = aDecoder.decodeObjectForKey("twitter_public_url") as? String
         twitterSecretKey = aDecoder.decodeObjectForKey("twitter_secret_key") as? String
         updateTime = aDecoder.decodeObjectForKey("update_time") as? String
         updateTimeMili = aDecoder.decodeObjectForKey("update_time_mili") as? String
         userId = aDecoder.decodeObjectForKey("user_id") as? String
         website = aDecoder.decodeObjectForKey("website") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if aboutme != nil{
			aCoder.encodeObject(aboutme, forKey: "aboutme")
		}
		if active != nil{
			aCoder.encodeObject(active, forKey: "active")
		}
		if address != nil{
			aCoder.encodeObject(address, forKey: "address")
		}
		if age != nil{
			aCoder.encodeObject(age, forKey: "age")
		}
		if city != nil{
			aCoder.encodeObject(city, forKey: "city")
		}
		if connectedFb != nil{
			aCoder.encodeObject(connectedFb, forKey: "connected_fb")
		}
		if connectedLinkedin != nil{
			aCoder.encodeObject(connectedLinkedin, forKey: "connected_linkedin")
		}
		if connectedTwitter != nil{
			aCoder.encodeObject(connectedTwitter, forKey: "connected_twitter")
		}
		if country != nil{
			aCoder.encodeObject(country, forKey: "country")
		}
		if createTime != nil{
			aCoder.encodeObject(createTime, forKey: "create_time")
		}
		if createTimeMili != nil{
			aCoder.encodeObject(createTimeMili, forKey: "create_time_mili")
		}
		if designation != nil{
			aCoder.encodeObject(designation, forKey: "designation")
		}
		if dob != nil{
			aCoder.encodeObject(dob, forKey: "dob")
		}
		if emailid != nil{
			aCoder.encodeObject(emailid, forKey: "emailid")
		}
		if fbId != nil{
			aCoder.encodeObject(fbId, forKey: "fb_id")
		}
		if fbSecretKey != nil{
			aCoder.encodeObject(fbSecretKey, forKey: "fb_secret_key")
		}
		if firstname != nil{
			aCoder.encodeObject(firstname, forKey: "firstname")
		}
		if gender != nil{
			aCoder.encodeObject(gender, forKey: "gender")
		}
		if industry != nil{
			aCoder.encodeObject(industry, forKey: "industry")
		}
		if industryname != nil{
			aCoder.encodeObject(industryname, forKey: "industryname")
		}
		if interest != nil{
			aCoder.encodeObject(interest, forKey: "interest")
		}
		if interestname != nil{
			aCoder.encodeObject(interestname, forKey: "interestname")
		}
		if lastLoginType != nil{
			aCoder.encodeObject(lastLoginType, forKey: "last_login_type")
		}
		if lastname != nil{
			aCoder.encodeObject(lastname, forKey: "lastname")
		}
		if lat != nil{
			aCoder.encodeObject(lat, forKey: "lat")
		}
		if linkedinId != nil{
			aCoder.encodeObject(linkedinId, forKey: "linkedin_id")
		}
		if linkedinPublicUrl != nil{
			aCoder.encodeObject(linkedinPublicUrl, forKey: "linkedin_public_url")
		}
		if linkedinSecretKey != nil{
			aCoder.encodeObject(linkedinSecretKey, forKey: "linkedin_secret_key")
		}
		if lng != nil{
			aCoder.encodeObject(lng, forKey: "lng")
		}
		if locationId != nil{
			aCoder.encodeObject(locationId, forKey: "location_id")
		}
		if msg != nil{
			aCoder.encodeObject(msg, forKey: "msg")
		}
		if organization != nil{
			aCoder.encodeObject(organization, forKey: "organization")
		}
		if password != nil{
			aCoder.encodeObject(password, forKey: "password")
		}
		if phone != nil{
			aCoder.encodeObject(phone, forKey: "phone")
		}
		if profileImg != nil{
			aCoder.encodeObject(profileImg, forKey: "profile_img")
		}
		if state != nil{
			aCoder.encodeObject(state, forKey: "state")
		}
		if status != nil{
			aCoder.encodeObject(status, forKey: "status")
		}
		if syncFb != nil{
			aCoder.encodeObject(syncFb, forKey: "sync_fb")
		}
		if syncLinkedin != nil{
			aCoder.encodeObject(syncLinkedin, forKey: "sync_linkedin")
		}
		if twitterId != nil{
			aCoder.encodeObject(twitterId, forKey: "twitter_id")
		}
		if twitterPublicUrl != nil{
			aCoder.encodeObject(twitterPublicUrl, forKey: "twitter_public_url")
		}
		if twitterSecretKey != nil{
			aCoder.encodeObject(twitterSecretKey, forKey: "twitter_secret_key")
		}
		if updateTime != nil{
			aCoder.encodeObject(updateTime, forKey: "update_time")
		}
		if updateTimeMili != nil{
			aCoder.encodeObject(updateTimeMili, forKey: "update_time_mili")
		}
		if userId != nil{
			aCoder.encodeObject(userId, forKey: "user_id")
		}
		if website != nil{
			aCoder.encodeObject(website, forKey: "website")
		}

	}

}