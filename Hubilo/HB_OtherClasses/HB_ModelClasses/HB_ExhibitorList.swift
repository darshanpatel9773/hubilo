//
//	HB_ExhibitorList.swift
//
//	Create by Priyank Gandhi on 27/6/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_ExhibitorList : NSObject, NSCoding{

	var exhibitorBrochure : String!
	var exhibitorDescription : String!
	var exhibitorEmailid : String!
	var exhibitorFb : String!
	var exhibitorId : String!
	var exhibitorLogoImg : String!
	var exhibitorName : String!
	var exhibitorPhone : String!
	var exhibitorPosition : String!
	var exhibitorTwitter : String!
	var exhibitorWebsite : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		exhibitorBrochure = dictionary["exhibitor_brochure"] as? String
		exhibitorDescription = dictionary["exhibitor_description"] as? String
		exhibitorEmailid = dictionary["exhibitor_emailid"] as? String
		exhibitorFb = dictionary["exhibitor_fb"] as? String
		exhibitorId = dictionary["exhibitor_id"] as? String
		exhibitorLogoImg = dictionary["exhibitor_logo_img"] as? String
		exhibitorName = dictionary["exhibitor_name"] as? String
		exhibitorPhone = dictionary["exhibitor_phone"] as? String
		exhibitorPosition = dictionary["exhibitor_position"] as? String
		exhibitorTwitter = dictionary["exhibitor_twitter"] as? String
		exhibitorWebsite = dictionary["exhibitor_website"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if exhibitorBrochure != nil{
			dictionary["exhibitor_brochure"] = exhibitorBrochure
		}
		if exhibitorDescription != nil{
			dictionary["exhibitor_description"] = exhibitorDescription
		}
		if exhibitorEmailid != nil{
			dictionary["exhibitor_emailid"] = exhibitorEmailid
		}
		if exhibitorFb != nil{
			dictionary["exhibitor_fb"] = exhibitorFb
		}
		if exhibitorId != nil{
			dictionary["exhibitor_id"] = exhibitorId
		}
		if exhibitorLogoImg != nil{
			dictionary["exhibitor_logo_img"] = exhibitorLogoImg
		}
		if exhibitorName != nil{
			dictionary["exhibitor_name"] = exhibitorName
		}
		if exhibitorPhone != nil{
			dictionary["exhibitor_phone"] = exhibitorPhone
		}
		if exhibitorPosition != nil{
			dictionary["exhibitor_position"] = exhibitorPosition
		}
		if exhibitorTwitter != nil{
			dictionary["exhibitor_twitter"] = exhibitorTwitter
		}
		if exhibitorWebsite != nil{
			dictionary["exhibitor_website"] = exhibitorWebsite
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         exhibitorBrochure = aDecoder.decodeObjectForKey("exhibitor_brochure") as? String
         exhibitorDescription = aDecoder.decodeObjectForKey("exhibitor_description") as? String
         exhibitorEmailid = aDecoder.decodeObjectForKey("exhibitor_emailid") as? String
         exhibitorFb = aDecoder.decodeObjectForKey("exhibitor_fb") as? String
         exhibitorId = aDecoder.decodeObjectForKey("exhibitor_id") as? String
         exhibitorLogoImg = aDecoder.decodeObjectForKey("exhibitor_logo_img") as? String
         exhibitorName = aDecoder.decodeObjectForKey("exhibitor_name") as? String
         exhibitorPhone = aDecoder.decodeObjectForKey("exhibitor_phone") as? String
         exhibitorPosition = aDecoder.decodeObjectForKey("exhibitor_position") as? String
         exhibitorTwitter = aDecoder.decodeObjectForKey("exhibitor_twitter") as? String
         exhibitorWebsite = aDecoder.decodeObjectForKey("exhibitor_website") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if exhibitorBrochure != nil{
			aCoder.encodeObject(exhibitorBrochure, forKey: "exhibitor_brochure")
		}
		if exhibitorDescription != nil{
			aCoder.encodeObject(exhibitorDescription, forKey: "exhibitor_description")
		}
		if exhibitorEmailid != nil{
			aCoder.encodeObject(exhibitorEmailid, forKey: "exhibitor_emailid")
		}
		if exhibitorFb != nil{
			aCoder.encodeObject(exhibitorFb, forKey: "exhibitor_fb")
		}
		if exhibitorId != nil{
			aCoder.encodeObject(exhibitorId, forKey: "exhibitor_id")
		}
		if exhibitorLogoImg != nil{
			aCoder.encodeObject(exhibitorLogoImg, forKey: "exhibitor_logo_img")
		}
		if exhibitorName != nil{
			aCoder.encodeObject(exhibitorName, forKey: "exhibitor_name")
		}
		if exhibitorPhone != nil{
			aCoder.encodeObject(exhibitorPhone, forKey: "exhibitor_phone")
		}
		if exhibitorPosition != nil{
			aCoder.encodeObject(exhibitorPosition, forKey: "exhibitor_position")
		}
		if exhibitorTwitter != nil{
			aCoder.encodeObject(exhibitorTwitter, forKey: "exhibitor_twitter")
		}
		if exhibitorWebsite != nil{
			aCoder.encodeObject(exhibitorWebsite, forKey: "exhibitor_website")
		}

	}

}