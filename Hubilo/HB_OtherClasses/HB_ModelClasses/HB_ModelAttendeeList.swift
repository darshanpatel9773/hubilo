//
//	HB_ModelAttendeeList.swift
//
//	Create by Pawan jaiswal on 25/6/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_ModelAttendeeList : NSObject, NSCoding{

	var aboutme : String!
	var createTimeMili : String!
	var designation : String!
	var fbId : String!
	var firstname : String!
	var isBookmark : Int!
	var lastname : String!
	var linkedinPublicUrl : String!
	var organization : String!
	var profileImg : String!
	var twitterPublicUrl : String!
	var userId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		aboutme = dictionary["aboutme"] as? String
		createTimeMili = dictionary["create_time_mili"] as? String
		designation = dictionary["designation"] as? String
		fbId = dictionary["fb_id"] as? String
		firstname = dictionary["firstname"] as? String
		isBookmark = dictionary["is_bookmark"] as? Int
		lastname = dictionary["lastname"] as? String
		linkedinPublicUrl = dictionary["linkedin_public_url"] as? String
		organization = dictionary["organization"] as? String
		profileImg = dictionary["profile_img"] as? String
		twitterPublicUrl = dictionary["twitter_public_url"] as? String
		userId = dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if aboutme != nil{
			dictionary["aboutme"] = aboutme
		}
		if createTimeMili != nil{
			dictionary["create_time_mili"] = createTimeMili
		}
		if designation != nil{
			dictionary["designation"] = designation
		}
		if fbId != nil{
			dictionary["fb_id"] = fbId
		}
		if firstname != nil{
			dictionary["firstname"] = firstname
		}
		if isBookmark != nil{
			dictionary["is_bookmark"] = isBookmark
		}
		if lastname != nil{
			dictionary["lastname"] = lastname
		}
		if linkedinPublicUrl != nil{
			dictionary["linkedin_public_url"] = linkedinPublicUrl
		}
		if organization != nil{
			dictionary["organization"] = organization
		}
		if profileImg != nil{
			dictionary["profile_img"] = profileImg
		}
		if twitterPublicUrl != nil{
			dictionary["twitter_public_url"] = twitterPublicUrl
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         aboutme = aDecoder.decodeObjectForKey("aboutme") as? String
         createTimeMili = aDecoder.decodeObjectForKey("create_time_mili") as? String
         designation = aDecoder.decodeObjectForKey("designation") as? String
         fbId = aDecoder.decodeObjectForKey("fb_id") as? String
         firstname = aDecoder.decodeObjectForKey("firstname") as? String
         isBookmark = aDecoder.decodeObjectForKey("is_bookmark") as? Int
         lastname = aDecoder.decodeObjectForKey("lastname") as? String
         linkedinPublicUrl = aDecoder.decodeObjectForKey("linkedin_public_url") as? String
         organization = aDecoder.decodeObjectForKey("organization") as? String
         profileImg = aDecoder.decodeObjectForKey("profile_img") as? String
         twitterPublicUrl = aDecoder.decodeObjectForKey("twitter_public_url") as? String
         userId = aDecoder.decodeObjectForKey("user_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if aboutme != nil{
			aCoder.encodeObject(aboutme, forKey: "aboutme")
		}
		if createTimeMili != nil{
			aCoder.encodeObject(createTimeMili, forKey: "create_time_mili")
		}
		if designation != nil{
			aCoder.encodeObject(designation, forKey: "designation")
		}
		if fbId != nil{
			aCoder.encodeObject(fbId, forKey: "fb_id")
		}
		if firstname != nil{
			aCoder.encodeObject(firstname, forKey: "firstname")
		}
		if isBookmark != nil{
			aCoder.encodeObject(isBookmark, forKey: "is_bookmark")
		}
		if lastname != nil{
			aCoder.encodeObject(lastname, forKey: "lastname")
		}
		if linkedinPublicUrl != nil{
			aCoder.encodeObject(linkedinPublicUrl, forKey: "linkedin_public_url")
		}
		if organization != nil{
			aCoder.encodeObject(organization, forKey: "organization")
		}
		if profileImg != nil{
			aCoder.encodeObject(profileImg, forKey: "profile_img")
		}
		if twitterPublicUrl != nil{
			aCoder.encodeObject(twitterPublicUrl, forKey: "twitter_public_url")
		}
		if userId != nil{
			aCoder.encodeObject(userId, forKey: "user_id")
		}

	}

}