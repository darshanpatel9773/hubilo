//
//	HB_Interest.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_Interest : NSObject, NSCoding{

	var interestId : String!
	var interestName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		interestId = dictionary["interest_id"] as? String
		interestName = dictionary["interest_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if interestId != nil{
			dictionary["interest_id"] = interestId
		}
		if interestName != nil{
			dictionary["interest_name"] = interestName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         interestId = aDecoder.decodeObjectForKey("interest_id") as? String
         interestName = aDecoder.decodeObjectForKey("interest_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if interestId != nil{
			aCoder.encodeObject(interestId, forKey: "interest_id")
		}
		if interestName != nil{
			aCoder.encodeObject(interestName, forKey: "interest_name")
		}

	}

}