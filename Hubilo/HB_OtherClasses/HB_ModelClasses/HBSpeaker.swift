//
//  HBSpeaker.swift
//
//  Created by Priyank Gandhi on 01/07/16
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

public class HBSpeaker: NSObject, Mappable, NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    internal let kHBSpeakerSpeakerNameKey: String = "speaker_name"
    internal let kHBSpeakerSpeakerRatingKey: String = "speaker_rating"
    internal let kHBSpeakerSpeakerLinkedinLinkKey: String = "speaker_linkedin_link"
    internal let kHBSpeakerSpeakerLongDescriptionKey: String = "speaker_long_description"
    internal let kHBSpeakerSpeakerTitleKey: String = "speaker_title"
    internal let kHBSpeakerSpeakerIdKey: String = "speaker_id"
    internal let kHBSpeakerSpeakerCategoryKey: String = "speaker_category"
    internal let kHBSpeakerSpeakerTwitterFollowKey: String = "speaker_twitter_follow"
    internal let kHBSpeakerSpeakerDescriptionKey: String = "speaker_description"
    internal let kHBSpeakerSpeakerPositionKey: String = "speaker_position"
    internal let kHBSpeakerSpeakerProfileImgKey: String = "speaker_profile_img"
    internal let kHBSpeakerSpeakerUserRatingKey: String = "speaker_user_rating"
    
    
    // MARK: Properties
    public var speakerName: String?
    public var speakerRating: String?
    public var speakerLinkedinLink: String?
    public var speakerLongDescription: String?
    public var speakerTitle: String?
    public var speakerId: String?
    public var speakerCategory: String?
    public var speakerTwitterFollow: String?
    public var speakerDescription: String?
    public var speakerPosition: String?
    public var speakerProfileImg: String?
    public var speakerUserRating: Double?
    
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the class based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the class based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        speakerName = json[kHBSpeakerSpeakerNameKey].string
        speakerRating = json[kHBSpeakerSpeakerRatingKey].string
        speakerLinkedinLink = json[kHBSpeakerSpeakerLinkedinLinkKey].string
        speakerLongDescription = json[kHBSpeakerSpeakerLongDescriptionKey].string
        speakerTitle = json[kHBSpeakerSpeakerTitleKey].string
        speakerId = json[kHBSpeakerSpeakerIdKey].string
        speakerCategory = json[kHBSpeakerSpeakerCategoryKey].string
        speakerTwitterFollow = json[kHBSpeakerSpeakerTwitterFollowKey].string
        speakerDescription = json[kHBSpeakerSpeakerDescriptionKey].string
        speakerPosition = json[kHBSpeakerSpeakerPositionKey].string
        speakerProfileImg = json[kHBSpeakerSpeakerProfileImgKey].string
        speakerUserRating = json[kHBSpeakerSpeakerUserRatingKey].double
        
    }
    public override init()
    {
        super.init()
    }
    
    // MARK: ObjectMapper Initalizers
    /**
     Map a JSON object to this class using ObjectMapper
     - parameter map: A mapping from ObjectMapper
     */
    required public init?(_ map: Map){
        
    }
    
    /**
     Map a JSON object to this class using ObjectMapper
     - parameter map: A mapping from ObjectMapper
     */
    public func mapping(map: Map) {
        speakerName <- map[kHBSpeakerSpeakerNameKey]
        speakerRating <- map[kHBSpeakerSpeakerRatingKey]
        speakerLinkedinLink <- map[kHBSpeakerSpeakerLinkedinLinkKey]
        speakerLongDescription <- map[kHBSpeakerSpeakerLongDescriptionKey]
        speakerTitle <- map[kHBSpeakerSpeakerTitleKey]
        speakerId <- map[kHBSpeakerSpeakerIdKey]
        speakerCategory <- map[kHBSpeakerSpeakerCategoryKey]
        speakerTwitterFollow <- map[kHBSpeakerSpeakerTwitterFollowKey]
        speakerDescription <- map[kHBSpeakerSpeakerDescriptionKey]
        speakerPosition <- map[kHBSpeakerSpeakerPositionKey]
        speakerProfileImg <- map[kHBSpeakerSpeakerProfileImgKey]
        speakerUserRating <- map[kHBSpeakerSpeakerUserRatingKey]
        
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String : AnyObject ] {
        
        var dictionary: [String : AnyObject ] = [ : ]
        if speakerName != nil {
            dictionary.updateValue(speakerName!, forKey: kHBSpeakerSpeakerNameKey)
        }
        if speakerRating != nil {
            dictionary.updateValue(speakerRating!, forKey: kHBSpeakerSpeakerRatingKey)
        }
        if speakerLinkedinLink != nil {
            dictionary.updateValue(speakerLinkedinLink!, forKey: kHBSpeakerSpeakerLinkedinLinkKey)
        }
        if speakerLongDescription != nil {
            dictionary.updateValue(speakerLongDescription!, forKey: kHBSpeakerSpeakerLongDescriptionKey)
        }
        if speakerTitle != nil {
            dictionary.updateValue(speakerTitle!, forKey: kHBSpeakerSpeakerTitleKey)
        }
        if speakerId != nil {
            dictionary.updateValue(speakerId!, forKey: kHBSpeakerSpeakerIdKey)
        }
        if speakerCategory != nil {
            dictionary.updateValue(speakerCategory!, forKey: kHBSpeakerSpeakerCategoryKey)
        }
        if speakerTwitterFollow != nil {
            dictionary.updateValue(speakerTwitterFollow!, forKey: kHBSpeakerSpeakerTwitterFollowKey)
        }
        if speakerDescription != nil {
            dictionary.updateValue(speakerDescription!, forKey: kHBSpeakerSpeakerDescriptionKey)
        }
        if speakerPosition != nil {
            dictionary.updateValue(speakerPosition!, forKey: kHBSpeakerSpeakerPositionKey)
        }
        if speakerProfileImg != nil {
            dictionary.updateValue(speakerProfileImg!, forKey: kHBSpeakerSpeakerProfileImgKey)
        }
        if speakerUserRating != nil {
            dictionary.updateValue(speakerUserRating!, forKey: kHBSpeakerSpeakerUserRatingKey)
        }
        
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.speakerName = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerNameKey) as? String
        self.speakerRating = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerRatingKey) as? String
        self.speakerLinkedinLink = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerLinkedinLinkKey) as? String
        self.speakerLongDescription = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerLongDescriptionKey) as? String
        self.speakerTitle = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerTitleKey) as? String
        self.speakerId = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerIdKey) as? String
        self.speakerCategory = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerCategoryKey) as? String
        self.speakerTwitterFollow = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerTwitterFollowKey) as? String
        self.speakerDescription = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerDescriptionKey) as? String
        self.speakerPosition = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerPositionKey) as? String
        self.speakerProfileImg = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerProfileImgKey) as? String
        self.speakerUserRating = aDecoder.decodeObjectForKey(kHBSpeakerSpeakerUserRatingKey) as? Double
        
    }
    
    public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(speakerName, forKey: kHBSpeakerSpeakerNameKey)
        aCoder.encodeObject(speakerRating, forKey: kHBSpeakerSpeakerRatingKey)
        aCoder.encodeObject(speakerLinkedinLink, forKey: kHBSpeakerSpeakerLinkedinLinkKey)
        aCoder.encodeObject(speakerLongDescription, forKey: kHBSpeakerSpeakerLongDescriptionKey)
        aCoder.encodeObject(speakerTitle, forKey: kHBSpeakerSpeakerTitleKey)
        aCoder.encodeObject(speakerId, forKey: kHBSpeakerSpeakerIdKey)
        aCoder.encodeObject(speakerCategory, forKey: kHBSpeakerSpeakerCategoryKey)
        aCoder.encodeObject(speakerTwitterFollow, forKey: kHBSpeakerSpeakerTwitterFollowKey)
        aCoder.encodeObject(speakerDescription, forKey: kHBSpeakerSpeakerDescriptionKey)
        aCoder.encodeObject(speakerPosition, forKey: kHBSpeakerSpeakerPositionKey)
        aCoder.encodeObject(speakerProfileImg, forKey: kHBSpeakerSpeakerProfileImgKey)
        aCoder.encodeObject(speakerUserRating, forKey: kHBSpeakerSpeakerUserRatingKey)
        
    }
    
}