//
//	HB_ModelEvent.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_ModelEvent : NSObject, NSCoding{

	var communityAccessCode : Int!
	var address : String!
	var buttonName : String!
	var city : String!
	var country : String!
	var email : String!
	var eventAttendeeCount : String!
	var eventCoverImg : String!
	var eventDate : String!
	var eventDescription : String!
	var eventEndDate : String!
	var eventEndTime : String!
	var eventEndTimeMili : String!
	var eventFb : String!
	var eventId : String!
	var eventLogoImg : String!
	var eventName : String!
	var eventOrganizerId : String!
	var eventRegistrationWebsite : String!
	var eventStartTime : String!
	var eventStartTimeMili : String!
	var eventTwitter : String!
	var eventVenueMap : String!
	var eventWebsite : String!
	var isPublished : String!
	var lat : String!
	var lng : String!
	var msgCount : Int!
	var notiCount : Int!
	var phone : String!
	var state : String!
	var ticketing : Int!
	var transactOnce : String!
	var userCommunityExist : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		communityAccessCode = dictionary["CommunityAccessCode"] as? Int
		address = dictionary["address"] as? String
		buttonName = dictionary["buttonName"] as? String
		city = dictionary["city"] as? String
		country = dictionary["country"] as? String
		email = dictionary["email"] as? String
		eventAttendeeCount = dictionary["event_attendee_count"] as? String
		eventCoverImg = dictionary["event_cover_img"] as? String
		eventDate = dictionary["event_date"] as? String
		eventDescription = dictionary["event_description"] as? String
		eventEndDate = dictionary["event_end_date"] as? String
		eventEndTime = dictionary["event_end_time"] as? String
		eventEndTimeMili = dictionary["event_end_time_mili"] as? String
		eventFb = dictionary["event_fb"] as? String
		eventId = dictionary["event_id"] as? String
		eventLogoImg = dictionary["event_logo_img"] as? String
		eventName = dictionary["event_name"] as? String
		eventOrganizerId = dictionary["event_organizer_id"] as? String
		eventRegistrationWebsite = dictionary["event_registration_website"] as? String
		eventStartTime = dictionary["event_start_time"] as? String
		eventStartTimeMili = dictionary["event_start_time_mili"] as? String
		eventTwitter = dictionary["event_twitter"] as? String
		eventVenueMap = dictionary["event_venue_map"] as? String
		eventWebsite = dictionary["event_website"] as? String
		isPublished = dictionary["is_published"] as? String
		lat = dictionary["lat"] as? String
		lng = dictionary["lng"] as? String
		msgCount = dictionary["msg_count"] as? Int
		notiCount = dictionary["noti_count"] as? Int
		phone = dictionary["phone"] as? String
		state = dictionary["state"] as? String
		ticketing = dictionary["ticketing"] as? Int
		transactOnce = dictionary["transactOnce"] as? String
		userCommunityExist = dictionary["userCommunityExist"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		var dictionary = NSMutableDictionary()
		if communityAccessCode != nil{
			dictionary["CommunityAccessCode"] = communityAccessCode
		}
		if address != nil{
			dictionary["address"] = address
		}
		if buttonName != nil{
			dictionary["buttonName"] = buttonName
		}
		if city != nil{
			dictionary["city"] = city
		}
		if country != nil{
			dictionary["country"] = country
		}
		if email != nil{
			dictionary["email"] = email
		}
		if eventAttendeeCount != nil{
			dictionary["event_attendee_count"] = eventAttendeeCount
		}
		if eventCoverImg != nil{
			dictionary["event_cover_img"] = eventCoverImg
		}
		if eventDate != nil{
			dictionary["event_date"] = eventDate
		}
		if eventDescription != nil{
			dictionary["event_description"] = eventDescription
		}
		if eventEndDate != nil{
			dictionary["event_end_date"] = eventEndDate
		}
		if eventEndTime != nil{
			dictionary["event_end_time"] = eventEndTime
		}
		if eventEndTimeMili != nil{
			dictionary["event_end_time_mili"] = eventEndTimeMili
		}
		if eventFb != nil{
			dictionary["event_fb"] = eventFb
		}
		if eventId != nil{
			dictionary["event_id"] = eventId
		}
		if eventLogoImg != nil{
			dictionary["event_logo_img"] = eventLogoImg
		}
		if eventName != nil{
			dictionary["event_name"] = eventName
		}
		if eventOrganizerId != nil{
			dictionary["event_organizer_id"] = eventOrganizerId
		}
		if eventRegistrationWebsite != nil{
			dictionary["event_registration_website"] = eventRegistrationWebsite
		}
		if eventStartTime != nil{
			dictionary["event_start_time"] = eventStartTime
		}
		if eventStartTimeMili != nil{
			dictionary["event_start_time_mili"] = eventStartTimeMili
		}
		if eventTwitter != nil{
			dictionary["event_twitter"] = eventTwitter
		}
		if eventVenueMap != nil{
			dictionary["event_venue_map"] = eventVenueMap
		}
		if eventWebsite != nil{
			dictionary["event_website"] = eventWebsite
		}
		if isPublished != nil{
			dictionary["is_published"] = isPublished
		}
		if lat != nil{
			dictionary["lat"] = lat
		}
		if lng != nil{
			dictionary["lng"] = lng
		}
		if msgCount != nil{
			dictionary["msg_count"] = msgCount
		}
		if notiCount != nil{
			dictionary["noti_count"] = notiCount
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if state != nil{
			dictionary["state"] = state
		}
		if ticketing != nil{
			dictionary["ticketing"] = ticketing
		}
		if transactOnce != nil{
			dictionary["transactOnce"] = transactOnce
		}
		if userCommunityExist != nil{
			dictionary["userCommunityExist"] = userCommunityExist
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         communityAccessCode = aDecoder.decodeObjectForKey("CommunityAccessCode") as? Int
         address = aDecoder.decodeObjectForKey("address") as? String
         buttonName = aDecoder.decodeObjectForKey("buttonName") as? String
         city = aDecoder.decodeObjectForKey("city") as? String
         country = aDecoder.decodeObjectForKey("country") as? String
         email = aDecoder.decodeObjectForKey("email") as? String
         eventAttendeeCount = aDecoder.decodeObjectForKey("event_attendee_count") as? String
         eventCoverImg = aDecoder.decodeObjectForKey("event_cover_img") as? String
         eventDate = aDecoder.decodeObjectForKey("event_date") as? String
         eventDescription = aDecoder.decodeObjectForKey("event_description") as? String
         eventEndDate = aDecoder.decodeObjectForKey("event_end_date") as? String
         eventEndTime = aDecoder.decodeObjectForKey("event_end_time") as? String
         eventEndTimeMili = aDecoder.decodeObjectForKey("event_end_time_mili") as? String
         eventFb = aDecoder.decodeObjectForKey("event_fb") as? String
         eventId = aDecoder.decodeObjectForKey("event_id") as? String
         eventLogoImg = aDecoder.decodeObjectForKey("event_logo_img") as? String
         eventName = aDecoder.decodeObjectForKey("event_name") as? String
         eventOrganizerId = aDecoder.decodeObjectForKey("event_organizer_id") as? String
         eventRegistrationWebsite = aDecoder.decodeObjectForKey("event_registration_website") as? String
         eventStartTime = aDecoder.decodeObjectForKey("event_start_time") as? String
         eventStartTimeMili = aDecoder.decodeObjectForKey("event_start_time_mili") as? String
         eventTwitter = aDecoder.decodeObjectForKey("event_twitter") as? String
         eventVenueMap = aDecoder.decodeObjectForKey("event_venue_map") as? String
         eventWebsite = aDecoder.decodeObjectForKey("event_website") as? String
         isPublished = aDecoder.decodeObjectForKey("is_published") as? String
         lat = aDecoder.decodeObjectForKey("lat") as? String
         lng = aDecoder.decodeObjectForKey("lng") as? String
         msgCount = aDecoder.decodeObjectForKey("msg_count") as? Int
         notiCount = aDecoder.decodeObjectForKey("noti_count") as? Int
         phone = aDecoder.decodeObjectForKey("phone") as? String
         state = aDecoder.decodeObjectForKey("state") as? String
         ticketing = aDecoder.decodeObjectForKey("ticketing") as? Int
         transactOnce = aDecoder.decodeObjectForKey("transactOnce") as? String
         userCommunityExist = aDecoder.decodeObjectForKey("userCommunityExist") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if communityAccessCode != nil{
			aCoder.encodeObject(communityAccessCode, forKey: "CommunityAccessCode")
		}
		if address != nil{
			aCoder.encodeObject(address, forKey: "address")
		}
		if buttonName != nil{
			aCoder.encodeObject(buttonName, forKey: "buttonName")
		}
		if city != nil{
			aCoder.encodeObject(city, forKey: "city")
		}
		if country != nil{
			aCoder.encodeObject(country, forKey: "country")
		}
		if email != nil{
			aCoder.encodeObject(email, forKey: "email")
		}
		if eventAttendeeCount != nil{
			aCoder.encodeObject(eventAttendeeCount, forKey: "event_attendee_count")
		}
		if eventCoverImg != nil{
			aCoder.encodeObject(eventCoverImg, forKey: "event_cover_img")
		}
		if eventDate != nil{
			aCoder.encodeObject(eventDate, forKey: "event_date")
		}
		if eventDescription != nil{
			aCoder.encodeObject(eventDescription, forKey: "event_description")
		}
		if eventEndDate != nil{
			aCoder.encodeObject(eventEndDate, forKey: "event_end_date")
		}
		if eventEndTime != nil{
			aCoder.encodeObject(eventEndTime, forKey: "event_end_time")
		}
		if eventEndTimeMili != nil{
			aCoder.encodeObject(eventEndTimeMili, forKey: "event_end_time_mili")
		}
		if eventFb != nil{
			aCoder.encodeObject(eventFb, forKey: "event_fb")
		}
		if eventId != nil{
			aCoder.encodeObject(eventId, forKey: "event_id")
		}
		if eventLogoImg != nil{
			aCoder.encodeObject(eventLogoImg, forKey: "event_logo_img")
		}
		if eventName != nil{
			aCoder.encodeObject(eventName, forKey: "event_name")
		}
		if eventOrganizerId != nil{
			aCoder.encodeObject(eventOrganizerId, forKey: "event_organizer_id")
		}
		if eventRegistrationWebsite != nil{
			aCoder.encodeObject(eventRegistrationWebsite, forKey: "event_registration_website")
		}
		if eventStartTime != nil{
			aCoder.encodeObject(eventStartTime, forKey: "event_start_time")
		}
		if eventStartTimeMili != nil{
			aCoder.encodeObject(eventStartTimeMili, forKey: "event_start_time_mili")
		}
		if eventTwitter != nil{
			aCoder.encodeObject(eventTwitter, forKey: "event_twitter")
		}
		if eventVenueMap != nil{
			aCoder.encodeObject(eventVenueMap, forKey: "event_venue_map")
		}
		if eventWebsite != nil{
			aCoder.encodeObject(eventWebsite, forKey: "event_website")
		}
		if isPublished != nil{
			aCoder.encodeObject(isPublished, forKey: "is_published")
		}
		if lat != nil{
			aCoder.encodeObject(lat, forKey: "lat")
		}
		if lng != nil{
			aCoder.encodeObject(lng, forKey: "lng")
		}
		if msgCount != nil{
			aCoder.encodeObject(msgCount, forKey: "msg_count")
		}
		if notiCount != nil{
			aCoder.encodeObject(notiCount, forKey: "noti_count")
		}
		if phone != nil{
			aCoder.encodeObject(phone, forKey: "phone")
		}
		if state != nil{
			aCoder.encodeObject(state, forKey: "state")
		}
		if ticketing != nil{
			aCoder.encodeObject(ticketing, forKey: "ticketing")
		}
		if transactOnce != nil{
			aCoder.encodeObject(transactOnce, forKey: "transactOnce")
		}
		if userCommunityExist != nil{
			aCoder.encodeObject(userCommunityExist, forKey: "userCommunityExist")
		}

	}

}