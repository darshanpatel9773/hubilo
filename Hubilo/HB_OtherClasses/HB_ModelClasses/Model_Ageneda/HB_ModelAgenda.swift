//
//	HB_ModelAgenda.swift
//
//	Create by Pawan jaiswal on 26/6/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_ModelAgenda : NSObject, NSCoding{

    var daylist:[HB_Daylist]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		daylist = [HB_Daylist]()
		if let daylistArray = dictionary["daylist"] as? [NSDictionary]{
			for dic in daylistArray{
				let value = HB_Daylist(fromDictionary: dic)
				daylist.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if daylist != nil{
			var dictionaryElements = [NSDictionary]()
			for daylistElement in daylist {
				dictionaryElements.append(daylistElement.toDictionary())
			}
			dictionary["daylist"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         daylist = aDecoder.decodeObjectForKey("daylist") as? [HB_Daylist]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if daylist != nil{
			aCoder.encodeObject(daylist, forKey: "daylist")
		}

	}

}