//
//	HB_AgendaList.swift
//
//	Create by Pawan jaiswal on 26/6/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_AgendaList : NSObject, NSCoding{

	var agendaLikes : String!
	var agendaLocation : String!
	var agendaDate : String!
	var agendaDescription : String!
	var agendaEndTime : String!
	var agendaEndTimeMili : String!
	var agendaEventId : String!
	var agendaId : String!
	var agendaName : String!
	var agendaStartTime : String!
	var agendaStartTimeMili : String!
	var createTime : String!
	var createTimeMili : String!
	var isLike : Int!
	var parallel : Int!
	var speakers : [HB_SpeakerList]!
	var updateTime : String!
	var updateTimeMili : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		agendaLikes = dictionary["agendaLikes"] as? String
		agendaLocation = dictionary["agendaLocation"] as? String
		agendaDate = dictionary["agenda_date"] as? String
		agendaDescription = dictionary["agenda_description"] as? String
		agendaEndTime = dictionary["agenda_end_time"] as? String
		agendaEndTimeMili = dictionary["agenda_end_time_mili"] as? String
		agendaEventId = dictionary["agenda_event_id"] as? String
		agendaId = dictionary["agenda_id"] as? String
		agendaName = dictionary["agenda_name"] as? String
		agendaStartTime = dictionary["agenda_start_time"] as? String
		agendaStartTimeMili = dictionary["agenda_start_time_mili"] as? String
		createTime = dictionary["create_time"] as? String
		createTimeMili = dictionary["create_time_mili"] as? String
		isLike = dictionary["isLike"] as? Int
		parallel = dictionary["parallel"] as? Int
		speakers = [HB_SpeakerList]()
		if let speakersArray = dictionary["speakers"] as? [NSDictionary]{
			for dic in speakersArray{
				let value = HB_SpeakerList(fromDictionary: dic)
				speakers.append(value)
			}
		}
		updateTime = dictionary["update_time"] as? String
		updateTimeMili = dictionary["update_time_mili"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if agendaLikes != nil{
			dictionary["agendaLikes"] = agendaLikes
		}
		if agendaLocation != nil{
			dictionary["agendaLocation"] = agendaLocation
		}
		if agendaDate != nil{
			dictionary["agenda_date"] = agendaDate
		}
		if agendaDescription != nil{
			dictionary["agenda_description"] = agendaDescription
		}
		if agendaEndTime != nil{
			dictionary["agenda_end_time"] = agendaEndTime
		}
		if agendaEndTimeMili != nil{
			dictionary["agenda_end_time_mili"] = agendaEndTimeMili
		}
		if agendaEventId != nil{
			dictionary["agenda_event_id"] = agendaEventId
		}
		if agendaId != nil{
			dictionary["agenda_id"] = agendaId
		}
		if agendaName != nil{
			dictionary["agenda_name"] = agendaName
		}
		if agendaStartTime != nil{
			dictionary["agenda_start_time"] = agendaStartTime
		}
		if agendaStartTimeMili != nil{
			dictionary["agenda_start_time_mili"] = agendaStartTimeMili
		}
		if createTime != nil{
			dictionary["create_time"] = createTime
		}
		if createTimeMili != nil{
			dictionary["create_time_mili"] = createTimeMili
		}
		if isLike != nil{
			dictionary["isLike"] = isLike
		}
		if parallel != nil{
			dictionary["parallel"] = parallel
		}
		if speakers != nil{
			var dictionaryElements = [NSDictionary]()
			for speakersElement in speakers {
				dictionaryElements.append(speakersElement.toDictionary())
			}
			dictionary["speakers"] = dictionaryElements
		}
		if updateTime != nil{
			dictionary["update_time"] = updateTime
		}
		if updateTimeMili != nil{
			dictionary["update_time_mili"] = updateTimeMili
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         agendaLikes = aDecoder.decodeObjectForKey("agendaLikes") as? String
         agendaLocation = aDecoder.decodeObjectForKey("agendaLocation") as? String
         agendaDate = aDecoder.decodeObjectForKey("agenda_date") as? String
         agendaDescription = aDecoder.decodeObjectForKey("agenda_description") as? String
         agendaEndTime = aDecoder.decodeObjectForKey("agenda_end_time") as? String
         agendaEndTimeMili = aDecoder.decodeObjectForKey("agenda_end_time_mili") as? String
         agendaEventId = aDecoder.decodeObjectForKey("agenda_event_id") as? String
         agendaId = aDecoder.decodeObjectForKey("agenda_id") as? String
         agendaName = aDecoder.decodeObjectForKey("agenda_name") as? String
         agendaStartTime = aDecoder.decodeObjectForKey("agenda_start_time") as? String
         agendaStartTimeMili = aDecoder.decodeObjectForKey("agenda_start_time_mili") as? String
         createTime = aDecoder.decodeObjectForKey("create_time") as? String
         createTimeMili = aDecoder.decodeObjectForKey("create_time_mili") as? String
         isLike = aDecoder.decodeObjectForKey("isLike") as? Int
         parallel = aDecoder.decodeObjectForKey("parallel") as? Int
         speakers = aDecoder.decodeObjectForKey("speakers") as? [HB_SpeakerList]
         updateTime = aDecoder.decodeObjectForKey("update_time") as? String
         updateTimeMili = aDecoder.decodeObjectForKey("update_time_mili") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if agendaLikes != nil{
			aCoder.encodeObject(agendaLikes, forKey: "agendaLikes")
		}
		if agendaLocation != nil{
			aCoder.encodeObject(agendaLocation, forKey: "agendaLocation")
		}
		if agendaDate != nil{
			aCoder.encodeObject(agendaDate, forKey: "agenda_date")
		}
		if agendaDescription != nil{
			aCoder.encodeObject(agendaDescription, forKey: "agenda_description")
		}
		if agendaEndTime != nil{
			aCoder.encodeObject(agendaEndTime, forKey: "agenda_end_time")
		}
		if agendaEndTimeMili != nil{
			aCoder.encodeObject(agendaEndTimeMili, forKey: "agenda_end_time_mili")
		}
		if agendaEventId != nil{
			aCoder.encodeObject(agendaEventId, forKey: "agenda_event_id")
		}
		if agendaId != nil{
			aCoder.encodeObject(agendaId, forKey: "agenda_id")
		}
		if agendaName != nil{
			aCoder.encodeObject(agendaName, forKey: "agenda_name")
		}
		if agendaStartTime != nil{
			aCoder.encodeObject(agendaStartTime, forKey: "agenda_start_time")
		}
		if agendaStartTimeMili != nil{
			aCoder.encodeObject(agendaStartTimeMili, forKey: "agenda_start_time_mili")
		}
		if createTime != nil{
			aCoder.encodeObject(createTime, forKey: "create_time")
		}
		if createTimeMili != nil{
			aCoder.encodeObject(createTimeMili, forKey: "create_time_mili")
		}
		if isLike != nil{
			aCoder.encodeObject(isLike, forKey: "isLike")
		}
		if parallel != nil{
			aCoder.encodeObject(parallel, forKey: "parallel")
		}
		if speakers != nil{
			aCoder.encodeObject(speakers, forKey: "speakers")
		}
		if updateTime != nil{
			aCoder.encodeObject(updateTime, forKey: "update_time")
		}
		if updateTimeMili != nil{
			aCoder.encodeObject(updateTimeMili, forKey: "update_time_mili")
		}

	}

}