//
//	HB_Daylist.swift
//
//	Create by Pawan jaiswal on 26/6/2016
//	Copyright © 2016. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_Daylist : NSObject, NSCoding{

	var agendaList : [HB_AgendaList]!
	var date : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		agendaList = [HB_AgendaList]()
		if let agendaListArray = dictionary["agendaList"] as? [NSDictionary]{
			for dic in agendaListArray{
				let value = HB_AgendaList(fromDictionary: dic)
				agendaList.append(value)
			}
		}
		date = dictionary["date"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if agendaList != nil{
			var dictionaryElements = [NSDictionary]()
			for agendaListElement in agendaList {
				dictionaryElements.append(agendaListElement.toDictionary())
			}
			dictionary["agendaList"] = dictionaryElements
		}
		if date != nil{
			dictionary["date"] = date
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         agendaList = aDecoder.decodeObjectForKey("agendaList") as? [HB_AgendaList]
         date = aDecoder.decodeObjectForKey("date") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if agendaList != nil{
			aCoder.encodeObject(agendaList, forKey: "agendaList")
		}
		if date != nil{
			aCoder.encodeObject(date, forKey: "date")
		}

	}

}