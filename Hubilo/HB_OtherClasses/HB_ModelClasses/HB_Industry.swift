//
//	HB_Industry.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class HB_Industry : NSObject, NSCoding{

	var industryId : String!
	var industryName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		industryId = dictionary["industry_id"] as? String
		industryName = dictionary["industry_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if industryId != nil{
			dictionary["industry_id"] = industryId
		}
		if industryName != nil{
			dictionary["industry_name"] = industryName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         industryId = aDecoder.decodeObjectForKey("industry_id") as? String
         industryName = aDecoder.decodeObjectForKey("industry_name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if industryId != nil{
			aCoder.encodeObject(industryId, forKey: "industry_id")
		}
		if industryName != nil{
			aCoder.encodeObject(industryName, forKey: "industry_name")
		}

	}

}