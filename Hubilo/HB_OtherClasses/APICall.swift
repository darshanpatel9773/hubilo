//
//  APICall.swift
//  Hubilo
//
//  Created by Aadil on 7/8/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class APICall: NSObject {
    class func requestGETURLParam(strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:(JSON) -> Void, failure:(NSError) -> Void){
        Alamofire.request(.GET, strURL, parameters: params, encoding: ParameterEncoding.JSON, headers: headers).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class func requestGETURL(strURL: String, success:(JSON) -> Void, failure:(NSError) -> Void) {
        Alamofire.request(.GET, strURL).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class func requestPOSTURL(strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:(JSON) -> Void, failure:(NSError) -> Void){
        Alamofire.request(.POST, strURL, parameters: params, encoding: ParameterEncoding.URL, headers: headers).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error!
                failure(error)
            }
        }
    }
}