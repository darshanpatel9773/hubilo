//
//  AppDelegate.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 10/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import Foundation
import SystemConfiguration
import ReachabilitySwift
import Kingfisher
import LinkedinSwift
import KYDrawerController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var reachability: Reachability?
    var defaultColor:UIColor! 
    var window: UIWindow?
    var eventInfoDic:NSMutableDictionary?
    var loginUserDetail:NSMutableDictionary?
    var isLoginedInUser:Bool = false
    var sharedManager : Globals = Globals.sharedInstance
    var deviceTokenStr : String = ""
    var selectedIndexEdit:Int = 0
    var selectedIndexMain:Int = 0
    var imageViewUser:UIImageView = UIImageView()
    var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])
         IQKeyboardManager.sharedManager().enable = true
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        
//        if (NSUserDefaults.standardUserDefaults().objectForKey("profile_pic") != nil) {
//            let iStrImage:String = NSUserDefaults.standardUserDefaults().objectForKey("profile_pic") as! String
//            let imageData = NSData(base64EncodedString: iStrImage, options: NSDataBase64DecodingOptions(rawValue: 0))
//            let decodedimage = UIImage(data: imageData!)
//            self.imageViewUser.image = decodedimage
//        }

//        if NSUserDefaults.standardUserDefaults().objectForKey("isFirstTime") == nil {
//            NSUserDefaults.standardUserDefaults().setObject("1", forKey: "isFirstTime")
//            let storyBoard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let iViewHomeController:HB_HomeViewController = storyBoard.instantiateViewControllerWithIdentifier("HB_HomeViewController") as! HB_HomeViewController
//            let navController:UINavigationController = UINavigationController.init(rootViewController: iViewHomeController)
//            self.window?.rootViewController = navController
//        }else {
//            let iNavController:UINavigationController = UINavigationController()
//            let elDrawer:KYDrawerController = iNavController.parentViewController as! KYDrawerController
//            let storyBoard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let iViewEventController:HB_EventInfoViewController = storyBoard.instantiateViewControllerWithIdentifier("HB_EventInfoViewController") as! HB_EventInfoViewController
//            let iMenuController:HB_MenuVC = storyBoard.instantiateViewControllerWithIdentifier("HB_MenuVC") as! HB_MenuVC
//            let navController = UINavigationController.init(rootViewController: iViewEventController)
//            elDrawer.mainViewController = navController
//            elDrawer.delegate = iMenuController
//            
////            let drawerController     = KYDrawerController()
////            drawerController.mainViewController = UINavigationController(
////                rootViewController: iViewEventController
////            )
////            drawerController.drawerViewController = iMenuController
////            window = UIWindow(frame: UIScreen.mainScreen().bounds)
////            window?.rootViewController = drawerController
//        }
        
        if (NSUserDefaults.standardUserDefaults().objectForKey("application_color") != nil) {
            defaultColor = NSUserDefaults.standardUserDefaults().colorForKey("application_color")!
        }else {
            NSUserDefaults.standardUserDefaults().setColor(UIColor.grayColor(), forKey: "application_color")
            NSUserDefaults.standardUserDefaults().synchronize()
        }

        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]

        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(AppDelegate.reachabilityChanged(_:)),
                                                         name: ReachabilityChangedNotification,
                                                         object: reachability)
       
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.colorApplication), name:"SetNavigationColor", object: nil)
        
        
        
        do {
            try reachability = Reachability(hostname:"www.google.com")
            try reachability?.startNotifier();
        }
        catch _{
            
        }

        let userDefaults = NSUserDefaults.standardUserDefaults()
        if (userDefaults.objectForKey("login") != nil && userDefaults.objectForKey("login") as! Bool != false) {
            sharedManager.currentUserID=userDefaults.objectForKey("userId") as! String
            sharedManager.currentUser=HB_ModelUser(fromDictionary: userDefaults.objectForKey("userInfo") as! NSDictionary)
            sharedManager.isUserLoggedIn=true
            if userDefaults.objectForKey("userCommunityExist") != nil {
                sharedManager.userCommunityExist = userDefaults.objectForKey("userCommunityExist") as! Int
            }
        }
        
        initializeNotificationServices()
        
//        let iLinkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81srtfywqlf57o", clientSecret: "HxKyVVilmgqrFku5", state: "DLKDJF45DIHUBILO", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://com.startup.linkedin.oauth/oauth"))
        
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
    }
    
    func colorApplication() {
        
        UINavigationBar.appearance().barTintColor = defaultColor
    }
    
    func initializeNotificationServices() -> Void {
        let settings = UIUserNotificationSettings(forTypes: [.Sound, .Alert, .Badge], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        
        // This is an asynchronous method to retrieve a Device Token
        // Callbacks are in AppDelegate.swift
        // Success = didRegisterForRemoteNotificationsWithDeviceToken
        // Fail = didFailToRegisterForRemoteNotificationsWithError
        
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        deviceTokenStr = convertDeviceTokenToString(deviceToken)
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Device token for push notifications: FAIL -- ")
        print(error.description)
        
    }
    
    func convertDeviceTokenToString(deviceToken:NSData) -> String {
        //  Convert binary Device Token to a String (and remove the <,> and white space charaters).
        var deviceTokenStr = deviceToken.description.stringByReplacingOccurrencesOfString(">", withString: "")
        deviceTokenStr = deviceTokenStr.stringByReplacingOccurrencesOfString("<", withString: "")
        deviceTokenStr = deviceTokenStr.stringByReplacingOccurrencesOfString(" ", withString: "")
        
        // Our API returns token in all uppercase, regardless how it was originally sent.
        // To make the two consistent, I am uppercasing the token string here.
        deviceTokenStr = deviceTokenStr.uppercaseString
        return deviceTokenStr
    }
    func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable() {
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            //            print("Not reachable")
            //            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            //            dispatch_async(dispatch_get_global_queue(priority, 0)) {
            //                // do some task
            //                dispatch_async(dispatch_get_main_queue()) {
                               //SPGUtility.sharedInstance().showAlert("Check your internet connection", titleMessage: "",viewController: nil)
            //                }
            //            }
        }
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        if LinkedinSwiftHelper.shouldHandleUrl(url) {
            return LinkedinSwiftHelper.application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    
    
}


public class ReachabilityCheck {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}

extension UIImageView {
    public func maskCircle(radius:CGFloat) {
        self.contentMode = UIViewContentMode.ScaleAspectFill
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = false
        self.clipsToBounds = true
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(CGImage: image!.CGImage!)
    }
}

extension String {

    
    func widthWithConstrainedHeight(height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.max, height: height)
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.width
    }
    
    
    
    func daySuffix(date: NSDate) -> String {
        let calendar = NSCalendar.currentCalendar()
        let dayOfMonth = calendar.component(.Day, fromDate: date)
        switch dayOfMonth {
        case 1, 21, 31: return "st"
        case 2, 22: return "nd"
        case 3, 23: return "rd"
        default: return "th"
        }
    }
}

extension NSDate {
    
    struct Date {
        static let timeFormatter: NSDateFormatter = {
            let df = NSDateFormatter()
            df.calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
            df.dateFormat = "h:mm a"
            return df
        }()
    }
    var time: String { return Date.timeFormatter.stringFromDate(self) }
    
    var year:    Int { return NSCalendar.autoupdatingCurrentCalendar().component(.Year,   fromDate: self) }
    var month:   Int { return NSCalendar.autoupdatingCurrentCalendar().component(.Month,  fromDate: self) }
    var day:     Int { return NSCalendar.autoupdatingCurrentCalendar().component(.Day,    fromDate: self) }
    
    struct DateComponents {
        static let formatter: NSDateComponentsFormatter = {
            let dcFormatter = NSDateComponentsFormatter()
            dcFormatter.calendar = NSCalendar(identifier: NSCalendarIdentifierISO8601)!
            dcFormatter.unitsStyle = .Full
            dcFormatter.maximumUnitCount = 1
            dcFormatter.zeroFormattingBehavior = .Default
            dcFormatter.allowsFractionalUnits = false
            dcFormatter.allowedUnits = [.Year, .Month, .Weekday, .Day, .Hour, .Minute, .Second]
            return dcFormatter
        }()
    }
    
    var elapsedTime: String {
        if timeIntervalSinceNow > -60.0  { return "Just Now" }
        if isDateInToday                 { return "Today at \(time)" }
        if isDateInYesterday             { return "Yesterday at \(time)" }
        return (DateComponents.formatter.stringFromTimeInterval(NSDate().timeIntervalSinceDate(self)) ?? "") + " ago"
    }
    var isDateInToday: Bool {
        return NSCalendar.autoupdatingCurrentCalendar().isDateInToday(self)
    }
    var isDateInYesterday: Bool {
        return NSCalendar.autoupdatingCurrentCalendar().isDateInYesterday(self)
    }
}

extension NSUserDefaults {
    
    func colorForKey(key: String) -> UIColor? {
        var color: UIColor?
        if let colorData = dataForKey(key) {
            color = NSKeyedUnarchiver.unarchiveObjectWithData(colorData) as? UIColor
        }
        return color
    }
    
    func setColor(color: UIColor?, forKey key: String) {
        var colorData: NSData?
        if let color = color {
            colorData = NSKeyedArchiver.archivedDataWithRootObject(color)
        }
        setObject(colorData, forKey: key)
    }
    
}

