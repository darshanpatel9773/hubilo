//
//  HB_ExhibitorsContactCell.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 17/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_ExhibitorsContactCell: UITableViewCell {

    @IBOutlet weak var lblOrganazation: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblAttendeeName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnProfilePic: UIButton!
    @IBOutlet var imgPic : UIImageView!
    @IBOutlet var btnDribble : UIButton!
    @IBOutlet var btnFB : UIButton!
    @IBOutlet var btnTwitter : UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCall: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
