//
//  HB_ExhibitorsInfoVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 17/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_ExhibitorsInfoVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate {
    var modelExhibitionList : HB_ExhibitorList!
    var sharedManager : Globals = Globals.sharedInstance
    var isLoadMore: Bool = false;
    var webHeight : Int = 0
    var originalHeight : Int = 0
    @IBOutlet weak var tblExhibitionInfo: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenu()
        // Do any additional setup after loading the view.
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if(modelExhibitionList.exhibitorBrochure == nil){
            return 2
        }else{
            return 3
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell1:UITableViewCell?=UITableViewCell()
        if indexPath.row == 0
        {
            let cell:HB_ExhibitorsContactCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsContactCell", forIndexPath: indexPath) as! HB_ExhibitorsContactCell
            cell.lblAttendeeName?.text=modelExhibitionList.exhibitorName
            cell.lblDesignation?.text=modelExhibitionList.exhibitorEmailid
            cell.lblCall?.text=modelExhibitionList.exhibitorPhone
            
            let myString: String = modelExhibitionList.exhibitorName
//            var myStringArr = myString.componentsSeparatedByString(" ")
            var newString: String = "Google "
            newString = newString.stringByAppendingString(myString)
            print(newString)
            var length: Int
            var finalLen:Int
            length = newString.characters.count
            finalLen = length - 6
            var myMutableString = NSMutableAttributedString()
            let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let firstAttributes = [NSFontAttributeName:UIFont.systemFontOfSize(13)]
            myMutableString = NSMutableAttributedString(string: newString as
                String, attributes: firstAttributes)
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: iAppDelegate.defaultColor, range: NSRange(location:6,length:finalLen))
        
            cell.lblOrganazation?.attributedText = myMutableString
            
            cell.lblAddress?.text = modelExhibitionList.exhibitorWebsite
            
            if modelExhibitionList.exhibitorWebsite.characters.count != 0
            {
                cell.btnDribble?.setImage(UIImage(named:"website_icon_active"), forState: .Normal)
            }
            
            if modelExhibitionList.exhibitorFb.characters.count != 0
            {
                cell.btnFB?.setImage(UIImage(named:"facebook_icon_active"), forState: .Normal)
            }
            
            if modelExhibitionList.exhibitorTwitter.characters.count != 0
            {
                cell.btnTwitter?.setImage(UIImage(named:"twitter_icon_active"), forState: .Normal)
            }
            
            
            
//            cell.imgPic.layer.borderWidth = 1
//            cell.imgPic.layer.borderColor = UIColor.lightGrayColor().CGColor
//            cell.imgPic.layer.cornerRadius = 45
//            cell.imgPic.clipsToBounds=true
            cell.imgPic.sd_setImageWithURL(NSURL(string:  Constants.ImagePaths.kExhibitorImagePath + modelExhibitionList.exhibitorLogoImg))
            cell.selectionStyle = .None
            
            // Configure the cell...
            //cell.textLabel?.text = "Row \(indexPath.row)"
            return cell
        }
        if indexPath.row == 1
        {
            let cell:SponsorDetailCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsDetailsCell", forIndexPath: indexPath) as! SponsorDetailCell
            cell.textViewDetail?.loadHTMLString(modelExhibitionList.exhibitorDescription as String, baseURL: nil)
            //
            //                        cell.btnMore!.addTarget(self, action: #selector(HB_AttendeeInfoVC.more_btn_click(_:)), forControlEvents: .TouchUpInside)
            
            
            cell.btnMore?.addTarget(self, action: #selector(loadMore), forControlEvents: UIControlEvents.TouchUpInside)
            
            cell.textViewDetail?.delegate=self
            cell.textViewDetail?.scrollView.scrollEnabled=false
            if modelExhibitionList.exhibitorDescription == "" {
                isLoadMore=true
                cell.btnMore?.hidden=true
            }
            else {
                cell.btnMore?.hidden=false
            }
            cell.selectionStyle = .None
            return cell
            
        }
        if indexPath.row == 2
        {
            let cell:HB_ExhibitorsBrochureCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsBrochureCell", forIndexPath: indexPath) as! HB_ExhibitorsBrochureCell
            cell.selectionStyle = .None
            return cell
            
        }
        return cell1!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 230
            
        }
//        if indexPath.row == 1
//        {
//            if isLoadMore{
//                return CGFloat(webHeight) + 30
//            }
//            return 0
//        }
        if indexPath.row == 2
        {
            return 60
        }
        return 0
        
        
    }

    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_ExhibitorsInfoVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="EXHIBITOR INFO"
        
        
    }
    
    
    @IBAction func onTapGoogleBtn(sender: AnyObject) {
    
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.google.co.in/search?site=webhp&source=hp&q=" + modelExhibitionList.exhibitorName)!)
        
    }
    
    
    @IBAction func back_btn_click(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
    self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction func onTapDownloadBrochure(sender: AnyObject) {
        
        if let brochure = modelExhibitionList.exhibitorBrochure
        {
            print(brochure)
            if let url = NSURL(string: "\(Constants.ImagePaths.kExhibitorBrochure)\(brochure)"){
                UIApplication.sharedApplication().openURL(url)
            }

        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Brochure not available.", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
//        if modelExhibitionList.exhibitorBrochure.characters.count != 0 {
//            
//        }
        
    }
    
    
    
    
    
    @IBAction func onTapWebsite(sender: AnyObject) {
        if modelExhibitionList.exhibitorWebsite.characters.count != 0 {
            UIApplication.sharedApplication().openURL(NSURL(string:modelExhibitionList.exhibitorWebsite)!)
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Link not found.", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func onTapFacebook(sender: AnyObject) {
        if modelExhibitionList.exhibitorFb.characters.count != 0 {
            UIApplication.sharedApplication().openURL(NSURL(string:modelExhibitionList.exhibitorFb)!)
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Link not found.", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func onTapTwitter(sender: AnyObject) {
        if modelExhibitionList.exhibitorTwitter.characters.count != 0 {
            UIApplication.sharedApplication().openURL(NSURL(string:modelExhibitionList.exhibitorTwitter)!)
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Link not found.", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadMore() {
        isLoadMore = !isLoadMore
        if isLoadMore == true
        {
            webHeight = originalHeight
            
        }
        else {
            webHeight=97
        }
        
        self.tblExhibitionInfo!.reloadData()
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        let webViewHeight:NSString = webView.stringByEvaluatingJavaScriptFromString("document.body.scrollHeight;")!
        
        self.originalHeight = webViewHeight.integerValue
    }
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        self.originalHeight=97
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
