//
//  CreateDiscussionVC.swift
//  Hubilo
//
//  Created by Aadil on 8/10/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
import KYDrawerController

class CreateDiscussionVC: UIViewController {
var sharedManager : Globals = Globals.sharedInstance
    @IBOutlet var txtTitle : UITextField?
    @IBOutlet var txtDescription : UITextField?
    @IBOutlet var btnCreate : UIButton?
    var dfdIDs : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMenu()
        // Do any additional setup after loading the view.
    }
    func setMenu() {
    let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
    myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
    myMenubtn.frame = CGRectMake(0, 0, 24, 24)
    myMenubtn.addTarget(self, action: #selector(CreateDiscussionVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    
    let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
    self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
    
    self.navigationItem.title = "Create Discussion"
        
    let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//    self.btnCreate?.backgroundColor = iAppDelegate.defaultColor
        
    
    let myCancelbtn:UIButton = UIButton(type: UIButtonType.Custom)
    myCancelbtn.setImage(UIImage(named: "Cancel"), forState: UIControlState.Normal)
    myCancelbtn.frame = CGRectMake(0, 0, 24, 24)
    myCancelbtn.addTarget(self, action: #selector(CreateDiscussionVC.cancelButtonTouched(_:)), forControlEvents:UIControlEvents.TouchUpInside)
    
    let myCustomCancelButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myCancelbtn)
    self.navigationItem.rightBarButtonItem = myCustomCancelButtonItem
    
    }
    
    func back_btn_click(sender:UIButton) {
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
    }
    
    func cancelButtonTouched(sender:UIButton) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func actionCreate(sender: UIButton) {
        if !sharedManager.isUserLoggedIn {
            SPGUtility.sharedInstance().showAlert("You need login first", titleMessage: "", viewController: self)
            return
        }
        
        if sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("Please join the community", titleMessage: "", viewController: self)
            return
        }
        
        if self.txtTitle?.text == ""
        {
            SPGUtility.sharedInstance().showAlert("Please enter valid Title", titleMessage: "", viewController: self)
            return
        }
        if self.txtDescription?.text == ""
        {
            SPGUtility.sharedInstance().showAlert("Please enter valid Description", titleMessage: "", viewController: self)
            return
        }
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                //var userFetch = "3113"
                let userFetch = sharedManager.currentUserID
                let url = (Constants.URL.DISCUSSION_POST_CCREATE)
                
                
                let str = String(UTF8String: (self.txtTitle?.text)!)?.base64Encoded()
                let str1 = String(UTF8String: (self.txtDescription?.text)!)?.base64Encoded()
                APICall.requestPOSTURL(url, params: ["event_id" : Constants.eventInfo.kEventId,"user_id" : userFetch, "title" : str1!,"description":str!], headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    if JSON["status"] == "success"
                    {
                        self.txtTitle?.text = ""
                        self.txtDescription?.text = ""
                        self.navigationController!.popViewControllerAnimated(true)

                       // SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }else {
                       // SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
