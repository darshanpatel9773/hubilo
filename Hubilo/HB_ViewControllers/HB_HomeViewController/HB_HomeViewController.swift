//
//  HB_HomeViewController.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 11/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
class HB_HomeViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var scrollVWIntroduction: UIScrollView!
    var arrMutableIntroduction:NSArray?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrMutableIntroduction=["Help 1","Help 2","Help 3","Help 4"]
        setIntroduction()
self.navigationController?.navigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true

    }
    // MARK: ScrollView Delegate
    func scrollViewDidScroll(scrollView: UIScrollView)
    {
    
        let page:Int = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageController.currentPage=page
        
        
    }
    // MARK: User Define
    func setIntroduction() {
        
        for iCounter in 0..<arrMutableIntroduction!.count
        {
            let iVWIntro:UIView=UIView(frame: CGRect(x: CGFloat(iCounter)*UIScreen.mainScreen().bounds.size.width, y: 0, width:UIScreen.mainScreen().bounds.size.width, height:UIScreen.mainScreen().bounds.size.height))
            let iImgVWIntro:UIImageView=UIImageView(frame: CGRect(x:0, y: 0, width:UIScreen.mainScreen().bounds.size.width, height:UIScreen.mainScreen().bounds.size.height))
            iImgVWIntro.image=UIImage(named: (arrMutableIntroduction?.objectAtIndex(iCounter))as! String)
            iImgVWIntro.contentMode=UIViewContentMode.ScaleToFill;
            //iImgVWIntro.backgroundColor=UIColor.redColor()
            iVWIntro.addSubview(iImgVWIntro)
         //   scrollVWIntroduction.backgroundColor=UIColor.blueColor()
            scrollVWIntroduction.addSubview(iVWIntro)
            
        }
        scrollVWIntroduction.contentSize=CGSizeMake(UIScreen.mainScreen().bounds.size.width*4, UIScreen.mainScreen().bounds.size.height)
    }
    //MARK : Button Click Event
    
    @IBAction func skip_btn_click(sender: AnyObject) {
        
        let elDrawer:KYDrawerController=(self.navigationController?.parentViewController as? KYDrawerController)!
        var navController:UINavigationController?
        let objEditPro:HB_SettingVC = self.storyboard?.instantiateViewControllerWithIdentifier("HB_SettingVC") as! HB_SettingVC
        navController=UINavigationController.init(rootViewController: objEditPro)
        elDrawer.mainViewController=navController;
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Closed, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
