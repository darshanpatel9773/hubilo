//
//  Discussion.swift
//  Hubilo
//
//  Created by Aadil on 8/1/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import ObjectMapper
class DiscussionList : NSObject, Mappable {
    var status = ""
    var msg = ""
    var discussionForumList : [Discussion]!
    override init() {
        
    }
    required init?(_ map: Map) {
        
    }
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        discussionForumList <- map["discussionForumList"]
    }
}
class  Discussion : NSObject, Mappable {
    var user_id = ""
    var firstname = ""
    var lastname = ""
    var profile_img = ""
    var organization = ""
    var designation = ""
    var aboutme = ""
    var linkedin_id = ""
    var linkedin_public_url = ""
    var fb_id = ""
    var twitter_public_url = ""
    var id = ""
    var title = ""
    var fdescription = ""
    var create_time_mili = ""
    var vote_count = ""
    var view_count = ""
    var comment_count = ""
    var vote = ""
    var is_bookmark = 0
    override init() {
        
    }
    required init?(_ map: Map) {
        
    }
    func mapping(map: Map) {
        user_id <- map["user_id"]
        firstname <- map["firstname"]
        lastname <- map["lastname"]
        profile_img <- map["profile_img"]
        organization <- map["organization"]
        fdescription <- map["description"]
        designation <- map["designation"]
        aboutme <- map["aboutme"]
        linkedin_id <- map["linkedin_id"]
        linkedin_public_url <- map["linkedin_public_url"]
        fb_id <- map["fb_id"]
        twitter_public_url <- map["twitter_public_url"]
        id <- map["id"]
        title <- map["title"]
        create_time_mili <- map["create_time_mili"]
        vote_count <- map["vote_count"]
        view_count <- map["view_count"]
        comment_count <- map["comment_count"]
        vote <- map["vote"]
        is_bookmark <- map["is_bookmark"]
    }
}