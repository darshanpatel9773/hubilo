//
//  HB_LoginVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 12/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftValidators
import SwiftLoader
class HB_LoginVC: UIViewController {
    
    @IBOutlet weak var btn_Signin: UIButton!
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    @IBOutlet var txtEmail : UITextField?
    @IBOutlet var txtPassword : UITextField?
    var sharedManager :  Globals = Globals.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        btn_Signin.backgroundColor = appDelegate.defaultColor
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HB_LoginVC.getProfileInfo(_:)), name:"GetLinkedInProfile", object: nil)
        if (appDelegate.loginUserDetail != nil) {
            self.dismissViewControllerAnimated(false, completion: nil)
        }
        
        //txtEmail?.text="bhavik@nectarbits.com"
        //txtPassword?.text="bhavik12"
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        txtEmail?.autocorrectionType=UITextAutocorrectionType.No
        txtPassword?.autocorrectionType=UITextAutocorrectionType.No
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Button Click Events
    
    @IBAction func  actionLinkedinLogin (sender: AnyObject){
        SPGUtility.sharedInstance().showAlert("Coming Soon", titleMessage: "Login", viewController: self)
        
    }
    
    @IBAction func  actionGoogleLogin (sender: AnyObject){
        SPGUtility.sharedInstance().showAlert("Coming Soon", titleMessage: "Login", viewController: self)
    }
    
    @IBAction func actionLogin(sender : AnyObject) {
        var isValid = true
        if !Validator.isEmail((txtEmail?.text)!) || Validator.isEmpty((txtEmail?.text)!){
            isValid = false
            SPGUtility.sharedInstance().showAlert("Please enter valid Email", titleMessage: "Error", viewController: self)
        }
        if  Validator.isEmpty((txtPassword?.text)!){
            isValid = false
            SPGUtility.sharedInstance().showAlert("Please enter valid Password", titleMessage: "Error", viewController: self)
        }
        
        if isValid{
            if ReachabilityCheck.isConnectedToNetwork()
            {
                SwiftLoader.show(animated: true)
                
                do {
                    let app : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    app.deviceTokenStr = "a4b3db361ffe6d06ae6f623389d3e41791e974a7"
                    let jsonData = try NSJSONSerialization.dataWithJSONObject(["emailid" : (self.txtEmail?.text)!,"password":(self.txtPassword?.text)!,"device":"ios","deviceId":"a4b3db361ffe6d06ae6f623389d3e41791e974a7","udId":"111"], options: NSJSONWritingOptions.init(rawValue: 0))
                   let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                    let url = (Constants.URL.USER_LOGIN + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                    APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                        SwiftLoader.hide()
                        
                        
                        
                        if JSON["status"] == "Success"
                        {
                            let user : HB_ModelUser = HB_ModelUser(fromDictionary: JSON.rawValue as! NSDictionary)
                            self.sharedManager.currentUserID = JSON["user_id"].rawString()!
                            //self.sharedManager.userCommunityExist = JSON["userCommunityExist"].int!
                            self.sharedManager.currentUser=user
                            self.sharedManager.isUserLoggedIn=true
                            
                            self.dismissViewControllerAnimated(true, completion: nil)
                            
                            let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
                            userDefaults.setBool(true, forKey: "login")
                            userDefaults.setObject(JSON.rawValue as! NSDictionary, forKey: "userInfo")
                            userDefaults.setObject(self.sharedManager.currentUserID, forKey: "userId")
                            //userDefaults.setObject(self.sharedManager.userCommunityExist, forKey: "userCommunityExist")
                            userDefaults.setObject(JSON["userKey"].rawString()!, forKey: "user_key")
                        }else {
                            SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                        }
                        }, failure: { (NSError) in
                            SwiftLoader.hide()
                            SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                    })
                    // here "jsonData" is the dictionary encoded in JSON data
                } catch let error as NSError {
                   SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
                }
                
                
                
            }
            else
            {
                
                SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
            }
        }
        
    }
    @IBAction func forgot_btn_click(sender: AnyObject) {
        
        self.performSegueWithIdentifier("ShowForgotVC", sender: self)
    }
    @IBAction func signup_btn_click(sender: AnyObject) {
        
        self.performSegueWithIdentifier("ShowSignupVC", sender: self)
    }
    @IBAction func btnFBLoginPressed(sender: AnyObject) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logInWithReadPermissions(["email"],
                                                fromViewController:self,
                                                handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
                                                    if (error == nil){
                                                        let fbloginresult : FBSDKLoginManagerLoginResult = result
                                                        if fbloginresult.isCancelled {
                                                            SPGUtility.sharedInstance().showAlert("Facebook login cancelled", titleMessage: "", viewController: self)
                                                            
                                                            return
                                                        }
                                                        
                                                        
                                                        if(fbloginresult.grantedPermissions.contains("email"))
                                                        {
                                                            self.getFBUserData()
                                                            fbLoginManager.logOut()
                                                        }
                                                    }
                                                    
        })
        
        //        fbLoginManager .logInWithReadPermissions(["email"], handler: { (result, error) -> Void in
        //
        //        })
    }
    
   
    
    func getFBUserData(){
        if((FBSDKAccessToken.currentAccessToken()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    print(result)
                    //let alert:UIAlertView=UIAlertView(title: "", message: "Login Successfull", delegate: self, cancelButtonTitle: "ok")
                    let lastname : String = result["last_name"] as! String
                    let email : String = result["email"] as! String
                    let fbid : String = result["id"] as! String
                    let firstName : String = result["first_name"] as! String
                    let profile : String = result["picture"]!!["data"]!!["url"] as! String
                   // alert.show()
                    
                    let app : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    
                    if app.deviceTokenStr == ""
                    {
                        app.deviceTokenStr="1234567890aaaa"
                    }
                    
                    let param : NSDictionary =                  ["state":"","designation":"","lastname":lastname,"lng":"","city":"","emailid":email,"organization":"","gender":"","industry":"","lat":"","deviceId":app.deviceTokenStr,"aboutme":"","fb_id":fbid,"website":"","firstname":firstName,"country":"","linkedin_id":"","address":"","device":"ios","login_with":"fb","password":"abc123","udId":"kbjj","profile_img":profile,"event":Constants.eventInfo.kEventId]
                    
                    if ReachabilityCheck.isConnectedToNetwork()
                    {
                        SwiftLoader.show(animated: true)
                        
                        do {
                            let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                            let url = (Constants.URL.SOCIAL_LOGIN + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                            APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                                SwiftLoader.hide()
                                
                               if JSON["status"] == "Success"
                                {
                                    let user : HB_ModelUser = HB_ModelUser(fromDictionary: JSON.rawValue as! NSDictionary)
                                    print(user)
                                    self.sharedManager.currentUser=user
                                    self.sharedManager.currentUserID = JSON["user"].rawString()!
                                    self.sharedManager.fbPic=profile
                                    self.sharedManager.isUserLoggedIn=true
                                    self.sharedManager.userCommunityExist = JSON["userCommunityExist"].int!
                                    self.dismissViewControllerAnimated(true, completion: nil)
                                    let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
                                    userDefaults.setBool(true, forKey: "login")
                                    userDefaults.setObject(JSON.rawValue as! NSDictionary, forKey: "userInfo")
                                    userDefaults.setObject(self.sharedManager.currentUserID, forKey: "userId")
                                     userDefaults.setObject(self.sharedManager.userCommunityExist, forKey: "userCommunityExist")
                                    userDefaults.setObject(JSON["userKey"].rawString()!, forKey: "user_key")
                                    
                                    
                                    
                                }else {
                                    SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                                }
                                }, failure: { (NSError) in
                                    SwiftLoader.hide()
                                    SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                            })
                            // here "jsonData" is the dictionary encoded in JSON data
                        } catch let error as NSError {
                            SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
                        }
                        
                        
                        
                    }
                    else
                    {
                        
                        SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
                    }
                    
                    
                    
                    //self.dismissViewControllerAnimated(true, completion: nil)
                }
            })
        }
    }
       
    
     func getProfileInfo(sender: AnyObject) {
        if let accessToken = NSUserDefaults.standardUserDefaults().objectForKey("LIAccessToken") {
            // Specify the URL string that we'll get the profile info from.
            let targetURLString = "https://api.linkedin.com/v1/people/~:(id,first-name,email-address,last-name,headline,picture-url,industry,summary,specialties,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)),educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes),associations,interests,num-recommenders,date-of-birth,publications:(id,title,publisher:(name),authors:(id,name),date,url,summary),patents:(id,title,summary,number,status:(id,name),office:(name),inventors:(id,name),date,url),languages:(id,language:(name),proficiency:(level,name)),skills:(id,skill:(name)),certifications:(id,name,authority:(name),number,start-date,end-date),courses:(id,name,number),recommendations-received:(id,recommendation-type,recommendation-text,recommender),honors-awards,three-current-positions,three-past-positions,volunteer)?format=json"
            
            
            // Initialize a mutable URL request object.
            let request = NSMutableURLRequest(URL: NSURL(string: targetURLString)!)
            
            // Indicate that this is a GET request.
            request.HTTPMethod = "GET"
            
            // Add the access token as an HTTP header field.
            request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            
            
            // Initialize a NSURLSession object.
            let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
            
            // Make the request.
            let task: NSURLSessionDataTask = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
                // Get the HTTP status code of the request.
                let statusCode = (response as! NSHTTPURLResponse).statusCode
                
                if statusCode == 200 {
                    // Convert the received JSON data into a dictionary.
                    do {
                        
                        let dataDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                        
                        
                        let iStrEmailAddress:String = dataDictionary.objectForKey("emailAddress") as! String
                        let iStrFirstName:String = dataDictionary.objectForKey("firstName") as! String
                        let iStrLastName:String = dataDictionary.objectForKey("lastName") as! String
                        let iStrLinkedinId:String = dataDictionary.objectForKey("id") as! String
                        let iStrPictureURL:String = dataDictionary.objectForKey("pictureUrl") as! String
                        let iStrIndustry:String = dataDictionary.objectForKey("industry") as! String
                        
                        
                        
                        let app : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        
                        if app.deviceTokenStr == ""
                        {
                            app.deviceTokenStr="1234567890aaaa"
                        }
                        
                        let param : NSDictionary = ["state":"","designation":"","lastname":iStrLastName,"lng":"","city":"","emailid":iStrEmailAddress,"organization":"","gender":"","industry":iStrIndustry,"lat":"","deviceId":app.deviceTokenStr,"aboutme":"","fb_id":"","website":"","firstname":iStrFirstName,"country":"","linkedin_id":iStrLinkedinId,"address":"","device":"ios","login_with":"linkedin","password":"abc123","udId":"kbjj","profile_img":iStrPictureURL,"event":Constants.eventInfo.kEventId]
                        
                        
                        if ReachabilityCheck.isConnectedToNetwork()
                        {
                            SwiftLoader.show(animated: true)
                            
                            do {
                                let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                                let url = (Constants.URL.SOCIAL_LOGIN + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                                    SwiftLoader.hide()
                                    
                                    if JSON["status"] == "Success"
                                    {
                                        let user : HB_ModelUser = HB_ModelUser(fromDictionary: JSON.rawValue as! NSDictionary)
                                        print(user)
                                        self.sharedManager.currentUser=user
                                        self.sharedManager.currentUserID = JSON["user"].rawString()!
                                        self.sharedManager.fbPic=iStrPictureURL
                                        self.sharedManager.isUserLoggedIn=true
                                        self.sharedManager.userCommunityExist = JSON["userCommunityExist"].int!
                                        self.dismissViewControllerAnimated(true, completion: nil)
                                        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
                                        userDefaults.setBool(true, forKey: "login")
                                        userDefaults.setObject(JSON.rawValue as! NSDictionary, forKey: "userInfo")
                                        userDefaults.setObject(self.sharedManager.currentUserID, forKey: "userId")
                                        userDefaults.setObject(self.sharedManager.userCommunityExist, forKey: "userCommunityExist")
                                        userDefaults.setObject(JSON["userKey"].rawString()!, forKey: "user_key")
                                        
                                        
                                        
                                    }else {
                                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                                    }
                                    }, failure: { (NSError) in
                                        SwiftLoader.hide()
                                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                                })
                                // here "jsonData" is the dictionary encoded in JSON data
                            } catch let error as NSError {
                                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
                            }
                            
                            
                            
                        }
                        else
                        {
                            
                            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
                        }

                        
                       
                    }
                    catch {
                        print("Could not convert JSON data into a dictionary.")
                    }
                }
            }
            
            task.resume()
        }
    }
    @IBAction func close_btn_click(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
