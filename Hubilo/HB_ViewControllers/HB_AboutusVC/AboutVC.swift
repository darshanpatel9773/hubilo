//
//  AboutVC.swift
//  Hubilo
//
//  Created by Aadil on 7/9/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
class AboutVC: UIViewController, UIWebViewDelegate{
    @IBOutlet var webView : UIWebView?
    override func viewDidLoad() {
        self.title="About us"
        super.viewDidLoad()
        webView?.loadHTMLString("Hubilo is an interactive and user-friendly mobile application to mobilize events. It makes networking and engaging at events easier. It empowers the organizers to manage and co-ordinate the event skillfully. Our mobile app operates in real time and aims to be a de-facto for events.<br/><br/>Event organizers can use this platform to customize their event, the description, the location, the schedules and the agenda. They can send event updates and notifications, announcements and schedule changes, to all the attendees. Hubilo enables organizers to popularize their event social media. It also helps monetize the event by getting more sponsorship and sponsors.<br/><br/>Instead of downloading multiple event apps, Hubilo serves as a common platform to several events. It facilitates users to discover and locate new events in the vicinity and updates them with the event happenings. Attendees get to network among each other before, during and after the event and helps them stay connected on social platforms like Facebook, Linkedln and Twitter.<br/><br/>Hubilo Private Limited is incubated at icreate (<a href='http://www.icreate.org.in'>www.icreate.org.in</a>), a Gujarat government initiative to promote entrepreneurship.", baseURL: nil)
        // Do any additional setup after loading the view.
        setMenu()
        
        
        
    }
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_ExhibitorsInfoVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="ABOUT US"
        
        
    }
    @IBAction func back_btn_click(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController=(self.navigationController?.parentViewController as? KYDrawerController)!
        var navController:UINavigationController?
        let objEditPro:HB_SettingVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_SettingVC") as! HB_SettingVC
        navController=UINavigationController.init(rootViewController: objEditPro)
        elDrawer.mainViewController=navController;
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Closed, animated: true)
        
    }

    func webViewDidFinishLoad(webView: UIWebView) {
        
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
