//
//  HB_MutualConnectionCell.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 19/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_MutualConnectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblAttendeeName:UILabel!
    @IBOutlet weak var lblAteendeeProfile:UILabel!
    @IBOutlet weak var imgAttendeeProfile:UIImageView!
    
}
