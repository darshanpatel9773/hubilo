//
//  HB_PeopleYouShouldKnowCell.swift
//  Hubilo
//
//  Created by Tushar Patel on 06/08/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_PeopleYouShouldKnowCell: UICollectionViewCell {
    
    @IBOutlet weak var lblAttendeeName:UILabel!
    @IBOutlet weak var lblAteendeeProfile:UILabel!
    @IBOutlet weak var imgAttendeeProfile:UIImageView!
    
}
