//
//  AttenddeeInfoCell.swift
//  Hubilo
//
//  Created by Aadil on 7/10/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class AttenddeeInfoCell: UITableViewCell {
    @IBOutlet var imgUser : UIImageView?
    @IBOutlet var lblName : UILabel?
    @IBOutlet var lblDeveloper : UILabel?
    @IBOutlet var lblPostion : UILabel?
    @IBOutlet var btnMessage : UIButton?
    @IBOutlet var btnBookmark : UIButton?
    @IBOutlet var btnFacebook : UIButton?
    @IBOutlet var btnLinekdIn : UIButton?
    @IBOutlet var btnTwitter : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
