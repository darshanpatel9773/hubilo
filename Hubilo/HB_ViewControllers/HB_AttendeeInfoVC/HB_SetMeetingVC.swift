//
//  HB_SetMeetingVC.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 28/09/16.
//  Copyright © 2016 Priyank Gandhi. All rights reserved.
//

import UIKit
import FSCalendar

class HB_SetMeetingVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var vWCalender:FSCalendar!
    var mutDictDateSlot:NSMutableDictionary = NSMutableDictionary()
    var strSelectedDate:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Diffrenace between two dates 
        let iStrStartDate:String = "2016-10-21"
        let iStrEndDate:String = "2016-10-22"
        let iStrStartTime:String = "10:00"
        let iStrEODTime:String = "22:00"
        let iStrEndTime:String = "18:00"
        strSelectedDate = iStrStartDate
        
        let iDateFormatter:NSDateFormatter = NSDateFormatter()
        iDateFormatter.dateFormat = "yyyy-MM-dd"
        iDateFormatter.timeZone = NSTimeZone.systemTimeZone()
        
        let iStartDate:NSDate = iDateFormatter.dateFromString(iStrStartDate)!
        let iEndDate:NSDate = iDateFormatter.dateFromString(iStrEndDate)!
        
        let iDiffDays = iEndDate.daysFrom(iStartDate)
        
        iDateFormatter.dateFormat = "HH:mm"
        let iStartTime:NSDate = iDateFormatter.dateFromString(iStrStartTime)!
        let iEODTime:NSDate = iDateFormatter.dateFromString(iStrEODTime)!
        let iEndTime:NSDate = iDateFormatter.dateFromString(iStrEndTime)!
        
        let iGregorian:NSCalendar = NSCalendar.init(calendarIdentifier:NSCalendarIdentifierGregorian)!
        iDateFormatter.dateFormat = "yyyy-MM-dd"
        var iDate:NSDate = iDateFormatter.dateFromString(iStrStartDate)!
        for iCounter in 0 ..< iDiffDays + 1 {
            
            let iArrTime:NSMutableArray = NSMutableArray()
            
            let units:NSCalendarUnit = [.Year , .Month , .Day , .Hour , .Minute]
            let Components:NSDateComponents = iGregorian.components(units, fromDate: iDate)
            let iArrStartTime:NSArray = iStrStartTime.componentsSeparatedByString(":")
            let iArrStartEODTime:NSArray = iStrEODTime.componentsSeparatedByString(":")
            let iArrEndTime:NSArray = iStrEndTime.componentsSeparatedByString(":")
            
            let iStrHour:NSString = iArrStartTime.objectAtIndex(0) as! NSString
            let iStrMinute:NSString = iArrStartTime.objectAtIndex(1) as! NSString
            
            Components.day = Components.day + iCounter
            
            
            //iDate = iGregorian.dateByAddingComponents(Components, toDate: iDate, options: NSCalendarOptions(rawValue: UInt(0)))!
            print(iDate)
            if iCounter == iDiffDays {
                //Consider End Date time
                
                let iStrEODHour:NSString = iArrEndTime.objectAtIndex(0) as! NSString
                Components.hour = iStrHour.integerValue
                Components.minute = iStrMinute.integerValue
                iDate = iGregorian.dateFromComponents(Components)!
                print(iDate)
                iDateFormatter.dateFormat = "HH:mm"
                
                
                iArrTime.addObject(iDateFormatter.stringFromDate(iDate))
                
                var iTimeHour:Int = iStrHour.integerValue
                while iTimeHour < iStrEODHour.integerValue {
                    
                    let iIncrementalComponents:NSDateComponents = NSDateComponents()
                    iIncrementalComponents.hour = 2
                    let iTimeDate:NSDate = iGregorian.dateByAddingComponents(iIncrementalComponents, toDate: iDate, options: NSCalendarOptions(rawValue: UInt(0)))!
                    iArrTime.addObject(iDateFormatter.stringFromDate(iTimeDate))
                    iDate = iTimeDate
                    iTimeHour = iTimeHour + 2
                }
                
            }else {
                
                
                
                let iStrEODHour:NSString = iArrStartEODTime.objectAtIndex(0) as! NSString
                Components.hour = iStrHour.integerValue
                Components.minute = iStrMinute.integerValue
                iDate = iGregorian.dateFromComponents(Components)!
                print(iDate)
                iDateFormatter.dateFormat = "HH:mm"
                
                
                iArrTime.addObject(iDateFormatter.stringFromDate(iDate))
                
                var iTimeHour:Int = iStrHour.integerValue
                while iTimeHour < iStrEODHour.integerValue {
                    
                    let iIncrementalComponents:NSDateComponents = NSDateComponents()
                    iIncrementalComponents.hour = 2
                    let iTimeDate:NSDate = iGregorian.dateByAddingComponents(iIncrementalComponents, toDate: iDate, options: NSCalendarOptions(rawValue: UInt(0)))!
                    iArrTime.addObject(iDateFormatter.stringFromDate(iTimeDate))
                    iDate = iTimeDate
                    iTimeHour = iTimeHour + 2
                }
                
                
            }
            //print(iArrTime)
            let iArrTimeSlot:NSMutableArray = NSMutableArray()
            for iCounter in 0 ..< iArrTime.count - 1 {
                let iStrTimeSlot:String = "\(iArrTime.objectAtIndex(iCounter))-\(iArrTime.objectAtIndex(iCounter+1))"
                iArrTimeSlot.addObject(iStrTimeSlot)
            }
            iDateFormatter.dateFormat = "yyyy-MM-dd"
            mutDictDateSlot.setObject(iArrTimeSlot, forKey: iDateFormatter.stringFromDate(iDate))
            
        }
        
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return (mutDictDateSlot.objectForKey(strSelectedDate)?.count)!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let iCell:HB_MettingCell = tableView.dequeueReusableCellWithIdentifier("HB_MettingCell", forIndexPath: indexPath) as! HB_MettingCell
        let iArrTime:NSArray = mutDictDateSlot.objectForKey(strSelectedDate) as! NSArray
        let iStrTotalTime:String = iArrTime.objectAtIndex(indexPath.section) as! String
        let iArrTimeSlot:NSArray = iStrTotalTime.componentsSeparatedByString("-")
        let iStrStartTime:String = iArrTimeSlot.objectAtIndex(0) as! String
        let iStrEndTime:String = iArrTimeSlot.objectAtIndex(1) as! String
        
        let iArrStartTime:NSArray = iStrStartTime.componentsSeparatedByString(":")
        let iArrEndTime:NSArray = iStrEndTime.componentsSeparatedByString(":")
        
        let iStrStartHour:NSString = iArrStartTime.objectAtIndex(0) as! NSString
        let iStrStartMinute:NSString = iArrStartTime.objectAtIndex(1) as! NSString
        let iStrEndHour:NSString = iArrEndTime.objectAtIndex(0) as! NSString
        let iStrEndMinute:NSString = iArrEndTime.objectAtIndex(1) as! NSString
        
        let iDateFormatter:NSDateFormatter = NSDateFormatter()
        iDateFormatter.dateFormat = "yyyy-MM-dd"
        var iDate:NSDate = iDateFormatter.dateFromString(strSelectedDate)!
        let iGregorian:NSCalendar = NSCalendar.init(calendarIdentifier:NSCalendarIdentifierGregorian)!
        let units:NSCalendarUnit = [.Year , .Month , .Day , .Hour , .Minute]
        let Components:NSDateComponents = iGregorian.components(units, fromDate:iDate)
        
        
        var iIntStartHour:Int = iStrStartHour.integerValue
        var iIntStartMinute:Int = iStrStartMinute.integerValue
        let iIntEndHour:Int = iStrEndHour.integerValue
        //let iIntEndMinute:Int = iStrEndMinute.integerValue
        
        Components.hour = iIntStartHour
        Components.minute = iIntStartMinute
        iDate = iGregorian.dateFromComponents(Components)!
        
        iDateFormatter.dateFormat = "hh:mm a"
        
        let iArrTimeSlots:NSMutableArray = NSMutableArray()
        iArrTimeSlots.addObject(iDateFormatter.stringFromDate(iDate))
        
        while iIntStartHour < iIntEndHour {
            let iIncrementalComponents:NSDateComponents = NSDateComponents()
            iIncrementalComponents.minute = 20
            let iTimeDate:NSDate = iGregorian.dateByAddingComponents(iIncrementalComponents, toDate: iDate, options: NSCalendarOptions(rawValue: UInt(0)))!
            iArrTimeSlots.addObject(iDateFormatter.stringFromDate(iTimeDate))
            iDate = iTimeDate
            iIntStartMinute = iIntStartMinute + 20
            if iIntStartMinute == 60 {
              iIntStartHour = iIntStartHour + 1
              iIntStartMinute = 0
            }
            
        }

        
        for iCounter in 11 ..< 17 {
            let iBtn:UIButton = iCell.iViewBtn.viewWithTag(iCounter) as! UIButton
                iBtn.setTitle(iArrTimeSlots.objectAtIndex(iCounter-11) as? String, forState: .Normal)
        }
        
        
        return iCell
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let iView:UIView = UIView(frame: CGRect(x:0,y:0,width:tableView.frame.size.width,height:35.0))
        
        let iLblTimeSlot:UILabel = UILabel(frame:CGRect(x:8,y:8,width:iView.frame.size.width,height:iView.frame.size.height))
        let iArrTime:NSArray = mutDictDateSlot.objectForKey(strSelectedDate) as! NSArray
        let iDateFormatter:NSDateFormatter = NSDateFormatter()
        let iArrTimeSlot:NSArray = ((iArrTime.objectAtIndex(section) as? String)?.componentsSeparatedByString("-"))!
        iDateFormatter.dateFormat="HH:mm"
        var iFirst:String = iArrTimeSlot.objectAtIndex(0) as! String
        var iSecond:String = iArrTimeSlot.objectAtIndex(1) as! String
        let iFirstTime:NSDate = iDateFormatter.dateFromString(iFirst)!
        let iSecondTime:NSDate = iDateFormatter.dateFromString(iSecond)!
        iDateFormatter.dateFormat = "hh:mm"
        iFirst = iDateFormatter.stringFromDate(iFirstTime)
        iSecond = iDateFormatter.stringFromDate(iSecondTime)
        iLblTimeSlot.text = iFirst + " to " + iSecond
        iView.addSubview(iLblTimeSlot)
        return iView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
