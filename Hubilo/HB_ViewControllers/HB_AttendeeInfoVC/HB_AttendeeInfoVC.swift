//
//  HB_AttendeeInfoVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 19/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
import ExpandingMenu
import SwiftValidators

class HB_AttendeeInfoVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIWebViewDelegate {

    @IBOutlet weak var collVWMutualConnectionnfo: UICollectionView!
    @IBOutlet weak var collVwPeopleMayKmow:UICollectionView!
    var dicAttendeeDInfo:NSMutableDictionary?
    var iSShowMore:Bool=false
    var dicInterestDic:NSMutableDictionary!
    var sharedManager : Globals = Globals.sharedInstance
    var isLoadMore: Bool = false;
    var webHeight : Int = 0
    var originalHeight : Int = 0
    @IBOutlet var popupView : UIView?
    @IBOutlet var txtMessage : UITextField?
    @IBOutlet weak var tblAttendeeInfo: UITableView!
    var selectedAtendee:NSMutableDictionary = NSMutableDictionary()
    
    @IBOutlet var messagePopupView : UIView?
    @IBOutlet var txt_sendMessage : UITextField?
    
    var mutArrConnectionSuggestion:NSMutableArray!
    var mutArrMutualFriends:NSMutableArray!
    var mutArrInterest:NSMutableArray!
    
    @IBOutlet var lbl_google: UILabel!
    @IBOutlet weak var scrVwAttendee:UIScrollView!
    
    //Attendee Info
    
    @IBOutlet var imgUser : UIImageView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblDeveloper : UILabel!
    @IBOutlet var btnGoogle : UIButton!
    @IBOutlet var btnMessage : UIButton!
    @IBOutlet var btnBookmark : UIButton!
    @IBOutlet var btnLinekdIn : UIButton!
    @IBOutlet var btnFacebook:UIButton!
    @IBOutlet var btnTwitter : UIButton!
    @IBOutlet var lblDescription:UILabel!
    @IBOutlet var btnMore:UIButton!
    var displayFixSize:CGFloat = 75
    var totalSize:CGFloat = 0
    var iStrFbId:String!
    
    //Constraint 
    @IBOutlet var heightAboutMe:NSLayoutConstraint!
    @IBOutlet var heightAboutMeVw:NSLayoutConstraint!
    @IBOutlet var heightAboutDescription:NSLayoutConstraint!
    @IBOutlet var heightButton:NSLayoutConstraint!
    @IBOutlet var topOfInterest:NSLayoutConstraint!
    @IBOutlet var topOfMutualConnection:NSLayoutConstraint!
    @IBOutlet var topOfPeopleMayKnow:NSLayoutConstraint!
    @IBOutlet var heightOfInterest:NSLayoutConstraint!
    @IBOutlet var heightOfPeopleKnow:NSLayoutConstraint!
    @IBOutlet var heightOfMutualConnection:NSLayoutConstraint!
    
    @IBOutlet var vwInterest:UIView!
    @IBOutlet var vWMutualConnection:UIView!
    @IBOutlet var VWPeopleKnow:UIView!
    
    var appDelegate:AppDelegate!
    
    
    
    var isFirstTime : Bool = false;
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        setMenu()
        popupView?.hidden=true
        messagePopupView?.hidden = true
        
        if ((self.dicAttendeeDInfo?.valueForKey("designation") as! String).characters.count > 0  && (self.dicAttendeeDInfo?.valueForKey("organization") as! String).characters.count > 0) {
           self.lblDeveloper!.text = (self.dicAttendeeDInfo?.valueForKey("designation") as! String) + " at " + ((self.dicAttendeeDInfo?.valueForKey("organization"))! as! String)
        }else if self.dicAttendeeDInfo?.valueForKey("designation") != nil{
            self.lblDeveloper!.text = (self.dicAttendeeDInfo?.valueForKey("designation") as! String)

        }else if ((self.dicAttendeeDInfo?.valueForKey("organization")) != nil) {
            self.lblDeveloper!.text = (self.dicAttendeeDInfo?.valueForKey("organization") as! String)
        }
        
        self.btnMessage.backgroundColor = appDelegate.defaultColor
        
        self.lblName!.text = (self.dicAttendeeDInfo?.valueForKey("firstname") as! String) + " "+((self.dicAttendeeDInfo?.valueForKey("lastname"))! as! String)
        
        self.imgUser.layer.cornerRadius = 50
        self.imgUser?.clipsToBounds = true
        
        let myString: String = (self.dicAttendeeDInfo?.valueForKey("firstname") as! String)
        var newString: String = "Google "
        newString = newString.stringByAppendingString(myString)
        print(newString)
        var length: Int
        var finalLen:Int
        length = newString.characters.count
        finalLen = length - 6
        var myMutableString = NSMutableAttributedString()
        let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let firstAttributes = [NSFontAttributeName:UIFont.systemFontOfSize(13)]
        myMutableString = NSMutableAttributedString(string: newString as
            String, attributes: firstAttributes)
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: iAppDelegate.defaultColor, range: NSRange(location:6,length:finalLen))
        lbl_google?.attributedText = myMutableString
        
        
        if self.dicAttendeeDInfo?.valueForKey("profile_img") != nil {
            let url:String = (Constants.ImagePaths.kAttendeeImagePath + ((self.dicAttendeeDInfo?.valueForKey("profile_img"))! as! String))
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(0, 0, 100, 100))
            imgVw.setImageWithString(self.dicAttendeeDInfo?.valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            self.imgUser.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(0, 0, 100, 100))
            imgVw.setImageWithString(self.dicAttendeeDInfo?.valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            self.imgUser.image = imgVw.image
        }
        let iStrAboutMe:String = (self.dicAttendeeDInfo?.valueForKey("aboutme") as? String)!
        let iFloatHeight = iStrAboutMe.heightWithConstrainedWidth(self.view.frame.size.width, font: self.lblDescription.font)
        btnMore.setTitleColor(appDelegate.defaultColor, forState: .Normal)
        
        vwInterest.hidden = true
        VWPeopleKnow.hidden = true
        vWMutualConnection.hidden = true
        
        if iFloatHeight > 21 {
            
            if iFloatHeight < displayFixSize {
                displayFixSize = iFloatHeight
                btnMore.hidden = true
            }
            
            totalSize = iFloatHeight
            heightAboutDescription.constant = heightAboutDescription.constant + displayFixSize
            heightAboutMeVw.constant =  heightAboutMeVw.constant + displayFixSize
            heightAboutMe.constant = displayFixSize + 37
            self.view.layoutIfNeeded()
            self.lblDescription.text = iStrAboutMe
        }else {
            heightAboutMeVw.constant = heightAboutMeVw.constant - 32
            btnMore.hidden = true
            heightAboutDescription.constant =  heightAboutDescription.constant - 32
            self.view.layoutIfNeeded()
        }
        
        
            if (dicAttendeeDInfo?.valueForKey("twitter_public_url") as? String)! != "" {
                    btnTwitter?.addTarget(self, action: #selector(actionTwitter), forControlEvents: UIControlEvents.TouchUpInside)
                    btnTwitter?.setImage(UIImage(named: "twitter_icon_active"), forState: UIControlState.Normal)
            }
            else
            {
                btnTwitter?.addTarget(self, action: #selector(actionTwitter), forControlEvents: UIControlEvents.TouchUpInside)
            }
        
            if (dicAttendeeDInfo?.valueForKey("linkedin_public_url") as? String)! != "" {
                    btnLinekdIn?.addTarget(self, action: #selector(actionIn), forControlEvents: UIControlEvents.TouchUpInside)
                    btnLinekdIn?.setImage(UIImage(named: "linkedin_icon_active"), forState: UIControlState.Normal)
            }
            else
            {
                btnLinekdIn?.addTarget(self, action: #selector(actionIn), forControlEvents: UIControlEvents.TouchUpInside)
            }
        
        print(dicAttendeeDInfo)
            if (dicAttendeeDInfo?.valueForKey("fb_id") as? String)! != "" {
            iStrFbId = "\(dicAttendeeDInfo!.valueForKey("fb_id")!)"
            btnFacebook?.addTarget(self, action: #selector(actionFacebook), forControlEvents: UIControlEvents.TouchUpInside)
            btnFacebook?.setImage(UIImage(named: "facebook_icon_active"), forState: UIControlState.Normal)
            }
            else
            {
                btnFacebook?.addTarget(self, action: #selector(actionFacebook), forControlEvents: UIControlEvents.TouchUpInside)
            }
        
            if ((dicAttendeeDInfo?.valueForKey("is_bookmark"))! as! NSObject == 0) {
                btnBookmark?.setImage(UIImage(named: "bookmark_normal"), forState: UIControlState.Normal)
            }else {
                btnBookmark?.setImage(UIImage(named: "bookmark_active"), forState: UIControlState.Normal)
            }
                btnBookmark?.addTarget(self, action: #selector(actionBookmark), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        

        self.mutArrInterest = NSMutableArray()
        self.mutArrMutualFriends = NSMutableArray()
        self.mutArrConnectionSuggestion = NSMutableArray()
        
        
        
        print(scrVwAttendee.frame)
        
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if ReachabilityCheck.isConnectedToNetwork() {
            if let iUrl = getNoteList() {
                SPGWebService.callGETWebService(iUrl, WSCompletionBlock: { (data, error) in
                    let iDictNote:NSDictionary = data as!NSDictionary
                    if iDictNote.objectForKey("status") as! String == "Success" {
                        self.txtMessage?.text = iDictNote.objectForKey("data") as? String
                    }
                })
            }
        }
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        scrVwAttendee.contentSize = CGSizeMake(scrVwAttendee.frame.size.width,UIScreen.mainScreen().bounds.size.height)
        if !isFirstTime {
            configureExpandingMenuButton()
        }
        isFirstTime=true
        var finalHeight:CGFloat = 0.0
        let lblHeight:CGFloat = 21.0
        let lblTop:CGFloat = 8.0
        let lblBottom:CGFloat = 8.0
       // let vWTop:CGFloat = 8.0
        let vWBottom:CGFloat = 8.0
        if ReachabilityCheck.isConnectedToNetwork()
        {
            //SwiftLoader.show(animated: true)
            if let iUrl = getAttendeeInfo() {
                SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                    print(iData)
                    if iData != nil {
                        self.mutArrInterest = NSMutableArray()
                        self.mutArrMutualFriends = NSMutableArray()
                        self.mutArrConnectionSuggestion = NSMutableArray()
                        
                        self.dicInterestDic = (iData as! NSDictionary).mutableCopy() as!  NSMutableDictionary
                        
                        //print(self.dicInterestDic)
                        
                        if self.dicInterestDic?.objectForKey("interestname") != nil {
                            self.mutArrInterest = (self.dicInterestDic?.objectForKey("interestname") as! NSArray).mutableCopy() as! NSMutableArray
                        }
                        
                        if self.dicInterestDic?.objectForKey("mutualFriends") != nil {
                            self.mutArrMutualFriends = (self.dicInterestDic?.objectForKey("mutualFriends") as! NSArray).mutableCopy() as! NSMutableArray
                        }
                        
                        if self.dicInterestDic?.objectForKey("connectionSuggestion") != nil {
                            self.mutArrConnectionSuggestion = (self.dicInterestDic?.objectForKey("connectionSuggestion") as! NSArray).mutableCopy() as! NSMutableArray
                        }
                        
                        
                        
                        
                        
                        if self.mutArrInterest.count == 0 {
                            
                        } else {
                            self.vwInterest.hidden = false
                            var iIntX:CGFloat = 3
                            var iIntY:CGFloat = 3
                            let iFloatButtonHeight:CGFloat = 30.0
                            
                            
                            let vWInnerInterest:UIView = self.vwInterest.viewWithTag(21)!
                            
                            for iCounter in 0 ..<  self.mutArrInterest.count {
                                
                                let iStrInterestName:String = self.mutArrInterest.objectAtIndex(iCounter) as! String
                                
                                
                                
                                let btnInterest:UIButton = UIButton()
                                
                                btnInterest.titleLabel?.font = UIFont.systemFontOfSize(12.0)
                                
                                let width:CGFloat = iStrInterestName.widthWithConstrainedHeight(iFloatButtonHeight, font: (btnInterest.titleLabel?.font)!)
                                
                                if width + iIntX > UIScreen.mainScreen().bounds.size.width - 20 {
                                    iIntY = iIntY + 30 + 5
                                    iIntX = 3
                                }
                                
                                btnInterest.frame = CGRectMake(iIntX, iIntY, width+10, iFloatButtonHeight)
                                btnInterest.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
                                btnInterest.setTitle(iStrInterestName, forState: .Normal)
                                btnInterest.setTitleColor(UIColor.blackColor(), forState: .Normal)
                                btnInterest.layer.cornerRadius = 4.0
                                iIntX = iIntX + width + 12
                                vWInnerInterest.addSubview(btnInterest)
                                
                            }
                            
                            if iIntY + iFloatButtonHeight > self.heightOfInterest.constant  {
                                self.heightOfInterest.constant = self.heightOfInterest.constant + 45
                                finalHeight = self.topOfPeopleMayKnow.constant + 45
                            }else {
                                self.heightOfInterest.constant = iIntY + iFloatButtonHeight + lblTop + lblBottom + lblHeight  + vWBottom + 3
                                finalHeight = iIntY + iFloatButtonHeight + lblTop + lblBottom + lblHeight  + vWBottom + 3
                            }
                            self.view.layoutIfNeeded()
                        }
                        
                        if self.mutArrConnectionSuggestion.count == 0 {
                            
                        }else {
                            if self.vwInterest.hidden {
                                self.topOfPeopleMayKnow.constant = 0
                            }else {
                                self.topOfPeopleMayKnow.constant = finalHeight
                            }
                            finalHeight = finalHeight + self.VWPeopleKnow.frame.size.height
                            self.view.layoutIfNeeded()
                            self.VWPeopleKnow.hidden = false
                            self.collVwPeopleMayKmow.reloadData()
                        }
                        if self.mutArrMutualFriends.count == 0 {
                            
                        }else {
                            var tempHeight:CGFloat = self.vWMutualConnection.frame.size.height
                            if  self.mutArrMutualFriends.count < 4 {
                                tempHeight = tempHeight / 2
                            }
                            self.heightOfMutualConnection.constant = tempHeight  + lblTop  + lblHeight
                            if self.VWPeopleKnow.hidden && self.vwInterest.hidden {
                                self.topOfMutualConnection.constant = 0
                            }else if self.VWPeopleKnow.hidden {
                                self.topOfMutualConnection.constant = finalHeight
                            }else {
                                
                                self.topOfMutualConnection.constant = finalHeight
                                finalHeight = finalHeight + tempHeight + lblBottom
                            }
                            
                            self.view.layoutIfNeeded()
                            self.vWMutualConnection.hidden = false
                            self.collVWMutualConnectionnfo.reloadData()
                        }
                        
                        self.scrVwAttendee.contentSize = CGSizeMake(self.scrVwAttendee.frame.size.width,self.heightAboutDescription.constant + finalHeight + 30)
                    }
 
                    }
            } else {
                // oops, something went wrong with the URL
            }
            
        }
        else {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    
    func getAttendeeInfo() -> NSURL? {
     
        var userId:String = ""
        if sharedManager.isUserLoggedIn {
            userId=sharedManager.currentUserID
        }
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_id" : userId,"event_id" : Constants.eventInfo.kEventId,"target_id" : dicAttendeeDInfo!.valueForKey("user_id")!], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.VIEW_ATENDEE_PROFILE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    func getNoteList() -> NSURL? {
         var userId:String = ""
        if sharedManager.isUserLoggedIn {
            userId=sharedManager.currentUserID
        }
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_id" : userId,"target_id" : dicAttendeeDInfo!.valueForKey("user_id")!], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.SINGLE_USER_NOTE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    @IBAction func btnMorePressed(sender:UIButton) {
        
        sender.selected = !sender.selected
         let size:CGSize = self.scrVwAttendee.contentSize
        if sender.selected {
            sender.setTitle("LESS", forState:.Normal)
            heightAboutDescription.constant = heightAboutDescription.constant + totalSize - displayFixSize
            heightAboutMeVw.constant =  heightAboutMeVw.constant + totalSize - displayFixSize
            heightAboutMe.constant = totalSize + 37
            self.scrVwAttendee.contentSize = CGSizeMake(self.scrVwAttendee.frame.size.width, size.height + totalSize - displayFixSize)
        }else {
            sender.setTitle("MORE", forState:.Normal)
            heightAboutDescription.constant = heightAboutDescription.constant - totalSize
            heightAboutMeVw.constant =  heightAboutMeVw.constant - totalSize
            heightAboutMe.constant = totalSize
            heightAboutDescription.constant = heightAboutDescription.constant + displayFixSize
            heightAboutMeVw.constant =  heightAboutMeVw.constant + displayFixSize
            heightAboutMe.constant = displayFixSize + 37
             self.scrVwAttendee.contentSize = CGSizeMake(self.scrVwAttendee.frame.size.width, size.height - totalSize + displayFixSize)
        }
        self.view.layoutIfNeeded()
        
    }
    
    @IBAction func onTapGoogleBtn(sender: AnyObject) {
        
        var temp = (self.dicAttendeeDInfo?.valueForKey("firstname") as! String) + " "+((self.dicAttendeeDInfo?.valueForKey("lastname"))! as! String) + (self.dicAttendeeDInfo?.valueForKey("designation") as! String) + " at " + ((self.dicAttendeeDInfo?.valueForKey("organization"))! as! String)
        temp = temp .stringByReplacingOccurrencesOfString(" ", withString: "")
        
        print("https://www.google.co.in/search?site=webhp&source=hp&q=" + (temp as String))
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.google.co.in/search?site=webhp&source=hp&q=" + (temp as String))!)
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if section == 0
        {
            return 1
        }
        if section == 1 {
            return 1
        }
        if section == 2 {
            if dicInterestDic != nil && dicInterestDic?.valueForKey("interestname") != nil {
                return 1
            }
            return 0
        }
        if section == 3 {
            return 0
        }
        return 0
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        
        if section == 0
        {
            return 0
        }
        if section == 1 {
            return 0
        }
        if section == 2 {
            return 30
        }
        if section == 3 {
            return 30
        }
        return 0
        
    }
    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        
//        let cell1:UITableViewCell?=UITableViewCell()
//        if indexPath.section == 0
//        {
//            let cell:AttenddeeInfoCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsContactCell", forIndexPath: indexPath) as! AttenddeeInfoCell
//            cell.lblDeveloper!.text=dicAttendeeDInfo?.valueForKey("designation") as? String
//            cell.lblName!.text = (dicAttendeeDInfo?.valueForKey("firstname") as! String) + " "+((dicAttendeeDInfo?.valueForKey("lastname"))! as! String)
//            cell.lblPostion!.text=dicAttendeeDInfo?.valueForKey("organization") as? String
//            cell.imgUser?.layer.cornerRadius=(cell.imgUser?.frame.size.width)!/2
//            cell.imgUser?.clipsToBounds = true
//            cell.imgUser?.sd_setImageWithURL(NSURL(string: (Constants.ImagePaths.kAttendeeImagePath + ((dicAttendeeDInfo?.valueForKey("profile_img"))! as! String) )
//), placeholderImage: UIImage(named: "login_default"))
//            
//            
//            if (dicAttendeeDInfo?.valueForKey("twitter_public_url") as? String)! != "" {
//                cell.btnTwitter?.addTarget(self, action: #selector(actionTwitter), forControlEvents: UIControlEvents.TouchUpInside)
//                cell.btnTwitter?.setImage(UIImage(named: "twitter_icon_active"), forState: UIControlState.Normal)
//            }
//            if (dicAttendeeDInfo?.valueForKey("linkedin_public_url") as? String)! != "" {
//                
//                cell.btnLinekdIn?.addTarget(self, action: #selector(actionIn), forControlEvents: UIControlEvents.TouchUpInside)
//                cell.btnLinekdIn?.setImage(UIImage(named: "linkedin_icon_active"), forState: UIControlState.Normal)
//            }
//
//            
//            if ((dicAttendeeDInfo?.valueForKey("is_bookmark"))! as! NSObject == 0) {
//                cell.btnBookmark?.setImage(UIImage(named: "bookmark_normal"), forState: UIControlState.Normal)
//            }
//            else {
//                cell.btnBookmark?.setImage(UIImage(named: "bookmark_active"), forState: UIControlState.Normal)
//            }
//            cell.btnBookmark?.tag=indexPath.row
//            cell.btnBookmark?.addTarget(self, action: #selector(actionBookmark), forControlEvents: UIControlEvents.TouchUpInside)
//            
//            // Configure the cell...
//            //cell.textLabel?.text = "Row \(indexPath.row)"
//            return cell
//        }
//        if indexPath.section == 1
//        {
//            let cell:SponsorDetailCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsDetailsCell", forIndexPath: indexPath) as! SponsorDetailCell
//            cell.textViewDetail?.loadHTMLString(dicAttendeeDInfo?.valueForKey("aboutme") as! String, baseURL: nil)
////            
////                        cell.btnMore!.addTarget(self, action: #selector(HB_AttendeeInfoVC.more_btn_click(_:)), forControlEvents: .TouchUpInside)
//            
//            
//            cell.btnMore?.addTarget(self, action: #selector(loadMore), forControlEvents: UIControlEvents.TouchUpInside)
//            
//            cell.textViewDetail?.delegate=self
//            cell.textViewDetail?.scrollView.scrollEnabled=false
//            if dicAttendeeDInfo?.valueForKey("aboutme") as! String == "" {
//                isLoadMore=true
//                cell.btnMore?.hidden=true
//            }
//            else {
//                cell.btnMore?.hidden=false
//            }
//            return cell
//            
//        }
//        if indexPath.section == 2
//        {
//            let cell:HB_AttendeeInterestCell = tableView.dequeueReusableCellWithIdentifier("HB_AttendeeInterestCell", forIndexPath: indexPath) as! HB_AttendeeInterestCell
//            let interests:NSArray=dicInterestDic?.valueForKey("interestname") as! NSArray
//            cell.txtAttendeeInterest.text=interests.componentsJoinedByString(", ")
//            
//            return cell
//            
//        }
//        if indexPath.section == 3
//        {
////            let cell:HB_AttendeeMutualConnectionCell = tableView.dequeueReusableCellWithIdentifier("HB_AttendeeMutualConnectionCell", forIndexPath: indexPath) as! HB_AttendeeMutualConnectionCell
////            collVWMutualConnectionnfo=cell.collectionVWMutual
////            return cell
//            
//        }
//        return cell1!
//    }
    func actionBookmark(sender : AnyObject){
        
        if !sharedManager.isUserLoggedIn {
            SPGUtility.sharedInstance().showAlert("You need to login.", titleMessage: "", viewController: self)
            return;
        }
        if sharedManager.userCommunityExist == 0
        {
            SPGUtility.sharedInstance().showAlert("Please Join the community", titleMessage: "", viewController: self)
            return;
        }
        let tmpButton : UIButton = sender as! UIButton
        let param : NSDictionary = ["user_id":sharedManager.currentUserID,"bookmark_user_id":(dicAttendeeDInfo?.valueForKey("user_id") as? String)!]
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.BOOKMARK_ADD + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    
                    
                    if JSON["status"] == "Success"
                    {
                        if JSON["add"] == 1 {
                            tmpButton.setImage(UIImage(named: "bookmark_active"), forState: UIControlState.Normal)
                        }
                        else {
                            tmpButton.setImage(UIImage(named: "bookmark_normal"), forState: UIControlState.Normal)
                            
                        }
                        
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
        
    }
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        var vw:UIView?
//        if section == 0 {
//            
//            vw=UIView(frame: CGRect(x: 0, y: 0, width: (UIScreen.mainScreen().bounds.width), height: 0))
//            return vw
//            
//        }
//        if section == 1 {
//            
//            vw=UIView(frame: CGRect(x: 0, y: 0, width: (UIScreen.mainScreen().bounds.width), height: 0))
//            
//            return vw
//        }
//        if section == 2 {
//            vw=UIView(frame: CGRect(x: 0, y: 0, width: (UIScreen.mainScreen().bounds.width), height: 30))
//            vw!.backgroundColor = UIColor.clearColor()
//            
//            let lblSpeaker:UILabel=UILabel(frame: CGRect(x: 15, y: 0, width: (UIScreen.mainScreen().bounds.width)-15, height: 30))
//            lblSpeaker.text = "Interest"
//            lblSpeaker.textColor=UIColor.grayColor()
//            lblSpeaker.font = lblSpeaker.font.fontWithSize(12)
//            vw!.addSubview(lblSpeaker);
//            return vw
//        }
//        if section == 3{
//            vw=UIView(frame: CGRect(x: 0, y: 0, width: (UIScreen.mainScreen().bounds.width), height: 30))
//            vw!.backgroundColor = UIColor.clearColor()
//            
//            let lblSpeaker:UILabel=UILabel(frame: CGRect(x: 15, y: 0, width: (UIScreen.mainScreen().bounds.width)-15, height: 30))
//            lblSpeaker.text = "Mutual Connections"
//            lblSpeaker.textColor=UIColor.grayColor()
//            lblSpeaker.font = lblSpeaker.font.fontWithSize(12)
//            vw!.addSubview(lblSpeaker);
//            return vw
//        }
//        
//        return vw
//    }
//    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
//    {
//        if indexPath.section == 0
//        {
//            return 225
//            
//        }
//        if indexPath.section == 1
//        {
//            
//            if isLoadMore{
//                return CGFloat(webHeight) + 30
//            }
//            
//            return 97
//        }
//        if indexPath.section == 2
//        {
//            return 74
//        }
//        if indexPath.section == 3
//        {
//            return 128
//        }
//        
//        return 0
//        
//        
//    }
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 4
//    }
     func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView.tag == 11 {
            if mutArrConnectionSuggestion != nil {
                return mutArrConnectionSuggestion.count
            }
        }else if collectionView.tag == 12 {
            if mutArrMutualFriends != nil {
                return mutArrMutualFriends.count
            }
        }
    
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        
        let cell = UICollectionViewCell()
        
        if collectionView.tag == 11 {
            let cellPeople:HB_PeopleYouShouldKnowCell = collectionView.dequeueReusableCellWithReuseIdentifier("HB_PeopleYouShouldKnowCell", forIndexPath: indexPath) as! HB_PeopleYouShouldKnowCell
            let iStrFullName:String = (mutArrConnectionSuggestion?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String)!+" " as String+((mutArrConnectionSuggestion?.objectAtIndex(indexPath.row).valueForKey("lastname"))! as! String)
            
            cellPeople.lblAttendeeName.text = iStrFullName
            cellPeople.lblAteendeeProfile.text = (mutArrConnectionSuggestion?.objectAtIndex(indexPath.row).valueForKey("designation") as! String)
            cellPeople.imgAttendeeProfile.layer.cornerRadius = 25.0
            cellPeople.imgAttendeeProfile.layer.masksToBounds = true
            if mutArrConnectionSuggestion?.objectAtIndex(indexPath.row).valueForKey("profile_img") != nil {
                let url:String = (Constants.ImagePaths.kAttendeeImagePath + ((mutArrConnectionSuggestion?.objectAtIndex(indexPath.row).valueForKey("profile_img"))! as! String))
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(mutArrConnectionSuggestion?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cellPeople.imgAttendeeProfile.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
            }else {
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(mutArrConnectionSuggestion?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cellPeople.imgAttendeeProfile.image = imgVw.image
            }
            
            return cellPeople
        }else if collectionView.tag == 12 {
            let cellMutual:HB_MutualConnectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("HB_MutualConnectionCell", forIndexPath: indexPath) as! HB_MutualConnectionCell
            
            let iStrFullName:String = (mutArrMutualFriends?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String)!+" " as String+((mutArrMutualFriends?.objectAtIndex(indexPath.row).valueForKey("lastname"))! as! String)
            
            cellMutual.lblAttendeeName.text = iStrFullName
            cellMutual.lblAteendeeProfile.text = (mutArrMutualFriends?.objectAtIndex(indexPath.row).valueForKey("designation") as! String)
            cellMutual.imgAttendeeProfile.layer.cornerRadius = 25.0
            cellMutual.imgAttendeeProfile.layer.masksToBounds = true
            if mutArrMutualFriends?.objectAtIndex(indexPath.row).valueForKey("profile_img") != nil {
                let url:String = (Constants.ImagePaths.kAttendeeImagePath + ((mutArrMutualFriends?.objectAtIndex(indexPath.row).valueForKey("profile_img"))! as! String))
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(mutArrMutualFriends?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cellMutual.imgAttendeeProfile.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
            }else {
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(mutArrMutualFriends?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cellMutual.imgAttendeeProfile.image = imgVw.image
            }
            
            
            return cellMutual
        }
     
        
        
        
       
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        if collectionView.tag == 11 {
            selectedAtendee = ((mutArrConnectionSuggestion?.objectAtIndex(indexPath.row))! as! NSDictionary).mutableCopy() as! NSMutableDictionary
        }else {
            selectedAtendee = ((mutArrMutualFriends?.objectAtIndex(indexPath.row))!  as! NSDictionary).mutableCopy() as! NSMutableDictionary
        }
        
        let objAttendInfoVC : HB_AttendeeInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_AttendeeInfoVC") as! HB_AttendeeInfoVC
        objAttendInfoVC.dicAttendeeDInfo = NSMutableDictionary()
        objAttendInfoVC.dicAttendeeDInfo = selectedAtendee
        self.navigationController?.pushViewController(objAttendInfoVC, animated: true)
        
    }
  
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_ExhibitorsInfoVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="ATTENDEE INFO"
        
        
    }
    
    @IBAction func SendMessageButton(sender: AnyObject) {
        
        if !sharedManager.isUserLoggedIn {
            SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
            return
        }
        
        if sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("Please join the community", titleMessage: "", viewController: self)
            return
        }
        messagePopupView?.hidden = false
        
    }
    
    @IBAction func onTapCrossMessage(sender: AnyObject) {
        messagePopupView?.hidden = true
    }
    
    @IBAction func onTapCancelMessage(sender: AnyObject) {
        messagePopupView?.hidden = true
    }
    
    @IBAction func onTapSendMessage(sender: AnyObject) {
        // send message api calling
      
        
        if Validator.isEmpty(txt_sendMessage!.text!){
            SPGUtility.sharedInstance().showAlert("Please enter a message.", titleMessage: "", viewController: self)
            return;
        }
        let strMessage = String(txt_sendMessage!.text!)
        
        let param : NSDictionary = ["message_text":strMessage,"user_sender_id":sharedManager.currentUserID,"user_receiver_id":dicAttendeeDInfo?.valueForKey("user_id") as! String]
        
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.SEND_MESSAGE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    if JSON["status"] == "Success"
                    {
                        //SPGUtility.sharedInstance().showAlert("Message send successfully.", titleMessage: "", viewController: self)
                        self.txt_sendMessage!.text=""
                        self.messagePopupView?.hidden = true
                        
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    
    @IBAction func back_btn_click(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }


    func heightForView(text:String) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width-16, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = UIFont.systemFontOfSize(12.0)
        label.text = text
        
        label.sizeToFit()
        return label.frame.height + 20
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadMore() {
        isLoadMore = !isLoadMore
        if isLoadMore == true
        {
            webHeight = originalHeight
            
        }
        else {
            webHeight=97
        }
        
        self.tblAttendeeInfo!.reloadData()
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        let webViewHeight:NSString = webView.stringByEvaluatingJavaScriptFromString("document.body.scrollHeight;")!
        
        self.originalHeight = webViewHeight.integerValue
    }
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        self.originalHeight=97
    }
    func actionTwitter(){
        if (dicAttendeeDInfo?.valueForKey("twitter_public_url") as? String)! == ""
        {
            SPGUtility.sharedInstance().showAlert("Twitter profile is not available", titleMessage: "", viewController: self)
        }
        else
        {
            let url:NSURL = NSURL(string: (dicAttendeeDInfo?.valueForKey("twitter_public_url") as? String)!)!
            
            if (UIApplication.sharedApplication().canOpenURL(url))
            {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    func actionFacebook() {
        
        if (dicAttendeeDInfo?.valueForKey("fb_id") as? String)! == ""
        {
            SPGUtility.sharedInstance().showAlert("Facebook profile is not available", titleMessage: "", viewController: self)
        }
        else
        {
//            let appUrl = NSURL(string: "fb://profile?app_scoped_user_id="+iStrFbId )!
//            
//            if UIApplication.sharedApplication().canOpenURL(appUrl) {
//                UIApplication.sharedApplication().openURL(appUrl)
//            } else {
                UIApplication.sharedApplication().openURL(NSURL(string: "https://www.facebook.com/app_scoped_user_id/\(iStrFbId)")!)
           // }
        }
    }
    func actionIn(){

        if (dicAttendeeDInfo?.valueForKey("linkedin_public_url") as? String)! == ""
        {
            SPGUtility.sharedInstance().showAlert("Linkedin profile is not available", titleMessage: "", viewController: self)
        }
        else
        {
            let url:NSURL = NSURL(string: (dicAttendeeDInfo?.valueForKey("linkedin_public_url") as? String)!)!
            
            if (UIApplication.sharedApplication().canOpenURL(url))
            {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
    private func configureExpandingMenuButton() {
        
        let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let menuButtonSize: CGSize = CGSize(width: 50.0, height: 50.0)
        let centerImage : UIImageView = UIImageView(frame: CGRectMake(0, 0, 40, 40))
        centerImage.setImageWithString("", color: iAppDelegate.defaultColor,circular:true)
        
        let imgMain = UIImageView(frame: CGRectMake(0, 0, 40, 40))
        imgMain.image = UIImage(named: "chooser-button-tab")
        
        UIGraphicsBeginImageContext(CGSizeMake(40, 40))
        centerImage.image!.drawInRect(CGRectMake(0, 0, 40, 40))
        imgMain.image!.drawInRect(CGRectMake(8, 8, 24, 24))
        
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let menuButton = ExpandingMenuButton(frame: CGRect(origin: CGPointZero, size: menuButtonSize), centerImage: finalImage!, centerHighlightedImage: finalImage!)
        menuButton.center = CGPointMake(self.view.bounds.width - 35.0, self.view.bounds.height - 35.0)
        menuButton.addShadowView()
        self.view.addSubview(menuButton)
        self.view.bringSubviewToFront(menuButton);
        //self.view.sendSubviewToBack(self.tblAttendeeList)
        func showAlert(title: String) {
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }

        
        let imgView:UIImageView = UIImageView(frame: CGRectMake(0, 0, 30, 30))
        imgView.setImageWithString("", color: iAppDelegate.defaultColor,circular:true)
        
        let item1 = ExpandingMenuItem(size: menuButtonSize, title: " Take a note ", image: UIImage(named: "float_note")!, highlightedImage: UIImage(named: "float_note")!, backgroundImage: imgView.image, backgroundHighlightedImage: UIImage(named: "float_note")) { () -> Void in
            
            if !self.sharedManager.isUserLoggedIn {
                SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
                return
            }
            
            if self.sharedManager.userCommunityExist == 0 {
                SPGUtility.sharedInstance().showAlert("Please join the community", titleMessage: "", viewController: self)
                return
            }
            
            self.popupView?.hidden=false
        }
        
        let item2 = ExpandingMenuItem(size: menuButtonSize, title: "Set Meeting", image: UIImage(named: "floatMeeting")!, highlightedImage: UIImage(named: "floatMeeting")!, backgroundImage: imgView.image, backgroundHighlightedImage: UIImage(named: "floatMeeting")) { () -> Void in
            
            if !self.sharedManager.isUserLoggedIn {
                SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
                return
            }
            
            if self.sharedManager.userCommunityExist == 0 {
                SPGUtility.sharedInstance().showAlert("Please join the community", titleMessage: "", viewController: self)
                return
            }
            
            let objSetMeetingVC : HB_SetMeetingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_SetMeetingVC") as! HB_SetMeetingVC
            self.navigationController?.pushViewController(objSetMeetingVC, animated: true)
        }
        
       
        
        
        menuButton.addMenuItems([item1,item2])
        
        menuButton.willPresentMenuItems = { (menu) -> Void in
            print("MenuItems will present.")
        }
        
        menuButton.didDismissMenuItems = { (menu) -> Void in
            print("MenuItems dismissed.")
        }
    }
    
    @IBAction func close(sender : UIButton) {
        popupView?.hidden=true
    }
    
    @IBAction func submitNotes (sender : UIButton) {
        
              
        
        if Validator.isEmpty(txtMessage!.text!){
            SPGUtility.sharedInstance().showAlert("Please enter a message.", titleMessage: "", viewController: self)
            return;
        }
        let strMessage = String(txtMessage!.text!).base64Encoded()
        
        let param : NSDictionary = ["note_data":strMessage,"user_id":sharedManager.currentUserID,"target_id":dicAttendeeDInfo?.valueForKey("user_id") as! String]
        
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            
            
            SwiftLoader.show(animated: true)
            
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.SAVE_NOTE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                print(url)
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    if JSON["status"] == "Success"
                    {
                        SPGUtility.sharedInstance().showAlert("Save note successfully.", titleMessage: "", viewController: self)
                        self.txt_sendMessage!.text=""
                        self.popupView?.hidden = true
                        
                    }else {
                        
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "",viewController: self)
    }
        
    }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
//        if segue.identifier == Constants.SegueConstant.kShowAttendeeInfoVC {
//            let objAttendInfoVC:HB_AttendeeInfoVC=(segue.destinationViewController as? HB_AttendeeInfoVC)!
//            
//            objAttendInfoVC.dicAttendeeDInfo = NSMutableDictionary()
//            objAttendInfoVC.dicAttendeeDInfo = selectedAtendee
//            
//        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
