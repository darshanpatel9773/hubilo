//
//  HB_BookMarkVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 12/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import SwiftLoader
import SDWebImage
class HB_BookMarkVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    var sharedManager : Globals = Globals.sharedInstance
    var bookmarkList : [BookMark] = []
    @IBOutlet weak var tblBookMarkList: UITableView!
    var searchBarButtonItem: UIBarButtonItem?
    var searchBar = UISearchBar()
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var mutArrDisplaySearch:[BookMark] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenu()
        getBookmarks()
        
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.sizeToFit()
        let searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        searchBar.barTintColor = UIColor.clearColor()
        searchBar.tintColor = appDelegate.defaultColor
        if let textFieldInsideSearchBar = self.searchBar.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = UIColor.whiteColor()
            
        }
        
        let iBtnClear = searchTextField?.valueForKey("_clearButton") as! UIButton
        iBtnClear.setImage((iBtnClear.imageView?.image?.imageWithRenderingMode(.AlwaysTemplate)), forState: .Normal)
        
        iBtnClear.tintColor = UIColor.whiteColor()
        
        
        searchBarButtonItem = navigationItem.rightBarButtonItem
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        tblBookMarkList.reloadData()
       }
    override func viewDidAppear(animated: Bool) {
        tblBookMarkList.reloadData()
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        guard bookmarkList.count > 0 else {
            return 0
        }
        return bookmarkList.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell:HB_BookMarkCell=tableView.dequeueReusableCellWithIdentifier("HB_BookMarkCell", forIndexPath: indexPath) as! HB_BookMarkCell
        cell.imgBookMarkCell.layer.cornerRadius =  25.0
        cell.selectionStyle = .None
        cell.lblName.text? = bookmarkList[indexPath.row].firstname + " " + bookmarkList[indexPath.row].lastname
        cell.lblCompany.text=bookmarkList[indexPath.row].organization
        cell.lblWorkProfile.text=bookmarkList[indexPath.row].designation
        cell.btnBookmark.tag = indexPath.row
        cell.btnBookmark?.addTarget(self, action: #selector(actionBookmark), forControlEvents: UIControlEvents.TouchUpInside)
       
        if bookmarkList[indexPath.row].profile_img.characters.count > 0 {
            let url:String = (Constants.ImagePaths.kAttendeeImagePath + (bookmarkList[indexPath.row].profile_img))
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(bookmarkList[indexPath.row].firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgBookMarkCell.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(bookmarkList[indexPath.row].firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgBookMarkCell.image = imgVw.image
        }
         cell.imgBookMarkCell.clipsToBounds = true
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let objAttendInfoVC : HB_AttendeeInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_AttendeeInfoVC") as! HB_AttendeeInfoVC
        objAttendInfoVC.dicAttendeeDInfo = NSMutableDictionary()
        let tmpDicData:NSMutableDictionary = NSMutableDictionary()
        tmpDicData.setObject(bookmarkList[indexPath.row].firstname, forKey: "firstname")
        tmpDicData.setObject(bookmarkList[indexPath.row].lastname, forKey: "lastname")
        tmpDicData.setObject(bookmarkList[indexPath.row].user_id, forKey: "user_id")
        tmpDicData.setObject(bookmarkList[indexPath.row].profile_img, forKey: "profile_img")
        tmpDicData.setObject(bookmarkList[indexPath.row].linkedin_public_url, forKey: "linkedin_public_url")
        tmpDicData.setObject(bookmarkList[indexPath.row].is_bookmark, forKey: "is_bookmark")
        tmpDicData.setObject(bookmarkList[indexPath.row].fb_id, forKey: "fb_id")
        tmpDicData.setObject(bookmarkList[indexPath.row].designation, forKey: "designation")
        tmpDicData.setObject(bookmarkList[indexPath.row].aboutme, forKey: "aboutme")
        tmpDicData.setObject(bookmarkList[indexPath.row].organization, forKey: "organization")
        tmpDicData.setObject(bookmarkList[indexPath.row].twitter_public_url, forKey: "twitter_public_url")
        objAttendInfoVC.dicAttendeeDInfo = tmpDicData
        self.navigationController?.pushViewController(objAttendInfoVC, animated: true)
    }
    
    func actionBookmark(sender : AnyObject){
        
        if !sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You must be loggedin in order to use this functionality", titleMessage: "", viewController: self)
            return;
        }
        let tmpButton : UIButton = sender as! UIButton
        //        ((mutArrAttendeeList?.objectAtIndex(indexPath.row).valueForKey("is_bookmark"))! as! NSObject == 0)
        
        let param : NSDictionary = ["user_id":sharedManager.currentUserID,"bookmark_user_id":(bookmarkList[tmpButton.tag].user_id )]
        print(param)
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.BOOKMARK_ADD + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    
                    
                    if JSON["status"] == "Success"
                    {
                        if JSON["add"] == 1 {
                            //tmpButton.setImage(UIImage(named: "bookmark_active"), forState: UIControlState.Normal)
                        }
                        else {
                            //tmpButton.setImage(UIImage(named: "bookmark_normal"), forState: UIControlState.Normal)
                            self.bookmarkList.removeAtIndex(tmpButton.tag)
                            
                        }
                        self.tblBookMarkList.reloadData()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
        
    }
    //MARK: User Define
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_SettingVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="BOOKMARK LIST"
        
      
        
        let mySerachbtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySerachbtn.setImage(UIImage(named: "HB_Search"), forState: UIControlState.Normal)
        mySerachbtn.frame = CGRectMake(0, 0, 24, 24)
        mySerachbtn.addTarget(self, action: #selector(HB_BookMarkVC.searchButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomSearchButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySerachbtn)
        self.navigationItem.rightBarButtonItem = myCustomSearchButtonItem
    }
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
        
        
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    @IBAction func searchButtonTouched(sender: AnyObject){
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_NoteListVC.backButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        showSearchBar()
    }
    func showSearchBar() {
        self.searchBar.text = ""
        self.searchBar.hidden = false
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        self.searchBar.hidden = true
        navigationItem.titleView = nil
        setMenu()
        UIView.animateWithDuration(0.3, animations: {
            self.searchBar.alpha = 0
            }, completion: { finished in
        })
    }
    
    func backButtonTouched(sender: UIButton) {
        hideSearchBar()
        if self.mutArrDisplaySearch.count > 0 {
            mutArrDisplaySearch = []
            self.mutArrDisplaySearch = self.bookmarkList
            self.tblBookMarkList.reloadData()
        }
    }
    
    
    func getBookmarks() {
      
        if !sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You must be loggedin in order to use this functionality", titleMessage: "", viewController: self)
            return;
        }
        let param : NSDictionary = ["user_id":sharedManager.currentUserID]
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.BOOKMARK_LIST + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    if JSON["msg"] == "Success"
                    {
                        if JSON["data"].array?.count != 0
                        {
                            for dt in JSON["data"].array!
                            {
                                let currentBookmark : BookMark = BookMark()
                                currentBookmark.user_id = dt["user_id"].rawString()!
                                currentBookmark.firstname = dt["firstname"].rawString()!
                                currentBookmark.lastname = dt["lastname"].rawString()!
                                currentBookmark.profile_img = dt["profile_img"].rawString()!
                                currentBookmark.designation = dt["designation"].rawString()!
                                currentBookmark.organization = dt["organization"].rawString()!
                                currentBookmark.aboutme = dt["aboutme"].rawString()!
                                currentBookmark.linkedin_public_url = dt["linkedin_public_url"].rawString()!
                                currentBookmark.fb_id = dt["fb_id"].rawString()!
                                currentBookmark.twitter_public_url = dt["twitter_public_url"].rawString()!
                                currentBookmark.is_bookmark = dt["is_bookmark"].intValue
                                self.bookmarkList.append(currentBookmark)
                            }
                            self.tblBookMarkList.hidden = false
                            self.tblBookMarkList.reloadData()
                        }
                        else
                        {
                            self.tblBookMarkList.hidden = true
                        }
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
