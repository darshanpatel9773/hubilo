//
//  HB_BookMarkCell.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 12/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_BookMarkCell: UITableViewCell {

    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblWorkProfile: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgBookMarkCell: UIImageView!
    @IBOutlet var img_outer_view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
