//
//  HB_SettingVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 12/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController

class HB_SettingVC: UIViewController {
    
     var elDrawer:KYDrawerController!
    
    
//    let arrSettingItem:NSArray=["Privacy Policy","Notifications","About","Help","Tell a friend"]
    
    @IBOutlet var notiSwitchBtn: UISwitch!
    let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    var navController:UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        elDrawer = (self.navigationController?.parentViewController as? KYDrawerController)!
//        elDrawer.mainViewController=navController;
//        elDrawer.setDrawerState(KYDrawerController.DrawerState.Closed, animated: true)
        setMenu()
       notiSwitchBtn.onTintColor = iAppDelegate.defaultColor
       
        // Do any additional setup after loading the view.
    }
    
    // new code
    
    @IBAction func onTapPrivacyPolicy(sender: AnyObject) {
        let elDrawer:KYDrawerController=(self.navigationController?.parentViewController as? KYDrawerController)!
        
        let objHelpVC:PrivacyVC=self.storyboard?.instantiateViewControllerWithIdentifier("PrivacyVC") as! PrivacyVC
      
        let navController:UINavigationController=UINavigationController.init(rootViewController: objHelpVC)
        elDrawer.mainViewController=navController;
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Closed, animated: true)
    }
    
    @IBAction func onTapAbout(sender: AnyObject) {
        
        let elDrawer:KYDrawerController=(self.navigationController?.parentViewController as? KYDrawerController)!
        
        let objHelpVC:AboutVC=self.storyboard?.instantiateViewControllerWithIdentifier("AboutVC") as! AboutVC
        
        let navController:UINavigationController=UINavigationController.init(rootViewController: objHelpVC)
        elDrawer.mainViewController=navController;
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Closed, animated: true)
        
    }
    
    @IBAction func onTapHelp(sender: AnyObject) {
        
        let elDrawer:KYDrawerController=(self.navigationController?.parentViewController as? KYDrawerController)!
        
        let objHelpVC:HB_HomeViewController=self.storyboard?.instantiateViewControllerWithIdentifier("HB_HomeViewController") as! HB_HomeViewController
        
        let navController:UINavigationController=UINavigationController.init(rootViewController: objHelpVC)
        elDrawer.mainViewController=navController;
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Closed, animated: true)
        }
    
    @IBAction func onTapTellaFriend(sender: AnyObject) {
        
        let iUserDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        
        let iStrEventName:String = iUserDefaults.objectForKey("eventName") as! String
        let iStrEventDate:String = iUserDefaults.objectForKey("eventDate") as! String
        let iStrEventAddress:String = iUserDefaults.objectForKey("eventAddress") as! String
        
        let iEventName = "Event Name: " + iStrEventName
        let iEventDate = "Event Date: " + iStrEventDate
        let iEventAddress = "Event Address: " + iStrEventAddress
        let iEventMessage = "Join the event to network with people coming to the event."
        let iEventAppURL:NSURL = NSURL(string: "https://itunes.apple.com/us/app/vg-startup-summit-2016/id1145031697?ls=1&mt=8")!
//        let iEventAppLink = "<html><body><a href='https://itunes.apple.com/us/app/vg-startup-summit-2016/id1145031697?ls=1&mt=8'>https://itunes.apple.com/us/app/vg-startup-summit-2016/id1145031697?ls=1&mt=8</a></body></html>"
        
        let objectsToShare = [iEventName,iEventDate,iEventAddress,iEventMessage,iEventAppURL]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        self.presentViewController(activityVC, animated: true, completion: nil)
        
    }
    
    @IBAction func onTapNotifications(sender: AnyObject) {
        if (sender.on == true){
            print("on")
        }
        else{
            print("off")
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    // old code
    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        
//        return 5
//    }
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        
//        
//        
//        
//        if arrSettingItem[indexPath.row] as! String == "Help"
//        {
//            
//            //HB_HomeViewController
//        }
//        else if arrSettingItem[indexPath.row] as! String == "About"
//        {
//            
//            //HB_HomeViewController
//        }
//        else if arrSettingItem[indexPath.row] as! String == "Privacy Policy"
//        {
//            
//            //HB_HomeViewController
//        }
//        else {
//            return;
//        }
////
//        
//    }
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
//    {
//        
//        let cell:UITableViewCell=tableView.dequeueReusableCellWithIdentifier("SettingCell", forIndexPath: indexPath)
//        cell.textLabel?.text=arrSettingItem.objectAtIndex(indexPath.row) as? String
//        
//       
//   
//       // cell.accessoryType = UITableViewCellAccessoryType.None
//    //cell.accessoryView = UIView(frame: CGRectMake(0, 0, 20, 20))
//      //  cell.accessoryView!.backgroundColor = UIImageView.()
//        let checkImage = UIImage(named: "right-arrow")
//        let checkmark = UIImageView(image: checkImage)
//        cell.accessoryView = checkmark
//    
//        if indexPath.row == 1 {
//            let switchDemo=UISwitch()
//            switchDemo.on = true
//            switchDemo.setOn(true, animated: false)
//            //switchDemo.transform = CGAffineTransformMakeScale(1.0, 0.75);
//            switchDemo.addTarget(self, action: #selector(HB_SettingVC.switchValueDidChange(_:)), forControlEvents: .ValueChanged)
//            cell.accessoryView=switchDemo
//            cell.selectionStyle = .None
//        }
//        
//    
//
//        
//        return cell
//        
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: User Define
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_SettingVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="SETTING"
        
    }
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
    }
    

//    func switchValueDidChange(sender:UISwitch!)
//    {
//        if (sender.on == true){
//            print("on")
//        }
//        else{
//            print("off")
//        }
//    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
