//
//  DiscussionDetailVC.swift
//  Hubilo
//
//  Created by Aadil on 8/3/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
import SDWebImage
import ObjectMapper
class DiscussionDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    @IBOutlet var lblName : UILabel?
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet var lblDate : UILabel?
    @IBOutlet var lblDescription : UILabel?
    @IBOutlet var lblPosition : UILabel?
    @IBOutlet var btnLike : UIButton?
    @IBOutlet var btnComment : UIButton?
    @IBOutlet var btnView : UIButton?
    @IBOutlet var imgUser : UIImageView?
    @IBOutlet var txtComment : UITextField?
    var currentdiscussion : Discussion?
    var currentComments : CommentList?
    @IBOutlet var tblView : UITableView?
    var sharedManager : Globals = Globals.sharedInstance
    var heightComment:CGFloat = 80.0
    var heightTitle:CGFloat = 0.0
    var heightDescription:CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        callView()
        setMenu()
        // Do any additional setup after loading the view.
    }

    func setupView(){
        self.lblName?.text = (currentdiscussion?.firstname)! + " " + (currentdiscussion?.lastname)!
        self.lblPosition?.text = (currentdiscussion?.designation)!
        self.lblTitle?.text = (currentdiscussion?.title)!
        self.lblDescription?.text = (currentdiscussion?.fdescription)!
        self.btnLike?.setTitle(currentdiscussion!.vote_count, forState: UIControlState.Normal)
        self.btnComment?.setTitle(currentdiscussion!.comment_count, forState: UIControlState.Normal)
        
        let iStrTitle:String = (self.lblTitle?.text)!
        let iStrDescription:String = (self.lblDescription?.text)!
        
        let heightTitle = iStrTitle.heightWithConstrainedWidth(UIScreen.mainScreen().bounds.size.width-32, font: self.lblTitle!.font)
        let heightDescription = iStrDescription.heightWithConstrainedWidth(UIScreen.mainScreen().bounds.size.width-32, font: (self.lblDescription?.font)!)
        self.heightTitle = heightTitle
        self.heightDescription = heightDescription
        
        
        
        
        let imgVwLike = UIImageView.init(image: UIImage(named:"ic_like"))
        imgVwLike.image = imgVwLike.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        btnLike!.setImage(imgVwLike.image, forState: UIControlState.Normal)
        if currentdiscussion!.vote == "1" {
           btnLike!.setImage(UIImage(named:"like_icon_active"), forState: .Normal)
        }else {
            btnLike!.tintColor = UIColor(red: 192/255.0, green: 192/255.0, blue: 192/255.0, alpha:1.0)
        }
        
        
//        if currentdiscussion!.vote == "1" {
//            let imgVwLike = UIImageView.init(image: UIImage(named:"like_icon_active"))
//            btnLike!.setImage(imgVwLike.image, forState: UIControlState.Normal)
//            btnLike!.tintColor = UIColor.greenColor()
//        }else {
//            let imgVwLike = UIImageView.init(image: UIImage(named:"like_icon_active"))
//            btnLike!.setImage(imgVwLike.image, forState: UIControlState.Normal)
//            btnLike!.tintColor = UIColor(red: 192/255.0, green: 192/255.0, blue: 192/255.0, alpha:1.0)
//        }
        
        self.btnView?.setTitle(currentdiscussion!.view_count, forState: UIControlState.Normal)
        
        let url:String = (Constants.ImagePaths.kAttendeeImagePath + (currentdiscussion!.profile_img) )
        
        dispatch_async(dispatch_get_main_queue()) {
            self.imgUser?.layer.cornerRadius = (self.imgUser?.frame.size.width)! / 2
            self.imgUser?.clipsToBounds = true
        }
        let tmpDate : NSDate = NSDate(timeIntervalSince1970:Double(currentdiscussion!.create_time_mili)!/1000)
        
        let dtFormatter : NSDateFormatter = NSDateFormatter()
        dtFormatter.dateFormat = "dd MMMM, YYYY"
        
        self.lblDate?.text = dtFormatter.stringFromDate(tmpDate)
        self.imgUser?.sd_setImageWithURL(NSURL(string: url), placeholderImage: UIImage(named: "login_default"))
        loadComments()
    }
    
 
    func loadComments(){
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                
                //var userFetch = "3113"
                let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : Constants.eventInfo.kEventId,"user_id" : userFetch, "dfid" : String(currentdiscussion!.id)], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.DISCUSSION_COMMENTS + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    if JSON["status"] == "Success"
                    {
                        self.currentComments = Mapper<CommentList>().map(JSON.rawValue)
                        self.tblView?.reloadData()
                    }else {
//                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
   @IBAction func actionLike(sender : UIButton) {
      
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            
            
            do {
                //var userFetch = "3113"
                if !sharedManager.isUserLoggedIn {
                     SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
                    return
                }
                
                if sharedManager.userCommunityExist == 0 {
                    SPGUtility.sharedInstance().showAlert("Please join the community", titleMessage: "", viewController: self)
                    return
                }
                
                SwiftLoader.show(animated: true)
                
                let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["df_id" : self.currentdiscussion!.id,"user_id" : userFetch], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.DISCUSSION_LIKE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    if JSON["status"] == "Success"
                    {
                        if JSON["isLike"] == 1
                        {
                            sender.setImage(UIImage(named:"like_icon_active"), forState: .Normal)
                            sender.setTitle(String(Int((sender.titleLabel?.text)!)! + 1) , forState: UIControlState.Normal)
                        }
                        else {
                            sender.setImage(UIImage(named: "ic_like"), forState: UIControlState.Normal)
                            sender.setTitle(String(Int((sender.titleLabel?.text)!)! - 1) , forState: UIControlState.Normal)
                        }
                        
                        // self.msgList = Mapper<DiscussionList>().map(JSON.rawValue)
                        self.tblView?.reloadData()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    @IBAction func postComments(){
        
        if !sharedManager.isUserLoggedIn {
            SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
            return
        }
        if sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You need to join community", titleMessage: "", viewController: self)
            return
        }
        
        if self.txtComment?.text == ""
        {
            SPGUtility.sharedInstance().showAlert("Please enter valid comment", titleMessage: "", viewController: self)
            return
        }
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                //var userFetch = "3113"
                let userFetch = sharedManager.currentUserID
//                let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : Constants.eventInfo.kEventId,"user_id" : userFetch, "dfid" : String(currentdiscussion!.id),"comment":String(self.txtComment?.text)], options: NSJSONWritingOptions.init(rawValue: 0))
                let url = (Constants.URL.DISCUSSION_POST_COMMENT)
                let str = String(UTF8String: (self.txtComment?.text)!)?.base64Encoded()
                APICall.requestPOSTURL(url, params: ["event_id" : Constants.eventInfo.kEventId,"user_id" : userFetch, "dfid" : String(currentdiscussion!.id),"comment":str!], headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    if JSON["status"] == "Success"
                    {
                        self.txtComment?.text = ""
                        self.loadComments()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    func callView(){
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                if !sharedManager.isUserLoggedIn {
                    return
                }
                //var userFetch = "3113"
                let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : Constants.eventInfo.kEventId,"user_id" : userFetch, "dfid" : String(currentdiscussion!.id)], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.DISCUSSION_FOUM_VIEW + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    if JSON["status"] == "Success"
                    {
                        self.tblView?.reloadData()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(DiscussionVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        self.navigationItem.title = "DISCUSSION INFO"
        
    }
    func back_btn_click(sender:UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard (self.currentComments != nil) else {
            return 0
        }
        return (self.currentComments?.commentList.count)!
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if heightComment > 15 {
            return heightComment + (80-15)
        }
        return 80;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : CommentCell = tableView.dequeueReusableCellWithIdentifier("cell1") as! CommentCell
            cell.lblName?.text = (self.currentComments?.commentList[indexPath.row].firstname)! + " " + (self.currentComments?.commentList[indexPath.row].lastname)!
        cell.lblComment?.text = self.currentComments?.commentList[indexPath.row].comment
        
        let iStrComment:String = (cell.lblComment?.text)!
        let iHeightComment:CGFloat = iStrComment.heightWithConstrainedWidth(UIScreen.mainScreen().bounds.size.width - 98, font: (cell.lblComment?.font)!)
        heightComment = iHeightComment
        let url:String = (Constants.ImagePaths.kAttendeeImagePath + ( self.currentComments?.commentList[indexPath.row].profile_img)! )
        
        dispatch_async(dispatch_get_main_queue()) {
            cell.imgUser?.layer.cornerRadius = (self.imgUser?.frame.size.width)! / 2
            cell.imgUser?.clipsToBounds = true
        }
        cell.imgUser?.sd_setImageWithURL(NSURL(string: url), placeholderImage: UIImage(named: "login_default"))
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 300
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
