//
//  HB_ScheduleQuestionsCell.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 19/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_ScheduleQuestionsCell: UITableViewCell {

    @IBOutlet weak var txtVWQuestions: UITextView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet var lblName : UILabel?
    @IBOutlet var lblPosition : UILabel?
    @IBOutlet var btnLike : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
