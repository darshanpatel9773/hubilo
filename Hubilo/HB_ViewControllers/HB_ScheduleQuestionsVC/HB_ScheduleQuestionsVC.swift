//
//  HB_ScheduleQuestionsVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 19/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
import SwiftValidators
class HB_ScheduleQuestionsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblQuestions: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var txtVWQuestion: PlaceholderTextView!
    var mutArrQuestions:NSMutableArray?
    var sharedManager : Globals = Globals.sharedInstance
    var agendaId : Int = 0
    var popular :[Questions] = []
    var recent :[Questions] = []
    var isRecent : Bool = true
    let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    @IBOutlet weak var btnPopular: UIButton!
    
    @IBOutlet weak var btnRecent: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mutArrQuestions=["Hi how are you?","Hi how are you? Hi how are you? Hi how are you? Hi how are you? Hi how are you? Hi how are you? Hi how are you? Hi how are you?","Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?Hi how are you?"]
        getQuestions()
        btnSend.backgroundColor = iAppDelegate.defaultColor
        setMenu()
        
        btnPopular.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
        btnPopular.backgroundColor = UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0)
        
        btnRecent.setTitleColor(UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0), forState: .Normal)
        btnRecent.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
    // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if isRecent {
        guard self.recent.count > 0 else {
            return 0
        }
        return self.recent.count
    }
    else {
        guard self.popular.count > 0 else {
            return 0
        }
        return self.popular.count
    }
}

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func btnLikePressed(sender:UIButton) {
        
        if !sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You need to be login first", titleMessage: "", viewController: self)
            return
        }
        
        let iStrUserID:String = sharedManager.currentUserID
        
        
        var tmpQuestion : Questions
        if isRecent {
            tmpQuestion  = recent[sender.tag]
        }
        else {
            tmpQuestion  = popular[sender.tag]
        }
        
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["question_id" :"\(tmpQuestion.question_id)" ,"user_id" : iStrUserID], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.QUESTION_LIKE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
           SPGWebService.callGETWebService(NSURL(string:url)!, WSCompletionBlock: { (data, error) in
            SwiftLoader.hide()
            if error != nil {
                print(error)
            }else {
                 let iMutDictData = data as! NSMutableDictionary
                
                let isLike : Int = iMutDictData.objectForKey("isLike") as! Int
                var iIntLikes =  (tmpQuestion.likes as NSString).intValue
                if isLike == 0 {
                    sender.setImage(UIImage(named:"like_icon"), forState: .Normal)
                    iIntLikes = iIntLikes - 1
                    tmpQuestion.is_like = 0
                }else {
                    sender.setImage(UIImage(named:"like_icon_active"), forState: .Normal)
                    iIntLikes = iIntLikes + 1
                    tmpQuestion.is_like = 1
                }
                tmpQuestion.likes = "\(iIntLikes)"
//                if iMutDictData.objectForKey("status") == "Success" {
//                    
//                }
                self.tblQuestions.reloadData()
            }
            
           })
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
        }
    
    }

func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
        let cell:HB_ScheduleQuestionsCell = tableView.dequeueReusableCellWithIdentifier("HB_ScheduleQuestionsCell", forIndexPath: indexPath) as! HB_ScheduleQuestionsCell
    var tmpQuestion : Questions
     if isRecent {
        tmpQuestion  = recent[indexPath.row]
    }
     else {
        tmpQuestion  = popular[indexPath.row]
    }
    print(tmpQuestion.likes)
    
        cell.selectionStyle = .None
        cell.txtVWQuestions.text=tmpQuestion.question
        cell.lblName?.text=tmpQuestion.firstname + tmpQuestion.lastname
        if tmpQuestion.designation.characters.count != 0 {
            cell.lblPosition?.text=tmpQuestion.designation + " at " + tmpQuestion.organization
        }
    
    if tmpQuestion.is_like == 0 {
           cell.btnLike!.setImage(UIImage(named:"like_icon"), forState: .Normal)
        }
    else {
        cell.btnLike!.setImage(UIImage(named:"like_icon_active"), forState: .Normal)
    }

        cell.btnLike?.setTitle(tmpQuestion.likes, forState: UIControlState.Normal)
        cell.imgProfile?.layer.cornerRadius=20
        cell.imgProfile?.clipsToBounds = true
    cell.btnLike?.tag = indexPath.row
    cell.btnLike?.addTarget(self, action: #selector(HB_ScheduleQuestionsVC.btnLikePressed(_:)), forControlEvents: .TouchUpInside)
//        cell.imgProfile?.sd_setImageWithURL(NSURL(string: Constants.ImagePaths.kAttendeeImagePath + tmpQuestion.profile_img ), placeholderImage: UIImage(named: "login_default"))
    
    if tmpQuestion.profile_img.characters.count > 0 {
        let url:String = Constants.ImagePaths.kAttendeeImagePath + tmpQuestion.profile_img
        let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
        imgVw.setImageWithString(tmpQuestion.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
        cell.imgProfile.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
    }else {
        let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
       imgVw.setImageWithString(tmpQuestion.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
        cell.imgProfile.image = imgVw.image
    }

    
    // Configure the cell...
        //cell.textLabel?.text = "Row \(indexPath.row)"
        return cell
   
}
    @IBAction func recentChanged (sender : AnyObject) {
        let segment : UISegmentedControl = sender as! UISegmentedControl
        if  segment.selectedSegmentIndex == 1 {
            isRecent = true
            self.tblQuestions.reloadData()
        }
        else {
            isRecent = false
            self.tblQuestions.reloadData()
        }
        
    }
    
    @IBAction func onTapRecentBtn(sender: AnyObject) {
        
        if !btnRecent.selected
        {
        
        btnPopular.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
        btnPopular.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        
        btnRecent.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
        btnRecent.backgroundColor = UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0)
        }
        isRecent = true
        self.tblQuestions.reloadData()
        
    }
    
    
    @IBAction func onTapPopularBtn(sender: AnyObject) {
        if !btnPopular.selected
        {

        
        btnPopular.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
        btnPopular.backgroundColor = UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0)
        
        btnRecent.setTitleColor(UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0), forState: .Normal)
        btnRecent.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        }
        isRecent = false
        self.tblQuestions.reloadData()
    }
    
    
func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
{
    return 64 + heightForView(self.recent[indexPath.row].question)
    
}

    func heightForView(text:String) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width-16, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = UIFont.systemFontOfSize(12.0)
        label.text = text
        
        label.sizeToFit()
        return label.frame.height + 20
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var tmpQuestion : Questions
        if isRecent {
            tmpQuestion  = recent[indexPath.row]
        }
        else {
            tmpQuestion  = popular[indexPath.row]
        }
        
        let tmpDicData:NSMutableDictionary = NSMutableDictionary()
        tmpDicData.setObject(tmpQuestion.firstname, forKey: "firstname")
        tmpDicData.setObject(tmpQuestion.lastname, forKey: "lastname")
        tmpDicData.setObject(tmpQuestion.user_id, forKey: "user_id")
        tmpDicData.setObject(tmpQuestion.profile_img, forKey: "profile_img")
        tmpDicData.setObject(tmpQuestion.linkedin_public_url, forKey: "linkedin_public_url")
        tmpDicData.setObject(tmpQuestion.is_bookmark, forKey: "is_bookmark")
        tmpDicData.setObject(tmpQuestion.fb_id, forKey: "fb_id")
        tmpDicData.setObject(tmpQuestion.designation, forKey: "designation")
        tmpDicData.setObject(tmpQuestion.aboutme, forKey: "aboutme")
        tmpDicData.setObject(tmpQuestion.organization, forKey: "organization")
        tmpDicData.setObject(tmpQuestion.twitter_public_url, forKey: "twitter_public_url")
  
        
        let objAttendInfoVC : HB_AttendeeInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_AttendeeInfoVC") as! HB_AttendeeInfoVC
        objAttendInfoVC.dicAttendeeDInfo = NSMutableDictionary()
        objAttendInfoVC.dicAttendeeDInfo = tmpDicData
        self.navigationController?.pushViewController(objAttendInfoVC, animated: true)
    }
    
    //let font = UIFont(name: "Helvetica", size: 20.0)
func setMenu() {
    let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
    myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
    myMenubtn.frame = CGRectMake(0, 0, 24, 24)
    myMenubtn.addTarget(self, action: #selector(HB_ExhibitorsInfoVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    
    let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
    self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
    self.navigationItem.title="SCHEDULE QUESTIONS"
    
    
}
    func  getQuestions(){
        let param : NSDictionary =                  ["event_id":Constants.eventInfo.kEventId,"agenda_id":agendaId,"question":txtVWQuestion.text,"user_id":sharedManager.currentUserID]
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.QUESTION_LIST + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    self.recent = [Questions]()
                    self.popular = [Questions] ()
                    if JSON["status"] == "Success"
                    {
                        print(JSON)
                        for question in JSON["recent"].array!
                        {
                            let tmpQuestion : Questions = Questions()
                            tmpQuestion.user_id = question["user_id"].rawString()!
                            tmpQuestion.question = question["question"].rawString()!
                            tmpQuestion.likes = question["likes"].rawString()!
                            tmpQuestion.question_id = question["question_id"].rawString()!
                            tmpQuestion.firstname = question["firstname"].rawString()!
                            tmpQuestion.lastname = question["lastname"].rawString()!
                            tmpQuestion.profile_img = question["profile_img"].rawString()!
                            tmpQuestion.designation = question["designation"].rawString()!
                            tmpQuestion.aboutme = question["aboutme"].rawString()!
                            tmpQuestion.linkedin_public_url = question["linkedin_public_url"].rawString()!
                            tmpQuestion.fb_id = question["fb_id"].rawString()!
                            tmpQuestion.twitter_public_url = question["twitter_public_url"].rawString()!
                            tmpQuestion.create_time_mili = question["create_time_mili"].rawString()!
                            tmpQuestion.organization = question["organization"].rawString()!
                            tmpQuestion.is_like = question["is_like"].intValue
                            tmpQuestion.is_bookmark = question["is_bookmark"].intValue
                            self.recent.append(tmpQuestion)
                        }
                        
                        for question in JSON["popular"].array!
                        {
                            let tmpQuestion : Questions = Questions()
                            tmpQuestion.user_id = question["user_id"].rawString()!
                            tmpQuestion.question = question["question"].rawString()!
                            tmpQuestion.likes = question["likes"].rawString()!
                            tmpQuestion.question_id = question["question_id"].rawString()!
                            tmpQuestion.firstname = question["firstname"].rawString()!
                            tmpQuestion.lastname = question["lastname"].rawString()!
                            tmpQuestion.profile_img = question["profile_img"].rawString()!
                            tmpQuestion.designation = question["designation"].rawString()!
                            tmpQuestion.aboutme = question["aboutme"].rawString()!
                            tmpQuestion.linkedin_public_url = question["linkedin_public_url"].rawString()!
                            tmpQuestion.fb_id = question["fb_id"].rawString()!
                            tmpQuestion.twitter_public_url = question["twitter_public_url"].rawString()!
                            tmpQuestion.create_time_mili = question["create_time_mili"].rawString()!
                            tmpQuestion.organization = question["organization"].rawString()!
                            tmpQuestion.is_like = question["is_like"].intValue
                            tmpQuestion.is_bookmark = question["is_bookmark"].intValue
                            self.popular.append(tmpQuestion)
                        }
                        self.tblQuestions.reloadData()
                        
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    @IBAction func send_btn_click(sender: AnyObject) {
        if !sharedManager.isUserLoggedIn {
            SPGUtility.sharedInstance().showAlert("You need to login.", titleMessage: "", viewController: self)
            return;
        }
        if sharedManager.userCommunityExist == 0
        {
            SPGUtility.sharedInstance().showAlert("Please Join the community", titleMessage: "", viewController: self)
            return;
        }
        
        if Validator.isEmpty(txtVWQuestion.text){
            SPGUtility.sharedInstance().showAlert("Please enter valid question.", titleMessage: "", viewController: self)
            return;
        }
        let strQuestion = String(txtVWQuestion.text).base64Encoded()

        let param : NSDictionary =                  ["event_id":Constants.eventInfo.kEventId,"agenda_id":agendaId,"question":strQuestion,"user_id":sharedManager.currentUserID]
        print(param)
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.POST_QUESTION + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    
                    
                    if JSON["status"] == "Success"
                    {
                        SPGUtility.sharedInstance().showAlert("Question posted successfully.", titleMessage: "", viewController: self)
                        self.txtVWQuestion.text=""
                        self.getQuestions()
                        
                        
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
        //tblQuestions.reloadData()
    }
  
@IBAction func back_btn_click(sender: AnyObject) {
    //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
    
    self.navigationController?.popViewControllerAnimated(true)
    
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
