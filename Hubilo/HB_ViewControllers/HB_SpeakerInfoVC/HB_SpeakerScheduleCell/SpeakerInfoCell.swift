//
//  SpeakerInfoCell.swift
//  Hubilo
//
//  Created by Aadil on 7/10/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class SpeakerInfoCell: UITableViewCell {
    @IBOutlet var imgPerson : UIImageView!
    @IBOutlet var lblName : UILabel?
    @IBOutlet var lblDeveloper : UILabel?
    @IBOutlet var lblPosition : UILabel?
    @IBOutlet var btnLikes : UIButton?
    @IBOutlet var btnFacebook : UIButton?
    @IBOutlet var btnTwitter : UIButton?
    @IBOutlet var btnIn : UIButton?
    @IBOutlet weak var cosmosView:CosmosView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
