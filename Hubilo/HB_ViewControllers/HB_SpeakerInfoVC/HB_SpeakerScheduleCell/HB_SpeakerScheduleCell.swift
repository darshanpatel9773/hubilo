//
//  HB_SpeakerScheduleCell.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 18/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_SpeakerScheduleCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var heightConstraintForSpeakers: NSLayoutConstraint!
    @IBOutlet weak var btnLike: LikeButton!
    @IBOutlet weak var lbladdress: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblagendaName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnProfileImg5: UIButton!
    
    @IBOutlet weak var vwProfileImages: UIView!
    @IBOutlet weak var btnProfileImg4: UIButton!
    @IBOutlet weak var btnProfileImg3: UIButton!
    @IBOutlet weak var btnProfileImg2: UIButton!
    @IBOutlet weak var btnProfileImg1: UIButton!
    @IBOutlet weak var collectionVW:UICollectionView!
    var mutSpeakersDetail:[HB_SpeakerList]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if mutSpeakersDetail != nil {
            return mutSpeakersDetail.count
        }
        return 0
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let iCell:HB_SpeakerPhotoCell = collectionView.dequeueReusableCellWithReuseIdentifier("HB_SpeakerPhotoCell", forIndexPath: indexPath) as! HB_SpeakerPhotoCell
        let speaker:HB_SpeakerList = mutSpeakersDetail[indexPath.row]
        
        iCell.imgVwProfile.layer.cornerRadius = iCell.imgVwProfile.frame.size.width / 2
        iCell.imgVwProfile.clipsToBounds = true
        
        if speaker.speakerProfileImg != nil {
            let url:String = Constants.ImagePaths.kSpeakerImagePath+speaker.speakerProfileImg
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(speaker.speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            iCell.imgVwProfile.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(speaker.speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            iCell.imgVwProfile.image = imgVw.image
        }
        
        return iCell;
    }
    
}
