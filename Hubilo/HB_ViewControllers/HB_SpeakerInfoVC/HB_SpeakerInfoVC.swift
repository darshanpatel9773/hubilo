//
//  HB_SpeakerInfoVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 18/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
class HB_SpeakerInfoVC: UIViewController,UITableViewDelegate,UITableViewDataSource, UIWebViewDelegate{
    var modelSpeaker : HB_SpeakerList!
    var modelAgenda : HB_ModelAgenda!
    var iMutArrAgenda:NSMutableArray!
    var sharedManager : Globals = Globals.sharedInstance
    var isLoadMore: Bool = false;
    var webHeight : Int = 0
    var originalHeight : Int = 0
    let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    
    
    
    @IBOutlet weak var tblSpeakerInfo: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenu()
        
        iMutArrAgenda = NSMutableArray()
        
        getData()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        tblSpeakerInfo.reloadData()
    }
    func getData(){
        
        var iStrUserId:String = ""
        if sharedManager.isUserLoggedIn {
                iStrUserId =  sharedManager.currentUserID
        }
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" :Constants.eventInfo.kEventId,"speaker_id":String(self.modelSpeaker!.speakerId),"user_id":iStrUserId], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.SPEAKER_AGENDA + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                SPGWebService.callGETWebService(NSURL(string:url)!, WSCompletionBlock: { (data, error) in
                    //print(data)
                    SwiftLoader.hide()
                    if error != nil {
                        print(error)
                    }else {
                        let iMutDicAgenda:NSDictionary = data as! NSDictionary
                        self.iMutArrAgenda = (iMutDicAgenda.objectForKey("data") as! NSArray).mutableCopy() as! NSMutableArray
                        print(self.iMutArrAgenda)
                        self.tblSpeakerInfo.reloadData()
                    }
                })
//                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
//                    SwiftLoader.hide()
//                    
//                    
//                    
//                    if JSON["status"] == "Success"
//                    {
//                        self.modelAgenda = HB_ModelAgenda(fromDictionary: JSON.rawValue as! NSDictionary)
//                        self.tblSpeakerInfo?.reloadData()
//                    }else {
//                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
//                    }
//                    }, failure: { (NSError) in
//                        SwiftLoader.hide()
//                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
//                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
    }
    
    func changeTextColor(str:String) -> NSMutableAttributedString {
        let myString:NSString = str
        var length: Int
        var finalLen:Int
        length = myString.length
        finalLen = length - 7
        var myMutableString = NSMutableAttributedString()
        //let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let firstAttributes = [NSFontAttributeName:UIFont.systemFontOfSize(13)]
        
        myMutableString = NSMutableAttributedString(string: myString as
            String, attributes: firstAttributes)
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: NSRange(location:7,length:finalLen))
        
        return myMutableString
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if self.iMutArrAgenda.count != 0 {
            return 2 + self.iMutArrAgenda.count
        }
        return 2
//        guard (self.iMutArrAgenda != nil) else {
//            return 2
//        }
//        return 3
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell1:UITableViewCell?=UITableViewCell()
        if indexPath.row == 0
        {
            let cell:SpeakerInfoCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsContactCell", forIndexPath: indexPath) as! SpeakerInfoCell
            if modelSpeaker.speakerTwitterFollow != "" {
                //                cell.btnTwitter?.addTarget(self, action: #selector(actionTwitter), forControlEvents: UIControlEvents.TouchUpInside)
                cell.btnTwitter?.setImage(UIImage(named: "twitter_icon_active"), forState: UIControlState.Normal)
            }
            
            if modelSpeaker.speakerLinkedinLink != "" {
                
                //                cell.btnIn?.addTarget(self, action: #selector(actionIn), forControlEvents: UIControlEvents.TouchUpInside)
                cell.btnIn?.setImage(UIImage(named: "linkedin_icon_active"), forState: UIControlState.Normal)
            }
            
            
            
            cell.btnFacebook?.addTarget(self, action: #selector(actionFacebook), forControlEvents: UIControlEvents.TouchUpInside)
            
            
            cell.lblName?.text = modelSpeaker.speakerTitle + modelSpeaker.speakerName
            
            let myString: String = modelSpeaker.speakerName!
            var myStringArr = myString.componentsSeparatedByString(" ")
            var newString: String = "Google "
            newString = newString.stringByAppendingString(myStringArr[0])
            print(newString)
            var length: Int
            var finalLen:Int
            length = newString.characters.count
            finalLen = length - 6
            var myMutableString = NSMutableAttributedString()
            let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let firstAttributes = [NSFontAttributeName:UIFont.systemFontOfSize(13)]
            myMutableString = NSMutableAttributedString(string: newString as
                String, attributes: firstAttributes)
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: iAppDelegate.defaultColor, range: NSRange(location:6,length:finalLen))
            
            
            cell.lblPosition?.attributedText = myMutableString
            
            
            cell.lblDeveloper?.text=modelSpeaker.speakerDescription
            
            if modelSpeaker.speakerRating == "0"
            {
                cell.btnLikes?.setTitle(modelSpeaker.speakerRating, forState: UIControlState.Normal)
                cell.btnLikes?.setImage(UIImage(named: "rating_icon"), forState: UIControlState.Normal)
            }
            else
            {
                cell.btnLikes?.setTitle(modelSpeaker.speakerRating, forState: UIControlState.Normal)
            }
            
            cell.imgPerson.maskCircle(40)
            
            let url: String = Constants.ImagePaths.kSpeakerImagePath + modelSpeaker.speakerProfileImg!
            
            if url.characters.count > 0  {
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(modelSpeaker.speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cell.imgPerson!.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
            }else {
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(modelSpeaker.speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cell.imgPerson!.image = imgVw.image
            }
            
            if sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 1 {
                let speakerUserRating:Double = (modelSpeaker.speakerUserRating as NSString).doubleValue
                
                cell.cosmosView.rating = speakerUserRating
                
                
                cell.cosmosView.settings.updateOnTouch = true
                cell.cosmosView.didFinishTouchingCosmos = { rating in
                    
                    if ReachabilityCheck.isConnectedToNetwork() {
                        SwiftLoader.show(animated: true)
                        do {
                            let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" :Constants.eventInfo.kEventId,"user_id":self.sharedManager.currentUserID,"speaker_id":self.modelSpeaker!.speakerId!,"rating":"\(rating)"], options: NSJSONWritingOptions.init(rawValue: 0))
                            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                            let url = (Constants.URL.SPEAKER_RATING + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                            APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                                SwiftLoader.hide()
                                
                                if JSON["status"] == "Success"
                                {
                                    
                                    self.speakerAverageRating()
                                    
                                }else {
                                    //SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                                }
                                }, failure: { (NSError) in
                                    
                                    SwiftLoader.hide()
                                    //SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                            })
                            // here "jsonData" is the dictionary encoded in JSON data
                        } catch let error as NSError {
                            SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
                        }
                        
                        
                        
                    }
                    else
                    {
                        
                        SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
                    }
                    
                }
            }else{
                cell.cosmosView.settings.updateOnTouch = false
            }
            
            //            cell.imgPerson.sd_setImageWithURL(NSURL(string: Constants.ImagePaths.kSpeakerImagePath + modelSpeaker.speakerProfileImg!))
            
            cell.selectionStyle = .None
            // Configure the cell...
            //cell.textLabel?.text = "Row \(indexPath.row)"
            return cell
        }
        if indexPath.row == 1
        {
            let cell:SponsorDetailCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsDetailsCell", forIndexPath: indexPath) as! SponsorDetailCell
            cell.selectionStyle = .None
            let html = "<style>body{color: #aaaaaa;font-size: 15px;font-family:helvetica;}span{color: #aaaaaa!important;font-size: 15px;}p{color: #aaaaaa!important;font-size: 15px;}ul{color: #aaaaaa!important;font-size: 15px;}li{color: #aaaaaa!important;font-size: 15px;}</style>" + (modelSpeaker.speakerLongDescription)!
            cell.textViewDetail?.loadHTMLString(html, baseURL: nil)
            cell.btnMore?.addTarget(self, action: #selector(loadMore), forControlEvents: UIControlEvents.TouchUpInside)
            [cell.btnMore?.setTitleColor(iAppDelegate.defaultColor, forState: .Normal)]
            cell.textViewDetail?.delegate=self
            cell.textViewDetail?.scrollView.scrollEnabled=false
            if modelSpeaker.speakerLongDescription == "" {
                isLoadMore=true
                cell.hidden = true
            }
            return cell
            
        }
        if indexPath.row > 1
        {
            let cell:HB_SpeakerScheduleCell = tableView.dequeueReusableCellWithIdentifier("HB_SpeakerScheduleCell", forIndexPath: indexPath) as! HB_SpeakerScheduleCell
            let index = indexPath.row - 2
            cell.lblagendaName.text = self.iMutArrAgenda.objectAtIndex(index)["agenda_name"] as? String
//            let _interval :  NSTimeInterval = (self.iMutArrAgenda.objectAtIndex(index)["agenda_start_time_mili"] as! NSString).doubleValue
//
//            let date : NSDate = NSDate(timeIntervalSince1970: _interval)
//            let formatter : NSDateFormatter = NSDateFormatter()
//            formatter.locale = NSLocale.currentLocale()
//            formatter.dateFormat = "hh:mm"

            //let iStartDate:String = formatter.stringFromDate(date)
            
            var iStrAgendaStartTime:String = self.iMutArrAgenda.objectAtIndex(index)["agenda_start_time"] as! String
            
            var iStrAgendaEndTime:String = self.iMutArrAgenda.objectAtIndex(index)["agenda_end_time"] as! String
            
            let formatter : NSDateFormatter = NSDateFormatter()
            formatter.locale = NSLocale.currentLocale()
            formatter.dateFormat = "HH:mm:ss"
            
            let iStartDate:NSDate = formatter.dateFromString(iStrAgendaStartTime)!
            let iEndDate:NSDate = formatter.dateFromString(iStrAgendaEndTime)!
            
            formatter.dateFormat = "hh:mm"
            
            iStrAgendaStartTime = formatter.stringFromDate(iStartDate)
            iStrAgendaEndTime = formatter.stringFromDate(iEndDate)
            
            cell.lbladdress.text = self.iMutArrAgenda.objectAtIndex(index)["agendaLocation"] as? String
            
            
            
            cell.lblTime.text = iStrAgendaStartTime + "-" + iStrAgendaEndTime
            
            let iMutArrSpeakers:NSMutableArray = (self.iMutArrAgenda.objectAtIndex(index)["speakers"] as! NSArray).mutableCopy() as! NSMutableArray
            
            
            print(iMutArrSpeakers.count)
            
            cell.btnProfileImg5.layer.cornerRadius =   cell.btnProfileImg5.frame.size.width/2
            cell.btnProfileImg5.clipsToBounds = true
            
            cell.btnProfileImg1.imageView!.layer.cornerRadius =   cell.btnProfileImg1.imageView!.frame.size.width/2
            cell.btnProfileImg1.imageView!.clipsToBounds = true
            
            cell.btnProfileImg2.imageView!.layer.cornerRadius =   cell.btnProfileImg2.imageView!.frame.size.width/2
            cell.btnProfileImg2.imageView!.clipsToBounds = true
            
            cell.btnProfileImg3.imageView!.layer.cornerRadius =   cell.btnProfileImg3.imageView!.frame.size.width/2
            cell.btnProfileImg3.imageView!.clipsToBounds = true
            
            cell.btnProfileImg4.imageView!.layer.cornerRadius =   cell.btnProfileImg4.imageView!.frame.size.width/2
            cell.btnProfileImg4.imageView!.clipsToBounds = true
            
            cell.btnProfileImg1.hidden = true
            cell.btnProfileImg2.hidden = true
            cell.btnProfileImg3.hidden = true
            cell.btnProfileImg4.hidden = true
            cell.btnProfileImg5.hidden = true
            
            for iCounter in 0 ..< iMutArrSpeakers.count {
                
                print(iMutArrSpeakers.objectAtIndex(iCounter))
                
                let iImageView:UIImageView = UIImageView()
                
                if iMutArrSpeakers.objectAtIndex(iCounter).valueForKey("speaker_profile_img") != nil {
                    let url:String = (Constants.ImagePaths.kSpeakerImagePath + ((iMutArrSpeakers.objectAtIndex(iCounter).valueForKey("speaker_profile_img"))! as! String))
                    let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                    imgVw.setImageWithString(iMutArrSpeakers.objectAtIndex(iCounter).valueForKey("speaker_name") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                    iImageView.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
                }else {
                    let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                    imgVw.setImageWithString(iMutArrSpeakers.objectAtIndex(iCounter).valueForKey("speaker_name") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                    iImageView.image = imgVw.image
                }
                
                switch iCounter {
                case 0:
                    cell.btnProfileImg1.hidden = false
                    cell.btnProfileImg1.setImage(iImageView.image, forState: .Normal)
                    break;
                    
                default:
                    break;
                }
                
            }
            
           
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = NSTimeZone.systemTimeZone()
            
            let calendar = NSCalendar.currentCalendar()
            print(self.iMutArrAgenda.objectAtIndex(index)["agenda_date"] as! String)
            let date = dateFormatter.dateFromString(self.iMutArrAgenda.objectAtIndex(index)["agenda_date"] as! String)
            let dateComponents = calendar.components(.Day, fromDate: date!)
            let numberFormatter = NSNumberFormatter()
            
            if #available(iOS 9.0, *) {
                numberFormatter.numberStyle = .OrdinalStyle
            } else {
                // Fallback on earlier versions
            }
            
            let day = numberFormatter.stringFromNumber(dateComponents.day)
            
            
            dateFormatter.dateFormat = "MMM, yyyy"
            
            
            let dateString = "\(day!) \(dateFormatter.stringFromDate(date!))"
            
            cell.lblDate.text = dateString
            
            
            
            
            
            cell.selectionStyle = .None
            return cell
            
        }
        return cell1!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 225
            
        }
        if indexPath.row == 1
        {
            if isLoadMore{
                return CGFloat(webHeight) + 30
            }
            return 97
        }
        if indexPath.row > 1
        {
            return 145
        }
        return 0
        
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
      
        if indexPath.row > 1 {
              let ObjScheduleDetail : HB_ScheduleInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_ScheduleInfoVC") as! HB_ScheduleInfoVC
            ObjScheduleDetail.modelSchedule = HB_AgendaList(fromDictionary: self.iMutArrAgenda.objectAtIndex(indexPath.row-2) as! NSDictionary)
             self.navigationController?.pushViewController(ObjScheduleDetail, animated: true)
        }
        
            
//        ObjScheduleDetail.modelSchedule =   self.iMutArrAgenda.objectAtIndex(indexPath.row) as! HB_AgendaList
       

    }
    
    func speakerAverageRating() {
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["speaker_id":self.modelSpeaker!.speakerId!], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.SPEAKER_RATING_UPDATE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                SwiftLoader.hide()
                if JSON["status"] == "Success"
                {
                    
                    //                    self.modelAgenda = HB_ModelAgenda(fromDictionary: JSON.rawValue as! NSDictionary)
                }else {
                    //SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                }
                }, failure: { (NSError) in
                    
                    SwiftLoader.hide()
                    //SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
            })
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
        }
    }
    
    func actionTwitter(){
        let url:NSURL = NSURL(string: self.modelSpeaker.speakerTwitterFollow!)!
        
        if (UIApplication.sharedApplication().canOpenURL(url))
        {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    func actionFacebook() {
        let url:NSURL = NSURL(string: "http://facebook.com")!
        
        if (UIApplication.sharedApplication().canOpenURL(url))
        {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    func actionIn(){
        let url:NSURL = NSURL(string: self.modelSpeaker.speakerLinkedinLink!)!
        
        if (UIApplication.sharedApplication().canOpenURL(url))
        {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_ExhibitorsInfoVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="SPEAKER INFO"
        
        
    }
    
    
    @IBAction func onTapGoogleButton(sender: AnyObject) {
        var temp = modelSpeaker.speakerDescription! as NSString
        temp = temp .stringByReplacingOccurrencesOfString(" ", withString: "")
        
        let myString: String = modelSpeaker.speakerName!
        var myStringArr = myString.componentsSeparatedByString(" ")
        
        print("https://www.google.co.in/search?site=webhp&source=hp&q=" + myStringArr [0] + (temp as String))
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.google.co.in/search?site=webhp&source=hp&q=" + myStringArr [0] + (temp as String))!)
    }
    
    
    @IBAction func back_btn_click(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction func onTapLinkedin(sender: AnyObject)
    {
        if modelSpeaker.speakerLinkedinLink != "" {
            let url:NSURL = NSURL(string: self.modelSpeaker.speakerLinkedinLink!)!
            
            if (UIApplication.sharedApplication().canOpenURL(url))
            {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Linkedin Profile Not Available", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapTwitter(sender: AnyObject)
    {
        if modelSpeaker.speakerTwitterFollow != "" {
            let url:NSURL = NSURL(string: self.modelSpeaker.speakerTwitterFollow!)!
            
            if (UIApplication.sharedApplication().canOpenURL(url))
            {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Twitter Profile Not Available", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadMore(sender : AnyObject) {
        let tmpButton : UIButton = sender as! UIButton
        
        isLoadMore = !isLoadMore
        if isLoadMore == true
        {
            tmpButton.setTitle("LESS", forState: .Normal)
            webHeight = originalHeight
            
        }
        else {
            tmpButton.setTitle("MORE", forState: .Normal)
            webHeight=97
        }
        
        self.tblSpeakerInfo!.reloadData()
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        let webViewHeight:NSString = webView.stringByEvaluatingJavaScriptFromString("document.body.scrollHeight;")!
        
        self.originalHeight = webViewHeight.integerValue
    }
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        self.originalHeight=97
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
