//
//  HB_EditProfileVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 12/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
import KYDrawerController
import Alamofire

class HB_EditProfileVC: UIViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var btnProfessionalProfile: UIButton!
    @IBOutlet weak var btnPersonalProfile: UIButton!
    @IBOutlet var user_profile_pic: UIImageView!
    @IBOutlet var btn_user_profile_pic: UIButton!
    @IBOutlet weak var vwPersonalProfile: UIView!
    @IBOutlet var Personal_View: UIView!
    @IBOutlet var Professional_View: UIView!
    var iIntTagSelection:Int = 1
    var sharedManager : Globals = Globals.sharedInstance
    
    @IBOutlet var txt_firstname : UITextField?
    @IBOutlet var txt_lastname : UITextField?
    @IBOutlet var txt_email : UITextField?
    @IBOutlet var txt_DD : UITextField?
    @IBOutlet var txt_MM : UITextField?
    @IBOutlet var txt_YYYY : UITextField?
    @IBOutlet var txt_country : UITextField?
    @IBOutlet var txt_city : UITextField?
    @IBOutlet var txt_aboutMe : UITextView?
    @IBOutlet var txt_organization : UITextField?
    @IBOutlet var txt_designation : UITextField?
    @IBOutlet var txt_industry : UITextField?
    @IBOutlet var txt_website : UITextField?
    @IBOutlet var txt_interest : UITextField?
    
    var mutIndustryData:NSMutableArray!
    var mutInterestData:NSMutableArray!
    var selectedIndustryId:String!
    var selectedIndustryName:String!
    var selectedInterestId:String!
    var selectedInterestName:String!
    var myDict:NSMutableDictionary!
    var dictIndustry:NSDictionary!
    var myInterest:HB_Interest!
    var myIndustry:HB_Industry!
    var mutArrInterest:NSMutableArray!
    var iStrIndustryId:String!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = false
        user_profile_pic.layer.cornerRadius = 50
        user_profile_pic.clipsToBounds = true
        
        btn_user_profile_pic.layer.cornerRadius = 50
        btn_user_profile_pic.clipsToBounds = true
        
        btnPersonalProfile.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
        btnPersonalProfile.backgroundColor = UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0)
        
        btnProfessionalProfile.setTitleColor(UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0), forState: .Normal)
        btnProfessionalProfile.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        setMenu()
        var userId : String = "0"
        if sharedManager.isUserLoggedIn{
            userId=sharedManager.currentUserID
        }
        
        mutIndustryData = NSMutableArray()
        mutInterestData = NSMutableArray()
        mutArrInterest = NSMutableArray()
        
        getUserData(userId)
        
        // NsNotification Declared
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HB_EditProfileVC.selectedCountry(_:)), name:"selectedCounty", object:myDict)
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HB_EditProfileVC.selectedIndustry(_:)), name:"selectedIndustry", object:dictIndustry)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HB_EditProfileVC.selectedInterest(_:)), name:"selectedInterest", object:dictIndustry)
    }
    
    func selectedCountry(notification: NSNotification){
        let dict = notification.object as! NSDictionary
        if let selcountry = dict["country"] {
            txt_country?.text = selcountry as? String
        }
    }
    
    func selectedIndustry(notification: NSNotification){
        let dict = notification.object as! NSDictionary
        myIndustry = HB_Industry(fromDictionary: dict)
        txt_industry?.text = myIndustry.industryName
        self.iStrIndustryId = myIndustry.industryId
    }
    
    func selectedInterest(notification: NSNotification){
        mutArrInterest = NSMutableArray()
        let iArrInterest = notification.object as! NSMutableArray
        mutArrInterest.addObjectsFromArray(iArrInterest as [AnyObject])
        var iStrInterest:String!
        
        for iCounter in 0 ..< iArrInterest.count {
            let iArrData:HB_Interest = iArrInterest.objectAtIndex(iCounter) as! HB_Interest
            if iCounter == 0 {
                iStrInterest = iArrData.interestName
            }else {
                iStrInterest = iStrInterest + "," + iArrData.interestName
            }
        }
        txt_interest!.text = iStrInterest
//        myIndustry = HB_Industry(fromDictionary: dict)
//        txt_industry?.text = myIndustry.industryName
    }
    
    //MARK : Button Click Events
    
    @IBAction func profile_btn_click(sender: UIButton) {
        
        if sender == btnPersonalProfile {
            if !btnPersonalProfile.selected
            {
                Professional_View.hidden=true
                Personal_View.hidden = false
                self.vwPersonalProfile.hidden=false
                btnPersonalProfile.selected=true
                btnProfessionalProfile.selected=false
                btnPersonalProfile.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
                btnPersonalProfile.backgroundColor = UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0)
                
                btnProfessionalProfile.setTitleColor(UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0), forState: .Normal)
                btnProfessionalProfile.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            }
        }
        else
        {
            if !btnProfessionalProfile.selected
            {
                Personal_View.hidden = true
                Professional_View.hidden = false
                btnPersonalProfile.selected=false
                self.vwPersonalProfile.hidden=false
                btnProfessionalProfile.selected=true
                btnPersonalProfile.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
                btnPersonalProfile.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
                
                btnProfessionalProfile.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
                btnProfessionalProfile.backgroundColor = UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0)
            }
        }
    }
    
    @IBAction func saveButtonTouched(sender: AnyObject) {
        
        //Mandentory
        
        for iView in Personal_View.subviews {
            if iView.isKindOfClass(UITextField) {
                let tempTxt = iView as! UITextField
                if tempTxt == txt_firstname {
                    if tempTxt.text?.characters.count == 0 {
                         SPGUtility.sharedInstance().showAlert("Please enter first name", titleMessage: "", viewController: self)
                        return
                    }
                }else if tempTxt == txt_lastname {
                    if tempTxt.text?.characters.count == 0 {
                        SPGUtility.sharedInstance().showAlert("Please enter last name", titleMessage: "", viewController: self)
                        return
                    }
                }
            }
        }
        
        for iView in Professional_View.subviews {
            if iView.isKindOfClass(UITextField) {
                let tempTxt = iView as! UITextField
                if tempTxt == txt_designation {
                    if tempTxt.text?.characters.count == 0 {
                        SPGUtility.sharedInstance().showAlert("Please enter designation", titleMessage: "", viewController: self)
                        return
                    }
                }else if tempTxt == txt_organization {
                    if tempTxt.text?.characters.count == 0 {
                        SPGUtility.sharedInstance().showAlert("Please enter organization", titleMessage: "", viewController: self)
                        return
                    }
                }
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        //upload photo
        var iStrUserId = ""
        if sharedManager.isUserLoggedIn {
            iStrUserId = sharedManager.currentUserID
        }else {
            return
        }
        
        //Use image name from bundle to create NSData
      
        
        //Now use image to create into NSData format
        let imageData:NSData = UIImageJPEGRepresentation(user_profile_pic.image!,1.0)!
        let iStrImage:String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        
        let iFirstName:String = (txt_firstname?.text!)!
        let iLastName:String = (txt_lastname?.text!)!
        let iEmail:String = (txt_email?.text!)!
        let iCountry:String = (txt_country?.text!)!
        let iCity:String = (txt_city?.text!)!
        let iAbtMe:String = (txt_aboutMe?.text!)!
        let iOrganization:String = (txt_organization?.text!)!
        let iDesignation:String = (txt_designation?.text!)!
        let iWebsite:String = (txt_website?.text!)!
        
        
        //Date
        let iStrDD:String = (txt_DD?.text!)!
        let iStrMM:String = (txt_MM?.text!)!
        let iStrYYYY:String = (txt_YYYY?.text)!
        
        let iArrDate:NSArray = NSArray(objects: iStrYYYY,iStrMM,iStrDD)
        let iStrDate:String = iArrDate.componentsJoinedByString("-")
        
        //Interest
        var iStrInterestId:String = ""
        for iCounter in 0 ..< mutArrInterest.count {
            let iArrData:HB_Interest = mutArrInterest.objectAtIndex(iCounter) as! HB_Interest
            if iCounter == 0 {
                iStrInterestId = iArrData.interestId
            }else {
                iStrInterestId = iStrInterestId + "," + iArrData.interestId
            }
        }
        
        //Industry 
        
        
        
        
        let parameters = ["user_id":iStrUserId,"user_profile_image":iStrImage]
        
        
        if ReachabilityCheck.isConnectedToNetwork() {
            
            
            
            
            SwiftLoader.show(animated: true)
            
                Alamofire.request(.POST,Constants.URL.UPLOAD_IMAGE, parameters: parameters,encoding: .URL).response(completionHandler: { (request, response, data, error) in
                    print(request)
                    print(response)
                    let str = NSString(data:data!, encoding: NSUTF8StringEncoding)
                    print(str)
                    print(error)
                    let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    
                    iAppDelegate.imageViewUser.image = self.user_profile_pic.image
                    NSNotificationCenter.defaultCenter().postNotificationName("setUserImage", object: nil)
                    NSUserDefaults.standardUserDefaults().setObject(iStrImage, forKey: "profile_pic")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    
                })
            
            do {
                //let app : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                //var userFetch = "3113"
                //let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["firstname" :iFirstName ,"lastname" : iLastName, "dob" :iStrDate,"aboutme":iAbtMe,"designation":iDesignation,"industry":iStrIndustryId,"website":iWebsite,"interest":iStrInterestId,"organization":iOrganization,"user_id":iStrUserId,"city":iCity,"country":iCountry], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.EDIT_PROFILE_UPDATE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    //SetHeaderData
                    
                    self.sharedManager.currentUser.firstname = iFirstName
                    self.sharedManager.currentUser.lastname = iLastName
                    self.sharedManager.currentUser.emailid = iEmail
                    
                       NSNotificationCenter.defaultCenter().postNotificationName("SetHeaderData", object: nil)
                    
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
        }
        
        
    }
    
//    if let iSendData = sendUserDataServer(iStrFirstName:iFirstName, iStrLastName: iLastName, iStrDob: <#T##String#>, iStrAboutMe: iAbtMe, iStrDesignation: iDesignation, iStrIndustry: "pending", iStrWebsite: iWebsite, iStrInterest: <#T##String#>, iStrOrgnization: iOrganization, iStrUserId: iStrUserId, iStrCity: iCity, iStrCountry: iCountry) {
//        
//        }
//        else
//        {
//            
//        }
    
    
        
        
        //send data to server
        
        
    
    
    
    @IBAction func btnDateSelected(textFld:UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .Date
        textFld.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(HB_EditProfileVC.handleDatePicker(_:)), forControlEvents: .ValueChanged)
    }
    
    func handleDatePicker(sender:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let iStrDate:String = dateFormatter.stringFromDate(sender.date)
        let iArrDate:NSArray = iStrDate.componentsSeparatedByString("-")
        
        txt_DD?.text = iArrDate.objectAtIndex(0) as? String
        txt_MM?.text = iArrDate.objectAtIndex(1) as? String
        txt_YYYY?.text = iArrDate.objectAtIndex(2) as? String
    }
    
    @IBAction func getAllCountry(sender:UITextField) {
        iIntTagSelection = 1
        self.performSegueWithIdentifier("getDetailEdit", sender: self)
    }
    
    @IBAction func getSelectedIndustry(sender:UITextField) {
        iIntTagSelection = 2
        self.performSegueWithIdentifier("getDetailEdit", sender: self)
    }
    
    @IBAction func getSelectedInterest(sender:UITextField) {
        iIntTagSelection = 3
        self.performSegueWithIdentifier("getDetailEdit", sender: self)
    }
    
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
        
        
    }
    
    @IBAction func onTapSelectProfilePic(sender: AnyObject) {
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Select Photo", message: "", preferredStyle: .ActionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Take a Photo", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Choose From Album", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        user_profile_pic.image = image
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    
    //MARK: User Define
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_EditProfileVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.navigationItem.title="Edit Profile"
        
        
        let mySavebtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySavebtn.frame = CGRectMake(0, 0, 40, 30)
        [mySavebtn .setTitle("Save", forState: UIControlState.Normal)]
        mySavebtn.addTarget(self, action: #selector(HB_EditProfileVC.saveButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomSaveButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySavebtn)
        self.navigationItem.rightBarButtonItem = myCustomSaveButtonItem
    }
    
    func getUserData(userId: String){
        if ReachabilityCheck.isConnectedToNetwork() {
            
            SwiftLoader.show(animated: true)
            var userId : String = "1378"
            if sharedManager.isUserLoggedIn{
                userId=sharedManager.currentUserID
            }
            if let iUrl = getUserDetailData(userId) {
                SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                    
                    let iUserDetail:HBUserDetail = HBUserDetail.init(object:iData!)
                    self.txt_firstname?.text = iUserDetail.firstname
                    self.txt_lastname?.text = iUserDetail.lastname
                    self.txt_email?.text = iUserDetail.emailid
                    //                    self.txt_dob?.text = iUserDetail.dob
                    self.txt_country?.text = iUserDetail.country
                    self.txt_city?.text = iUserDetail.city
                    self.txt_aboutMe?.text = iUserDetail.aboutme
                    self.txt_organization?.text = iUserDetail.organization
                    self.txt_designation?.text = iUserDetail.designation
                    self.txt_industry?.text = iUserDetail.industryname
                    self.txt_website?.text = iUserDetail.website
                    self.txt_interest?.text = iUserDetail.interestname
                    self.selectedIndustryId = iUserDetail.industry
                    self.selectedIndustryName = iUserDetail.industryname
                    self.selectedInterestId = iUserDetail.interest
                    self.selectedInterestName = iUserDetail.interestname
                    self.Professional_View.hidden=true
                    self.Personal_View.hidden = false
                    self.vwPersonalProfile.hidden=false
                    self.btnPersonalProfile.selected=true
                    self.btnProfessionalProfile.selected=false
                    
                
                    
                    let iStrDob:String = iUserDetail.dob!
                    if iStrDob != "0000-00-00" {
                        let arrComponents:NSArray = iStrDob.componentsSeparatedByString("-")
                        self.txt_DD?.text = arrComponents.objectAtIndex(2) as? String
                        self.txt_MM?.text = arrComponents.objectAtIndex(1) as? String
                        self.txt_YYYY?.text = arrComponents.objectAtIndex(0) as? String
                    }
                    
                    if iUserDetail.profileImg != nil {
                        let url:String = (Constants.ImagePaths.kAttendeeImagePath + iUserDetail.profileImg!)
                        let imgVw:UIImageView = UIImageView(frame:self.user_profile_pic.frame)
                        imgVw.setImageWithString(iUserDetail.firstname! + " " + iUserDetail.lastname! , color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                        self.user_profile_pic.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
                    }else {
                        let imgVw:UIImageView = UIImageView(frame:self.user_profile_pic.frame)
                        imgVw.setImageWithString(iUserDetail.firstname! + " " + iUserDetail.lastname! , color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                        self.user_profile_pic.image = imgVw.image
                    }
                    
                    
                    self.getEditDetailList()
                    
                    let iArrInterest:NSArray = self.selectedInterestId.componentsSeparatedByString(",")
                    for iCounter in 0 ..< self.mutInterestData.count {
                        let iInterest:HB_Interest = HB_Interest(fromDictionary: self.mutInterestData.objectAtIndex(iCounter) as! NSDictionary) 
                        if iArrInterest.containsObject(iInterest.interestId) {
                            self.mutArrInterest.addObject(self.mutInterestData.objectAtIndex(iCounter))
                        }
                    }
                    
                    self.iStrIndustryId = iUserDetail.industry
                    
                    
                }
            } else {
                SPGUtility.sharedInstance().showAlert("oops, something went wrong with the URL", titleMessage: "", viewController: self)
            }
            
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
            
        }
    }
    
    func getUserDetailData(userDataID: String) -> NSURL? {
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_id" : userDataID], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.GET_ALL_DETAILS_SINGLE_USER + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getEditDetailList(){
        if ReachabilityCheck.isConnectedToNetwork() {
            let urL:NSURL = NSURL(string: Constants.URL.INDUSTRY_LIST)!
            SPGWebService.callGETWebService(urL) { (iData, iError) in
                if iError != nil {
                    
                }else {
                    self.mutIndustryData = NSMutableArray()
                    let iDataDict = (iData as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    self.mutIndustryData = (iDataDict.valueForKey("data") as! NSArray).mutableCopy() as! NSMutableArray
                }
            }
//            if iUrl = Constants.URL.INDUSTRY_LIST
//            {
//                
//            } else {
//                SPGUtility.sharedInstance().showAlert("oops, something went wrong with the URL", titleMessage: "", viewController: self)
//            }
            
            let urL1:NSURL = NSURL(string: Constants.URL.INTEREST_LIST)!
            SPGWebService.callGETWebService(urL1) { (iData, iError) in
                if iError != nil {
                    
                }else {
                    self.mutInterestData = NSMutableArray()
                    let iMutDictInterestData = (iData as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    self.mutInterestData = (iMutDictInterestData.valueForKey("data") as! NSArray).mutableCopy() as! NSMutableArray
                    
                }
                SwiftLoader.hide()
            }
//            if let iUrl = getInterestList() {
//                
//            } else {
//                SPGUtility.sharedInstance().showAlert("oops, something went wrong with the URL", titleMessage: "", viewController: self)
//            }
            
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    func sendUserDataServer(iStrFirstName:String,iStrLastName:String,iStrDob:String,iStrAboutMe:String,iStrDesignation:String,iStrIndustry:String,iStrWebsite:String,iStrInterest:String,iStrOrgnization:String,iStrUserId:String,iStrCity:String,iStrCountry:String) -> NSURL? {
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["firstname" : iStrFirstName,"lastname" : iStrLastName,"dob" : iStrDob,"aboutme" : iStrAboutMe,"designation" : iStrDesignation,"industry" : iStrIndustry,"website" : iStrWebsite,"interest" : iStrInterest,"organization" : iStrOrgnization,"user_id" : iStrUserId,"city" : iStrCity,"country" : iStrCountry], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.EDIT_PROFILE_UPDATE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "getDetailEdit" {
            
            let iViewController:HB_EditInfoVC = (segue.destinationViewController as? HB_EditInfoVC)!
            switch iIntTagSelection {
            case 2:
                iViewController.mutGetEditData = mutIndustryData
                iViewController.selectedIndustryId = self.selectedIndustryId
                iViewController.selectedIndustryName = self.selectedIndustryName
                break
            case 3:
                iViewController.mutGetEditData = mutInterestData
                iViewController.selectedInterestId = self.selectedInterestId
                iViewController.selectedInterestName = self.selectedInterestName
                break
            default:
                break
            }
            iViewController.numberTag = iIntTagSelection
        }
    }
    
    
}
