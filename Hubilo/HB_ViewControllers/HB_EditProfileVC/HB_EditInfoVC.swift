//
//  HB_EditInfoVC.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 31/07/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
import SwiftyJSON


class HB_EditInfoVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    
    var numberTag:Int!
    @IBOutlet weak var tblVwEditDetail:UITableView!
    var searchBarButtonItem: UIBarButtonItem?
    var searchBar = UISearchBar()
    var sharedManager : Globals = Globals.sharedInstance
    var mutGetEditData:NSMutableArray!
    var mutDisplayArray:NSMutableArray!
    var countries:NSMutableArray = NSMutableArray()
    var selectedIndustryId:String!
    var selectedIndustryName:String!
    var selectedInterestId:String!
    var selectedInterestName:String!
    var arrInterestId:NSArray!
    var mutArrInterest:NSMutableArray!
    
    
    
    @IBOutlet var buttonHeight:NSLayoutConstraint!
    @IBOutlet var horizontalspacing:NSLayoutConstraint!
    @IBOutlet var horizontalspacing2:NSLayoutConstraint!
    @IBOutlet var btnSave:UIButton!
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSave.backgroundColor = appDelegate.defaultColor
        // search bar
        mutArrInterest = NSMutableArray()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.sizeToFit()
        let searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        searchBar.barTintColor = UIColor.clearColor()
        searchBar.tintColor = appDelegate.defaultColor
        if let textFieldInsideSearchBar = self.searchBar.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = UIColor.whiteColor()
            
        }
        
        let iBtnClear = searchTextField?.valueForKey("_clearButton") as! UIButton
        iBtnClear.setImage((iBtnClear.imageView?.image?.imageWithRenderingMode(.AlwaysTemplate)), forState: .Normal)
        
        iBtnClear.tintColor = UIColor.whiteColor()
        searchBarButtonItem = navigationItem.rightBarButtonItem
        
        setMenu()
        mutDisplayArray = NSMutableArray()
        
        if numberTag == 1 {
            horizontalspacing.constant = 0
            buttonHeight.constant = 0
            horizontalspacing2.constant = 0
            self.view.layoutIfNeeded()
            //Country array
            for code in NSLocale.ISOCountryCodes() as [String] {
                let id = NSLocale.localeIdentifierFromComponents([NSLocaleCountryCode: code])
                let name:String = NSLocale(localeIdentifier: "en_IN").displayNameForKey(NSLocaleIdentifier, value: id) ?? "Country not found for code: \(code)"
                countries.addObject(name)
            }
            mutDisplayArray = countries.mutableCopy() as! NSMutableArray
        }else if numberTag == 2 {
            //Industry or Interest
            horizontalspacing.constant = 0
            buttonHeight.constant = 0
            horizontalspacing2.constant = 0
            self.view.layoutIfNeeded()
            
            self.setDisplayArrayAs(self.mutGetEditData! as [AnyObject])
            
            
            
        }else if numberTag == 3 {
            tblVwEditDetail.allowsMultipleSelection = true
            horizontalspacing.constant = 8
            buttonHeight.constant = 30
            horizontalspacing2.constant = 9
            self.view.layoutIfNeeded()
            arrInterestId = NSArray()
            arrInterestId = self.selectedInterestId.componentsSeparatedByString(",")
            
            self.setDisplayArrayAs(self.mutGetEditData! as [AnyObject])
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSavePressed(sender:UIButton) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("selectedInterest", object:mutArrInterest)
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_EditInfoVC.backButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        switch numberTag {
        case 1:
            self.navigationItem.title="COUNTRIES"
            break;
        case 2:
            self.navigationItem.title="INDUSTRIES"
            break;
        case 3:
            self.navigationItem.title="INTEREST"
            break;
        default:
            break;
        }
        
        let mySerachbtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySerachbtn.setImage(UIImage(named: "HB_Search"), forState: UIControlState.Normal)
        mySerachbtn.frame = CGRectMake(0, 0, 24, 24)
        mySerachbtn.addTarget(self, action: #selector(HB_EditInfoVC.searchButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomSearchButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySerachbtn)
        self.navigationItem.rightBarButtonItem = myCustomSearchButtonItem
        
    }
    
    @IBAction func searchButtonTouched(sender: AnyObject){
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_EditInfoVC.backSeachButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        showSearchBar()
    }
    
    func showSearchBar() {
        self.searchBar.hidden = false
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        self.searchBar.hidden = true
        navigationItem.titleView = nil
        setMenu()
        UIView.animateWithDuration(0.3, animations: {
            self.searchBar.alpha = 0
            }, completion: { finished in
        })
    }
    
    func backButtonTouched(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func backSeachButtonTouched(sender: UIButton) {
        hideSearchBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        switch numberTag {
        case 1:
            if searchText.characters.count == 0 {
                mutDisplayArray = countries.mutableCopy() as! NSMutableArray
            }else {
                mutDisplayArray.removeAllObjects()
                mutDisplayArray = countries.mutableCopy() as! NSMutableArray
                let iPredicate:NSPredicate = NSPredicate(format:"SELF CONTAINS[c] %@",searchText)
                let iArrTemp:NSArray = (mutDisplayArray as NSArray).filteredArrayUsingPredicate(iPredicate)
                mutDisplayArray = iArrTemp.mutableCopy() as! NSMutableArray
            }
            break;
        case 2:
            
            
            if searchText.characters.count == 0 {
                self.mutDisplayArray = self.mutGetEditData!.mutableCopy() as! NSMutableArray
            } else {
                self.mutDisplayArray.removeAllObjects()
                self.mutDisplayArray = self.mutGetEditData!.mutableCopy() as! NSMutableArray
                let searchResults:NSArray = self.mutDisplayArray!.filter{
                    let industryName = $0["industry_name"]!!.lowercaseString
                    
                    return industryName.rangeOfString(searchText.lowercaseString) != nil
                }
                self.mutDisplayArray = searchResults.mutableCopy() as! NSMutableArray
            }
            
            
            
            break;
        case 3:
            if searchText.characters.count == 0 {
                self.mutDisplayArray = self.mutGetEditData!.mutableCopy() as! NSMutableArray
            } else {
                self.mutDisplayArray.removeAllObjects()
                self.mutDisplayArray = self.mutGetEditData!.mutableCopy() as! NSMutableArray
                let searchResults:NSArray = self.mutDisplayArray!.filter{
                    let industryName = $0["interest_name"]!!.lowercaseString
                    
                    return industryName.rangeOfString(searchText.lowercaseString) != nil
                }
                self.mutDisplayArray = searchResults.mutableCopy() as! NSMutableArray
            }
            break;
        default:
            break;
        }
        tblVwEditDetail.reloadData()
    }
    
    // MARK: - UIViewController Others
    func setDisplayArrayAs(iArrRef:NSArray) {
        self.self.mutDisplayArray!.removeAllObjects()
        self.self.mutDisplayArray!.addObjectsFromArray(iArrRef as [AnyObject])
        self.tblVwEditDetail.reloadData()
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mutDisplayArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cellEditData", forIndexPath: indexPath)
        
        switch numberTag {
        case 1:
            cell.textLabel?.text = mutDisplayArray.objectAtIndex(indexPath.row) as? String
            
            break;
        case 2:
            let iIndustryData:HB_Industry = HB_Industry(fromDictionary:
                mutDisplayArray.objectAtIndex(indexPath.row) as! NSDictionary)
            if iIndustryData.industryId == selectedIndustryId {
                self.tblVwEditDetail.selectRowAtIndexPath(indexPath, animated: false, scrollPosition:.None)
            }
            cell.textLabel?.text = iIndustryData.industryName
            break;
        case 3:
            let iInterestData:HB_Interest = HB_Interest(fromDictionary:
                mutDisplayArray.objectAtIndex(indexPath.row) as! NSDictionary)
            if arrInterestId.containsObject(iInterestData.interestId) {
                self.mutArrInterest.addObject(iInterestData)
                self.tblVwEditDetail.selectRowAtIndexPath(indexPath, animated: false, scrollPosition:.None)
            }
            cell.textLabel?.text = iInterestData.interestName
            break;
        default:
            break;
        }
        
        
        return cell
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch numberTag {
        case 1:
            let country:String = mutDisplayArray.objectAtIndex(indexPath.row) as! String
            let myDict:NSMutableDictionary = ["country":country]
        NSNotificationCenter.defaultCenter().postNotificationName("selectedCounty", object:myDict)
            self.navigationController!.popViewControllerAnimated(true)
            break;
        case 2:
            let industry:NSDictionary = mutDisplayArray.objectAtIndex(indexPath.row) as! NSDictionary
    NSNotificationCenter.defaultCenter().postNotificationName("selectedIndustry", object:industry)
            
            break;
        case 3:
            let interest:HB_Interest = HB_Interest(fromDictionary:mutDisplayArray.objectAtIndex(indexPath.row) as! NSDictionary)
            self.mutArrInterest.addObject(interest)
            print(mutArrInterest)
            break;
        default:
            break;
        }
        
        
    }
    
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if numberTag == 3 {
            let removeValue:HB_Interest = HB_Interest(fromDictionary: mutDisplayArray.objectAtIndex(indexPath.row) as! NSDictionary)
            for iCounter in 0 ..<  mutArrInterest.count {
                let insertValue:HB_Interest =  mutArrInterest.objectAtIndex(iCounter) as! HB_Interest
                if insertValue.interestId == removeValue.interestId {
                    mutArrInterest.removeObjectAtIndex(iCounter)
                    break
                }
            }
            print(mutArrInterest)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
