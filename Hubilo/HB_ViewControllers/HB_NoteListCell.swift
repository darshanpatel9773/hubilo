//
//  HB_NoteListCell.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 02/08/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_NoteListCell: UITableViewCell {

    @IBOutlet var imgVwAttandee:UIImageView!
    @IBOutlet var lblAttendeeName:UILabel!
    @IBOutlet var lblDate:UILabel!
    @IBOutlet var lbldesignation:UILabel!
    @IBOutlet var lblNote:UILabel!
    
    @IBOutlet var heightOflblNote:NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
