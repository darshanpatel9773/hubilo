//
//  Comments.swift
//  Hubilo
//
//  Created by Aadil on 8/3/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import ObjectMapper
class CommentList : NSObject, Mappable {
    var status = ""
    var msg = ""
    var commentList : [Comments]!
    var commentCount = 0
    override init() {
        
    }
    required init?(_ map: Map) {
        
    }
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        commentList <- map["commentList"]
        commentCount <- map["commentCount"]
    }
}
class Comments: NSObject, Mappable {
    var user_id = ""
    var firstname = ""
    var lastname = ""
    var profile_img = ""
    var aboutme = ""
    var designation = ""
    var organization = ""
    var linkedin_public_url = ""
    var fb_id = ""
    var twitter_public_url = ""
    var comment = ""
    var create_time_mili = ""
    var is_bookmark = ""
    override init() {
        
    }
    required init?(_ map: Map) {
        
    }
    func mapping(map: Map) {
        user_id <- map["user_id"]
        firstname <- map["firstname"]
        lastname <- map["lastname"]
        profile_img <- map["profile_img"]
        aboutme <- map["aboutme"]
        designation <- map["designation"]
        organization <- map["organization"]
        linkedin_public_url <- map["linkedin_public_url"]
        fb_id <- map["fb_id"]
        twitter_public_url <- map["twitter_public_url"]
        create_time_mili <- map["create_time_mili"]
        comment <- map["comment"]
        is_bookmark <- map["is_bookmark"]
    }
}
