//
//  HB_AttendeeVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 15/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import SwiftLoader
import ExpandingMenu

class HB_AttendeeVC: UIViewController,UISearchBarDelegate {

    @IBOutlet weak var tblAttendeeList: UITableView!
    var searchBarButtonItem: UIBarButtonItem?
    var searchBar = UISearchBar()
     var modelAttendeeList:HB_ModelAttendeeList?
    var mutArrAttendeeList:NSMutableArray!
    var modelArrAttendeeList:NSMutableArray?
    var sharedManager : Globals = Globals.sharedInstance
    var isFirstTime : Bool = false;
    var mutArrDisplaySearch:NSMutableArray!
    var isConnectionVw:Bool = false
    var isAttendee:Bool = false
    var isPeopleVw:Bool = false
    @IBOutlet weak var imgVwDefault:UIImageView!
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mutArrDisplaySearch = NSMutableArray()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.sizeToFit()
        let searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        searchBar.barTintColor = UIColor.clearColor()
        searchBar.tintColor = appDelegate.defaultColor
        if let textFieldInsideSearchBar = self.searchBar.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = UIColor.whiteColor()
            
        }
        
        let iBtnClear = searchTextField?.valueForKey("_clearButton") as! UIButton
        iBtnClear.setImage((iBtnClear.imageView?.image?.imageWithRenderingMode(.AlwaysTemplate)), forState: .Normal)
        
        iBtnClear.tintColor = UIColor.whiteColor()
        
        
        searchBarButtonItem = navigationItem.rightBarButtonItem
        setMenu()
        
        
       }
    
    override func viewWillAppear(animated: Bool) {
        
        if self.searchBar.text?.characters.count > 0 {
         return
        }
        
        if ReachabilityCheck.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            var userId : String = "0"
            if sharedManager.isUserLoggedIn{
                userId=sharedManager.currentUserID
            }
            
          
            
            if let iUrl = getUrlDetailForAttedeeList(Constants.eventInfo.kEventId, userID:userId, filter:"0") {
                SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                    // print(iData)
                    let dicAttendeeList:NSDictionary = iData as! NSDictionary
                    
                    self.modelArrAttendeeList=NSMutableArray()
                    self.mutArrAttendeeList = (dicAttendeeList.valueForKey("communitylist") as! NSArray).mutableCopy() as! NSMutableArray
                    self.isAttendee = true
                    self.isConnectionVw = false
                    self.isPeopleVw = false
                    self.mutArrDisplaySearch = self.mutArrAttendeeList!.mutableCopy() as! NSMutableArray
                    
                    SwiftLoader.hide()
                    self.tblAttendeeList.reloadData()
                }
            } else {
                SPGUtility.sharedInstance().showAlert("oops, something went wrong with the URL", titleMessage: "", viewController: self)
                // oops, something went wrong with the URL
            }
            
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
            
        }
    }
    
    func actionBookmark(sender : AnyObject){
        
        if !sharedManager.isUserLoggedIn  && sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You must be loggedin in order to use this functionality", titleMessage: "", viewController: self)
            return;
        }
        let tmpButton : UIButton = sender as! UIButton
//        ((mutArrAttendeeList?.objectAtIndex(indexPath.row).valueForKey("is_bookmark"))! as! NSObject == 0)
        
        let param : NSDictionary = ["user_id":sharedManager.currentUserID,"bookmark_user_id":(mutArrDisplaySearch?.objectAtIndex(tmpButton.tag).valueForKey("user_id") as? String)!]
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(param, options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.BOOKMARK_ADD + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    
                    
                    if JSON["status"] == "Success"
                    {
                        if JSON["add"] == 1 {
                            tmpButton.setImage(UIImage(named: "bookmark_active"), forState: UIControlState.Normal)
                            
                            let dictTemp:NSDictionary =
                                self.mutArrDisplaySearch.objectAtIndex(tmpButton.tag) as! NSDictionary
                            let mutDictTemp = dictTemp.mutableCopy()
                            mutDictTemp.setValue(1, forKey: "is_bookmark")
                            self.mutArrDisplaySearch.removeObjectAtIndex(tmpButton.tag)
                            self.mutArrDisplaySearch.insertObject(mutDictTemp, atIndex: tmpButton.tag)
                           
                        }
                        else {
                            tmpButton.setImage(UIImage(named: "bookmark_normal"), forState: UIControlState.Normal)
                            let dictTemp:NSDictionary =
                                self.mutArrDisplaySearch.objectAtIndex(tmpButton.tag) as! NSDictionary
                            let mutDictTemp = dictTemp.mutableCopy()
                            mutDictTemp.setValue(0, forKey: "is_bookmark")
                            self.mutArrDisplaySearch.removeObjectAtIndex(tmpButton.tag)
                            self.mutArrDisplaySearch.insertObject(mutDictTemp, atIndex: tmpButton.tag)
                        }
                        
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
        
    }

    override func viewDidAppear(animated: Bool) {
        if !isFirstTime {
            configureExpandingMenuButton()
        }
        
        
        isFirstTime=true
        
        
      //  self.navigationController?.navigationBarHidden = true
       
        
        
//        if let iUrl = getUrlDetailForAttedeeList("285", userID:"0", filter:"0") {
//            SPGWebService.callGETWebService(iUrl) { (iData, iError) in
//                print(iData)
//                let dicAttendList:NSDictionary = iData as! NSDictionary
//
//                self.mutArrAttendeeList =  dicAttendList.valueForKey("communitylist") as? NSMutableArray
//                for i in 0..<self.mutArrAttendeeList?.count {
//                    print(i)
//                }
//                
//                
//                
//            }
//        } else {
//            // oops, something went wrong with the URL
      //  }
        
        
        
        //tblAttendeeList.reloadData()
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if mutArrDisplaySearch?.count > 0 && mutArrDisplaySearch != nil {
            return (mutArrDisplaySearch?.count)!
        }
        return 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        self.searchBar.resignFirstResponder()
        self.performSegueWithIdentifier(Constants.SegueConstant.kShowAttendeeInfoVC, sender: self)
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Constants.SegueConstant.kShowAttendeeInfoVC {
            let objAttendInfoVC:HB_AttendeeInfoVC=(segue.destinationViewController as? HB_AttendeeInfoVC)!
            
            objAttendInfoVC.dicAttendeeDInfo=NSMutableDictionary()
            objAttendInfoVC.dicAttendeeDInfo=(self.mutArrDisplaySearch!.objectAtIndex((self.tblAttendeeList.indexPathForSelectedRow!.row)) as! NSDictionary).mutableCopy() as? NSMutableDictionary
            
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
     //   modelAttendeeList=modelArrAttendeeList?.objectAtIndex(indexPath.row) as? HB_ModelAttendeeList
        let cell:HB_BookMarkCell=tableView.dequeueReusableCellWithIdentifier("HB_BookMarkCell", forIndexPath: indexPath) as! HB_BookMarkCell
        //cell.imgBookMarkCell.layer.cornerRadius =  cell.imgBookMarkCell.frame.size.width/2
        cell.selectionStyle = .None
//        cell.lblCompany.text=(modelAttendeeList?.organization)! as String
//        cell.lblName.text=(modelAttendeeList?.lastname)! as String + (modelAttendeeList?.firstname)! as String
        cell.lblWorkProfile.text = (mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("designation") as? String)!
//        modelAttendeeList?.designation
        cell.lblCompany.text = (mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("organization") as? String)!
//            modelAttendeeList?.organization
        cell.imgBookMarkCell.clipsToBounds = true
//
        cell.imgBookMarkCell.maskCircle(25.0)
        cell.lblName.text=(mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String)!+" " as String+((mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("lastname"))! as! String)
        //profile_img
        
        
        cell.imgBookMarkCell.setImageWithString(cell.lblName.text, color:UIColor.darkGrayColor(), circular: true)
        if ((mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("is_bookmark"))! as! NSObject == 0) {
            cell.btnBookmark?.setImage(UIImage(named: "bookmark_normal"), forState: UIControlState.Normal)
        }
        else {
            cell.btnBookmark?.setImage(UIImage(named: "bookmark_active"), forState: UIControlState.Normal)
        }
        cell.btnBookmark?.tag=indexPath.row
        cell.btnBookmark?.addTarget(self, action: #selector(actionBookmark), forControlEvents: UIControlEvents.TouchUpInside)
        if mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("profile_img") != nil {
            let url:String = (Constants.ImagePaths.kAttendeeImagePath + ((mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("profile_img"))! as! String))
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgBookMarkCell.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgBookMarkCell.image = imgVw.image
        }
       
        
        
        cell.lblCompany.text=mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("organization") as? String
    
        /*
         [cell.imageView sd_setImageWithURL:[NSURL URLWithString:@"http://www.domain.com/path/to/image.jpg"]
         placeholderImage:[UIImage imageNamed:@"placeholder.png"]
         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
         ... completion code here ...
         }];
         
        
         */
        
        
        return cell
        
    }
    //MARK: User Define
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_AttendeeVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="ATTENDEE"
        
        let mySerachbtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySerachbtn.setImage(UIImage(named: "HB_Search"), forState: UIControlState.Normal)
        mySerachbtn.frame = CGRectMake(0, 0, 24, 24)
        mySerachbtn.addTarget(self, action: #selector(HB_AttendeeVC.searchButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomSearchButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySerachbtn)
        self.navigationItem.rightBarButtonItem = myCustomSearchButtonItem
        
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
  
        print(self.mutArrDisplaySearch.count)
        if self.mutArrDisplaySearch!.count > 0 {
            if searchText.characters.count == 0 {
                if isAttendee {
                    self.mutArrDisplaySearch = self.mutArrAttendeeList!.mutableCopy() as! NSMutableArray
                }else if isPeopleVw{
                    self.mutArrDisplaySearch = self.modelArrAttendeeList?.mutableCopy() as! NSMutableArray
                }else if isConnectionVw{
                    self.mutArrDisplaySearch = self.modelArrAttendeeList?.mutableCopy() as! NSMutableArray
                }
                
            } else {
                self.mutArrDisplaySearch.removeAllObjects()
                if isAttendee {
                    self.mutArrDisplaySearch = self.mutArrAttendeeList!.mutableCopy() as! NSMutableArray
                }else if isPeopleVw{
                    self.mutArrDisplaySearch = self.modelArrAttendeeList?.mutableCopy() as! NSMutableArray
                }else if isConnectionVw{
                    self.mutArrDisplaySearch = self.modelArrAttendeeList?.mutableCopy() as! NSMutableArray
                }
                let searchResults:NSArray = self.mutArrDisplaySearch!.filter{
                    let firstName = $0["firstname"]!!.lowercaseString
                    let lastName = $0["lastname"]!!.lowercaseString
                    let designation = $0["designation"]!!.lowercaseString
                    let organization = $0["organization"]!!.lowercaseString
                    
                    return firstName.rangeOfString(searchText.lowercaseString) != nil
                        || lastName.rangeOfString(searchText.lowercaseString) != nil || designation.rangeOfString(searchText.lowercaseString) != nil || organization.rangeOfString(searchText.lowercaseString) != nil
                }
                self.mutArrDisplaySearch = searchResults.mutableCopy() as! NSMutableArray
            }
            tblAttendeeList.reloadData()
        }
    }
    
    @IBAction func searchButtonTouched(sender: AnyObject){
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_AttendeeVC.backButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
    
        showSearchBar()
    }
    func showSearchBar() {
        self.searchBar.text = ""
        self.searchBar.hidden = false
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        self.searchBar.hidden = true
        navigationItem.titleView = nil
        setMenu()
        UIView.animateWithDuration(0.3, animations: {
            self.searchBar.alpha = 0
            }, completion: { finished in
        })
    }
    
    func backButtonTouched(sender: UIButton) {
        hideSearchBar()
        if self.mutArrDisplaySearch.count > 0 {
            self.mutArrDisplaySearch.removeAllObjects()
            self.mutArrDisplaySearch = self.mutArrAttendeeList!.mutableCopy() as! NSMutableArray
            tblAttendeeList.reloadData()
        }
    }
    
    private func configureExpandingMenuButton() {
        let menuButtonSize: CGSize = CGSize(width: 50.0, height: 50.0)
        let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let centerImage : UIImageView = UIImageView(frame: CGRectMake(0, 0, 40, 40))
        centerImage.setImageWithString("", color: iAppDelegate.defaultColor,circular:true)
        
        let imgMain = UIImageView(frame: CGRectMake(0, 0, 40, 40))
        imgMain.image = UIImage(named: "chooser-button-tab")
        
        UIGraphicsBeginImageContext(CGSizeMake(40, 40))
        centerImage.image!.drawInRect(CGRectMake(0, 0, 40, 40))
        imgMain.image!.drawInRect(CGRectMake(8, 8, 24, 24))
        
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let menuButton = ExpandingMenuButton(frame: CGRect(origin: CGPointZero, size: menuButtonSize), centerImage: finalImage!, centerHighlightedImage: finalImage!)
        menuButton.center = CGPointMake(self.view.bounds.width - 35.0, self.view.bounds.height - 35.0)
        menuButton.addShadowView()
        self.view.addSubview(menuButton)
        self.view.bringSubviewToFront(menuButton);
        self.view.sendSubviewToBack(self.tblAttendeeList)
        func showAlert(title: String) {
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }

        
        let imgView:UIImageView = UIImageView(frame: CGRectMake(0, 0, 30, 30))
        imgView.setImageWithString("", color: iAppDelegate.defaultColor,circular:true)

        
        let item2 = ExpandingMenuItem(size: menuButtonSize, title: " Connections ", image: UIImage(named: "float_connection")!, highlightedImage: UIImage(named: "float_connection")!, backgroundImage: imgView.image, backgroundHighlightedImage: UIImage(named: "float_connection")) { () -> Void in
            //showAlert("Call")
           
            if self.sharedManager.userCommunityExist == 0
            {
                self.tblAttendeeList.hidden = true
                SPGUtility.sharedInstance().showAlert("Please Join the community", titleMessage: "", viewController: self)
                return
            }
            
            if ReachabilityCheck.isConnectedToNetwork() {
                
                SwiftLoader.show(animated: true)
                var userId : String = "0"
                if self.sharedManager.isUserLoggedIn{
                    userId=self.sharedManager.currentUserID
                }
                if let iUrl = self.getUrlDetailForAttedeeList(Constants.eventInfo.kEventId, userID:userId, filter:"2") {
                    SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                        // print(iData)
                        let dicAttendeeList:NSDictionary = iData as! NSDictionary
                        
                        self.modelArrAttendeeList=NSMutableArray()
                        self.modelArrAttendeeList = dicAttendeeList.valueForKey("communitylist") as? NSMutableArray
                        
                        if dicAttendeeList["status"] as! String == "Fail" {
                            //SPGUtility.sharedInstance().showAlert(dicAttendeeList["msg"] as! String, titleMessage: "", viewController: self)
                        }
                        if self.modelArrAttendeeList?.count > 0 {
                            self.tblAttendeeList.hidden = false
                            self.imgVwDefault.hidden = true
                            self.isConnectionVw = true
                            self.isAttendee = false
                            self.isPeopleVw = false
                            self.mutArrDisplaySearch = self.modelArrAttendeeList?.mutableCopy() as? NSMutableArray
                            self.tblAttendeeList.reloadData()
                        }else {
                            self.tblAttendeeList.hidden = true
                            self.imgVwDefault.hidden = false
                        }
                        SwiftLoader.hide()
                    }
                } else {
                    SPGUtility.sharedInstance().showAlert("oops, something went wrong with the URL", titleMessage: "", viewController: self)
                    // oops, something went wrong with the URL
                }
                
            }
            else
            {
                SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
                
            }
        }
        
        let item1 = ExpandingMenuItem(size: menuButtonSize, title: " People who viewed ", image: UIImage(named: "float_viewed")!, highlightedImage: UIImage(named: "float_viewed")!, backgroundImage: imgView.image, backgroundHighlightedImage: UIImage(named: "float_viewed")) { () -> Void in
            
            if self.sharedManager.userCommunityExist == 0
            {
                self.tblAttendeeList.hidden = true
                SPGUtility.sharedInstance().showAlert("Please Join the community", titleMessage: "", viewController: self)
                return
            }
            
            if ReachabilityCheck.isConnectedToNetwork() {
                
                SwiftLoader.show(animated: true)
                var userId : String = "0"
                if self.sharedManager.isUserLoggedIn{
                    userId=self.sharedManager.currentUserID
                }
                if let iUrl = self.getUrlDetailForAttedeeList(Constants.eventInfo.kEventId, userID:userId, filter:"1") {
                    SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                        // print(iData)
                        let dicAttendeeList:NSDictionary = iData as! NSDictionary
                        
                        self.modelArrAttendeeList=NSMutableArray()
                        self.modelArrAttendeeList = dicAttendeeList.valueForKey("communitylist") as? NSMutableArray
                        
                        if dicAttendeeList["status"] as! String == "Fail" {
                            //SPGUtility.sharedInstance().showAlert(dicAttendeeList["msg"] as! String, titleMessage: "", viewController: self)
                        }
                        if self.modelArrAttendeeList?.count > 0{
                            self.tblAttendeeList.hidden = false
                            self.imgVwDefault.hidden = true
                            self.isConnectionVw = false
                            self.isAttendee = false
                            self.isPeopleVw = true
                            self.mutArrDisplaySearch = self.modelAttendeeList?.mutableCopy() as? NSMutableArray
                            self.tblAttendeeList.reloadData()
                        }else {
                            self.tblAttendeeList.hidden = true
                            self.imgVwDefault.hidden = false
                        }                    }
                    SwiftLoader.hide()
                } else {
                    SPGUtility.sharedInstance().showAlert("oops, something went wrong with the URL", titleMessage: "", viewController: self)
                    // oops, something went wrong with the URL
                }
                
            }
            else
            {
                SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
                
            }
           
            
        }
        
        let item5 = ExpandingMenuItem(size: menuButtonSize, title: " Named Z-A ", image: UIImage(named: "float_za")!, highlightedImage: UIImage(named: "float_za")!, backgroundImage:imgView.image, backgroundHighlightedImage: UIImage(named: "float_za")) { () -> Void in

            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "firstname", ascending: false, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
            let sortedResults: NSArray = (self.mutArrDisplaySearch! as NSArray).sortedArrayUsingDescriptors([descriptor])
            self.mutArrDisplaySearch! = sortedResults.mutableCopy() as! NSMutableArray
            self.tblAttendeeList.hidden = false
            self.tblAttendeeList.reloadData()
        }
        
        let item4 = ExpandingMenuItem(size: menuButtonSize, title: " Named A-Z ", image: UIImage(named: "float_az")!, highlightedImage: UIImage(named: "float_az")!, backgroundImage: imgView.image, backgroundHighlightedImage: UIImage(named: "float_az")) { () -> Void in
            
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "firstname", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
            let sortedResults: NSArray = (self.mutArrDisplaySearch! as NSArray).sortedArrayUsingDescriptors([descriptor])
            self.mutArrDisplaySearch! = sortedResults.mutableCopy() as! NSMutableArray
            self.tblAttendeeList.hidden = false
            self.tblAttendeeList.reloadData()
        }
        
        let item3 = ExpandingMenuItem(size: menuButtonSize, title: " Recently Added ", image: UIImage(named: "float_recent")!, highlightedImage: UIImage(named: "float_recent")!, backgroundImage: imgView.image, backgroundHighlightedImage: UIImage(named: "float_recent")) { () -> Void in
            if ReachabilityCheck.isConnectedToNetwork() {
                
                SwiftLoader.show(animated: true)
                var userId : String = "0"
                if self.sharedManager.isUserLoggedIn{
                    userId=self.sharedManager.currentUserID
                }
                if let iUrl = self.getUrlDetailForAttedeeList(Constants.eventInfo.kEventId, userID:userId, filter:"0") {
                    SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                        // print(iData)
                        let dicAttendeeList:NSDictionary = iData as! NSDictionary
                        
                       
                        self.mutArrAttendeeList = dicAttendeeList.valueForKey("communitylist") as? NSMutableArray
                        
                        if self.mutArrAttendeeList?.count > 0{
                            self.tblAttendeeList.hidden = false
                            self.imgVwDefault.hidden = true
                            self.isConnectionVw = false
                            self.isAttendee = true
                            self.isPeopleVw = false
                            self.mutArrDisplaySearch = self.mutArrAttendeeList?.mutableCopy() as! NSMutableArray
                            self.tblAttendeeList.reloadData()
                        }else {
                            self.tblAttendeeList.hidden = true
                            self.imgVwDefault.hidden = false
                        }
                        
                        
                        SwiftLoader.hide()
                        
                    }
                } else {
                    SPGUtility.sharedInstance().showAlert("oops, something went wrong with the URL", titleMessage: "", viewController: self)
                    // oops, something went wrong with the URL
                }
                
            }
            else
            {
                SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
                
            }
            
        }
        //chooser-moment-button
        //
        //        let item3 = ExpandingMenuItem(size: menuButtonSize, title: "Camera", image: UIImage(named: "chooser-moment-icon-camera")!, highlightedImage: UIImage(named: "chooser-moment-icon-camera-highlighted")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
        //            showAlert("Camera")
        //        }
        //
        //        let item4 = ExpandingMenuItem(size: menuButtonSize, title: "Thought", image: UIImage(named: "chooser-moment-icon-thought")!, highlightedImage: UIImage(named: "chooser-moment-icon-thought-highlighted")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
        //            showAlert("Thought")
        //        }
        //
        //        let item5 = ExpandingMenuItem(size: menuButtonSize, title: "Sleep", image: UIImage(named: "chooser-moment-icon-sleep")!, highlightedImage: UIImage(named: "chooser-moment-icon-sleep-highlighted")!, backgroundImage: UIImage(named: "chooser-moment-button"), backgroundHighlightedImage: UIImage(named: "chooser-moment-button-highlighted")) { () -> Void in
        //            showAlert("Sleep")
        //        }
        //
        
        menuButton.addMenuItems([item1, item2, item3, item4, item5])
        
        menuButton.willPresentMenuItems = { (menu) -> Void in
            print("MenuItems will present.")
        }
        
        menuButton.didDismissMenuItems = { (menu) -> Void in
            print("MenuItems dismissed.")
        }
    }
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
    }
    
    func getUrlDetailForAttedeeList(eventID: String,userID: String,filter: String) -> NSURL? {
        var iURL = NSURL()
        do {
          let iJsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" :eventID ,"user_id" : userID, "filter" :filter], options: NSJSONWritingOptions.init(rawValue: 0))
            let iStrURLData = NSString(data: iJsonData, encoding: NSUTF8StringEncoding)!
            let iStrURL = (Constants.URL.GET_ALL_ATENDEE  + (iStrURLData as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iURL = NSURL(string:iStrURL as String)!
        }catch  {
            iURL = NSURL(string:"")!
        }
        return iURL
    }
    
  
 

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
