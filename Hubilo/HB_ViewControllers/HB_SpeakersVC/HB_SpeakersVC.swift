//
//  HB_SpeakersVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 15/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import SwiftLoader
import Kingfisher
import SDWebImage

class HB_SpeakersVC: UIViewController,UISearchBarDelegate {
    
    var searchBar = UISearchBar()
    var searchBarButtonItem: UIBarButtonItem?
    @IBOutlet weak var tblSpakersList: UITableView!
    var dicSpeakerList:NSMutableDictionary?
    var mutArrSpeakerData:NSMutableArray!
    var modelSpeaker:HB_Speaker?
    var mutArrDisplaySearch:NSMutableArray!
    var sharedManager : Globals = Globals.sharedInstance
    var appDelegate:AppDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mutArrDisplaySearch = NSMutableArray()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.sizeToFit()
        let searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        searchBar.barTintColor = UIColor.clearColor()
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        searchBar.tintColor = appDelegate.defaultColor
        
        if let textFieldInsideSearchBar = self.searchBar.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = UIColor.whiteColor()
            
        }
        
        let iBtnClear = searchTextField?.valueForKey("_clearButton") as! UIButton
        iBtnClear.setImage((iBtnClear.imageView?.image?.imageWithRenderingMode(.AlwaysTemplate)), forState: .Normal)
        
        iBtnClear.tintColor = UIColor.whiteColor()
        
        
        searchBarButtonItem = navigationItem.rightBarButtonItem
        setMenu()
        mutArrSpeakerData = NSMutableArray()
        mutArrDisplaySearch = NSMutableArray()
        
        self.getSpeakersList()
        
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        
    }
    
    
    func getSpeakersList()  {
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            if let iUrl = getSpeakerList() {
                SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                    if let iError = iError
                    {
                        print("aErrorObj===\(iError.description)")
                        SwiftLoader.hide()
                    }else {
                        self.mutArrSpeakerData = NSMutableArray()
                        let iMutDictSpeakerData:NSMutableDictionary = (iData as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        self.mutArrSpeakerData = (iMutDictSpeakerData.objectForKey("data") as! NSArray).mutableCopy() as! NSMutableArray
                        self.mutArrDisplaySearch = self.mutArrSpeakerData.mutableCopy() as! NSMutableArray
                        print(self.mutArrSpeakerData)
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            self.tblSpakersList.reloadData()
                        })
                        
                        
                        
                        SwiftLoader.hide()
                    }
                }
            }
            else {
                // oops, something went wrong with the URL
            }
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mutArrDisplaySearch!.count > 0 {
            return mutArrDisplaySearch.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let iSpeakerData:HB_SpeakerList = HB_SpeakerList(fromDictionary: mutArrDisplaySearch.objectAtIndex(indexPath.row) as! NSDictionary)
        
        let iTblVwSpeakerCell:HB_SpeakersCell = tableView.dequeueReusableCellWithIdentifier("HB_SpeakersCell", forIndexPath:indexPath) as! HB_SpeakersCell
        
        iTblVwSpeakerCell.lblSpeakerName.text = iSpeakerData.speakerName!
        iTblVwSpeakerCell.lblSpeakerDesignation.text = iSpeakerData.speakerDescription!
        
        iTblVwSpeakerCell.tag = indexPath.row;
        
        
        
        if iSpeakerData.speakerProfileImg != nil {
            let url:String = (Constants.ImagePaths.kSpeakerImagePath + (iSpeakerData.speakerProfileImg)!)
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(iSpeakerData.speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            iTblVwSpeakerCell.lblSpeakerThumbnail.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(iSpeakerData.speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            iTblVwSpeakerCell.lblSpeakerThumbnail.image = imgVw.image
        }
        
        
        
        
        iTblVwSpeakerCell.lblSpeakerThumbnail.maskCircle(25.0)
        
        if iSpeakerData.speakerRating! == "0"
        {
            iTblVwSpeakerCell.btnRating?.hidden = true
        }
        else
        {
            iTblVwSpeakerCell.btnRating?.hidden = false
            iTblVwSpeakerCell.btnRating?.setTitle(iSpeakerData.speakerRating!, forState: UIControlState.Normal)
        }
        
        iTblVwSpeakerCell.selectionStyle = .None
        
        return iTblVwSpeakerCell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let iSpeakerData:HB_SpeakerList = HB_SpeakerList(fromDictionary: mutArrDisplaySearch.objectAtIndex(indexPath.row) as! NSDictionary)
        print(iSpeakerData)
        let ObjScheduleDetail : HB_SpeakerInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_SpeakerInfoVC") as! HB_SpeakerInfoVC
        ObjScheduleDetail.modelSpeaker = iSpeakerData
        self.navigationController?.pushViewController(ObjScheduleDetail, animated: true)
        //self.performSegueWithIdentifier(Constants.SegueConstant.kShowSpeakerInfoVC, sender: self)
    }
    //MARK: User Define
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_SpeakersVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        self.navigationItem.title="SPEAKERS"
        
        let mySerachbtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySerachbtn.setImage(UIImage(named: "HB_Search"), forState: UIControlState.Normal)
        mySerachbtn.frame = CGRectMake(0, 0, 24, 24)
        mySerachbtn.addTarget(self, action: #selector(HB_SpeakersVC.searchButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomSearchButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySerachbtn)
        self.navigationItem.rightBarButtonItem = myCustomSearchButtonItem
        
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    @IBAction func searchButtonTouched(sender: AnyObject){
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_SpeakersVC.backButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        showSearchBar()
    }
    
    func showSearchBar() {
        self.searchBar.text = ""
        self.searchBar.hidden = false
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
        
    }
    
    func hideSearchBar() {
        self.searchBar.hidden = true
        navigationItem.titleView = nil
        setMenu()
        UIView.animateWithDuration(0.3, animations: {
            self.searchBar.alpha = 0
            }, completion: { finished in
        })
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count == 0 {
            self.mutArrDisplaySearch = self.mutArrSpeakerData!.mutableCopy() as! NSMutableArray
        } else {
            self.mutArrDisplaySearch.removeAllObjects()
            self.mutArrDisplaySearch = self.mutArrSpeakerData!.mutableCopy() as! NSMutableArray
            let searchResults:NSArray = self.mutArrDisplaySearch!.filter{
                let industryName = $0["speaker_name"]!!.lowercaseString
                let industryPosition = $0["speaker_description"]!!.lowercaseString
                
                return industryName.rangeOfString(searchText.lowercaseString) != nil || industryPosition .rangeOfString(searchText.lowercaseString) != nil
            }
            self.mutArrDisplaySearch = searchResults.mutableCopy() as! NSMutableArray
        }
        tblSpakersList.reloadData()
    }
    
    func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
    }
    
    func backButtonTouched(sender: UIButton) {
        hideSearchBar()
        if self.mutArrDisplaySearch.count > 0 {
            self.mutArrDisplaySearch.removeAllObjects()
            self.mutArrDisplaySearch = self.mutArrSpeakerData!.mutableCopy() as! NSMutableArray
            self.tblSpakersList.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideSearchBar()
    }
    func getSpeakerList() -> NSURL? {
        
       
        
        var userId:String = ""
        if sharedManager.isUserLoggedIn {
            userId = sharedManager.currentUserID
        }
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_id" : userId,"event_id" : Constants.eventInfo.kEventId], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.SPEAKER_LIST + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
