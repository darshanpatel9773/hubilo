//
//  HB_SpeakersCell.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 30/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_SpeakersCell: UITableViewCell {
    
    
    @IBOutlet weak var lblSpeakerName: UILabel!
    @IBOutlet weak var lblSpeakerDesignation: UILabel!
    @IBOutlet weak var lblSpeakerRating: UILabel!
    @IBOutlet weak var lblSpeakerThumbnail: UIImageView!
    @IBOutlet var btnRating : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
