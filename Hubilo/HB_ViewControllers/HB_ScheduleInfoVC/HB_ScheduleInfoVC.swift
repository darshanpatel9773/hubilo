//
//  HB_ScheduleInfoVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 18/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
class HB_ScheduleInfoVC: UIViewController,UITableViewDelegate,UITableViewDataSource, UIWebViewDelegate {
    var modelSchedule : HB_AgendaList!
    var sharedManager : Globals = Globals.sharedInstance
    var isLoadMore: Bool = false;
    var webHeight : Int = 0
    var originalHeight : Int = 0
    var iSpeakerData:HB_SpeakerList!
    var heightOfSchedule:CGFloat = 21
    var isSecondLoad:Bool = false

    @IBOutlet weak var tblSchduleInfo: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenu()
        //self.loadMore(UIButton())
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        tblSchduleInfo.reloadData()
        isSecondLoad = true
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if section == 0
        {
        return 1
        }
        if section == 1 {
            return 1
        }
        if section == 2 {
            return modelSchedule.speakers.count
        }
        return 0
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
    
        if section == 0
        {
            return 0
        }
        if section == 1 {
            return 0
        }
        if section == 2 {
            if  modelSchedule.speakers.count > 0 {
                    return 25
            }
            return 0
        }
        return 0
    
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell1:UITableViewCell?=UITableViewCell()
        if indexPath.section == 0
        {
            let cell:ScheduleInfoCell = tableView.dequeueReusableCellWithIdentifier("HB_SpeakerScheduleCell", forIndexPath: indexPath) as! ScheduleInfoCell
            
            cell.lblScheduleName?.text = modelSchedule.agendaName
            
            let iStrAgenda:String = modelSchedule.agendaName!
            let heightOfAgenda:CGFloat = iStrAgenda.heightWithConstrainedWidth((cell.lblScheduleName?.frame.size.width)!, font: (cell.lblScheduleName?.font)!)
            heightOfSchedule = heightOfAgenda
            
            
            
            
            var dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            //  dateFormatter.timeZone = NSTimeZone(name: "IST")
            
            
            let date:NSDate = dateFormatter.dateFromString(modelSchedule.agendaDate!)!
            dateFormatter.dateStyle = .MediumStyle

            dateFormatter.dateFormat = "dd MMM yyyy"
            
            //  let strDate:NSString=dateFormatter.stringFromDate(date)
            let strSuffixOfDate:NSString=daySuffix(date)
            print(strSuffixOfDate)
            
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: date)
            
            let year =  components.year
            let month = components.month
            let day = components.day
            
            let months = dateFormatter.monthSymbols
            let monthSymbol = months[month-1]
            var monthText = monthSymbol as NSString
             monthText = monthText.substringWithRange(NSRange(location: 0, length: 3))
            
            cell.lblDate?.text = String(day) + (strSuffixOfDate as String) + " " + (monthText as String) + ", " + String(year)
            dateFormatter.dateFormat = "EEEE"
            //let dayOfWeekString = dateFormatter.stringFromDate(date)
            
            
            //cell.lblDate?.text=modelSchedule.agendaDate
            dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            let dateStartTime = dateFormatter.dateFromString(modelSchedule.agendaStartTime)
            
            let dateEndTime = dateFormatter.dateFromString(modelSchedule.agendaEndTime)
            dateFormatter.dateFormat = "hh:mm" // or //h:mm a
            let dateString = dateFormatter.stringFromDate(dateStartTime!) + "-" + dateFormatter.stringFromDate(dateEndTime!)
            cell.lblTime!.text=dateString

            if modelSchedule.agendaLocation == ""
            {
                cell.imgAddress.hidden = true
                modelSchedule.agendaLocation=""
            }
            cell.lblAddress?.text=modelSchedule.agendaLocation
            
            let html = "<style>body{color: #aaaaaa;font-size: 13px;font-family:helvetica;}</style>" + (modelSchedule.agendaDescription)!

            cell.lblDescription?.loadHTMLString(html, baseURL: nil)
            cell.btnFav?.setTitle(modelSchedule.agendaLikes, forState: UIControlState.Normal)
            if modelSchedule.isLike == 1 {
                cell.btnFav?.setImage(UIImage(named: "favorite_icon_active"), forState: UIControlState.Normal)
                
            }
            cell.btnFav?.tag=indexPath.row
            cell.btnFav?.addTarget(self, action: #selector(actionLike), forControlEvents: UIControlEvents.TouchUpInside)
            cell.selectionStyle = .None
            
            // Configure the cell...
            //cell.textLabel?.text = "Row \(indexPath.row)"
            return cell
        }
        if indexPath.section == 1
        {
            let cell:SponsorDetailCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsDetailsCell", forIndexPath: indexPath) as! SponsorDetailCell
            let html = "<style>body{color: #aaaaaa;font-size: 13px;font-family:helvetica;}</style>" + (modelSchedule.agendaDescription)!
            cell.textViewDetail?.loadHTMLString(html, baseURL: nil)
            cell.btnMore?.addTarget(self, action: #selector(loadMore), forControlEvents: UIControlEvents.TouchUpInside)
            if modelSchedule.agendaDescription == ""
            {
                cell.btnMore?.hidden=true
                isLoadMore=true
            }
            cell.textViewDetail?.delegate=self
            cell.textViewDetail?.scrollView.scrollEnabled=false
            cell.selectionStyle = .None
            return cell
            
        }
        if indexPath.section > 1
        {
            let cell:HB_BookMarkCell = tableView.dequeueReusableCellWithIdentifier("HB_BookMarkCell", forIndexPath: indexPath) as! HB_BookMarkCell
            cell.imgBookMarkCell.layer.cornerRadius =  cell.imgBookMarkCell.frame.size.width/2
            cell.imgBookMarkCell.clipsToBounds = true
            cell.lblName.text=modelSchedule.speakers[indexPath.row].speakerName
            cell.lblWorkProfile.text=modelSchedule.speakers[indexPath.row].speakerDescription
            
            if modelSchedule.speakers[indexPath.row].speakerProfileImg != nil {
                let url:String = Constants.ImagePaths.kSpeakerImagePath+modelSchedule.speakers[indexPath.row].speakerProfileImg
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(modelSchedule.speakers[indexPath.row].speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cell.imgBookMarkCell.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
            }else {
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(modelSchedule.speakers[indexPath.row].speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cell.imgBookMarkCell.image = imgVw.image
            }

            
//            cell.imgBookMarkCell.sd_setImageWithURL(NSURL(string: Constants.ImagePaths.kSpeakerImagePath+modelSchedule.speakers[indexPath.row].speakerProfileImg), placeholderImage: UIImage(named: "login_default"))
            cell.selectionStyle = .None
            return cell
            
        }
        return cell1!
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var vw:UIView?
        
        if section == 0 {
            
            vw=UIView(frame: CGRect(x: 0, y: 0, width: (UIScreen.mainScreen().bounds.width), height: 0))
            return vw
            
        }
        if section == 1 {
            
            vw=UIView(frame: CGRect(x: 0, y: 0, width: (UIScreen.mainScreen().bounds.width), height: 40))
            
            return vw
        }
        if section == 2 {
            vw=UIView(frame: CGRect(x: 0, y: 0, width: (UIScreen.mainScreen().bounds.width), height: 40))
            vw!.backgroundColor = UIColor.clearColor()
            
            if modelSchedule.speakers.count != 0 {
                let lblSpeaker:UILabel=UILabel(frame: CGRect(x: 15, y: 0, width: (UIScreen.mainScreen().bounds.width)-15, height: 30))
                lblSpeaker.text = "Speakers"
                lblSpeaker.textColor=UIColor.grayColor()
                lblSpeaker.font = lblSpeaker.font.fontWithSize(14)
                vw!.addSubview(lblSpeaker);
            }
            
                return vw
        }
        
        return vw
    }
    func daySuffix(date: NSDate) -> String {
        let calendar = NSCalendar.currentCalendar()
        let dayOfMonth = calendar.component(.Day, fromDate: date)
        switch dayOfMonth {
        case 1: fallthrough
        case 21: fallthrough
        case 31: return "st"
        case 2: fallthrough
        case 22: return "nd"
        case 3: fallthrough
        case 23: return "rd"
        default: return "th"
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if  indexPath.section == 2 {            
            let ObjScheduleDetail : HB_SpeakerInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_SpeakerInfoVC") as! HB_SpeakerInfoVC
            ObjScheduleDetail.modelSpeaker = modelSchedule.speakers![indexPath.row]
            self.navigationController?.pushViewController(ObjScheduleDetail, animated: true)
        }
    }
    
    func actionLike (sender : UIButton) {
        if sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 1 {
            if ReachabilityCheck.isConnectedToNetwork()
            {
                SwiftLoader.show(animated: true)
                
                do {
                    let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : Constants.eventInfo.kEventId,"user_id":sharedManager.currentUserID,"agenda_id":modelSchedule.agendaId], options: NSJSONWritingOptions.init(rawValue: 0))
                    let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                    let url = (Constants.URL.AGENDA_LIKE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                    APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                        SwiftLoader.hide()
                        if JSON["status"] == "Success"
                        {
                            if JSON["isLike"] == 1 {
                                sender.setImage(UIImage(named: "favorite_icon_active"), forState: UIControlState.Normal)
                                self.modelSchedule.isLike=1
                                self.modelSchedule.agendaLikes = String(Int(self.modelSchedule.agendaLikes)!  + 1)
                            }
                            else {
                                sender.setImage(UIImage(named: "favorite_icon_normal"), forState: UIControlState.Normal)
                                self.modelSchedule.isLike=0
                                self.modelSchedule.agendaLikes = String(Int(self.modelSchedule.agendaLikes)!  - 1)
                            }
                            self.tblSchduleInfo.reloadData()
                        }else {
                            SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                        }
                        }, failure: { (NSError) in
                            SwiftLoader.hide()
                            SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                    })
                    // here "jsonData" is the dictionary encoded in JSON data
                } catch let error as NSError {
                    SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
                }
            }
            else
            {
                
                SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
            }
        }
        else {
            SPGUtility.sharedInstance().showAlert("You must be logged in to like this agenda.", titleMessage: "", viewController: self)
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            if heightOfSchedule > 21 {
                return (90-21) + heightOfSchedule
            }
            return 90
        }
        if indexPath.section == 1
        {
            if isLoadMore == true
            {
                return CGFloat(webHeight)
            }
            return 45
        }
        if indexPath.section > 1
        {
            return 100
        }
        return 0
    }
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_ExhibitorsInfoVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="SCHEDULE INFO"
        
        
    }
    
    func loadMore(sender : AnyObject) {
        isLoadMore = !isLoadMore
        let tmpButton : UIButton = sender as! UIButton
        if isLoadMore == true
        {
            webHeight = originalHeight
            tmpButton.setTitle("MORE", forState: .Normal)
        }
        else {
            webHeight=97
            tmpButton.setTitle("LESS", forState: .Normal)
        }
        
        self.tblSchduleInfo!.reloadData()
    }
    @IBAction func back_btn_click(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }


    func webViewDidFinishLoad(webView: UIWebView) {
        let webViewHeight:NSString = webView.stringByEvaluatingJavaScriptFromString("document.body.scrollHeight;")!
        
        self.originalHeight = webViewHeight.integerValue
       
    }
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        self.originalHeight=97
    }
    
    @IBAction func actionQuestion (sender : AnyObject){
        if sharedManager.isUserLoggedIn {
            let objQuestionDetail : HB_ScheduleQuestionsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_ScheduleQuestionsVC") as! HB_ScheduleQuestionsVC
            objQuestionDetail.agendaId=Int(modelSchedule.agendaId)!
            self.navigationController?.pushViewController(objQuestionDetail, animated: true)
        }
        else {
            SPGUtility.sharedInstance().showAlert("You must be loggedin to post the question.", titleMessage: "", viewController: self)
            return;
        }
        
        
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
