//
//  ScheduleInfoCell.swift
//  Hubilo
//
//  Created by Aadil on 7/9/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class ScheduleInfoCell: UITableViewCell {
    @IBOutlet var lblScheduleName : UILabel?
    @IBOutlet var lblTime : UILabel?
    @IBOutlet var btnFav : UIButton?
    @IBOutlet var lblDate : UILabel?
    @IBOutlet var lblAddress : UILabel?
    @IBOutlet var lblDescription : UIWebView?
    @IBOutlet var btnMore : UIButton?
    @IBOutlet var lblNoOfLikes : UILabel?
    @IBOutlet var imgAddress:UIImageView!
    @IBOutlet var imgTime:UIImageView!
    @IBOutlet var heightOfScheduleConstraint:NSLayoutConstraint!
    @IBOutlet var heightOfScheduleViewConstraint:NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
