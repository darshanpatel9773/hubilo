//
//  SponsorDetailCell.swift
//  Hubilo
//
//  Created by Aadil on 7/9/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class SponsorDetailCell: UITableViewCell {
    @IBOutlet var textViewDetail : UIWebView?
    @IBOutlet var height : NSLayoutConstraint?
    @IBOutlet var btnMore : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
