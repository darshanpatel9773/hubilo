//
//  SponsorInfoCell.swift
//  Hubilo
//
//  Created by Aadil on 7/9/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class SponsorInfoCell: UITableViewCell {
    @IBOutlet var lblSponsorName : UILabel?
    @IBOutlet var lblSponsorEmail : UILabel?
    @IBOutlet var lblSponsorOrganization : UILabel?
    @IBOutlet var btnSponsor : UIImageView?
    @IBOutlet var btnfacebook: UIButton?
    @IBOutlet var btnTwitter : UIButton?
    @IBOutlet var btnWebsite : UIButton?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
