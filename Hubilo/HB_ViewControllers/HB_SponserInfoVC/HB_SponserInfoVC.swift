//
//  HB_SponserInfoVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 18/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_SponserInfoVC: UIViewController,UITableViewDataSource,UITableViewDelegate, UIWebViewDelegate {
    var modelSPonserobj : HB_ModelSponsers!
    var sharedManager : Globals = Globals.sharedInstance
    var isLoadMore: Bool = false;
    var webHeight : Int = 97
    var originalHeight : Int = 0
    var url : NSURL!
    let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    @IBOutlet var tblView : UITableView?
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenu()
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if(modelSPonserobj.sponsorBrochure == nil){
            return 2
        }else{
            return 3
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell1:UITableViewCell?=UITableViewCell()
        if indexPath.row == 0
        {
            let cell:SponsorInfoCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsContactCell", forIndexPath: indexPath) as! SponsorInfoCell
            cell.lblSponsorName?.text=modelSPonserobj.sponsorName
            cell.lblSponsorEmail?.text=modelSPonserobj.sponsorEmailid
            
            let myString: String = modelSPonserobj.sponsorName
//            var myStringArr = myString.componentsSeparatedByString(" ")
            var newString: String = "Google "
            newString = newString.stringByAppendingString(myString)
            print(newString)
            var length: Int
            var finalLen:Int
            length = newString.characters.count
            finalLen = length - 6
            var myMutableString = NSMutableAttributedString()
            let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let firstAttributes = [NSFontAttributeName:UIFont.systemFontOfSize(13)]
            myMutableString = NSMutableAttributedString(string: newString as
                String, attributes: firstAttributes)
            myMutableString.addAttribute(NSForegroundColorAttributeName, value: iAppDelegate.defaultColor, range: NSRange(location:6,length:finalLen))
            
            cell.lblSponsorOrganization?.attributedText = myMutableString
            
            let url:String = (Constants.ImagePaths.kSponserImagePath + (modelSPonserobj?.sponsorLogoImg)! )
            //            cell.btnSponsor?.layer.cornerRadius=(cell.btnSponsor?.frame.size.height)!/2 - 10
            //            cell.btnSponsor?.clipsToBounds=true
            
            
            //            if tmpQuestion.profile_img.characters.count > 0 {
            //                let url:String = Constants.ImagePaths.kAttendeeImagePath + tmpQuestion.profile_img
            //                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            //                imgVw.setImageWithString(modelSPonserobj.sponsorName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            //                cell.imgProfile.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
            //            }else {
            //                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            //                imgVw.setImageWithString(tmpQuestion.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            //                cell.imgProfile.image = imgVw.image
            //            }
            
            
            cell.btnSponsor!.sd_setImageWithURL(NSURL(string: url), placeholderImage:UIImage(named:"login_default"), completed:nil)
            if modelSPonserobj.sponsorWebsite.characters.count != 0
            {
                cell.btnWebsite?.setImage(UIImage(named:"website_icon_active"), forState: .Normal)
            }
            
            if modelSPonserobj.sponsorFb.characters.count != 0
            {
                cell.btnfacebook?.setImage(UIImage(named:"facebook_icon_active"), forState: .Normal)
            }
            
            if modelSPonserobj.sponsorTwitter.characters.count != 0
            {
                cell.btnTwitter?.setImage(UIImage(named:"twitter_icon_active"), forState: .Normal)
            }
            
            
            cell.selectionStyle = .None
            
            // Configure the cell...
            //cell.textLabel?.text = "Row \(indexPath.row)"
            return cell
        }
        if indexPath.row == 1
        {
            let cell:SponsorDetailCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsDetailsCell", forIndexPath: indexPath) as! SponsorDetailCell
            
            let html = "<style>body{color: #aaaaaa;font-size: 15px;}span{color: #aaaaaa!important;font-size: 15px;}p{color: #aaaaaa!important;font-size: 15px;}ul{color: #aaaaaa!important;font-size: 15px;}li{color: #aaaaaa!important;font-size: 15px;}</style>" + (modelSPonserobj.sponsorDescription)!
            
            cell.textViewDetail?.loadHTMLString(html, baseURL: nil)
            cell.btnMore?.addTarget(self, action: #selector(loadMore), forControlEvents: UIControlEvents.TouchUpInside)
            cell.btnMore?.setTitleColor(iAppDelegate.defaultColor, forState: .Normal)
            cell.textViewDetail?.delegate=self
            cell.textViewDetail?.scrollView.scrollEnabled=false
           
            cell.selectionStyle = .None
            return cell
            
        }
        if indexPath.row == 2
        {
            let cell:HB_ExhibitorsBrochureCell = tableView.dequeueReusableCellWithIdentifier("HB_ExhibitorsBrochureCell", forIndexPath: indexPath) as! HB_ExhibitorsBrochureCell
            cell.selectionStyle = .None
            
            return cell
            
        }
        return cell1!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 225
            
        }
        if indexPath.row == 1
        {
            if isLoadMore == true {
                return CGFloat(webHeight) + 30
            }
            return 97
        }
        if indexPath.row == 2
        {
            return 60
        }
        return 0
        
        
    }
    
    func loadMore(sender : AnyObject) {
        let tmpButton : UIButton = sender as! UIButton
        
        isLoadMore = !isLoadMore
        if isLoadMore == true
        {
            webHeight = originalHeight
            tmpButton.setTitle("LESS", forState: .Normal)
        }
        else {
            webHeight=97
            tmpButton.setTitle("MORE", forState: .Normal)
        }
        
        self.tblView!.reloadData()
    }
    
    func downloadBroucher() {
        if (UIApplication.sharedApplication().canOpenURL(url))
        {
            UIApplication.sharedApplication().openURL(url)
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Brochure not available.", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_ExhibitorsInfoVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="SPONSOR INFO"
        
        
    }
    @IBAction func back_btn_click(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        let webViewHeight:NSString = webView.stringByEvaluatingJavaScriptFromString("document.body.scrollHeight;")!
        
        self.originalHeight = webViewHeight.integerValue
    }
    
    
    @IBAction func onTapGoogleButton(sender: AnyObject) {
        
        var temp = modelSPonserobj.sponsorName as NSString
        temp = temp .stringByReplacingOccurrencesOfString(" ", withString: "")
        
        print("https://www.google.co.in/search?site=webhp&source=hp&q=" + (temp as String))
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.google.co.in/search?site=webhp&source=hp&q=" + (temp as String))!)
    }
    
    
    
    @IBAction func onTapDownloadBrochure(sender: AnyObject) {
        
        if let brochure = modelSPonserobj.sponsorBrochure
        {
            print(brochure)
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Brochure not available.", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapWebsiteLink(sender: AnyObject) {
        if modelSPonserobj.sponsorWebsite.characters.count != 0 {
            UIApplication.sharedApplication().openURL(NSURL(string:modelSPonserobj.sponsorWebsite)!)
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Sponsor's website is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func onTapFacebookLink(sender: AnyObject) {
        if modelSPonserobj.sponsorFb.characters.count != 0 {
            UIApplication.sharedApplication().openURL(NSURL(string:modelSPonserobj.sponsorFb)!)
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Facebook link is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTapTwitterLink(sender: AnyObject) {
        if modelSPonserobj.sponsorTwitter.characters.count != 0 {
            UIApplication.sharedApplication().openURL(NSURL(string:modelSPonserobj.sponsorTwitter)!)
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Twitter link is not available", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        self.originalHeight=97
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
