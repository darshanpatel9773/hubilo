//
//  HB_ExihibitorsVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 14/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import SwiftLoader

class HB_ExihibitorsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var tblExhibitorsList: UITableView!
    var searchBarButtonItem: UIBarButtonItem?
    var searchBar = UISearchBar()
    var iMutArrExhibitorData:NSMutableArray!
    var iSearchExhibitorData:NSMutableArray!
    var modelExhibitorList:HB_ExhibitorList!
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iSearchExhibitorData = NSMutableArray()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.sizeToFit()
        let searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        searchBar.barTintColor = UIColor.clearColor()
        searchBar.tintColor = appDelegate.defaultColor
        if let textFieldInsideSearchBar = self.searchBar.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = UIColor.whiteColor()
            
        }
        
        let iBtnClear = searchTextField?.valueForKey("_clearButton") as! UIButton
        iBtnClear.setImage((iBtnClear.imageView?.image?.imageWithRenderingMode(.AlwaysTemplate)), forState: .Normal)
        
        iBtnClear.tintColor = UIColor.whiteColor()
        
        searchBarButtonItem = navigationItem.rightBarButtonItem
        setMenu()
        iMutArrExhibitorData = NSMutableArray()
        iSearchExhibitorData = NSMutableArray()
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            if let iUrl = getExhibitorsList() {
                SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                    SwiftLoader.hide()
                    let iDataDict = iData as! NSDictionary
                    let statusStr:NSString!
                    statusStr = iDataDict.valueForKey("status") as? NSString
                    if statusStr != "Fail"
                    {
                        self.tblExhibitorsList.hidden = false
                        self.iMutArrExhibitorData = (iDataDict.valueForKey("data") as! NSArray).mutableCopy() as! NSMutableArray
                        self.iSearchExhibitorData = self.iMutArrExhibitorData.mutableCopy() as! NSMutableArray
                        self.tblExhibitorsList.reloadData()
                    }
                    else
                    {
                        self.tblExhibitorsList.hidden = true
                    }
                }
            }
            else {
                // oops, something went wrong with the URL
            }
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func setDisplayArrayAs(iArrRef:NSArray)
    {
        //        self.mutArrDisplayCustomerData!.removeAllObjects()
        //        self.mutArrDisplayCustomerData!.addObjectsFromArray(iArrRef as [AnyObject])
        //        self.tblExhibitorsList.reloadData()
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        
        
        
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return iSearchExhibitorData.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        modelExhibitorList=HB_ExhibitorList(fromDictionary:
            iSearchExhibitorData.objectAtIndex(indexPath.row) as! NSDictionary)
        
        
        
        
        let cell:HB_BookMarkCell=tableView.dequeueReusableCellWithIdentifier("HB_BookMarkCell", forIndexPath: indexPath) as! HB_BookMarkCell
        //cell.imgBookMarkCell.layer.cornerRadius =  cell.imgBookMarkCell.frame.size.width/2
        //cell.imgBookMarkCell.clipsToBounds = true
        //print(modelExhibitorList.exhibitorEmailid)
        //        let iName = modelExhibitorList.exhibitorName
        //        let iEmailId=modelExhibitorList.exhibitorEmailid
        cell.lblName.text=modelExhibitorList.exhibitorName
        cell.lblWorkProfile.text=modelExhibitorList.exhibitorEmailid
        var url:String = (Constants.ImagePaths.kExhibitorImagePath + (modelExhibitorList?.exhibitorLogoImg)! )
        url=url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        //cell.imgBookMarkCell.sd_setImageWithURL(NSURL(string: url))
        //        cell.img_outer_view.layer.shadowOffset = CGSize(width: 0.1, height: 0.1)
        //        cell.img_outer_view.layer.shadowOpacity = 0.5
        //
        //        cell.imgBookMarkCell.maskCircle(25.0)
        //        cell.imgBookMarkCell.clipsToBounds = true
         cell.imgBookMarkCell.sd_setImageWithURL(NSURL(string: url), placeholderImage:UIImage(named:"HB_Exhibitors"), completed:nil)
        //        cell.lblCompany.text=""
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        modelExhibitorList=HB_ExhibitorList(fromDictionary:
            iSearchExhibitorData.objectAtIndex(indexPath.row) as! NSDictionary)
        let objSponsorrDetail:HB_ExhibitorsInfoVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_ExhibitorsInfoVC") as! HB_ExhibitorsInfoVC
        
        objSponsorrDetail.modelExhibitionList = modelExhibitorList
        self.navigationController?.pushViewController(objSponsorrDetail, animated: true)
        //self.performSegueWithIdentifier(Constants.SegueConstant.kShowExibitorsInfoVC, sender: self)
    }
    
    //MARK: User Define
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_ExihibitorsVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.navigationItem.title = "EXHIBITORS"
        let mySerachbtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySerachbtn.setImage(UIImage(named: "HB_Search"), forState: UIControlState.Normal)
        mySerachbtn.frame = CGRectMake(0, 0, 24, 24)
        mySerachbtn.addTarget(self, action: #selector(HB_ExihibitorsVC.searchButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomSearchButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySerachbtn)
        self.navigationItem.rightBarButtonItem = myCustomSearchButtonItem
        
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    @IBAction func searchButtonTouched(sender: AnyObject){
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_ExihibitorsVC.backButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        showSearchBar()
    }
    func showSearchBar() {
        self.searchBar.text = ""
        self.searchBar.hidden = false
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        self.searchBar.hidden = true
        navigationItem.titleView = nil
        setMenu()
        UIView.animateWithDuration(0.3, animations: {
            self.searchBar.alpha = 0
            }, completion: { finished in
        })
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if iMutArrExhibitorData.count > 0 {
        if searchText.characters.count == 0 {
            self.iSearchExhibitorData = self.iMutArrExhibitorData.mutableCopy() as! NSMutableArray
        } else {
            self.iSearchExhibitorData.removeAllObjects()
            self.iSearchExhibitorData = self.iMutArrExhibitorData.mutableCopy() as! NSMutableArray
            let searchResults:NSArray = self.iSearchExhibitorData!.filter{
                let exhibitorName = $0["exhibitor_name"]!!.lowercaseString
                let exhibitorEmail = $0["exhibitor_emailid"]!!.lowercaseString
                return exhibitorName.rangeOfString(searchText.lowercaseString) != nil
                    || exhibitorEmail.rangeOfString(searchText.lowercaseString) != nil
            }
            self.iSearchExhibitorData = searchResults.mutableCopy() as! NSMutableArray
        }
        
            self.tblExhibitorsList.reloadData()
        }
        
    }
    
    func backButtonTouched(sender: UIButton) {
        hideSearchBar()
        if self.iSearchExhibitorData.count > 0 {
            self.iSearchExhibitorData.removeAllObjects()
            self.iSearchExhibitorData = self.iMutArrExhibitorData.mutableCopy() as! NSMutableArray
            self.tblExhibitorsList.reloadData()
        }
    }
    
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
        
        
    }
    
    func getExhibitorsList() -> NSURL? {
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : Constants.eventInfo.kEventId], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.EXHIBITOR_LIST + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
