//
//  CommentCell.swift
//  Hubilo
//
//  Created by Aadil on 8/3/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    @IBOutlet var lblName : UILabel?
    @IBOutlet var lblComment : UILabel?
    @IBOutlet var imgUser : UIImageView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
