//
//  DiscussionVC.swift
//  Hubilo
//
//  Created by Aadil on 7/31/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
import SDWebImage
import ObjectMapper
import KYDrawerController

class DiscussionVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    var searchBar = UISearchBar()
    var searchBarButtonItem: UIBarButtonItem?
    @IBOutlet var imgNoMessage : UIImageView?
    @IBOutlet var tblView : UITableView?
    var filteredList : DiscussionList!
    var msgList : DiscussionList!
    var selectedSorted : String?
    var sharedManager : Globals = Globals.sharedInstance
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    @IBOutlet weak var btnNewest: UIButton!
    
    @IBOutlet weak var btnMostActive: UIButton!
    
    @IBOutlet weak var btnMostVoted: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.sizeToFit()
        let searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "search..", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        searchBar.barTintColor = UIColor.clearColor()
        searchBar.tintColor = appDelegate.defaultColor
        if let textFieldInsideSearchBar = self.searchBar.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = UIColor.whiteColor()
            
        }
        
        let iBtnClear = searchTextField?.valueForKey("_clearButton") as! UIButton
        iBtnClear.setImage((iBtnClear.imageView?.image?.imageWithRenderingMode(.AlwaysTemplate)), forState: .Normal)
        
        iBtnClear.tintColor = UIColor.whiteColor()
        
        //self.msgList = Mapper<DiscussionList>()
        
        
        searchBarButtonItem = navigationItem.rightBarButtonItem
        setMenu()
        self.tblView?.tableFooterView = UIView()
        selectedSorted = "newest"
        if sharedManager.isUserLoggedIn == false {
            self.navigationController?.popViewControllerAnimated(true)
        }
        else {
            loadMessages()
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        loadMessages()
    }
    
    func loadMessages() {
      
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                
                var userFetch = ""
                if sharedManager.isUserLoggedIn {
                        userFetch = sharedManager.currentUserID
                }
                
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : Constants.eventInfo.kEventId,"user_id" : userFetch, "sort_order" : "\(selectedSorted!)"], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.DISCUSSION_FORUM + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    if JSON["status"] == "Success"
                    {
                        self.msgList = Mapper<DiscussionList>().map(JSON.rawValue)
                        self.filteredList = Mapper<DiscussionList>().map(JSON.rawValue)
                        self.tblView?.reloadData()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard (filteredList != nil) else {
            return 0
            self.imgNoMessage?.hidden = false
        }
        self.imgNoMessage?.hidden = true
        return filteredList.discussionForumList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : DiscussionCell = tableView.dequeueReusableCellWithIdentifier("cell1") as! DiscussionCell
        
        let url:String = (Constants.ImagePaths.kAttendeeImagePath + (self.filteredList!.discussionForumList[indexPath.row].profile_img) )
        
        dispatch_async(dispatch_get_main_queue()) {
            cell.imgUser?.layer.cornerRadius = (cell.imgUser?.frame.size.width)! / 2
            cell.imgUser?.clipsToBounds = true
        }
    
        cell.lblName?.text = self.filteredList!.discussionForumList[indexPath.row].firstname + " " + self.filteredList!.discussionForumList[indexPath.row].lastname
        cell.lblTitle?.text = self.filteredList!.discussionForumList[indexPath.row].title
        cell.lblDescription?.text = self.filteredList!.discussionForumList[indexPath.row].fdescription
        cell.lblDesignation?.text = self.filteredList!.discussionForumList[indexPath.row].designation
        let forumDate : NSDate = NSDate(timeIntervalSince1970:Double(self.filteredList!.discussionForumList[indexPath.row].create_time_mili)!/1000)
        
        let dtFormatter : NSDateFormatter = NSDateFormatter()
       
        
        cell.btnLike?.setTitle(self.filteredList!.discussionForumList[indexPath.row].vote_count, forState: UIControlState.Normal)
        let imgVwLike = UIImageView.init(image: UIImage(named:"ic_like"))
        imgVwLike.image = imgVwLike.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        cell.btnLike?.setImage(imgVwLike.image, forState: UIControlState.Normal)
        
        if self.filteredList!.discussionForumList[indexPath.row].vote == "1" {
            cell.btnLike?.setImage(UIImage(named:"like_icon_active"), forState: .Normal)
        }else {
            cell.btnLike?.tintColor = UIColor(red: 192/255.0, green: 192/255.0, blue: 192/255.0, alpha:1.0)
        }
        
        cell.btnLike?.tag=indexPath.row;
        cell.btnLike?.addTarget(self, action: #selector(actionLike), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        cell.btnComment?.setTitle(self.filteredList!.discussionForumList[indexPath.row].comment_count, forState: UIControlState.Normal)
        cell.btnView?.setTitle(self.filteredList!.discussionForumList[indexPath.row].view_count, forState: UIControlState.Normal)
        
      let calendar = NSCalendar.currentCalendar()
        let dateComponents = calendar.components(.Day, fromDate: forumDate)
        let numberFormatter = NSNumberFormatter()
        
        if #available(iOS 9.0, *) {
            numberFormatter.numberStyle = .OrdinalStyle
        } else {
            // Fallback on earlier versions
        }
        
        let day = numberFormatter.stringFromNumber(dateComponents.day)
        
        
        dtFormatter.dateFormat = "MMM, yyyy"
        
        
        let dateString = "\(day!) \(dtFormatter.stringFromDate(forumDate))"
        
        
        
        cell.lblDate?.text = dateString
        cell.lblDate?.textColor = appDelegate.defaultColor
//        cell.imgUser?.sd_setImageWithURL(NSURL(string: url), placeholderImage: UIImage(named: "login_default"))
        
        if url.characters.count > 0  {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(self.filteredList!.discussionForumList[indexPath.row].firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgUser!.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(self.filteredList!.discussionForumList[indexPath.row].firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgUser!.image = imgVw.image
        }
        
        return cell
    }
    
    func actionLike(sender : UIButton) {
      
        if !sharedManager.isUserLoggedIn {
            SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
            return
        }
        if sharedManager.userCommunityExist == 0
        {
            SPGUtility.sharedInstance().showAlert("Please Join the community", titleMessage: "", viewController: self)
            return;
        }
        
        let btn : UIButton = sender
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                //var userFetch = "3113"
                let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["df_id" : self.filteredList!.discussionForumList[btn.tag].id,"user_id" : userFetch], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.DISCUSSION_LIKE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    let imgVwLike = UIImageView.init(image: UIImage(named:"like_icon_active"))
//                    imgVwLike.image = imgVwLike.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                    btn.setImage(imgVwLike.image, forState: UIControlState.Normal)
                    if JSON["status"] == "Success"
                    {
                        if JSON["isLike"] == 1
                        {
                            dispatch_async(dispatch_get_main_queue(), {
                                self.filteredList!.discussionForumList[btn.tag].vote = "1"
                                self.filteredList!.discussionForumList[btn.tag].vote_count = String ( Int(self.filteredList!.discussionForumList[btn.tag].vote_count)! + 1 )
                                btn.setImage(UIImage(named:"like_icon_active"), forState: .Normal)
                                btn.setTitle(String(Int((btn.titleLabel?.text)!)! + 1) , forState: UIControlState.Normal)
                            })
                         
                        }
                        else {
                            dispatch_async(dispatch_get_main_queue(), {
                                
                                self.filteredList!.discussionForumList[btn.tag].vote = "0"
                                self.filteredList!.discussionForumList[btn.tag].vote_count = String ( Int(self.filteredList!.discussionForumList[btn.tag].vote_count)! - 1 )
                           btn.setImage(UIImage(named:"ic_like"), forState: .Normal)
                            btn.setTitle(String(Int((btn.titleLabel?.text)!)! - 1) , forState: UIControlState.Normal)
                                })
                        }
                        
                       // self.msgList = Mapper<DiscussionList>().map(JSON.rawValue)
                        self.tblView?.reloadData()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    

    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 165;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.searchBar.resignFirstResponder()
        let discussion : DiscussionDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DiscussionDetailVC") as! DiscussionDetailVC
        
        discussion.currentdiscussion = self.filteredList!.discussionForumList[indexPath.row]
        self.navigationController?.pushViewController(discussion, animated: true)
        
    }
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(DiscussionVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        self.navigationItem.title = "DISCUSSION FORUM"
        
        
        let mySerachbtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySerachbtn.setImage(UIImage(named: "HB_Search"), forState: UIControlState.Normal)
        mySerachbtn.frame = CGRectMake(0, 0, 24, 24)
        mySerachbtn.addTarget(self, action: #selector(DiscussionVC.searchButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomSearchButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySerachbtn)
        
        
        
        let createbtn:UIButton = UIButton(type: UIButtonType.Custom)
        createbtn.setImage(UIImage(named: "chooser-button-tab"), forState: UIControlState.Normal)
        createbtn.frame = CGRectMake(0, 0, 24, 24)
        createbtn.addTarget(self, action: #selector(DiscussionVC.createButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let createbtn1:UIBarButtonItem = UIBarButtonItem(customView: createbtn)
        self.navigationItem.setRightBarButtonItems([createbtn1,myCustomSearchButtonItem], animated: true)
        // self.navigationItem.rightBarButtonItem = myCustomSearchButtonItem
    }
    func back_btn_click(sender:UIButton) {
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
    }
    
    
    @IBAction func createButton(sender: AnyObject){
        let discussion : CreateDiscussionVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("CreateDiscussionVC") as! CreateDiscussionVC
        self.navigationController?.pushViewController(discussion, animated: true)
        
    }
    
    @IBAction func discussBtnClicked(sender: UIButton){
        if sender.tag == 11
        {
            
            btnNewest.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
            btnNewest.backgroundColor = UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0)
            
            btnMostActive.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnMostActive.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            btnMostVoted.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnMostVoted.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            selectedSorted = "newest"
            loadMessages()
        }
        else if sender.tag == 12
        {
            btnMostActive.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
            btnMostActive.backgroundColor = UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0)
            
            btnNewest.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnNewest.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            btnMostVoted.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnMostVoted.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            selectedSorted = "most_active"
            loadMessages()
        }
        else if sender.tag == 13
        {
            btnMostVoted.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
            btnMostVoted.backgroundColor = UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0)
            
            btnMostActive.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnMostActive.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            btnNewest.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnNewest.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            selectedSorted = "most_voted"
            loadMessages()
        }
        
        
    }
    
    
    @IBAction func searchButtonTouched(sender: AnyObject){
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_SpeakersVC.backButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        showSearchBar()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if !ReachabilityCheck.isConnectedToNetwork() {
//            SPGUtility.sharedInstance().showAlert("data not available", titleMessage: "", viewController: self)
            return
        }
        if self.msgList.discussionForumList.count > 0 {
        if searchText.characters.count == 0 {
            self.filteredList.discussionForumList.removeAll()
            for dis in self.msgList.discussionForumList
            {
                self.filteredList.discussionForumList.append(dis)
            }
        } else {
            //self.filteredList.discussionForumList.removeAllObjects()
            self.filteredList.discussionForumList.removeAll()
            for dis in self.msgList.discussionForumList
            {
                self.filteredList.discussionForumList.append(dis)
            }
            for dis in self.filteredList.discussionForumList
            {
                if dis.title.lowercaseString.containsString(searchText.lowercaseString) || dis.fdescription.lowercaseString.containsString(searchText.lowercaseString) || dis.firstname.lowercaseString.containsString(searchText.lowercaseString) || dis.lastname.lowercaseString.containsString(searchText.lowercaseString) || dis.designation.lowercaseString.containsString(searchText.lowercaseString)
                {
                    
                }
                else{
                    self.filteredList.discussionForumList.removeAtIndex(self.filteredList.discussionForumList.indexOf(dis)!)
                }
            }
        }
            self.tblView!.reloadData()
        }
    }
    
    
    func showSearchBar() {
        self.searchBar.hidden = false
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItems(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
       // print(iMutArrSpeakerData)
    }
    
    func hideSearchBar() {
        self.searchBar.hidden = true
        navigationItem.titleView = nil
        setMenu()
        UIView.animateWithDuration(0.3, animations: {
            self.searchBar.alpha = 0
            }, completion: { finished in
        })
    }
    
    func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
    }
    
    func backButtonTouched(sender: UIButton) {
        hideSearchBar()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideSearchBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func actionValueChanged(sender : UISegmentedControl)
    {
        if ( sender.selectedSegmentIndex == 0 )
        {
            selectedSorted = "newest"
            loadMessages()
        }
        else if ( sender.selectedSegmentIndex == 1 )
        {
            selectedSorted = "most_active"
            loadMessages()
        }
        else if ( sender.selectedSegmentIndex == 2 )
        {
            selectedSorted = "most_voted"
            loadMessages()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
