//
//  HB_ForgotPassVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 12/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import TextFieldEffects

class HB_ForgotPassVC: UIViewController {
    
    @IBOutlet weak var txtFldEmailId:HoshiTextField!
    @IBOutlet weak var btnForgetPassword:UIButton!
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        btnForgetPassword.backgroundColor = appDelegate.defaultColor
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK Button Click Event
    @IBAction func close_btn_click(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }

    
    @IBAction func btnForgetPressed(sender:UIButton) {
        
        if ReachabilityCheck.isConnectedToNetwork() {
            if let url = getUrlForgetPassword(Constants.eventInfo.kEventId, emailId: txtFldEmailId.text!){
                SPGWebService.callGETWebService(url, WSCompletionBlock: { (data, error) in
                    if error != nil {
                        print(error?.localizedDescription)
                    }else {
                        let iDictData:NSDictionary = data as! NSDictionary
                        if iDictData.objectForKey("status") as! String == "Success" {
                            SPGUtility.sharedInstance().showAlert(iDictData.objectForKey("msg") as! String, titleMessage: "", viewController: self)
                        }else {
                            SPGUtility.sharedInstance().showAlert(iDictData.objectForKey("msg") as! String, titleMessage: "", viewController: self)
                        }
                    }
                })
            }
            
        }else {
            SPGUtility.sharedInstance().showAlert("Newtork not availble", titleMessage: "", viewController: self)
        }
        
    }
    
    func getUrlForgetPassword(eventID: String,emailId: String) -> NSURL? {
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["emailid" : emailId,"event" : eventID], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.FORGET_PASSWORD + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
  

}
