//
//  HB_PollQuestionCell.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 27/08/16.
//  Copyright © 2016 nirav. All rights reserved.
//

import UIKit

class HB_PollQuestionCell: UITableViewCell {
    
    @IBOutlet weak var lblQuestion:UILabel!
    @IBOutlet weak var lblPollType:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
