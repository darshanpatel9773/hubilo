//
//  HB_PollListVC.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 26/08/16.
//  Copyright © 2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import SwiftLoader
import SDWebImage

class HB_PollListVC: UIViewController,UISearchBarDelegate {
    
    var searchBar = UISearchBar()
    var searchBarButtonItem: UIBarButtonItem?
    @IBOutlet var tblPollList : UITableView?
    var mutArrSearchPollDisplay : NSMutableArray = NSMutableArray()
    var mutArrPollList : NSMutableArray = NSMutableArray()
    var selectedSorted : String?
    var sharedManager : Globals = Globals.sharedInstance
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var likePollList:HBPollList!
    
    
    @IBOutlet weak var btnNewest: UIButton!
    
    @IBOutlet weak var btnMostActive: UIButton!
    
    @IBOutlet weak var btnMostVoted: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.sizeToFit()
        let searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "search..", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        searchBar.barTintColor = UIColor.clearColor()
        searchBar.tintColor = appDelegate.defaultColor
        if let textFieldInsideSearchBar = self.searchBar.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = UIColor.whiteColor()
        }
        
        let iBtnClear = searchTextField?.valueForKey("_clearButton") as! UIButton
        iBtnClear.setImage((iBtnClear.imageView?.image?.imageWithRenderingMode(.AlwaysTemplate)), forState: .Normal)
        
        iBtnClear.tintColor = UIColor.whiteColor()
        searchBarButtonItem = navigationItem.rightBarButtonItem
        setMenu()
        self.tblPollList?.tableFooterView = UIView()
        
        selectedSorted = "all"
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadPollList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadPollList(){
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            do {
                var userFetch = ""
                if sharedManager.isUserLoggedIn {
                    userFetch = sharedManager.currentUserID
                }
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : Constants.eventInfo.kEventId,"user_id" : userFetch, "sort_order" : "\(selectedSorted!)"], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.GET_ALL_POLL + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
              SPGWebService.callGETWebService(NSURL(string:url)!,WSCompletionBlock: { (data, error) in
                if error != nil {
                    print(error)
                    SwiftLoader.hide()
                }else {
                    let iDictPollList:NSDictionary = data as! NSDictionary
                    let iMutArrPollList:NSMutableArray = (iDictPollList.objectForKey("pollList") as! NSArray).mutableCopy() as! NSMutableArray
                    
//                    let tempPollList:NSMutableArray = NSMutableArray()
                    self.mutArrPollList = NSMutableArray()
                    self.mutArrSearchPollDisplay = NSMutableArray()
                    
                    for iCounter in 0 ..< iMutArrPollList.count {
                        let iDictPollDecoded:NSMutableDictionary = NSMutableDictionary()
                        
                        var iStrAboutMe:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("aboutme") as! String
                        iStrAboutMe = iStrAboutMe.base64Decoded()
                        iDictPollDecoded.setObject(iStrAboutMe, forKey:"aboutme")
                        
                        var iStrCreateTimeMili:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("create_time_mili") as! String
                        iStrCreateTimeMili = iStrCreateTimeMili.base64Decoded()
                        iDictPollDecoded.setObject(iStrCreateTimeMili, forKey:"create_time_mili")
                        
                        var iStrDesignation:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("designation") as! String
                        iStrDesignation = iStrDesignation.base64Decoded()
                        iDictPollDecoded.setObject(iStrDesignation, forKey:"designation")
                        
                        var iStrEndTime:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("end_time") as! String
                        iStrEndTime = iStrEndTime.base64Decoded()
                        iDictPollDecoded.setObject(iStrEndTime, forKey:"end_time")
                        
                        var iStrStartTime:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("start_time") as! String
                        iStrStartTime = iStrStartTime.base64Decoded()
                        iDictPollDecoded.setObject(iStrStartTime, forKey:"start_time")
                        
                        var iStrFbId:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("fb_id") as! String
                        iStrFbId = iStrFbId.base64Decoded()
                        iDictPollDecoded.setObject(iStrFbId, forKey:"fb_id")
                        
                        var iStrFirstName:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("firstname") as! String
                        iStrFirstName = iStrFirstName.base64Decoded()
                        iDictPollDecoded.setObject(iStrFirstName, forKey:"firstname")
                        
                        var iStrInstruction:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("instruction") as! String
                        iStrInstruction = iStrInstruction.base64Decoded()
                        iDictPollDecoded.setObject(iStrInstruction, forKey:"instruction")
                        
                        let iStrISBookMark:Int = iMutArrPollList.objectAtIndex(iCounter).valueForKey("is_bookmark") as! Int
                        //iStrCreateTimeMili = iStrCreateTimeMili.base64Decoded()
                        iDictPollDecoded.setObject(iStrISBookMark, forKey:"is_bookmark")
                        
                        var iStrLastName:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("lastname") as! String
                        iStrLastName = iStrLastName.base64Decoded()
                        iDictPollDecoded.setObject(iStrLastName, forKey:"lastname")
                        
                         var iStrLinkedinPublicURL:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("linkedin_public_url") as! String
                        iStrLinkedinPublicURL = iStrLinkedinPublicURL.base64Decoded()
                        iDictPollDecoded.setObject(iStrLinkedinPublicURL, forKey:"linkedin_public_url")
                        
                         var iStrOrganization:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("organization") as! String
                        iStrOrganization = iStrOrganization.base64Decoded()
                        iDictPollDecoded.setObject(iStrOrganization, forKey:"organization")
                        
                         var iStrPollId:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("poll_id") as! String
                        iStrPollId = iStrPollId.base64Decoded()
                        iDictPollDecoded.setObject(iStrPollId, forKey:"poll_id")
                        
                         let iStrPollType:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("poll_type") as! String
                        //iStrPollType = iStrPollType.base64Decoded()
                        iDictPollDecoded.setObject(iStrPollType, forKey:"poll_type")
                        
                         var iStrProfileImg:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("profile_img") as! String
                        iStrProfileImg = iStrProfileImg.base64Decoded()
                        iDictPollDecoded.setObject(iStrProfileImg, forKey:"profile_img")
                        
                         var iStrQuestion:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("question") as! String
                        iStrQuestion = iStrQuestion.base64Decoded()
                        iDictPollDecoded.setObject(iStrQuestion, forKey:"question")
                        
                        var iStrAnswer:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("answer") as! String
                        iStrAnswer = iStrAnswer.base64Decoded()
                        iDictPollDecoded.setObject(iStrAnswer, forKey:"answer")
                        
                        if iMutArrPollList.objectAtIndex(iCounter).valueForKey("did_vote") != nil {
                            var iStrDidVote:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("did_vote") as! String
                            iStrDidVote = iStrDidVote.base64Decoded()
                            iDictPollDecoded.setObject(iStrDidVote, forKey:"did_vote")
                        }

                        
                         let iStrTotalAnswers:Int = iMutArrPollList.objectAtIndex(iCounter).valueForKey("totalAnswers") as! Int
                        //iStrTotalAnswers = iStrTotalAnswers.base64Decoded()
                        iDictPollDecoded.setObject(iStrTotalAnswers, forKey:"totalAnswers")
                        
                         var iStrTwitterPublicURL:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("twitter_public_url") as! String
                        iStrTwitterPublicURL = iStrTwitterPublicURL.base64Decoded()
                        iDictPollDecoded.setObject(iStrTwitterPublicURL, forKey:"twitter_public_url")
                        
                        var iStrUserId:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("user_id") as! String
                        iStrUserId = iStrUserId.base64Decoded()
                        iDictPollDecoded.setObject(iStrUserId, forKey:"user_id")
                        
                        var iStrVoteCount:String = iMutArrPollList.objectAtIndex(iCounter).valueForKey("vote_count") as! String
                        iStrVoteCount = iStrVoteCount.base64Decoded()
                        iDictPollDecoded.setObject(iStrVoteCount, forKey:"vote_count")
                        
                        let iPoll:HBPollList = HBPollList(fromDictionary: iDictPollDecoded)
                        self.mutArrPollList.addObject(iPoll)
                    }
//                    
//                    let descriptor: NSSortDescriptor = NSSortDescriptor(key: "create_time_mili", ascending: false)
//                    let sortedResults: NSArray = tempPollList.sortedArrayUsingDescriptors([descriptor])
//                    
//                    for iCounter in 0 ..< sortedResults.count {
//                        let iPoll:HBPollList = HBPollList(fromDictionary: sortedResults.objectAtIndex(iCounter) as! NSDictionary)
//                        self.mutArrPollList.addObject(iPoll)
//                    }
                    
                    self.mutArrSearchPollDisplay = self.mutArrPollList.mutableCopy() as! NSMutableArray
                    
                    self.tblPollList?.reloadData()
                    SwiftLoader.hide()
                    
                }
                
              })
                
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_PollListVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.navigationItem.title = "POLLS"
        
        
        let mySerachbtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySerachbtn.setImage(UIImage(named: "HB_Search"), forState: UIControlState.Normal)
        mySerachbtn.frame = CGRectMake(0, 0, 24, 24)
        mySerachbtn.addTarget(self, action: #selector(HB_PollListVC.searchButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        let myCustomSearchButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySerachbtn)
        
        
        
        let createbtn:UIButton = UIButton(type: UIButtonType.Custom)
        createbtn.setImage(UIImage(named: "chooser-button-tab"), forState: UIControlState.Normal)
        createbtn.frame = CGRectMake(0, 0, 24, 24)
        createbtn.addTarget(self, action: #selector(HB_PollListVC.createButton(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let createbtn1:UIBarButtonItem = UIBarButtonItem(customView: createbtn)
        self.navigationItem.setRightBarButtonItems([createbtn1,myCustomSearchButtonItem], animated: true)
        // self.navigationItem.rightBarButtonItem = myCustomSearchButtonItem
    }
    

    func back_btn_click(sender:UIButton) {
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
    }
    
    
    
    
    @IBAction func createButton(sender: AnyObject){
        let pollVC : HB_CreatePollVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_CreatePollVC") as! HB_CreatePollVC
        self.navigationController?.pushViewController(pollVC, animated: true)
    }
    
    @IBAction func searchButtonTouched(sender: AnyObject){
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_PollListVC.backButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        showSearchBar()
    }
    
    func backButtonTouched(sender: UIButton) {
        hideSearchBar()
    }
    
    func showSearchBar() {
        self.searchBar.hidden = false
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItems(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        self.searchBar.hidden = true
        navigationItem.titleView = nil
        setMenu()
        UIView.animateWithDuration(0.3, animations: {
            self.searchBar.alpha = 0
            }, completion: { finished in
        })
    }
    
    
    
    
    @IBAction func btnPollClicked(sender: UIButton){
        if sender.tag == 11
        {
            
            btnNewest.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
            btnNewest.backgroundColor = UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0)
            
            btnMostActive.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnMostActive.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            btnMostVoted.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnMostVoted.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            selectedSorted = "all"
            loadPollList()
        }
        else if sender.tag == 12
        {
            btnMostActive.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
            btnMostActive.backgroundColor = UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0)
            
            btnNewest.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnNewest.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            btnMostVoted.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnMostVoted.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            selectedSorted = "newest"
            loadPollList()
        }
        else if sender.tag == 13
        {
            btnMostVoted.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
            btnMostVoted.backgroundColor = UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0)
            
            btnMostActive.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnMostActive.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            btnNewest.setTitleColor(UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0), forState: .Normal)
            btnNewest.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
            
            selectedSorted = "most_voted"
            loadPollList()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return mutArrSearchPollDisplay.count
        
        
//        guard (filteredList != nil) else {
//            return 0
//            self.imgNoMessage?.hidden = false
//        }
//        self.imgNoMessage?.hidden = true
//        return filteredList.discussionForumList.count
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : HB_PollListCell = tableView.dequeueReusableCellWithIdentifier("cell1") as! HB_PollListCell
        let iPoll:HBPollList = mutArrSearchPollDisplay.objectAtIndex(indexPath.row) as! HBPollList
        
        let url:String = (Constants.ImagePaths.kAttendeeImagePath + iPoll.profileImg )
        
        dispatch_async(dispatch_get_main_queue()) {
            cell.imgUser?.layer.cornerRadius = (cell.imgUser?.frame.size.width)! / 2
            cell.imgUser?.clipsToBounds = true
        }

        cell.lblName?.text = iPoll.firstname + " " + iPoll.lastname
        cell.lblQuestion?.text = iPoll.question
        cell.lblDesignation?.text = iPoll.designation
        let pollDate : NSDate = NSDate(timeIntervalSince1970:Double(iPoll.createTimeMili)!/1000)

        let dtFormatter : NSDateFormatter = NSDateFormatter()
        
        
        cell.btnLike?.setTitle(iPoll.voteCount, forState: UIControlState.Normal)
        cell.btnVote?.setTitle("\(iPoll.totalAnswers)", forState: .Normal)
        
        
        var imgVwLike:UIImageView = UIImageView()
        imgVwLike = UIImageView.init(image: UIImage(named:"favorite_icon_normal"))
        if iPoll.didVote != nil {
            if iPoll.didVote == "1" {
                imgVwLike = UIImageView.init(image: UIImage(named:"favorite_icon_active"))
            }
        }
        
        cell.btnLike?.setImage(imgVwLike.image, forState: UIControlState.Normal)
        cell.btnLike?.tag=indexPath.row;
        cell.btnLike?.addTarget(self, action: #selector(actionLike), forControlEvents: UIControlEvents.TouchUpInside)
//
//        
//        cell.btnComment?.setTitle(self.filteredList!.discussionForumList[indexPath.row].comment_count, forState: UIControlState.Normal)
//        cell.btnView?.setTitle(self.filteredList!.discussionForumList[indexPath.row].view_count, forState: UIControlState.Normal)
//        
        let calendar = NSCalendar.currentCalendar()
        let dateComponents = calendar.components(.Day, fromDate: pollDate)
        let numberFormatter = NSNumberFormatter()

        if #available(iOS 9.0, *) {
            numberFormatter.numberStyle = .OrdinalStyle
        } else {
            // Fallback on earlier versions
        }
        
        let day = numberFormatter.stringFromNumber(dateComponents.day)
        dtFormatter.dateFormat = "MMM, yyyy"
        let dateString = "\(day!) \(dtFormatter.stringFromDate(pollDate))"
            cell.lblDate?.text = dateString
        cell.lblDate?.textColor = appDelegate.defaultColor
//        //        cell.imgUser?.sd_setImageWithURL(NSURL(string: url), placeholderImage: UIImage(named: "login_default"))
//        
        if url.characters.count > 0  {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(iPoll.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgUser!.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(iPoll.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgUser!.image = imgVw.image
        }
        
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 158;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pollDetailVC : HB_PollDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_PollDetailVC") as! HB_PollDetailVC
        pollDetailVC.selectedPollList = mutArrSearchPollDisplay.objectAtIndex(indexPath.row) as! HBPollList
        self.navigationController?.pushViewController(pollDetailVC, animated: true)
    }
    
    
    func actionLike(sender : UIButton) {
        
        if !sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
            return
        }
        
        if sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You need to join community", titleMessage: "", viewController: self)
            return
        }
       
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
           likePollList =  mutArrSearchPollDisplay.objectAtIndex(sender.tag) as! HBPollList
            do {
                //var userFetch = "3113"
                let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["pid" : likePollList.pollId,"user_id" : userFetch], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.LIKE_POLL + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                   
                    if JSON["status"] == "Success"
                    {
                        if JSON["isLike"] == 1
                        {
                            dispatch_async(dispatch_get_main_queue(), {
                                self.likePollList.didVote = "1"
                                self.likePollList.voteCount = String ( Int(self.likePollList.voteCount)! + 1 )
                                sender.tintColor = UIColor.greenColor()
                                sender.setTitle(String(Int((sender.titleLabel?.text)!)! + 1) , forState: UIControlState.Normal)
                                sender.setImage(UIImage(named:"favorite_icon_active"), forState: .Normal)
                            })
                        }
                        else {
                            dispatch_async(dispatch_get_main_queue(), {
                                self.likePollList.didVote = "0"
                                self.likePollList.voteCount = String ( Int(self.likePollList.voteCount)! - 1 )
                                sender.tintColor = UIColor.greenColor()
                                sender.setTitle(String(Int((sender.titleLabel?.text)!)! - 1) , forState: UIControlState.Normal)
                                sender.setImage(UIImage(named:"favorite_icon_normal"), forState: .Normal)
                            })
                        }
                        self.mutArrSearchPollDisplay.removeObjectAtIndex(sender.tag)
                        self.mutArrSearchPollDisplay.insertObject(self.likePollList, atIndex: sender.tag)
                        self.tblPollList?.reloadData()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
