//
//  HB_PollUserCell.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 27/08/16.
//  Copyright © 2016 nirav. All rights reserved.
//

import UIKit

class HB_PollUserCell: UITableViewCell {
    
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblUserDesignation:UILabel!
    @IBOutlet weak var btnLike:UIButton!
    @IBOutlet weak var btnVote:UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
