//
//  HB_CreatePollVC.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 26/08/16.
//  Copyright © 2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import TextFieldEffects
import SwiftLoader
import BEMCheckBox


class HB_CreatePollVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,BEMCheckBoxDelegate {
    
    var sharedManager : Globals = Globals.sharedInstance
    
    @IBOutlet weak var txtFldQuestion:UITextField!
    @IBOutlet weak var txtFldInstruction:UITextField!
    @IBOutlet weak var txtFldStartDate:UITextField!
    @IBOutlet weak var txtFldEndDate:UITextField!
    @IBOutlet weak var txtFldResponse:UITextField!
    @IBOutlet weak var btnIncludeOtherOptions:DLRadioButton!
    @IBOutlet weak var txtFldOption1:UITextField!
    @IBOutlet weak var txtFldOption2:UITextField!
    @IBOutlet weak var vWOption:UIView!
    @IBOutlet weak var checkBox:BEMCheckBox!
    
    @IBOutlet weak var vWMainHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var vWOptionHeightSonstraint:NSLayoutConstraint!
    @IBOutlet weak var scrollVwCreatePoll:UIScrollView!
    @IBOutlet weak var vWMainBottomConstraint:NSLayoutConstraint!
    
    var pickerVwStart:UIDatePicker = UIDatePicker()
    var pickerVwEnd:UIDatePicker = UIDatePicker()
    var pickerVwResponse:UIPickerView = UIPickerView()
    var isOtherOptions:Bool = false
    
    var lastTag:Int = 2
    var arrResopnse:NSArray = NSArray(objects:"Only single response allowed","Multiple response allowed")
    var isMultiple:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenu()
        checkBox.boxType = .Square
        checkBox.delegate = self
        pickerVwStart.datePickerMode = .Date
        pickerVwEnd.datePickerMode = .Date
        txtFldStartDate.inputView = pickerVwStart
        pickerVwStart.tag = 11
        txtFldEndDate.inputView = pickerVwEnd
        pickerVwEnd.tag = 12
        pickerVwStart.minimumDate = NSDate()
        pickerVwEnd.minimumDate = NSDate()
        pickerVwResponse.delegate = self
        pickerVwResponse.dataSource = self
        txtFldResponse.inputView = pickerVwResponse
        txtFldResponse.text = arrResopnse.objectAtIndex(0) as? String
        
    pickerVwStart.addTarget(self, action: #selector(HB_CreatePollVC.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
    pickerVwEnd.addTarget(self, action: #selector(HB_CreatePollVC.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
txtFldStartDate.addDoneOnKeyboardWithTarget(self,action:#selector(HB_CreatePollVC.doneStartAction(_:)))
    txtFldEndDate.addDoneOnKeyboardWithTarget(self,action:#selector(HB_CreatePollVC.doneEndAction(_:)))
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_CreatePollVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        self.navigationItem.title = "Create Polls"
        let myCancelbtn:UIButton = UIButton(type: UIButtonType.Custom)
        myCancelbtn.setImage(UIImage(named: "Cancel"), forState: UIControlState.Normal)
        myCancelbtn.frame = CGRectMake(0, 0, 24, 24)
        myCancelbtn.addTarget(self, action: #selector(HB_CreatePollVC.cancelButtonTouched(_:)), forControlEvents:UIControlEvents.TouchUpInside)
        
        let myCustomCancelButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myCancelbtn)
        self.navigationItem.rightBarButtonItem = myCustomCancelButtonItem
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if sender.tag == 11 {
            txtFldStartDate.text = dateFormatter.stringFromDate(sender.date)
            pickerVwEnd.minimumDate = sender.date
        }else if sender.tag == 12 {
            txtFldEndDate.text = dateFormatter.stringFromDate(sender.date)
        }
    }
    
    func doneStartAction(barButton:UIBarButtonItem) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtFldStartDate.text = dateFormatter.stringFromDate(pickerVwStart.date)
        pickerVwEnd.minimumDate = pickerVwStart.date
        txtFldStartDate.resignFirstResponder()
    }
    
    func doneEndAction(barButton:UIBarButtonItem) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtFldEndDate.text = dateFormatter.stringFromDate(pickerVwEnd.date)
        txtFldEndDate.resignFirstResponder()
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField)  {
        
        if lastTag < 10 {
            if lastTag == textField.tag {
                let txtFldOption:HoshiTextField = HoshiTextField(frame:CGRectMake(20,(textField.frame.origin.y + textField.frame.size.height + 8),280, 35))
                txtFldOption.placeholder = "Option \(lastTag + 1)"
                txtFldOption.borderInactiveColor = UIColor.lightGrayColor()
                txtFldOption.borderActiveColor = UIColor.clearColor()
                txtFldOption.tag = lastTag + 1
                txtFldOption.font = UIFont.systemFontOfSize(15.0)
                txtFldOption.delegate = self
                lastTag = lastTag + 1
                vWOption.addSubview(txtFldOption)
                vWMainHeightConstraint.constant = vWMainHeightConstraint.constant + txtFldOption.frame.size.height + 8
                vWOptionHeightSonstraint.constant = vWOptionHeightSonstraint.constant + txtFldOption.frame.size.height + 8
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnCancelPressed(sender:UIButton) {
        self.navigationController!.popViewControllerAnimated(true)
    }

    @IBAction func btnSubmitPressed(sender:UIButton) {
        
        if !sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("you need to login", titleMessage:"", viewController: self)
            return
        }
        
        if txtFldQuestion.text?.characters.count < 0 {
            SPGUtility.sharedInstance().showAlert("Please add question", titleMessage:"", viewController: self)
            return
        }
        
        if txtFldInstruction.text?.characters.count < 0 {
            SPGUtility.sharedInstance().showAlert("Please add instruction", titleMessage:"", viewController: self)
            return
        }
        
        if txtFldStartDate.text?.characters.count < 0 {
            SPGUtility.sharedInstance().showAlert("Please add start date", titleMessage:"", viewController: self)
            return
        }
        
        if txtFldEndDate.text?.characters.count < 0 {
            SPGUtility.sharedInstance().showAlert("Please add end date", titleMessage:"", viewController: self)
            return
        }
        
        if txtFldOption1.text?.characters.count < 0 {
            SPGUtility.sharedInstance().showAlert("Please add option 1", titleMessage:"", viewController: self)
            return
        }
        
        if txtFldOption2.text?.characters.count < 0 {
            SPGUtility.sharedInstance().showAlert("Please add option 2", titleMessage:"", viewController: self)
            return
        }
        
        var iStrUserId:String = sharedManager.currentUserID
        iStrUserId = iStrUserId.base64Encoded()
        
        var iStrEventId:String = Constants.eventInfo.kEventId
        iStrEventId = iStrEventId.base64Encoded()
        
        var iStrQuestion:String = txtFldQuestion.text!
        iStrQuestion = iStrQuestion.base64Encoded()
        
        var iStrInstruction:String = txtFldInstruction.text!
        iStrInstruction = iStrInstruction.base64Encoded()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        var date:NSDate = dateFormatter.dateFromString(txtFldStartDate.text!)!
        var timeInterval:NSTimeInterval = (date.timeIntervalSince1970)*1000
        var iIntTime:Int = Int(timeInterval)
        var iStrStartDate:String = "\(iIntTime)"
        iStrStartDate = iStrStartDate.base64Encoded()
        
         date = dateFormatter.dateFromString(txtFldEndDate.text!)!
         timeInterval = (date.timeIntervalSince1970)*1000
          iIntTime = Int(timeInterval)
        var iStrEndDate:String = "\(iIntTime)"
        iStrEndDate = iStrEndDate.base64Encoded()
        
        var iStrIncludeOtherOption:String = ""
        if isOtherOptions {
            iStrIncludeOtherOption = "1"
        }else {
            iStrIncludeOtherOption = "0"
        }
        
        iStrIncludeOtherOption = iStrIncludeOtherOption.base64Encoded()
        
        var iStrMutliple:String = ""
        
        if isMultiple {
            iStrMutliple = "multiple"
        }else {
            iStrMutliple = "single"
        }
        iStrMutliple = iStrMutliple.base64Encoded()
        
        let iMutArrOption:NSMutableArray = NSMutableArray()
        iMutArrOption.addObject("")
        for iCounter in 1 ..< lastTag {
            let txtFldOptions:HoshiTextField = self.vWOption.viewWithTag(iCounter) as! HoshiTextField
            if txtFldOptions.text?.characters.count > 0 {
                iMutArrOption.addObject(txtFldOptions.text!)
            }
        }
        var iStrOption:String = ""
        do {
            let iData:NSData = try NSJSONSerialization.dataWithJSONObject(iMutArrOption, options: NSJSONWritingOptions.init(rawValue: 0))
            let iStrJsonString:String = String.init(data: iData, encoding: NSUTF8StringEncoding)!
            iStrOption = iStrJsonString
        }catch {
            
        }
        iStrOption = iStrOption.base64Encoded()
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
        do {
            
            
            let url = (Constants.URL.CREATE_POLL)
            
            APICall.requestPOSTURL(url, params: ["user_id" : iStrUserId,"event_id" : iStrEventId, "question" :iStrQuestion,"instruction":iStrInstruction,"options":iStrOption,"start_date":iStrStartDate,"end_date":iStrEndDate,"includeOthers":iStrIncludeOtherOption,"singleormultipleoption":iStrMutliple], headers: nil, success: { (JSON) in
                SwiftLoader.hide()
                
                if JSON["status"] == "Success"
                {
                    
                    self.navigationController!.popViewControllerAnimated(true)
                    
                    // SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                }else {
                    // SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                }
                }, failure: { (NSError) in
                    SwiftLoader.hide()
                    SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
            })
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
        }
    }
    else
    {
    
    SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
    }

    
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
      return 1
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrResopnse.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrResopnse.objectAtIndex(row) as? String
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtFldResponse.text = arrResopnse.objectAtIndex(row) as? String
        if row == 1 {
            isMultiple = true
        }else {
            isMultiple = false
        }
    }
    
    func back_btn_click(sender:UIButton) {
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
    }
    
    func cancelButtonTouched(sender:UIButton) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    
    
    func didTapCheckBox(checkBox: BEMCheckBox) {
        let state = checkBox.on ? true : false
        isOtherOptions = state
    }
   

}
