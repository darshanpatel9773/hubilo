//
//  HB_PollDetailVC.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 26/08/16.
//  Copyright © 2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader

class HB_PollDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var selectedPollList:HBPollList!
    
    @IBOutlet weak var lblQuestion:UILabel!
    @IBOutlet weak var lblPollType:UILabel!
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblUserDesignation:UILabel!
    @IBOutlet weak var btnLike:UIButton!
    @IBOutlet weak var btnVote:UIButton!
    @IBOutlet weak var vWPollOption:UIView!
    @IBOutlet weak var vWPollChart:UIView!
    
    var sharedManager : Globals = Globals.sharedInstance
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var arrAnswer:NSMutableArray = NSMutableArray()
    var radioButtons:DLRadioButton!
    var heightBar:CGFloat = 0.0
    var heightRadioVw:CGFloat = 0.0
    var heightPollQuestion:CGFloat = 0.0
    var iStrType:String = ""
    
    @IBOutlet weak var tblPollDetail:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_PollDetailVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        self.navigationItem.title = "POLL INFO"
        
        tblPollDetail.estimatedRowHeight = 50.0
        tblPollDetail.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view.
        
//        lblQuestion.text = selectedPollList.question
//        lblPollType.text = selectedPollList.pollType
//        
//        let url:String = (Constants.ImagePaths.kAttendeeImagePath + selectedPollList.profileImg )
//        
//        dispatch_async(dispatch_get_main_queue()) {
//            self.imgUser?.layer.cornerRadius = (self.imgUser?.frame.size.width)! / 2
//            self.imgUser?.clipsToBounds = true
//        }
//        lblUserName?.text = selectedPollList.firstname + " " + selectedPollList.lastname
//        lblUserDesignation?.text = selectedPollList.designation
       
        tblPollDetail.tableFooterView = UIView()

        do {
         arrAnswer = try NSJSONSerialization.JSONObjectWithData(selectedPollList.answer.dataUsingEncoding(NSUTF8StringEncoding)!, options: NSJSONReadingOptions.MutableContainers) as! NSMutableArray
            
           print(arrAnswer)
           //createPollBar()
        }catch {
            
        }
    }
    
    func back_btn_click(sender:UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func createPollBar()  {
//        let iDictTemp:NSDictionary = arrAnswer.objectAtIndex(0) as! NSDictionary
//        let iStrType:String = iDictTemp.objectForKey("type") as! String
// 
//        let XValue:CGFloat = 4.0
//        var YValue:CGFloat = 4.0
//        let btnHeight:CGFloat = 25.0
//        
//        let iDictOptins:NSDictionary = arrAnswer.objectAtIndex(1) as! NSDictionary
//        
//        let btnRadioOption:DLRadioButton = createRadioButton(CGRectMake(XValue, YValue,(vWPollOption.frame.size.width - 16), btnHeight), title:iDictOptins.objectForKey("val") as! String, color: UIColor.orangeColor())
//        YValue = YValue + btnHeight
//        
//        if iStrType == "multiple" {
//           btnRadioOption.multipleSelectionEnabled = true
//        }else {
//           btnRadioOption.multipleSelectionEnabled = false
//        }
//         vWPollOption.addSubview(btnRadioOption)
//        var otherButtons : [DLRadioButton] = []
//        
//        for iCounter in 2 ..< arrAnswer.count {
//            let iDictOptins:NSDictionary = arrAnswer.objectAtIndex(iCounter) as! NSDictionary
//            let OtherOption:DLRadioButton =  createRadioButton(CGRectMake(XValue, YValue,(vWPollOption.frame.size.width - 16), btnHeight), title: iDictOptins.objectForKey("val") as! String, color: UIColor.orangeColor())
//            YValue = YValue + btnHeight
//            otherButtons.append(OtherOption)
//            vWPollOption.addSubview(OtherOption)
//        }
//        
//        btnRadioOption.otherButtons = otherButtons
    }
    
    private func createRadioButton(frame : CGRect, title : String, color : UIColor,isEditable:Bool) -> DLRadioButton {
        let radioButton = DLRadioButton(frame: frame);
        radioButton.titleLabel!.font = UIFont.systemFontOfSize(14);
        radioButton.setTitle(title, forState: UIControlState.Normal);
        radioButton.setTitleColor((UIColor(red: 123.0/255.0, green: 123.0/255.0, blue: 123.0/255.0, alpha: 1.0)), forState: UIControlState.Normal);
        if isEditable {
        for view in radioButton.subviews {
            if view.isKindOfClass(UILabel) {
                let lblTemp:UILabel = view as! UILabel
                let txtFld:UITextField = UITextField(frame: CGRectMake(20.0,0,radioButton.frame.size.width - 35.0,radioButton.frame.size.height))
                txtFld.font = UIFont.systemFontOfSize(12.0)
                txtFld.placeholder = "Mention if other"
                txtFld.text = title
                txtFld.textColor = (UIColor.blackColor())
                lblTemp.hidden = true
                radioButton.addSubview(txtFld)
            }
            }
        }
        radioButton.iconColor = color;
        radioButton.indicatorColor = color;
        radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left;
        radioButton.addTarget(self, action: #selector(HB_PollDetailVC.logSelectedButton(_:)), forControlEvents: UIControlEvents.TouchUpInside);
        return radioButton;
    }
    
    func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.multipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(button.tag)
               // print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(radioButton.selectedButton()?.tag)
            btnVotePollPressed((radioButton.selectedButton()?.tag)!)
            //print(String(format: "%@ is selected.\n", radioButton.selectedButton()!.titleLabel!.text!));
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let pollCell = UITableViewCell()
        
        if indexPath.row == 0 {
            let cell:HB_PollQuestionCell = tableView.dequeueReusableCellWithIdentifier("HB_PollQuestionCell", forIndexPath: indexPath) as! HB_PollQuestionCell
            cell.lblQuestion.text = selectedPollList.question
            cell.lblPollType.text = selectedPollList.pollType
            
            let iStrQuestion:String = selectedPollList.question
            let heightOfQuestion:CGFloat = iStrQuestion.heightWithConstrainedWidth(UIScreen.mainScreen().bounds.size.width - 32, font: cell.lblQuestion.font)
            heightPollQuestion = heightOfQuestion
            
            cell.selectionStyle = .None
            return cell
        }else if indexPath.row == 1 {
            let cell:HB_PollOptionCell = tableView.dequeueReusableCellWithIdentifier("HB_PollOptionCell", forIndexPath: indexPath) as! HB_PollOptionCell
            if cell.vWPollOption != nil {
                cell.vWPollOption.subviews.forEach({ $0.removeFromSuperview() })}
            
            let iDictTemp:NSDictionary = arrAnswer.objectAtIndex(0) as! NSDictionary
            iStrType = iDictTemp.objectForKey("type") as! String
            let iStrOthers:String = iDictTemp.objectForKey("others") as! String
            var iStrOthersResponded:Int = 0
            if iDictTemp.objectForKey("others_responded") != nil {
                iStrOthersResponded = iDictTemp.objectForKey("others_responded") as! Int
            }
            var iStrOtherResponse:String = ""
            if iDictTemp.objectForKey("others_response") != nil {
                iStrOtherResponse = iDictTemp.objectForKey("others_response") as! String
            }
            
            let XValue:CGFloat = 4.0
            var YValue:CGFloat = 4.0
            var btnHeight:CGFloat = 25.0
            print(selectedPollList.startTime)
            
            let dtFormatter = NSDateFormatter()
            dtFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let iEndDate:NSDate = dtFormatter.dateFromString(selectedPollList.endTime)!
            
            var isPollCompleted:Bool = false
            if NSDate().timeIntervalSince1970 < iEndDate.timeIntervalSince1970 {
                isPollCompleted = false
            }else {
                isPollCompleted = true
            }
            
            var btnRadioOption:DLRadioButton = DLRadioButton()
            btnRadioOption.titleLabel?.lineBreakMode = .ByWordWrapping
            var intStart:Int = 0
            if iStrOthers == "1" {
                let iFloatHeight = iStrOtherResponse.heightWithConstrainedWidth(cell.vWPollOption.frame.size.width - 16, font:(btnRadioOption.titleLabel?.font)!)
                if iFloatHeight > 25 {
                    btnHeight = iFloatHeight
                }
                btnRadioOption = createRadioButton(CGRectMake(XValue, YValue,(cell.vWPollOption.frame.size.width - 16), btnHeight), title:iStrOtherResponse, color: colorValue(1),isEditable: true)
                if isPollCompleted {
                    btnRadioOption.iconSize = 0.0
                    btnRadioOption.indicatorSize = 0.0
                }
                
                if iStrOthersResponded == 1 {
                    btnRadioOption.selected = true
                }
                YValue = YValue + btnHeight
                btnHeight = 25.0
                if iStrType == "multiple" {
                    btnRadioOption.multipleSelectionEnabled = true
                }else {
                    btnRadioOption.multipleSelectionEnabled = false
                }
                intStart = 1
                btnRadioOption.tag = 1
                cell.vWPollOption.addSubview(btnRadioOption)
            }else {
                let iDictOptins:NSDictionary = arrAnswer.objectAtIndex(1) as! NSDictionary
                var iStrUserResponded:Int = 0
                if iDictOptins.objectForKey("responded") != nil {
                    iStrUserResponded = iDictOptins.objectForKey("responded") as! Int
                }
                var iStrOption:String = ""
                if iDictOptins.objectForKey("val") != nil {
                    iStrOption = iDictOptins.objectForKey("val") as! String
                }
                
                let iFloatHeight = iStrOption.heightWithConstrainedWidth(cell.vWPollOption.frame.size.width - 16, font:(btnRadioOption.titleLabel?.font)!)
                if iFloatHeight > 25 {
                    btnHeight = iFloatHeight
                }
                btnRadioOption = createRadioButton(CGRectMake(XValue, YValue,(cell.vWPollOption.frame.size.width - 16), btnHeight), title:iStrOption, color: colorValue(1),isEditable: false)
                if isPollCompleted {
                    btnRadioOption.iconSize = 0.0
                    btnRadioOption.indicatorSize = 0.0
                }
                if iStrUserResponded == 1 {
                    btnRadioOption.selected = true
                }
                
                YValue = YValue + btnHeight
                btnHeight = 25.0
                if iStrType == "multiple" {
                    btnRadioOption.multipleSelectionEnabled = true
                }else {
                    btnRadioOption.multipleSelectionEnabled = false
                }
                intStart = 2
                btnRadioOption.tag = 2
                cell.vWPollOption.addSubview(btnRadioOption)
            }
            
            
            
            var otherButtons : [DLRadioButton] = []
            
            for iCounter in intStart ..< arrAnswer.count {
                let iDictOptins:NSDictionary = arrAnswer.objectAtIndex(iCounter) as! NSDictionary
                
                var iStrOption:String = ""
                if iDictOptins.objectForKey("val") != nil {
                    iStrOption = iDictOptins.objectForKey("val") as! String
                    
                }
                
                let iFloatHeight = iStrOption.heightWithConstrainedWidth(cell.vWPollOption.frame.size.width - 16, font:(btnRadioOption.titleLabel?.font)!)
                if iFloatHeight > 25 {
                    btnHeight = iFloatHeight
                }
                
                let OtherOption:DLRadioButton =  createRadioButton(CGRectMake(XValue, YValue,(cell.vWPollOption.frame.size.width - 16), btnHeight), title: iDictOptins.objectForKey("val") as! String, color: colorValue(iCounter),isEditable: false)
                OtherOption.titleLabel?.lineBreakMode = .ByWordWrapping
                if isPollCompleted {
                    OtherOption.iconSize = 0.0
                    OtherOption.indicatorSize = 0.0
                }
                var iStrUserResponded:Int = 0
                if iDictOptins.objectForKey("responded") != nil {
                    iStrUserResponded = iDictOptins.objectForKey("responded") as! Int
                }
                if iStrUserResponded == 1 {
                    OtherOption.selected = true
                }
                if iStrType == "multiple" {
                    OtherOption.multipleSelectionEnabled = true
                }else {
                    OtherOption.multipleSelectionEnabled = false
                }
                OtherOption.tag = iCounter + 1
                YValue = YValue + btnHeight
                btnHeight = 25.0
                otherButtons.append(OtherOption)
                cell.vWPollOption.addSubview(OtherOption)
            }
            
            btnRadioOption.otherButtons = otherButtons
            heightRadioVw = YValue + 8.0
            cell.selectionStyle = .None
            return cell
        }else if indexPath.row == 2 {
            let cell:HB_PollChartCell = tableView.dequeueReusableCellWithIdentifier("HB_PollChartCell", forIndexPath: indexPath) as! HB_PollChartCell
            if cell.vWPollChart != nil {
                cell.vWPollChart.subviews.forEach({ $0.removeFromSuperview() })}
            heightBar = 0.0
            let iDictTemp:NSDictionary = arrAnswer.objectAtIndex(0) as! NSDictionary
            let iStrOthers:String = iDictTemp.objectForKey("others") as! String
           // let iFloatOthersResult:CGFloat = iDictTemp.objectForKey("others_result") as! CGFloat
            var iStrOthersResult = ""
            if let strResult = iDictTemp.objectForKey("others_result") {
                iStrOthersResult = "\(strResult)"
            }
            var otherResult:CGFloat  = 0.0
            if let n = NSNumberFormatter().numberFromString(iStrOthersResult) {
                otherResult = CGFloat(n)
            }
            
            var XValue:CGFloat = 20.0
            let YValue:CGFloat = 8.0
            let vWWidth:CGFloat = 15.0
            
            if iStrOthers == "1" {
                let vWPlot:UIView = UIView()
                let lblChart:UILabel = UILabel()
                lblChart.frame = CGRectMake(XValue-10.0,YValue,40.0,10.0)
                lblChart.font = UIFont.systemFontOfSize(10.0)
                lblChart.text = "\(otherResult)"
                lblChart.textColor = colorValue(0)
                lblChart.textAlignment = .Center
                if heightBar < otherResult {
                    heightBar = otherResult
                }
                vWPlot.frame = CGRectMake(XValue,YValue + 10.0, vWWidth,otherResult)
                vWPlot.backgroundColor = colorValue(0)
                cell.vWPollChart.addSubview(lblChart)
                cell.vWPollChart.addSubview(vWPlot)
                XValue = XValue + vWWidth + 20.0
            }
            
            for iCounter in 1 ..< arrAnswer.count {
                let iDictTemp:NSDictionary = arrAnswer.objectAtIndex(iCounter) as! NSDictionary
                print(iDictTemp.objectForKey("result"))

             
                var iStrResult = ""
                if let strResult = iDictTemp.objectForKey("result") {
                    iStrResult = "\(strResult)"
                }
                var result:CGFloat  = 0.0
                if let n = NSNumberFormatter().numberFromString(iStrResult) {
                    result = CGFloat(n)
                }
                
                let vWPlot:UIView = UIView()
                let lblChart:UILabel = UILabel()
              
                    lblChart.frame = CGRectMake(XValue-10.0,YValue,40.0,10.0)
                    lblChart.font = UIFont.systemFontOfSize(10.0)
                    lblChart.text = "\(result)"
                    lblChart.textColor = colorValue(iCounter)
                    lblChart.textAlignment = .Center
                    vWPlot.frame = CGRectMake(XValue,YValue + 10.0, vWWidth,result)
                    vWPlot.backgroundColor = colorValue(iCounter)
                    if heightBar < result {
                        heightBar = result
                    }
                
               
                XValue = XValue + vWWidth + 20.0
                cell.vWPollChart.addSubview(lblChart)
                cell.vWPollChart.addSubview(vWPlot)
            }
            
            
            heightBar = heightBar + 35.0
            var iFrameVw:CGRect = cell.vWPollChart.frame
            iFrameVw.size.height = heightBar
            cell.vWPollChart.frame = iFrameVw
            cell.selectionStyle = .None
            
            return cell
        }else if indexPath.row == 3 {
            let cell:HB_PollUserCell = tableView.dequeueReusableCellWithIdentifier("HB_PollUserCell", forIndexPath: indexPath) as! HB_PollUserCell
            
            let url:String = (Constants.ImagePaths.kAttendeeImagePath + selectedPollList.profileImg )
            
            dispatch_async(dispatch_get_main_queue()) {
                cell.imgUser?.layer.cornerRadius = (cell.imgUser?.frame.size.width)! / 2
                cell.imgUser?.clipsToBounds = true
            }
           
            
            cell.lblUserName.text = selectedPollList.firstname + " " + selectedPollList.lastname
            cell.lblUserDesignation.text = selectedPollList.designation
            
            if url.characters.count > 0  {
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(selectedPollList.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cell.imgUser!.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
            }else {
                let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
                imgVw.setImageWithString(selectedPollList.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                cell.imgUser!.image = imgVw.image
            }
            
            cell.btnLike?.setTitle(selectedPollList.voteCount, forState: UIControlState.Normal)
            cell.btnVote?.setTitle("\(selectedPollList.totalAnswers)", forState: .Normal)
            
            
            var imgVwLike:UIImageView = UIImageView()
            //
             imgVwLike = UIImageView.init(image: UIImage(named:"favorite_icon_normal"))
            if selectedPollList.didVote != nil {
                if selectedPollList.didVote == "1" {
                    imgVwLike = UIImageView.init(image: UIImage(named:"favorite_icon_active"))
                }
            }

            cell.btnLike?.setImage(imgVwLike.image, forState: UIControlState.Normal)
            cell.btnLike?.addTarget(self, action: #selector(actionLike), forControlEvents: UIControlEvents.TouchUpInside)
            
            cell.selectionStyle = .None
            return cell
        }
        
        return pollCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            if heightPollQuestion > 23 {
                return 50 + heightPollQuestion
            }
            return 50
        case 1:
            return heightRadioVw
        case 2:
            return heightBar
        case 3:
            return 66
        default:
            return 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func colorValue(index:Int) -> UIColor {
        
        switch index {
        case 0:
            return UIColor(red: 241/255.0, green: 193/255.0, blue: 2/255.0, alpha: 1.0)
        case 1:
            return UIColor(red: 255/255.0, green: 32/255.0, blue: 95/255.0, alpha: 1.0)
        case 2:
            return UIColor(red: 38/255.0, green: 84/255.0, blue: 255/255.0, alpha: 1.0)
        case 3:
            return UIColor(red: 0/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1.0)
        case 4:
            return UIColor(red: 45/255.0, green: 186/255.0, blue: 236/255.0, alpha: 1.0)
        case 5:
            return UIColor(red: 13/255.0, green: 178/255.0, blue: 20/255.0, alpha: 1.0)
        case 6:
            return UIColor(red: 255/255.0, green: 150/255.0, blue: 47/255.0, alpha: 1.0)
        case 7:
            return UIColor(red: 106/255.0, green: 152/255.0, blue: 255/255.0, alpha: 1.0)
        case 8:
            return UIColor(red: 23/255.0, green: 220/255.0, blue: 167/255.0, alpha: 1.0)
        case 9:
            return UIColor(red: 3/255.0, green: 218/255.0, blue: 215/255.0, alpha: 1.0)
        case 10:
            return UIColor(red: 16/255.0, green: 149/255.0, blue: 226/255.0, alpha: 1.0)
        default:
            break
        }
        
        return UIColor.yellowColor()
    }
    
    func actionLike(sender : UIButton) {
        
        if !sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
            return
        }
        
        if sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You need to join community", titleMessage: "", viewController: self)
            return
        }
        
        
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                //var userFetch = "3113"
                let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["pid" : selectedPollList.pollId,"user_id" : userFetch], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.LIKE_POLL + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    if JSON["status"] == "Success"
                    {
                        if JSON["isLike"] == 1
                        {
                            dispatch_async(dispatch_get_main_queue(), {
                                self.selectedPollList.didVote = "1"
                                self.selectedPollList.voteCount = String ( Int(self.selectedPollList.voteCount)! + 1 )
                                sender.tintColor = UIColor.greenColor()
                                sender.setTitle(String(Int((sender.titleLabel?.text)!)! + 1) , forState: UIControlState.Normal)
                                sender.setImage(UIImage(named:"favorite_icon_active"), forState: .Normal)
                            })
                        }
                        else {
                            dispatch_async(dispatch_get_main_queue(), {
                                self.selectedPollList.didVote = "0"
                                self.selectedPollList.voteCount = String ( Int(self.selectedPollList.voteCount)! - 1 )
                                sender.tintColor = UIColor.greenColor()
                                sender.setTitle(String(Int((sender.titleLabel?.text)!)! - 1) , forState: UIControlState.Normal)
                                sender.setImage(UIImage(named:"favorite_icon_normal"), forState: .Normal)
                            })
                        }
                        
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    func btnVotePollPressed(tag:Int) {
        if !sharedManager.isUserLoggedIn {
            SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
            return
        }
        
        if sharedManager.userCommunityExist == 0 {
            SPGUtility.sharedInstance().showAlert("You need to join community", titleMessage: "", viewController: self)
            return
        }
        
        var iStrPassTYpe:String = ""
        if iStrType == "multiple" {
           iStrPassTYpe = "checkbox"
        }else {
            iStrPassTYpe = "radio"
        }
        
        for iCounter in 0 ..< arrAnswer.count {
            let iDictOptins:NSMutableDictionary = arrAnswer.objectAtIndex(iCounter) as! NSMutableDictionary
            
            if iCounter == 0 {
                iDictOptins.setObject(0,forKey:"others_responded")
            }else {
                iDictOptins.setObject(0,forKey:"responded")
            }
            
            if iCounter == 0 {
                if iCounter == tag - 1 {
                 iDictOptins.setObject(1,forKey:"others_responded")
                }
            }else {
                if iCounter == tag - 1 {
                    iDictOptins.setObject(1,forKey:"responded")
                }
            }
            self.arrAnswer.removeObjectAtIndex(iCounter)
            self.arrAnswer.insertObject(iDictOptins, atIndex: iCounter)
        }
        print(arrAnswer)
        let iDictOptins:NSDictionary = arrAnswer.objectAtIndex(tag-1) as! NSDictionary
        let iStrPid:Int = iDictOptins.objectForKey("id") as! Int
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                //var userFetch = "3113"
                let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_id" : userFetch,"event_id":Constants.eventInfo.kEventId,"theId":iStrPid,"pid" : selectedPollList.pollId,"type":iStrPassTYpe], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.VOTE_POLL + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    if JSON["status"] == "Success"
                    {
                        let iDictAnswer:NSDictionary = JSON["result"].dictionaryObject!
                        for iCounter in 0 ..< self.arrAnswer.count {
                            let iDictResult:NSMutableDictionary = self.arrAnswer.objectAtIndex(iCounter) as! NSMutableDictionary
                            if iCounter == 0 {
                                var iStrOthersResult = ""
                                if let strResult = iDictAnswer.objectForKey("others") {
                                    iStrOthersResult = "\(strResult)"
                                }
                                var otherResult:CGFloat  = 0.0
                                if let n = NSNumberFormatter().numberFromString(iStrOthersResult) {
                                    otherResult = CGFloat(n)
                                }
                                iDictResult.setObject("\(otherResult)", forKey: "others_result")
                                self.arrAnswer.removeObjectAtIndex(iCounter)
                                self.arrAnswer.insertObject(iDictResult, atIndex: iCounter)
                            }else {
                                var iStrResult = ""
                                if let strResult = iDictAnswer.objectForKey("\(iCounter)") {
                                    iStrResult = "\(strResult)"
                                }
                                var result:CGFloat  = 0.0
                                if let n = NSNumberFormatter().numberFromString(iStrResult) {
                                    result = CGFloat(n)
                                }
                                iDictResult.setObject("\(result)", forKey: "result")
                                self.arrAnswer.removeObjectAtIndex(iCounter)
                                self.arrAnswer.insertObject(iDictResult, atIndex: iCounter)
                            }
                            
                        }
                        self.tblPollDetail.reloadData()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
