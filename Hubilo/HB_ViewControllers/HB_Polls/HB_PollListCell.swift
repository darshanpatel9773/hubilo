//
//  HB_PollListCell.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 26/08/16.
//  Copyright © 2016 nirav. All rights reserved.
//

import UIKit

class HB_PollListCell: UITableViewCell {
    
    @IBOutlet var lblQuestion : UILabel?
    @IBOutlet var lblDescription : UILabel?
    @IBOutlet var imgUser : UIImageView?
    @IBOutlet var lblName : UILabel?
    @IBOutlet var lblDesignation : UILabel?
    @IBOutlet var btnLike : UIButton?
    @IBOutlet var btnVote : UIButton?
    @IBOutlet var lblDate : UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
