//
//  HB_ScheduleVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 14/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import SwiftLoader
import SDWebImage
import FSCalendar

class HB_ScheduleVC: UIViewController,CLWeeklyCalendarViewDelegate,UITableViewDataSource,UITableViewDelegate, KYDrawerControllerDelegate,FSCalendarDelegate,FSCalendarDataSource {
    @IBOutlet weak var tblSchedule: UITableView!
    @IBOutlet var btnSchedule : UIButton?
    @IBOutlet var btnMySchedule : UIButton?
    @IBOutlet var calendar:FSCalendar!
    var isMySchedule : Bool = false
    var modelAgenda:HB_ModelAgenda!
    var modelAgendaList:HB_AgendaList!
    var modelDayList:HB_Daylist!
    var model_Speakers:HB_Speaker!
    var mutDicAgenda:NSMutableDictionary!
    var mutArrAgendaList:NSMutableArray!
    var mutArrDayListL:NSMutableArray!
     var numberSection=0
    var numberofRows=0
    var mutableArrRows:NSMutableArray!
    var rowCounter:Int=0
    var calendarView:CLWeeklyCalendarView?
    var mutArrSectionObjects:NSMutableArray?
    var dicrowsObjects:NSMutableDictionary?
    var sharedManager : Globals = Globals.sharedInstance
    let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
//        tblSchedule.reloadData()
//        super.viewWillAppear(true)
        
        
        if (calendarView == nil)
        {
            
            calendarView=CLWeeklyCalendarView(frame: CGRect(x: 0, y: 64, width: UIScreen.mainScreen().bounds.size.width , height: 70))
            
            self.view.addSubview(calendarView!)
            
        }
        
        calendar.scope =  .Week
        
        btnSchedule!.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
        btnSchedule!.backgroundColor = UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0)
        
        btnMySchedule!.setTitleColor(UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0), forState: .Normal)
        btnMySchedule!.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        
        setMenu()
        if ReachabilityCheck.isConnectedToNetwork()
        {
            
            
            SwiftLoader.show(animated: true)
            var url : NSURL!
            if sharedManager.isUserLoggedIn {
                url = getScheduleInfo(Constants.eventInfo.kEventId, userID: sharedManager.currentUserID)
            }
            else {
                url = getScheduleInfo(Constants.eventInfo.kEventId, userID: "0")
            }
            if let iUrl = url {
                SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                    // print(iData)
                    let dicAttendeeList:NSDictionary = iData as! NSDictionary
                    
                    self.modelAgenda=HB_ModelAgenda(fromDictionary: dicAttendeeList)
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        self.calendarView?.redrawToDate(dateFormatter.dateFromString((self.modelAgenda.daylist.first?.date)!));
                        self.dailyCalendarViewDidSelect(dateFormatter.dateFromString((self.modelAgenda.daylist.first?.date)!))
                        self.calendarView?.selectedDate=dateFormatter.dateFromString((self.modelAgenda.daylist.first?.date)!)
                    })
                    
                    
                    print(self.modelAgenda.daylist)
                    
                    print(self.modelAgenda.daylist.count)
                    self.mutArrDayListL=NSMutableArray()
                    for dayObje in self.modelAgenda.daylist
                    {
                        self.mutArrDayListL.addObject(dayObje)
                        
                        
                    }
                    
                    //   self.modelAgendaList=HB_AgendaList(fromDictionary: self.modelAgendaList.)
                    
                    //   print(self.mutArrAttendeeList)
                    //   print(self.mutArrAttendeeList?.count)
                    //                for obj in self.mutArrAttendeeList!
                    //                {
                    //
                    //                    //      print(obj)
                    //
                    //                    self.modelAttendeeList = HB_ModelAttendeeList(fromDictionary:self.mutArrAttendeeList as! NSDictionary)
                    //
                    //
                    //                    //      print(self.modelAttendeeList)
                    //                    //self.modelArrAttendeeList?.addObject(self.modelAttendeeList!)
                    //
                    //                    self.modelArrAttendeeList?.addObject(self.modelArrAttendeeList!)
                    //                }
                    //
                    
                    SwiftLoader.hide()
                    
                }
            } else {
                // oops, something went wrong with the URL
            }
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
        
        calendarView?.delegate=self
        // Do any additional setup after loading the view.
    }
    

    override func viewDidAppear(animated: Bool) {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tblSchedule.reloadData()
    }
    
    func drawerController(drawerController: KYDrawerController, stateChanged state: KYDrawerController.DrawerState) {
            self.viewDidLoad()
    }
    func getScheduleInfo(eventID: String,userID: String) -> NSURL? {
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_id" : userID,"event_id" : eventID], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.SCHEDULE_ALL_DETAILS + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return numberSection
    
    
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
    
        let objects:NSMutableArray=dicrowsObjects?.valueForKey("Section\(indexPath.section)") as! NSMutableArray
        
        modelAgendaList=objects.objectAtIndex(indexPath.row) as! HB_AgendaList
        
        if isMySchedule{
            if modelAgendaList.isLike == 0 {
                return 0
            }
            
        }
        
        if modelAgendaList.speakers.count>0
        {
                return 130
        }
        
        
        return 75
    
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        guard (mutableArrRows != nil) else {
            return 0
        }
        return mutableArrRows.objectAtIndex(section) as! Int
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell:HB_SpeakerScheduleCell = tableView.dequeueReusableCellWithIdentifier("HB_SpeakerScheduleCell", forIndexPath: indexPath) as! HB_SpeakerScheduleCell
        
        
        let objects:NSMutableArray=dicrowsObjects?.valueForKey("Section\(indexPath.section)") as! NSMutableArray
        
        modelAgendaList=objects.objectAtIndex(indexPath.row) as! HB_AgendaList
        cell.btnLike.tag=indexPath.row+indexPath.section+1000
        cell.btnLike.section=indexPath.section
        cell.btnLike.row=indexPath.row
        cell.btnLike.addTarget(self, action: #selector(actionLike), forControlEvents: UIControlEvents.TouchUpInside)
        if sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 1 {
            if modelAgendaList.isLike == 1 {
                cell.btnLike.setImage(UIImage(named: "favorite_icon_active"), forState: UIControlState.Normal)
            }
            else {
                cell.btnLike.setImage(UIImage(named: "favorite_icon_normal"), forState: UIControlState.Normal)
            }
        }
        
        cell.collectionVW.tag = indexPath.row
        
        if modelAgendaList.speakers.count > 0 {
            cell.mutSpeakersDetail = modelAgendaList.speakers
        }
        cell.collectionVW.reloadData()
        
//        if modelAgendaList.speakers.count>0
//        {
//            cell.btnProfileImg1.hidden=false
//            if modelAgendaList.speakers.count==1 {
//            
//                for view in cell.btnProfileImg1.subviews
//                {
//                    view.removeFromSuperview()
//                }
//                let img1 : UIImageView = UIImageView(frame: CGRectMake(5, 0, 25+10, 25+10))
//                img1.layer.cornerRadius =   img1.frame.size.width/2
//                img1.clipsToBounds = true
//                
//                
////                img1.sd_setImageWithURL(NSURL(string: Constants.ImagePaths.kSpeakerImagePath+modelAgendaList.speakers[0].speakerProfileImg), placeholderImage: UIImage(named: "login_default"))
//                
//                if modelAgendaList.speakers[0].speakerProfileImg != nil {
//                    let url:String = Constants.ImagePaths.kSpeakerImagePath+modelAgendaList.speakers[0].speakerProfileImg
//                    let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
//                    imgVw.setImageWithString(modelAgendaList.speakers[0].speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
//                    img1.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
//                }else {
//                    let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
//                     imgVw.setImageWithString(modelAgendaList.speakers[0].speakerName, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
//                    img1.image = imgVw.image
//                }
//                
//                
//                cell.btnProfileImg1.addSubview(img1)
//                
//                cell.btnProfileImg2.hidden=true
//                cell.btnProfileImg3.hidden=true
//                cell.btnProfileImg4.hidden=true
//                cell.btnProfileImg5.hidden=true
//                
//            }
//            else
//            {
//                
//                
//            }
//            
//            if modelAgendaList.speakers.count==2 {
//                
//                for view in cell.btnProfileImg2.subviews
//                {
//                    view.removeFromSuperview()
//                }
//                let img1 : UIImageView = UIImageView(frame: CGRectMake(0, 0, 25+10, 25+10))
//                img1.layer.cornerRadius =   img1.frame.size.width/2
//                img1.clipsToBounds = true
//                img1.sd_setImageWithURL(NSURL(string: Constants.ImagePaths.kSpeakerImagePath+modelAgendaList.speakers[1].speakerProfileImg))
//                cell.btnProfileImg2.addSubview(img1)
//                cell.btnProfileImg3.hidden=true
//                cell.btnProfileImg4.hidden=true
//                cell.btnProfileImg5.hidden=true
//                
//            }
//            else
//            {
//                
//            }
//            if modelAgendaList.speakers.count==3 {
//                
//                for view in cell.btnProfileImg3.subviews
//                {
//                    view.removeFromSuperview()
//                }
//                let img1 : UIImageView = UIImageView(frame: CGRectMake(0, -20, 25+10, 25+10))
//                img1.layer.cornerRadius =   img1.frame.size.width/2
//                img1.clipsToBounds = true
//                img1.sd_setImageWithURL(NSURL(string: Constants.ImagePaths.kSpeakerImagePath+modelAgendaList.speakers[2].speakerProfileImg))
//                cell.btnProfileImg3.addSubview(img1)
//                cell.btnProfileImg4.hidden=true
//                cell.btnProfileImg5.hidden=true
//                
//            }
//            else
//            {
//                
//                
//            }
//            if modelAgendaList.speakers.count==4 {
//                for view in cell.btnProfileImg4.subviews
//                {
//                    view.removeFromSuperview()
//                }
//                let img1 : UIImageView = UIImageView(frame: CGRectMake(0, -20, 25+10, 25+10))
//                img1.layer.cornerRadius =   img1.frame.size.width/2
//                img1.clipsToBounds = true
//                img1.sd_setImageWithURL(NSURL(string: Constants.ImagePaths.kSpeakerImagePath+modelAgendaList.speakers[3].speakerProfileImg))
//                cell.btnProfileImg4.addSubview(img1)
//                cell.btnProfileImg5.hidden=true
//                
//            }
//            else
//            {
//                
//                
//            }
//            if modelAgendaList.speakers.count==5 {
//                
//                for view in cell.btnProfileImg5.subviews
//                {
//                    view.removeFromSuperview()
//                }
//                let img1 : UIImageView = UIImageView(frame: CGRectMake(0, -20, 25+10, 30+10))
//                img1.layer.cornerRadius =   img1.frame.size.width/2
//                img1.clipsToBounds = true
//                img1.sd_setImageWithURL(NSURL(string: Constants.ImagePaths.kSpeakerImagePath+modelAgendaList.speakers[4].speakerProfileImg))
//                cell.btnProfileImg5.addSubview(img1)
//                
//            }
//            cell.btnProfileImg1.imageView!.layer.cornerRadius =   cell.btnProfileImg1.imageView!.frame.size.width/2
//            cell.btnProfileImg1.imageView!.clipsToBounds = true
//            
//            cell.btnProfileImg2.imageView!.layer.cornerRadius =   cell.btnProfileImg2.imageView!.frame.size.width/2
//            cell.btnProfileImg2.imageView!.clipsToBounds = true
//            
//            cell.btnProfileImg3.imageView!.layer.cornerRadius =   cell.btnProfileImg3.imageView!.frame.size.width/2
//            cell.btnProfileImg3.imageView!.clipsToBounds = true
//            
//            cell.btnProfileImg4.imageView!.layer.cornerRadius =   cell.btnProfileImg4.imageView!.frame.size.width/2
//            cell.btnProfileImg4.imageView!.clipsToBounds = true
//            
//            cell.btnProfileImg5.layer.cornerRadius =   cell.btnProfileImg5.frame.size.width/2
//            cell.btnProfileImg5.clipsToBounds = true
//            cell.selectionStyle = .None
//            
//                cell.heightConstraintForSpeakers.constant=43
//            
//        }
        
        
        let dateFormatter = NSDateFormatter()
      //  16:36:00.000Z
        dateFormatter.dateFormat = "HH:mm:ss"
        let dateStartTime = dateFormatter.dateFromString(modelAgendaList.agendaStartTime)
        
        let dateEndTime = dateFormatter.dateFromString(modelAgendaList.agendaEndTime)
        
        // To convert the date into an HH:mm format
        dateFormatter.dateFormat = "hh:mm" // or //h:mm a
        let dateString = dateFormatter.stringFromDate(dateStartTime!) + "-" + dateFormatter.stringFromDate(dateEndTime!)
        cell.lblTime.text=dateString
        
        print(dateString)
        
        cell.lblagendaName.text=modelAgendaList.agendaName
        //cell.lbladdress.text=modelAgendaList.agendaLocation
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        
        let objects:NSMutableArray=dicrowsObjects?.valueForKey("Section\(section)") as! NSMutableArray
        
        modelAgendaList=objects.objectAtIndex(0) as! HB_AgendaList
        if isMySchedule {
            if modelAgendaList.isLike == 0 {
                return 0
            }
        }

        return 30
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var vw:UIView?
        vw=UIView(frame: CGRect(x: 0, y: 0, width: (UIScreen.mainScreen().bounds.width), height: 30))
        vw!.backgroundColor = UIColor(colorLiteralRed: 239.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1)
        
        let lblSpeaker:UILabel=UILabel(frame: CGRect(x: 15, y: 0, width: (UIScreen.mainScreen().bounds.width)-15, height: 30))
        
        let objects:NSMutableArray=dicrowsObjects?.valueForKey("Section\(section)") as! NSMutableArray
        
        modelAgendaList=objects.objectAtIndex(0) as! HB_AgendaList
        
        
        let dateFormatter = NSDateFormatter()
        //  16:36:00.000Z
        dateFormatter.dateFormat = "HH:mm:ss"
        let dateStartTime = dateFormatter.dateFromString(modelAgendaList.agendaStartTime)
        
     //   let dateEndTime = dateFormatter.dateFromString(modelAgendaList.agendaEndTime)
        
        // To convert the date into an HH:mm format
        dateFormatter.dateFormat = "hh:mm a" // or //h:mm a
        let dateString = dateFormatter.stringFromDate(dateStartTime!)

        lblSpeaker.text = dateString
//        lblSpeaker.textColor=UIColor(colorLiteralRed: 84.0/255.0, green: 180.0/255.0, blue: 129.0/255.0, alpha: 1)
        lblSpeaker.textColor = iAppDelegate.defaultColor
        lblSpeaker.font = lblSpeaker.font.fontWithSize(12)
        vw!.addSubview(lblSpeaker);
        if isMySchedule {
            if modelAgendaList.isLike == 0 {
                return UIView()
            }
        }
        
        return vw
        
        
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let objects:NSMutableArray=dicrowsObjects?.valueForKey("Section\(indexPath.section)") as! NSMutableArray
        modelAgendaList=objects.objectAtIndex(indexPath.row) as! HB_AgendaList
        
        let ObjScheduleDetail : HB_ScheduleInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_ScheduleInfoVC") as! HB_ScheduleInfoVC
        ObjScheduleDetail.modelSchedule=modelAgendaList
        self.navigationController?.pushViewController(ObjScheduleDetail, animated: true)
        //self.performSegueWithIdentifier(Constants.SegueConstant.kShowScheduleInfoVC, sender: self)
        
    }
    
    
    
    
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_SettingVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="SCHEDULE"
        
        
        
    }
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
        
        
    }
    
    /*
     
     -(NSDictionary *)CLCalendarBehaviorAttributes
     {
     return @{
     CLCalendarWeekStartDay : @2,                 //Start Day of the week, from 1-7 Mon-Sun -- default 1
     //             CLCalendarDayTitleTextColor : [UIColor yellowColor],
     //             CLCalendarSelectedDatePrintColor : [UIColor greenColor],
     };
     }
     
     
     
     -(void)dailyCalendarViewDidSelect:(NSDate *)date
     {
     //You can do any logic after the view select the date
     }s
     
     */
    
    //    func CLCalendarBehaviorAttributes() -> NSDictionary {
    //
    //    }
    //    func  dailyCalendarViewDidSelect(date: NSDate!) {
    //
    //    }
    //    func CLCalendarBehaviorAttributes() -> [NSObject : AnyObject]! {
    //        let weekDay:CLCalendarWeekStartDay
    //        let dict:[NSObject : AnyObject]=[ weekDay : 2]
    //
    //        return dict as [NSObject : AnyObject]
    //
    //
    //    }
    func dailyCalendarViewDidSelect(date: NSDate!) {
        
        print(date)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        calendarView?.selectedDate=dateFormatter.dateFromString("2015-12-07")
        //calendarView?.selectedDate=
        if (mutArrDayListL != nil)
        {
            
            //calendarView?.selectedDate=dateFormatter.dateFromString()
            print(dateFormatter.stringFromDate(date))
            let resultPredicate = NSPredicate(format: "date contains[c] %@", dateFormatter.stringFromDate(date))
            let result:NSArray =  mutArrDayListL.filteredArrayUsingPredicate(resultPredicate)
            
            if result.count>0 {
                //
                //            guard let date:NSDate = dateFormatter.dateFromString(()!) else {
                //                assert(false, "no date from string")
                //
                //            }
                
                modelDayList=result.objectAtIndex(0) as! HB_Daylist
                
                // tblSchedule.reloadData()
                
                print(result)
                var time:NSString?
                time=""
                numberSection=0
                numberofRows=1
                mutableArrRows=NSMutableArray()
                dicrowsObjects=NSMutableDictionary()
                mutArrSectionObjects=NSMutableArray()

                let objects=NSMutableArray()
                for item in modelDayList.agendaList {
                    
                    
                    modelAgendaList=item as HB_AgendaList
                    if modelAgendaList.agendaStartTime != time {
                      
                        
                        if isMySchedule {
                            if modelAgendaList.isLike == 0 {
                                numberSection += 0
                            }
                            else {
                                numberSection += 1
                            }
                        }
                        else {
                            numberSection += 1
                            
                        }
                    mutableArrRows.addObject(numberofRows)
                        numberofRows=1
                        
                        
                        time=modelAgendaList.agendaStartTime as NSString
                        
                    }
                    else
                    {
                        numberofRows += 1
                    
                        
                    
                    }
                    objects.addObject(modelAgendaList)
                    
                }
                mutableArrRows.removeObjectAtIndex(0)
                mutableArrRows.addObject(numberofRows)

                var staticCounter:Int=0
                for icounter in 0..<numberSection
                {
                
                    for iSubCounter in 0..<Int(mutableArrRows.objectAtIndex(icounter) as! NSNumber){
                        
                    mutArrSectionObjects?.addObject(objects.objectAtIndex(staticCounter))
                        staticCounter += 1
                        
                    }
                      dicrowsObjects?.setObject(mutArrSectionObjects!, forKey: "Section\(icounter)")
                    mutArrSectionObjects=NSMutableArray()
                    
                }
                
               print(dicrowsObjects)
        
               
            }
            else {
                mutableArrRows=nil
                numberSection=0
                return

            }
          tblSchedule.reloadData()
            
        }
    }
        //-(CLWeeklyCalendarView *)calendarView
        //    {
        //    if(!_calendarView){
        //    _calendarView = [[CLWeeklyCalendarView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, CALENDER_VIEW_HEIGHT)];
        //    _calendarView.delegate = self;
        //    }
        //    return _calendarView;
        //    }
        
        
    func actionLike (sender: AnyObject)
    {
        let btn : LikeButton = sender as! LikeButton
        if sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 1 {
            if ReachabilityCheck.isConnectedToNetwork()
            {
                SwiftLoader.show(animated: true)
                
                do {
                    let objects:NSMutableArray=dicrowsObjects?.valueForKey("Section\(btn.section)") as! NSMutableArray
                    
                    modelAgendaList=objects.objectAtIndex(btn.row) as! HB_AgendaList
                    
                    
                    let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" :Constants.eventInfo.kEventId,"user_id":sharedManager.currentUserID,"agenda_id":modelAgendaList.agendaId], options: NSJSONWritingOptions.init(rawValue: 0))
                    let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                    let url = (Constants.URL.AGENDA_LIKE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                    APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                        SwiftLoader.hide()
                        
                        
                        
                        if JSON["status"] == "Success"
                        {
                            if JSON["isLike"] == 1 {
                                btn.setImage(UIImage(named: "favorite_icon_active"), forState: UIControlState.Normal)
                                self.modelAgendaList.isLike=1
                                
                                self.modelAgendaList=objects.objectAtIndex(btn.row) as! HB_AgendaList
                                self.modelAgendaList.isLike=1
                                self.modelAgendaList.agendaLikes = String(Int(self.modelAgendaList.agendaLikes)!  + 1)
                            }
                            else {
                                btn.setImage(UIImage(named: "favorite_icon_normal"), forState: UIControlState.Normal)
                                self.modelAgendaList.isLike=0
                                self.modelAgendaList.agendaLikes = String(Int(self.modelAgendaList.agendaLikes)! - 1)
                            }
                            self.tblSchedule.reloadData()
                        }else {
                            SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                        }
                        }, failure: { (NSError) in
                            SwiftLoader.hide()
                            SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                    })
                    // here "jsonData" is the dictionary encoded in JSON data
                } catch let error as NSError {
                    SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
                }
                
                
                
            }
            else
            {
                
                SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
            }
        }
        else if !sharedManager.isUserLoggedIn
        {
            SPGUtility.sharedInstance().showAlert("You need to login first", titleMessage: "", viewController: self)
        }
        else {
            SPGUtility.sharedInstance().showAlert("Please Join the community", titleMessage: "", viewController: self)
        }
        
    }
    
    @IBAction func actionSchedule (sender : UIButton)
    {
        isMySchedule=false
        self.tblSchedule.hidden = false
        self.tblSchedule.reloadData()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        btnSchedule!.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
        btnSchedule!.backgroundColor = UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0)
        
        btnMySchedule!.setTitleColor(UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0), forState: .Normal)
        btnMySchedule!.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
        
        
        
        //self.dailyCalendarViewDidSelect(dateFormatter.dateFromString((self.modelAgenda.daylist.last?.date)!))
    }
    
    @IBAction func actionMySchedule (sender : UIButton)
    {
        if sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 1 {
            isMySchedule=true
            self.tblSchedule.hidden = false
            self.tblSchedule.reloadData()
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            btnMySchedule!.setTitleColor(UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0), forState: .Normal)
            btnMySchedule!.backgroundColor = UIColor(red: 145/255.0, green: 146/255.0, blue: 147/255.0, alpha: 1.0)
            
            btnSchedule!.setTitleColor(UIColor(red: 145.0/255.0, green: 146.0/255.0, blue: 147.0/255.0, alpha: 1.0), forState: .Normal)
            btnSchedule!.backgroundColor = UIColor(red: 227.0/255.0, green: 228.0/255.0, blue: 229.0/255.0, alpha: 1.0)
           // self.dailyCalendarViewDidSelect(dateFormatter.dateFromString((self.modelAgenda.daylist.last?.date)!))
        }
        else if sharedManager.isUserLoggedIn && sharedManager.userCommunityExist == 0
        {
            self.tblSchedule.hidden = true
        }
        else if !sharedManager.isUserLoggedIn
        {
            self.tblSchedule.hidden = true
//            SPGUtility.sharedInstance().showAlert("You must be logged in to see your schedule.", titleMessage: "", viewController: self)
        }
    }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
    
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        
}
