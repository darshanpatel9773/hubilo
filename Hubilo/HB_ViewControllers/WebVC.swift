//
//  WebVC.swift
//  Hubilo
//
//  Created by Aadil on 7/27/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
class WebVC: UIViewController, UIWebViewDelegate {
    @IBOutlet var webView : UIWebView?
    @IBOutlet var btnDone : UIButton?
    var url : NSURL!
    override func viewDidLoad() {
        super.viewDidLoad()
        if ReachabilityCheck.isConnectedToNetwork()
        {
            
        webView?.loadRequest(NSURLRequest(URL: url))
            
        }
        else {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        // Do any additional setup after loading the view.
    }

    func webViewDidFinishLoad(webView: UIWebView) {
        SwiftLoader.hide()
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        SwiftLoader.show(animated: true)
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        SwiftLoader.hide()
    }
    
    @IBAction func actionDone (sender : UIButton) {
        self.dismissViewControllerAnimated(true) { 
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
