//
//  HB_SignupVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 12/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import TextFieldEffects
import SwiftLoader
class HB_SignupVC: UIViewController,UIAlertViewDelegate {

    
   
    
    @IBOutlet weak var btn_Signup: UIButton!
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    @IBOutlet weak var txtPassword: HoshiTextField!
    @IBOutlet weak var txtLastName: HoshiTextField!
    @IBOutlet weak var txtFirstName: HoshiTextField!
    var isSignupSuceesfull:Bool=false
    @IBOutlet weak var txtEmail: HoshiTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        btn_Signup.backgroundColor = appDelegate.defaultColor
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btn_Signup_click(sender: AnyObject) {
     
        if txtFirstName.text?.characters.count == 0
        {
            SPGUtility.sharedInstance().showAlert("Please enter first name", titleMessage: "", viewController: self)
        }
        else if txtLastName.text?.characters.count == 0
        {
            SPGUtility.sharedInstance().showAlert("Please enter last name", titleMessage: "", viewController: self)
        }
        else if txtEmail.text?.characters.count == 0
        {
            SPGUtility.sharedInstance().showAlert("Please enter email address", titleMessage: "", viewController: self)
        }
        else if txtPassword.text?.characters.count == 0
        {
            SPGUtility.sharedInstance().showAlert("Please enter password", titleMessage: "", viewController: self)
        }
        else
        {
            if ReachabilityCheck.isConnectedToNetwork()
            {
                SwiftLoader.show(animated: true)
                if let iUrl = getUrlSignUp() {
                    SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                        print(iData)
                        SwiftLoader.hide()
                        let dicEvent:NSDictionary = iData as! NSDictionary
                        if dicEvent.valueForKey("status") as! String == "Fail"
                        {
                            
                            //                    SPGUtility.sharedInstance().showAlert(dicEvent.valueForKey("msg") as! NSString, titleMessage: "", viewController: self)
                            
                            
                            let alert = UIAlertController(title: "", message: dicEvent.valueForKey("msg") as? String, preferredStyle: .Alert)
                            let ok = UIAlertAction(title: "OK", style:.Default, handler: {(alert :UIAlertAction!) in
                                
                            })
                            alert.addAction(ok)
                            self.presentViewController(alert, animated: true, completion: nil)
                            
                            self.isSignupSuceesfull=false
                        }
                        else
                        {
                            
                            let userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                            userDefaults.setObject(dicEvent.valueForKey("userKey") as! String, forKey: "user_key")
                            userDefaults.synchronize()
                            
                            let alert = UIAlertController(title: "", message: dicEvent.valueForKey("msg") as? String, preferredStyle: .Alert)
                            let ok = UIAlertAction(title: "OK", style:.Default, handler: {(alert :UIAlertAction!) in
                                
                                self.dismissViewControllerAnimated(false, completion: nil)
                            })
                            
                            
                            
                            alert.addAction(ok)
                            self.presentViewController(alert, animated: true, completion: nil)
                            self.isSignupSuceesfull=true
                            self.appDelegate.loginUserDetail=dicEvent as? NSMutableDictionary
                            
                            
                            //
                        }
                        self.txtLastName.text = ""
                        self.txtFirstName.text = ""
                        self.txtPassword.text = ""
                        self.txtEmail.text = ""
                        
                        
                        
                    }
                } else {
                    txtLastName.text = ""
                    txtFirstName.text = ""
                    txtPassword.text = ""
                    txtEmail.text = ""
                    // oops, something went wrong with the URL
                }
            }
            else
            {
                
                SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
                
                
            }
        }
    }
    
    
    @IBAction func btnLoginPage(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    //MARK:Button Click Events 
    @IBAction func close_btn_click(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    func getUrlSignUp() -> NSURL? {
        let app : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var iUrl:NSURL = NSURL()
        do {
            
            
            if app.deviceTokenStr == ""
            {
                app.deviceTokenStr="1234567890aaaa"
            }
            
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["emailid" : txtEmail.text!,"password" : txtPassword.text!,"firstname" : txtFirstName.text!,"lastname" : txtLastName.text!,"device" : "ios","deviceId" : app.deviceTokenStr,"event":Constants.eventInfo.kEventId], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.USER_SIGNUP + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
   

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
