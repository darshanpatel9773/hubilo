//
//  HB_NotificationCell.swift
//  Hubilo
//
//  Created by Priyank Gandhi on 02/09/16.
//  Copyright © 2016 nirav. All rights reserved.
//

import UIKit

class HB_NotificationCell: UITableViewCell {
    
    @IBOutlet weak var imgVwUser:UIImageView!
    @IBOutlet weak var lblNotification:UILabel!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
