//
//  MessageDetailVC.swift
//  Hubilo
//
//  Created by Aadil on 7/29/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
import SDWebImage
import ObjectMapper
import JSQMessagesViewController

class MessageDetailVC: JSQMessagesViewController {
    var sharedManager : Globals = Globals.sharedInstance
    let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImageWithColor(UIColor.lightGrayColor())
    var outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImageWithColor(UIColor(red: 36.0/255.0, green: 156.0/255.0, blue : 91.0/255.0, alpha: 1.0))
    var messages = [JSQMessage]()
    var msgList : PersonalMessage?
    var targetId : Int = 0
    var targetName : String = ""
    var targetImage : UIImage!
    private var displayName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImageWithColor(iAppDelegate.defaultColor)
        //self.addDemoMessages()
        self.navigationController?.navigationBarHidden = false
        self.setMenu()
//        incomingBubble = JSQMessagesBubbleImageFactory(bubbleImage: UIImage.jsq_bubbleCompactTaillessImage(), capInsets: UIEdgeInsetsZero, layoutDirection: UIApplication.sharedApplication().userInterfaceLayoutDirection).incomingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleBlueColor())
//        outgoingBubble = JSQMessagesBubbleImageFactory(bubbleImage: UIImage.jsq_bubbleCompactTaillessImage(), capInsets: UIEdgeInsetsZero, layoutDirection: UIApplication.sharedApplication().userInterfaceLayoutDirection).outgoingMessagesBubbleImageWithColor(UIColor.lightGrayColor())
       
        getMessages()
        collectionView?.collectionViewLayout.springinessEnabled = false
        
        automaticallyScrollsToMostRecentMessage = true
        
        self.collectionView?.reloadData()
        self.collectionView?.layoutIfNeeded()
        
       //  setupBackButton()
        // Do any additional setup after loading the view.
    }
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(goBack), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title=targetName.uppercaseString
        
        
    }
    
    
    func getMessages() {
        //USER_MESSAGE
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                
                var userFetch = ""
                
                //userFetch = "1378"
                //targetId = 1205
                //self.senderId = "1378"
                userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject([ "page" : "0","self_user_id":userFetch,"target_user_id":self.targetId], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.USER_MESSAGE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    
                    
                    if JSON["status"] == "Success"
                    {
                        self.msgList = Mapper<PersonalMessage>().map(JSON.rawValue)
                        
                        for newMsg in (self.msgList?.message_data)! {
                            //let sender =  "Server" : self.senderId
                            //let messageContent = "Message nr. \(i)"
                            let message = JSQMessage(senderId: newMsg.sender_id, displayName: self.sharedManager.currentUser.firstname , text: (newMsg.text ).base64Decoded())
                            self.messages += [message]
                        }
                        
                        self.reloadMessagesView()
                        
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : JSQMessagesCollectionViewCell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! JSQMessagesCollectionViewCell
        
        let data = messages[indexPath.row]
        switch(data.senderId) {
        case self.senderId:
            cell.textView.textColor = UIColor.whiteColor()
        default:
           cell.textView.textColor = UIColor.whiteColor()
        }
        
        return cell;
    }
    
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
        self.messages += [message]
        self.finishSendingMessage()
        self.sendMessage(text)
    }
    
    
    
    func sendMessage(msgText : String) {
        
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_sender_id" : userFetch,"message_text" : msgText,"user_receiver_id" : targetId], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.SEND_MESSAGE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    
                    
                    if JSON["status"] == "Success"
                    {
                        //                        self.msgList = Mapper<Messages>().map(JSON.rawValue)
                        //                        self.tblView?.reloadData()
                        //self.loadMessages()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
        
        
    }
    
    override func didPressAccessoryButton(sender: UIButton!) {
        
    }
   
    func reloadMessagesView() {
        self.collectionView?.reloadData()
    }
    
    func setupBackButton() {
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem = backButton
    }
    func backButtonTapped() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
//    // MARK: JSQMessagesViewController method overrides
//    override func didPressSendButton(button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: NSDate) {
//        /**
//         *  Sending a message. Your implementation of this method should do *at least* the following:
//         *
//         *  1. Play sound (optional)
//         *  2. Add new id<JSQMessageData> object to your data source
//         *  3. Call `finishSendingMessage`
//         */
//        
//        let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
//        self.messages.append(message)
//        self.finishSendingMessageAnimated(true)
//    }
//    
    //MARK: JSQMessages CollectionView DataSource
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MessageDetailVC {
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
   
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        let data = self.messages[indexPath.row]
        return data
    }
    
    
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, didDeleteMessageAtIndexPath indexPath: NSIndexPath!) {
        self.messages.removeAtIndex(indexPath.row)
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = messages[indexPath.row]
        switch(data.senderId) {
        case self.senderId:
            return self.outgoingBubble
        default:
            return self.incomingBubble
        }
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        //return UIImage(named: "login_default")
        
//        let diameter = UInt(collectionView.collectionViewLayout.outgoingAvatarViewSize.width)
//        let image = targetImage
        
        let data = messages[indexPath.row]
        switch(data.senderId) {
        case self.senderId:
            return nil
           //return JSQMessagesAvatarImageFactory.avatarImageWithImage(UIImage(named: "login_default"), diameter: diameter)
        default:
            return nil
            //return JSQMessagesAvatarImageFactory.avatarImageWithImage(image, diameter: diameter)
        }
        
    }
}
extension MessageDetailVC {
    func addDemoMessages() {
        for i in 1...10 {
            let sender = (i%2 == 0) ? "Server" : self.senderId
            let messageContent = "Message nr. \(i)"
            let message = JSQMessage(senderId: sender, displayName: sender, text: messageContent)
            self.messages += [message]
        }
        self.reloadMessagesView()
    }
    
    func setup() {
        self.senderId = sharedManager.currentUserID
        self.senderDisplayName = UIDevice.currentDevice().identifierForVendor?.UUIDString
        
    }
}
