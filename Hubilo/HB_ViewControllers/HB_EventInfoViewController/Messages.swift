//
//  Messages.swift
//  Hubilo
//
//  Created by Aadil on 7/28/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import ObjectMapper
class MessageData : NSObject, Mappable {
    var id = 0
    var text = ""
    var time : Double = 0
    var sender_id = ""
    var receiver_id = ""
    override init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        text <- map["text"]
        time <- map["time"]
        sender_id <- map["sender_id"]
        receiver_id <- map["receiver_id"]
    }
}
class PersonalMessage: NSObject, Mappable {
    var status = ""
    var msg = ""
    var message_data : [MessageData]!
    var target_user_img = ""
    override init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        message_data <- map["message_data"]
        target_user_img <- map["target_user_img"]
    }
}
class Messages: NSObject, Mappable {
    var status = ""
    var msg = ""
    var recenthistorylist : [HistoryList]!
    override init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    
     func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        recenthistorylist <- map["recenthistorylist"]
    }
}

class HistoryList : NSObject, Mappable {
    var user_id = ""
    var message_text = ""
    var time : Double = 0
    var state = 0
    var firstname = ""
    var lastname = ""
    var profile_img = ""
    override init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    func mapping(map: Map) {
        user_id <- map ["user_id"]
        message_text <- map ["message_text"]
        time <- map ["time"]
        state <- map ["state"]
        firstname <- map ["firstname"]
        lastname <- map ["lastname"]
        profile_img <- map ["profile_img"]
    }
}
