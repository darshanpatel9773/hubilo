//
//  MessageCell.swift
//  Hubilo
//
//  Created by Aadil on 7/28/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    @IBOutlet var imgPic: UIImageView?
    @IBOutlet var badgeView : UIView?
    @IBOutlet var lblBadgeCount : UILabel?
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet var lblDescription : UILabel?
    @IBOutlet var lblTime : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //imgPic?.layer.cornerRadius = (imgPic?.frame.size.width)! / 2
        //imgPic?.clipsToBounds = true
        
        badgeView?.layer.cornerRadius = (badgeView?.frame.size.width)! / 2
        badgeView?.clipsToBounds = true
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
