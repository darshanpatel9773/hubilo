//
//  HB_NotificationVC.swift
//  Hubilo
//
//  Created by Darshan Patel on 28/07/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader

class HB_NotificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblNotification:UITableView!
    @IBOutlet weak var imgVwNotification:UIImageView!
    var sharedManager : Globals = Globals.sharedInstance
    
    var mutArrNotificationList:NSMutableArray = NSMutableArray()
    var mutDictUserList:NSMutableDictionary = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_NotificationVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        
        self.navigationItem.title = "NOTIFICATIONS"
        tblNotification.tableFooterView = UIView()
        
        loadNotification()
    }
    
    func loadNotification(){
        
        if ReachabilityCheck.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            var userId : String = "0"
            if sharedManager.isUserLoggedIn{
                userId=sharedManager.currentUserID
            }
            if let iUrl = getUrlNotificationList(Constants.eventInfo.kEventId, userID:userId) {
                SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                    print(iData)
                    let dicNotificationList:NSDictionary = iData as! NSDictionary
                    self.mutArrNotificationList = NSMutableArray()
                    self.mutDictUserList = NSMutableDictionary()
                    if dicNotificationList.valueForKey("status") as! String == "Success" {
                        
                        self.mutArrNotificationList = (dicNotificationList.valueForKey("notification_data") as? NSMutableArray)!
                        self.mutDictUserList = (dicNotificationList.valueForKey("userData") as? NSMutableDictionary)!
                        if self.mutArrNotificationList.count > 0 {
                            self.imgVwNotification.hidden = true
                            self.tblNotification.hidden = false
                        }
                    }else {
                       self.imgVwNotification.hidden = false
                       self.tblNotification.hidden = true
                    }
                   
                    
                    
                    SwiftLoader.hide()
                    
                   self.tblNotification.reloadData()
                }
            } else {
                SPGUtility.sharedInstance().showAlert("oops, something went wrong with the URL", titleMessage: "", viewController: self)
            }
        }
        else {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
    }
    
    func getUrlNotificationList(eventID: String,userID: String) -> NSURL? {
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_id" : userID,"event_id" : eventID], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.GET_ALL_NOTIFICATION + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    
    func back_btn_click(sender:UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mutArrNotificationList.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellNotification:HB_NotificationCell = tableView.dequeueReusableCellWithIdentifier("NotificationCell", forIndexPath: indexPath) as! HB_NotificationCell
        
         let dictUser:NSDictionary = self.mutDictUserList.objectForKey(mutArrNotificationList.objectAtIndex(indexPath.row).valueForKey("user_id")!)! as! NSDictionary
        
        cellNotification.lblNotification.text = (dictUser.objectForKey("firstname") as! String) + " " + (dictUser.objectForKey("lastname") as! String) + " " + (mutArrNotificationList.objectAtIndex(indexPath.row).valueForKey("msg") as? String)!
        
        let date:NSDate = NSDate(timeIntervalSince1970:mutArrNotificationList.objectAtIndex(indexPath.row).valueForKey("time") as! NSTimeInterval/1000)
        
        let formatter : NSDateFormatter = NSDateFormatter()
        formatter.locale = NSLocale.currentLocale()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        cellNotification.lblTime.text = DateCalculationVC.getDateCaption(formatter.stringFromDate(date))

        cellNotification.imgVwUser.image = UIImage()
        
        cellNotification.imgVwUser.layer.cornerRadius = 25.0
        cellNotification.imgVwUser.clipsToBounds = true
        
       
        if dictUser.objectForKey("profile_img") != nil {
            let url:String = Constants.ImagePaths.kAttendeeImagePath + (dictUser.objectForKey("profile_img") as! String)
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(dictUser.objectForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cellNotification.imgVwUser.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(dictUser.objectForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cellNotification.imgVwUser.image = imgVw.image
        }
        
        return cellNotification
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
