//
//  HB_MessagesVC.swift
//  Hubilo
//
//  Created by Darshan Patel on 28/07/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import SwiftLoader
import ObjectMapper
import SDWebImage
class HB_MessagesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var imgNoMessage : UIImageView?
    @IBOutlet var tblView : UITableView?
    var sharedManager : Globals = Globals.sharedInstance
    var msgList : Messages!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
        setMenu()
        self.tblView?.tableFooterView = UIView()
        
        if sharedManager.isUserLoggedIn == false  &&  sharedManager.userCommunityExist == 0 {
             SPGUtility.sharedInstance().showAlert("You must be logged in to see your messages", titleMessage: "", viewController: self)
            self.navigationController?.popViewControllerAnimated(true)
        }
        else {
             loadMessages()
        }
        
    }
    
    
    
    func loadMessages() {
       
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            
            do {
                
                //var userFetch = "3113"
                let userFetch = sharedManager.currentUserID
                let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_id" : userFetch], options: NSJSONWritingOptions.init(rawValue: 0))
                let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                let url = (Constants.URL.MESSAGES + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                APICall.requestGETURLParam(url, params: nil, headers: nil, success: { (JSON) in
                    SwiftLoader.hide()
                    
                    
                    
                    if JSON["status"] == "Success"
                    {
                        self.msgList = Mapper<Messages>().map(JSON.rawValue)
                        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "time", ascending: false)
                        let sortedResults: NSArray = (self.msgList.recenthistorylist  as NSArray).sortedArrayUsingDescriptors([descriptor])
                        self.msgList.recenthistorylist = sortedResults.mutableCopy() as! [HistoryList] 
                        self.tblView?.reloadData()
                    }else {
                        SPGUtility.sharedInstance().showAlert(JSON["msg"].rawString()!, titleMessage: "", viewController: self)
                    }
                    }, failure: { (NSError) in
                        SwiftLoader.hide()
                        SPGUtility.sharedInstance().showAlert(NSError.localizedDescription, titleMessage: "", viewController: self)
                })
                // here "jsonData" is the dictionary encoded in JSON data
            } catch let error as NSError {
                SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
            }
            
            
            
        }
        else
        {
            
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard (msgList != nil) else {
            return 0
            self.imgNoMessage?.hidden = false
        }
        self.imgNoMessage?.hidden = true
        return msgList.recenthistorylist.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : MessageCell = tableView.dequeueReusableCellWithIdentifier("cell1") as! MessageCell
        cell.lblTitle?.text = self.msgList.recenthistorylist[indexPath.row].firstname + " " + self.msgList.recenthistorylist[indexPath.row].lastname
        cell.lblDescription?.text = String(self.msgList!.recenthistorylist[indexPath.row].message_text).base64Decoded()
        
        let _interval :  NSTimeInterval = self.msgList!.recenthistorylist[indexPath.row].time
        let date : NSDate = NSDate(timeIntervalSince1970: _interval/1000)
        let formatter : NSDateFormatter = NSDateFormatter()
        formatter.locale = NSLocale.currentLocale()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        cell.lblTime?.text = DateCalculationVC.getDateCaption(formatter.stringFromDate(date))
        
//        let flags:UInt8 = NSCalendarUnit.Second
//        
//        unsigned flags = NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour |  NSCalendarUnitDay;
//        NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//        NSDateComponents *components = [calendar components:flags fromDate:toDate toDate:today options:0];
//        
//        let iStrTimeAgoInterval = NSCalendar.autoupdatingCurrentCalendar().dateByAddingUnit(.Second, value: -59, toDate: date, options: [])!
//            .elapsedTime        // "Just Now"
//        
//        NSCalendar.autoupdatingCurrentCalendar().dateByAddingUnit(.Minute, value: -1, toDate: date, options: [])!
//            .elapsedTime        // "Today at 4:27 PM"
//        NSCalendar.autoupdatingCurrentCalendar().dateByAddingUnit(.Hour, value: -1, toDate: date, options: [])!
//            .elapsedTime        // "Today at 3:28 PM"
//        
//        NSCalendar.autoupdatingCurrentCalendar().dateByAddingUnit(.Day, value: -1, toDate: date, options: [])!
//            .elapsedTime        // "Yesterday at 4:28 PM"
//        NSCalendar.autoupdatingCurrentCalendar().dateByAddingUnit(.WeekOfYear, value: -1, toDate: date, options: [])!
//            .elapsedTime        // "7 days ago"
//        
//        NSCalendar.autoupdatingCurrentCalendar().dateByAddingUnit(.Month, value: -1, toDate: date, options: [])!
//            .elapsedTime        // "30 days ago"
//        NSCalendar.autoupdatingCurrentCalendar().dateByAddingUnit(.Year, value: -1, toDate: date, options: [])!
//            .elapsedTime        // "12 months ago"
        
        //cell.lblTime?.text = iStrTimeAgoInterval;
        
        
        let url:String = (Constants.ImagePaths.kAttendeeImagePath + (self.msgList!.recenthistorylist[indexPath.row].profile_img) )
        
        dispatch_async(dispatch_get_main_queue()) {
            cell.imgPic?.layer.cornerRadius = (cell.imgPic?.frame.size.width)! / 2
            cell.imgPic?.clipsToBounds = true
        }
        
        
        if url.characters.count > 0  {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(self.msgList.recenthistorylist[indexPath.row].firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgPic!.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(self.msgList.recenthistorylist[indexPath.row].firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgPic!.image = imgVw.image
        }

        
       // cell.imgPic?.sd_setImageWithURL(NSURL(string: url), placeholderImage: UIImage(named: "login_default"))
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 83;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let msgVC : MessageDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MessageDetailVC") as! MessageDetailVC
        
        msgVC.targetId = Int(self.msgList!.recenthistorylist[indexPath.row].user_id )!
        msgVC.targetName = self.msgList!.recenthistorylist[indexPath.row].firstname + self.msgList!.recenthistorylist[indexPath.row].lastname
        msgVC.targetImage = (tableView.cellForRowAtIndexPath(indexPath) as! MessageCell).imgPic?.image
        self.navigationController?.pushViewController(msgVC, animated: true)
        
    }
    
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_MessagesVC.back_btn_click(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        self.navigationItem.title = "MESSAGES"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    
    func back_btn_click(sender:UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension String {
    
    func base64Encoded() -> String {
        let plainData = dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        return base64String!
    }
    
    func base64Decoded() -> String {
        let decodedData = NSData(base64EncodedString: self, options:NSDataBase64DecodingOptions(rawValue: 0))
        let decodedString = NSString(data: decodedData!, encoding: NSUTF8StringEncoding)
        return String(decodedString!)
    }
}