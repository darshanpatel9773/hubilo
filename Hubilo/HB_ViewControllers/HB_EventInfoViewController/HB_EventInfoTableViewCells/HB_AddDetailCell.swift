//
//  HB_AddDetailCell.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 11/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import MapKit
class HB_AddDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDaysCount: UILabel!
    @IBOutlet weak var lblDayTime: UILabel!
    @IBOutlet weak var lblEventPepole: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var map: MKMapView!

    @IBOutlet weak var btnJoin: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
