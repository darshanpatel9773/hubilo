//
//  HB_OtherDetailsCell.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 11/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_OtherDetailsCell: UITableViewCell {
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var txtVWEventDetail: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
