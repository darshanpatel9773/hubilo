//
//  HB_EventInfoViewController.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 11/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import ExpandingMenu
import KYDrawerController
import MapKit
import CoreLocation
import SwiftLoader
import Alamofire
import SDWebImage
import MessageUI

class HB_EventInfoViewController: UIViewController,UITableViewDelegate,CLLocationManagerDelegate,MKMapViewDelegate,MFMailComposeViewControllerDelegate {
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var isFirstTime : Bool = false;
    @IBOutlet weak var map: MKMapView!
    @IBOutlet var viewCall: UIView!
    var locationManager: CLLocationManager!
    var modelEventObj:HB_ModelEvent?
    var iSShowMore:Bool=false
    var sharedManager : Globals = Globals.sharedInstance
    @IBOutlet weak var tblEventInfo: DTParallaxTableView!
    @IBOutlet var gradientView : UIView?
    @IBOutlet weak var btnCall:UIButton!
    @IBOutlet weak var btnEmail:UIButton!
    var iStrLatLng:String = ""
    var iStrEventDate:String = ""
    
    @IBOutlet weak var imgNetworkError:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewCall.hidden = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRectMake(0, 0, self.view.frame.size.width, 60)
        
        // 3
        let color1 = UIColor.init(white: 0.0, alpha: 1.0) .CGColor as CGColorRef
        let color2 = UIColor.init(white: 0.0, alpha: 0.0).CGColor as CGColorRef
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.colors = [color1, color2]
        
        // 4
        gradientLayer.locations = [0.0, 1.0]
        
        // 5
        self.gradientView!.layer.addSublayer(gradientLayer)
        // self.navigationController?.navigationBarHidden = false
        
        // let refreshButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Refresh, target: self, action: #selector(HB_EventInfoViewController.menuButtonTouched(_:)))
        //  navigationItem.leftBarButtonItem = refreshButton
        
        //navigationController?.navigationBar.barTintColor = UIColor.greenColor()
        // title = "Search result"
        // Do any additional setup after loading the view.

    
        
        if ReachabilityCheck.isConnectedToNetwork() {
            imgNetworkError.hidden = true
            SPGWebService.callGETWebService(getSiderBar(Constants.eventInfo.kEventId)!, WSCompletionBlock: { (data, error) in
                if error != nil {
                    print(error)
                }else {
                    let dicSideList:NSDictionary = data as! NSDictionary
                    
                    if (dicSideList.objectForKey("status") != nil) {
//                        let iMutArrSidebar:NSMutableArray = dicSideList.objectForKey("sideBar") as! NSMutableArray
                        let iStrColor:String = dicSideList.objectForKey("data")!.objectForKey("color") as! String
                        let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        iAppDelegate.defaultColor = self.colorWithHexString(iStrColor)
                        NSUserDefaults.standardUserDefaults().setColor(self.colorWithHexString(iStrColor), forKey: "application_color")
                        NSUserDefaults.standardUserDefaults().synchronize()
                         NSNotificationCenter.defaultCenter().postNotificationName("GetMenuItens", object: nil)
                        NSNotificationCenter.defaultCenter().postNotificationName("SetNavigationColor", object: nil)
                        self.dataEventLoad()
                    }
                    
                }
            })
        }else {
            imgNetworkError.hidden = false
            let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            iAppDelegate.defaultColor = NSUserDefaults.standardUserDefaults().colorForKey("application_color")!
            NSNotificationCenter.defaultCenter().postNotificationName("GetMenuItens", object: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("SetNavigationColor", object: nil)
        }
        
        
        
        
    }
    
    func dataEventLoad()  {
        if ReachabilityCheck.isConnectedToNetwork()
        {
            SwiftLoader.show(animated: true)
            var config : SwiftLoader.Config = SwiftLoader.Config()
            config.size = 190
            let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            config.spinnerColor = iAppDelegate.defaultColor
            
            config.spinnerColor = UIColor(colorLiteralRed: 36.0/255.0, green: 156.0/255.0, blue: 91.0/255.0, alpha: 1)
            config.spinnerColor = iAppDelegate.defaultColor
            config.foregroundColor = .blackColor()
            config.foregroundAlpha = 0.3
            
            SwiftLoader.setConfig(config)
            
            var userId = ""
            
            if sharedManager.isUserLoggedIn {
                userId = sharedManager.currentUserID
            }
            
            
            Alamofire.request(.GET, getUrlWithEventId(Constants.eventInfo.kEventId,userId:userId)!, parameters: [:])
                .responseJSON { response in
                    //                    print(response.request)  // original URL request
                    //                    print(response.response) // URL response
                    //                    print(response.data)     // server data
                    //                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        let dicEvent:NSDictionary = JSON as! NSDictionary
                        self.appDelegate.eventInfoDic=JSON as? NSMutableDictionary
                        print(self.appDelegate.eventInfoDic)
                        self.modelEventObj=HB_ModelEvent(fromDictionary: dicEvent.valueForKey("data") as! NSDictionary)
                        self.sharedManager.backgroundImg=Constants.ImagePaths.kEventCoverImagePath + (self.modelEventObj?.eventCoverImg)!
                        
                        print(self.modelEventObj)
                        
                        
                        self.btnCall.setTitle(self.modelEventObj?.phone, forState: .Normal)
                        self.btnEmail.setTitle(self.modelEventObj?.email, forState: .Normal)
                        
                        let iUserDefaults :NSUserDefaults = NSUserDefaults.standardUserDefaults()
                        iUserDefaults.setObject(self.modelEventObj!.eventName, forKey: "eventName")
                        iUserDefaults.setObject(self.modelEventObj!.address, forKey: "eventAddress")
                        iUserDefaults.setObject(self.modelEventObj!.eventDate,forKey: "eventDate")
                        iUserDefaults.setObject(self.modelEventObj!.eventStartTime,forKey: "eventStartTime")
                        iUserDefaults.setObject(self.modelEventObj!.eventEndDate,forKey: "eventEndDate")
                        iUserDefaults.setObject(self.modelEventObj!.eventEndTime,forKey: "eventEndTime")
                        iUserDefaults.synchronize()
                        
                        self.sharedManager.userCommunityExist = (self.modelEventObj?.userCommunityExist)!
                        
                        SwiftLoader.hide()
                        self.tblEventInfo.reloadData()
                        self.setHeaderImage()
                        self.iSShowMore=true
                        self.tblEventInfo.reloadData()
                    }
                    else {
                        // oops, something went wrong with the URL
                    }
                    self.configureExpandingMenuButton()
            }
        }
        else {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
        }

    }
    
    func setHeaderImage() {
        
        
        let strUrl=Constants.ImagePaths.kEventCoverImagePath + (modelEventObj?.eventCoverImg)!
        
        SDWebImageManager.sharedManager().downloadImageWithURL(NSURL(string: strUrl), options: [],progress: nil, completed: {[weak self] (image, error, cached, finished, url) in
            
            if let wSelf = self {
                
                //On Main Thread
                dispatch_async(dispatch_get_main_queue()){
                
                    let headerView:DTParallaxHeaderView = DTParallaxHeaderView(frame: CGRect(x: 0, y: 0, width:UIScreen.mainScreen().bounds.size.width, height:230), withImage: image, withTabBar: nil)
                    
                    self!.tblEventInfo.setDTHeaderView(headerView)
                    self!.tblEventInfo.sendSubviewToBack(headerView)
                    
                
                }
            }
            })
        
        
       // let headerView:DTParallaxHeaderView=DTParallaxHeaderView(frame: CGRect(x: 0, y: 0, width:UIScreen.mainScreen().bounds.size.width, height:150), withImageUrl: strUrl, withTabBar: nil)
       // print(headerView.imageUrl)
        
        
    }
    func getUrlWithEventId(eventID: String,userId:String) -> NSURL? {
        
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : eventID,"user_id":userId], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.EVENT_ALL_DETAILS + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    func getSiderBar(eventID:String) -> NSURL? {
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : eventID], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.APPLICATION_DATA + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
        self.navigationController?.navigationBarHidden = true
        
        
        
        print(self.navigationController?.viewControllers)
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if (modelEventObj != nil) {
            self.setHeaderImage()
        }
        if  !isFirstTime {
            
        }
        
        isFirstTime = true
    }
    
    // MARK: - ENSideMenu Delegate
    
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
        
    }
    
    @IBAction func more_btn_click(sender: AnyObject) {
        
        
        
        let btnMore:UIButton=sender as! UIButton
        
        if iSShowMore
        {
            iSShowMore=false
            
            //   let indexPath = NSIndexPath(forRow: 1, inSection: 0)
            //  tblEventInfo.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Top)
            tblEventInfo.reloadData()
            
            btnMore.setTitle("MORE", forState: .Normal)
            
        }
        else
        {
            
            iSShowMore=true
            let indexPath = NSIndexPath(forRow: 1, inSection: 0)
            //     tblEventInfo.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Top)
            tblEventInfo.reloadData()
            
            btnMore.setTitle("LESS", forState: .Normal)
            
            
        }
        //self.performSegueWithIdentifier("ShowLoginVC", sender: self)
    }
    @IBAction func join_btn_click(sender: AnyObject) {
        if !sharedManager.isUserLoggedIn
        {
            self.performSegueWithIdentifier("ShowLoginVC", sender: self)
        }
        else {
            
            if sharedManager.userCommunityExist == 0 && modelEventObj?.ticketing == 0 {
                if ReachabilityCheck.isConnectedToNetwork()
                {
                    SwiftLoader.show(animated: true)
                    do {
                        var userFetch = ""
                        if sharedManager.isUserLoggedIn {
                            userFetch = sharedManager.currentUserID
                        }
                        let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : Constants.eventInfo.kEventId,"user_id" : userFetch], options: NSJSONWritingOptions.init(rawValue: 0))
                        let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
                        let url = (Constants.URL.JOIN_COMMUNITY + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
                        SPGWebService.callGETWebService(NSURL(string:url)!, WSCompletionBlock: { (data, error) in
                            if error != nil {
                                print(error)
                                SwiftLoader.hide()
                            }else {
                                let iDictJoin:NSDictionary = data as! NSDictionary
                                self.sharedManager.userCommunityExist = iDictJoin.objectForKey("userCommunityExist") as! Int
                                let userDefaults = NSUserDefaults.standardUserDefaults()
                                 userDefaults.setObject(self.sharedManager.userCommunityExist, forKey:"userCommunityExist")
                                self.tblEventInfo.reloadData()
                                SwiftLoader.hide()
                            }
                        })
                    } catch let error as NSError {
                        SPGUtility.sharedInstance().showAlert(error.localizedDescription, titleMessage: "", viewController: self)
                    }
                }
                else
                {
                    SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
                }
            }else {
                
                let userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                let iStrUserKey = userDefaults.objectForKey("user_key") as! String
                
                let url:NSURL = NSURL(string:"http://hubilo.com/community/IOSbuyticket.php?user=" + iStrUserKey)!
                if (UIApplication.sharedApplication().canOpenURL(url))
                {
                    let web : WebVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("WebVC") as! WebVC
                    web.url = url
                    self.presentViewController(web, animated: true, completion: nil)
                    //UIApplication.sharedApplication().openURL(url!)
                }
            }
            
        }
    }
    
    
    func heightForView(text:String) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width-16, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = UIFont.systemFontOfSize(12.0)
        label.text = text
        
        label.sizeToFit()
        return label.frame.height + 20
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if (modelEventObj != nil) {
            return 2
        }
        return 0
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell1:UITableViewCell?=UITableViewCell()
        if indexPath.row == 0
        {
            let cell:HB_AddDetailCell = tableView.dequeueReusableCellWithIdentifier("HB_AddDetailCell", forIndexPath: indexPath) as! HB_AddDetailCell
            let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            cell.btnJoin.backgroundColor = iAppDelegate.defaultColor
            
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setObject(modelEventObj?.userCommunityExist, forKey:"userCommunityExist")
        
            
            
            if modelEventObj?.userCommunityExist == 0 && modelEventObj?.ticketing == 0 {
                cell.btnJoin .setTitle("JOIN", forState: .Normal)
            }else if modelEventObj?.userCommunityExist == 0 && modelEventObj?.ticketing == 1 {
                cell.btnJoin .setTitle(modelEventObj?.buttonName, forState: .Normal)
            }else if modelEventObj?.userCommunityExist == 1 && modelEventObj?.ticketing == 0 {
                cell.btnJoin.hidden = true
            }else if modelEventObj?.userCommunityExist == 1 && modelEventObj?.ticketing == 0 && (modelEventObj?.transactOnce)! == 1 {
                cell.btnJoin .setTitle("BUY MORE TICKETS", forState: .Normal)
            }else if modelEventObj?.userCommunityExist == 1 && modelEventObj?.ticketing == 1 && (modelEventObj?.transactOnce)! == 1 {
                cell.btnJoin.hidden = true
            }else if modelEventObj?.userCommunityExist == 1 && modelEventObj?.ticketing == 1 {
                cell.btnJoin.hidden = true
            }
            
             cell.btnJoin.addTarget(self, action: #selector(HB_EventInfoViewController.join_btn_click(_:)), forControlEvents: .TouchUpInside)
            
            map=cell.map
            iStrLatLng = "\(self.modelEventObj!.lat!),\(self.modelEventObj!.lng!)"
            
            let tapGestur = UITapGestureRecognizer(target: self, action:#selector(HB_EventInfoViewController.btnMapClicked(_:))) as UITapGestureRecognizer
            cell.map.addGestureRecognizer(tapGestur)
            
            
            
            let eventLocation = CLLocationCoordinate2DMake( Double((self.modelEventObj?.lat)!)!, Double((self.modelEventObj?.lng)!)!)
            // Drop a pin
            let dropPin = MKPointAnnotation()
            dropPin.coordinate = eventLocation
            dropPin.title = "Event Location"
            cell.map.addAnnotation(dropPin)
            
            let span = MKCoordinateSpanMake(0.001, 0.001)
            let region = MKCoordinateRegion(center: eventLocation, span: span)
            cell.map.setRegion(region, animated: true)
            
            cell.lblAddress.text=modelEventObj?.address
            cell.lblEventName.text=modelEventObj?.eventName
            cell.lblEventPepole.text=(modelEventObj?.eventAttendeeCount)! + " People going"
            
          
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            //  dateFormatter.timeZone = NSTimeZone(name: "IST")
            
            
            let date:NSDate = dateFormatter.dateFromString((modelEventObj?.eventDate)!)!
            
            dateFormatter.dateFormat = "EEEE"
            let dayOfWeekString = dateFormatter.stringFromDate(date)
            
            dateFormatter.dateFormat = "hh:mm:ss"
            let amDate = dateFormatter.dateFromString((modelEventObj?.eventStartTime)!)
            dateFormatter.dateFormat = "hh:mm aa"
            cell.lblDayTime.text = dayOfWeekString + " at " + dateFormatter.stringFromDate(amDate!)
            
           
            
         

            
            
            dateFormatter.dateFormat = "dd MMMM yyyy"
            
            //  let strDate:NSString=dateFormatter.stringFromDate(date)
            let strSuffixOfDate:NSString=daySuffix(date)
            print(strSuffixOfDate)
            
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: date)
            
            let year =  components.year
            let month = components.month
            let day = components.day
            
            let months = dateFormatter.monthSymbols
            let monthSymbol = months[month-1]
            
            
            cell.lblEventDate.text = String(day) + (strSuffixOfDate as String) + " " + monthSymbol + ", " + String(year)
            cell.lblEventDate.textColor = iAppDelegate.defaultColor
            
            
            iStrEventDate = cell.lblEventDate.text!
            let iUserDefaults = NSUserDefaults.standardUserDefaults()
            iUserDefaults.setObject(iStrEventDate, forKey: "eventDate")
            iUserDefaults.synchronize()
            
            cell.lblDaysCount.text = "\(date.daysFrom(NSDate())) days to go"
            cell.lblDaysCount.textColor = iAppDelegate.defaultColor
            
            // Configure the cell...
            //cell.textLabel?.text = "Row \(indexPath.row)"
            return cell
        }
        if indexPath.row == 1
        {
            let cell:HB_OtherDetailsCell = tableView.dequeueReusableCellWithIdentifier("HB_OtherDetailsCell", forIndexPath: indexPath) as! HB_OtherDetailsCell
            
            do {
                let html = "<style>body{color: #8B8A8A;font-size: 15px;}</style>" + (modelEventObj?.eventDescription)!
                let str = try NSAttributedString(data: (html.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!), options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                cell.txtVWEventDetail.attributedText=str
                
            } catch {
                print(error)
            }
            
            cell.btnMore.addTarget(self, action: #selector(HB_EventInfoViewController.more_btn_click(_:)), forControlEvents: .TouchUpInside)
            return cell
            
        }
        return cell1!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 325
            
        }
        if indexPath.row == 1
        {
            if iSShowMore {
                //    let cell:HB_OtherDetailsCell = tableView.cellForRowAtIndexPath(indexPath) as! HB_OtherDetailsCell
                return heightForView((modelEventObj?.eventDescription)!)+100
                
            }
            return 165
        }
        return 0
        
        
    }
    
    //    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    //    {
    //
    //        let headerView:UIView=UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.size.width, height: 250))
    //        let imgHeaderView:UIImageView=UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.size.width, height: 250))
    //
    //        imgHeaderView.image=UIImage(named: "apple")
    //        imgHeaderView.contentMode = .ScaleAspectFill
    //        headerView.addSubview(imgHeaderView)
    //        //headerView.clipsToBounds=true
    //        self.view.sendSubviewToBack(headerView)
    //        return headerView
    //
    //
    //
    //    }
    
    func daySuffix(date: NSDate) -> String {
        let calendar = NSCalendar.currentCalendar()
        let dayOfMonth = calendar.component(.Day, fromDate: date)
        switch dayOfMonth {
        case 1: fallthrough
        case 21: fallthrough
        case 31: return "st"
        case 2: fallthrough
        case 22: return "nd"
        case 3: fallthrough
        case 23: return "rd"
        default: return "th"
        }
    }
    
    func btnMapClicked(recognizer:UITapGestureRecognizer) {
        print("http://maps.google.com/?q=\(iStrLatLng)")
        UIApplication.sharedApplication().openURL(NSURL(string:
            "http://maps.google.com/?q=\(iStrLatLng)")!)
    }
    
    private func configureExpandingMenuButton() {
        let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let menuButtonSize: CGSize = CGSize(width: 50.0, height: 50.0)
        let centerImage : UIImageView = UIImageView(frame: CGRectMake(0, 0, 40, 40))
        centerImage.setImageWithString("", color: iAppDelegate.defaultColor,circular:true)
        
        let imgMain = UIImageView(frame: CGRectMake(0, 0, 40, 40))
            imgMain.image = UIImage(named: "chooser-button-tab")
        
        UIGraphicsBeginImageContext(CGSizeMake(40, 40))
        centerImage.image!.drawInRect(CGRectMake(0, 0, 40, 40))
        imgMain.image!.drawInRect(CGRectMake(8, 8, 24, 24))
        
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let menuButton = ExpandingMenuButton(frame: CGRect(origin: CGPointZero, size: menuButtonSize), centerImage: finalImage!, centerHighlightedImage:finalImage!)
        
        menuButton.center = CGPointMake(self.view.bounds.width - 45.0, self.view.bounds.height - 45.0)
        menuButton.addShadowView()
        self.view.addSubview(menuButton)
        self.view.bringSubviewToFront(menuButton);
        
        func showAlert(title: String) {
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        let imgView:UIImageView = UIImageView(frame: CGRectMake(0, 0, 30, 30))
        imgView.setImageWithString("", color: iAppDelegate.defaultColor,circular:true)
        
        let item1 = ExpandingMenuItem(size: menuButtonSize, title: " Call ", image: UIImage(named: "phone")!, highlightedImage: UIImage(named: "phone")!, backgroundImage: imgView.image, backgroundHighlightedImage: UIImage(named: "phone")) { () -> Void in
            self.viewCall.hidden = false
        }
        
        let item2 = ExpandingMenuItem(size: menuButtonSize, title: " Share ", image: UIImage(named: "sharing")!, highlightedImage: UIImage(named: "sharing")!, backgroundImage: imgView.image, backgroundHighlightedImage: UIImage(named: "sharing")) { () -> Void in
            
//            let dateFormatter = NSDateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd"
//            let date:NSDate = dateFormatter.dateFromString((self.modelEventObj?.eventDate)!)!
//            dateFormatter.dateFormat = "dd MMMM yyyy"
            //let strSuffixOfDate:NSString=self.daySuffix(date)
            
            let iEventName = "Event Name: \(self.modelEventObj!.eventName)"
            
            let iEventDate = "Event Date: \(self.iStrEventDate)"
           
            let iEventAddress = "Event Address: \(self.modelEventObj!.address)"
            
            let iEventMessage = "Join the event to network with people coming to the event."
            
            let iEventAppLink = "<html><body><a href='https://itunes.apple.com/us/app/vg-startup-summit-2016/id1145031697?ls=1&mt=8'>https://itunes.apple.com/us/app/vg-startup-summit-2016/id1145031697?ls=1&mt=8</a></body></html>"
            
                let objectsToShare = [iEventName,iEventDate,iEventAddress,iEventMessage,iEventAppLink]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.presentViewController(activityVC, animated: true, completion: nil)
            
        }
    
      
        
        menuButton.addMenuItems([item1, item2])
        
        menuButton.willPresentMenuItems = { (menu) -> Void in
            print("MenuItems will present.")
        }
        
        menuButton.didDismissMenuItems = { (menu) -> Void in
            print("MenuItems dismissed.")
        }
    }
    
    @IBAction func btnMailPressed(sender:UIButton) {
        
        if MFMailComposeViewController.canSendMail() {
            let iMailViewController:MFMailComposeViewController = MFMailComposeViewController()
            iMailViewController.mailComposeDelegate = self
            iMailViewController.setToRecipients([(self.modelEventObj?.email)!])
//            iMailViewController.setCcRecipients([(self.modelEventObj?.email)!])
            iMailViewController.setMessageBody("Hubilo send mail", isHTML: false)
            iMailViewController.setSubject("Hubilo Event")
            self.presentViewController(iMailViewController, animated: true, completion: nil)
        }
        
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        switch result {
        case .Sent:
            break;
        case .Saved:
            break;
        case .Failed:
            break;
        case .Cancelled:
            break;
        default:
            break;
        }
        self.dismissViewControllerAnimated(true, completion:nil)
    }
    
    @IBAction func btnCallPressed(sender:UIButton) {
        let url:NSURL = NSURL(string: "telprompt://"+(self.modelEventObj?.phone)!)!
        
        if (UIApplication.sharedApplication().canOpenURL(url))
        {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let location = locations.last
        
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        map.setRegion(region, animated: true)
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        if (annotation is MKUserLocation) {
            return nil
        }
        
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        }
        else {
            
            pinView?.annotation = annotation
        }
        
        return pinView
    }
    
    func colorWithHexString (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substringFromIndex(1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.grayColor()
        }
        
        let rString = (cString as NSString).substringToIndex(2)
        let gString = ((cString as NSString).substringFromIndex(2) as NSString).substringToIndex(2)
        let bString = ((cString as NSString).substringFromIndex(4) as NSString).substringToIndex(2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        NSScanner(string: rString).scanHexInt(&r)
        NSScanner(string: gString).scanHexInt(&g)
        NSScanner(string: bString).scanHexInt(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    @IBAction func actionWebsite (sender : AnyObject){
        let url = NSURL(string: (modelEventObj?.eventWebsite)!)
        if (UIApplication.sharedApplication().canOpenURL(url!))
        {
            let web : WebVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("WebVC") as! WebVC
            web.url = url
            self.presentViewController(web, animated: true, completion: nil)
            //UIApplication.sharedApplication().openURL(url!)
        }
    }
    
    @IBAction func actionMessage (sender : AnyObject){
        if sharedManager.isUserLoggedIn == false {
            SPGUtility.sharedInstance().showAlert("You must be logged in to see messages", titleMessage: "", viewController: self)
            return
        }
        let iMessagesVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_MessagesVC") as! HB_MessagesVC
        
        self.navigationController?.pushViewController(iMessagesVc, animated: true)
    }
    @IBAction func actionNotification (sender : AnyObject){
        if sharedManager.isUserLoggedIn == false {
            SPGUtility.sharedInstance().showAlert("You must be logged in to see notification", titleMessage: "", viewController: self)
            return
        }
        
         let iNotificationVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_NotificationVC") as! HB_NotificationVC
        
        self.navigationController?.pushViewController(iNotificationVc, animated: true)
    }
    
    @IBAction func btnCancelPopup(sender: AnyObject) {
        viewCall.hidden = true
    }
    /*
     // MARK: - Exapanding Menu
     
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension UIView {
    
    func addShadowView(width:CGFloat=1.2, height:CGFloat=1.2, Opacidade:Float=0.9, maskToBounds:Bool=true, radius:CGFloat=2.5){
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = Opacidade
        self.layer.masksToBounds = maskToBounds
    }
    
}

extension NSDate {
    func numberOfDaysUntilDateTime(toDateTime: NSDate, inTimeZone timeZone: NSTimeZone? = nil) -> Int {
        let calendar = NSCalendar.currentCalendar()
        if let timeZone = timeZone {
            calendar.timeZone = timeZone
        }
        
        var fromDate: NSDate?, toDate: NSDate?
        
        calendar.rangeOfUnit(.Day, startDate: &fromDate, interval: nil, forDate: self)
        calendar.rangeOfUnit(.Day, startDate: &toDate, interval: nil, forDate: toDateTime)
        
        let difference = calendar.components(.Day, fromDate: fromDate!, toDate: toDate!, options: [])
        return difference.day
    }
}

extension NSDate {
    func yearsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    func monthsFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    func weeksFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    func daysFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    func hoursFrom(date: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    func minutesFrom(date: NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    func secondsFrom(date: NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
    func offsetFrom(date: NSDate) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date))y"   }
        if monthsFrom(date)  > 0 { return "\(monthsFrom(date))M"  }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date))w"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date))d"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date))h"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date))m" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))s" }
        return ""
    }
}

//extension UIButton {
//    override func addShadowView(width:CGFloat=0.2, height:CGFloat=0.2, Opacidade:Float=0.7, maskToBounds:Bool=false, radius:CGFloat=0.5){
//        self.layer.shadowColor = UIColor.blackColor().CGColor
//        self.layer.shadowOffset = CGSize(width: width, height: height)
//        self.layer.shadowRadius = radius
//        self.layer.shadowOpacity = Opacidade
//        self.layer.masksToBounds = maskToBounds
//    }
//}
