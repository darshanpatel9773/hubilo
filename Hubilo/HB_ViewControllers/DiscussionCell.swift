//
//  DiscussionCell.swift
//  Hubilo
//
//  Created by Aadil on 8/1/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class DiscussionCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet var lblDescription : UILabel?
    @IBOutlet var imgUser : UIImageView?
    @IBOutlet var lblName : UILabel?
    @IBOutlet var lblDesignation : UILabel?
    @IBOutlet var btnLike : UIButton?
    @IBOutlet var btnComment : UIButton?
    @IBOutlet var btnView : UIButton?
    @IBOutlet var lblDate : UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
