//
//  HB_VenueMapVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 14/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController

class HB_VenueMapVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    let arrVenueItem:NSArray=["Vastrapur Ahmedabad","+91 9999999999"]
    
    let imgVenueItem:NSArray=["location_icon","call"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenu()

        
        // Do any additional setup after loading the view.
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        
    return arrVenueItem.count
    
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
     {
        let cell:HB_VenueMapCell=tableView.dequeueReusableCellWithIdentifier("HB_VenueMapCell", forIndexPath: indexPath) as! HB_VenueMapCell
        cell.imgMap.image=UIImage(named:imgVenueItem.objectAtIndex(indexPath.row) as! String)
        cell.imgMap.contentMode=UIViewContentMode.ScaleAspectFit
        cell.imgborder.layer.borderColor=UIColor.grayColor().CGColor
        cell.imgborder.layer.borderWidth=0.5
        cell.selectionStyle = .None
        cell.lblMapView.text=arrVenueItem.objectAtIndex(indexPath.row) as? String
        return cell
            
    
    }
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_SettingVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.title="VENUE MAP"
        
    }
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
