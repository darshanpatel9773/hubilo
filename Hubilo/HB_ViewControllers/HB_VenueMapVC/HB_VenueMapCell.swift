//
//  HB_VenueMapCell.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 14/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_VenueMapCell: UITableViewCell {

    @IBOutlet weak var imgborder: UIImageView!
    @IBOutlet weak var lblMapView: UILabel!
    @IBOutlet weak var imgMap: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
