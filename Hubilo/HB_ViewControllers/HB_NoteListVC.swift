//
//  HB_NoteListVC.swift
//  Hubilo
//
//  Created by Darshan Patel on 28/07/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import SwiftLoader

class HB_NoteListVC: UIViewController,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource {
    
    var searchBarButtonItem: UIBarButtonItem?
    var searchBar = UISearchBar()
    var sharedManager : Globals = Globals.sharedInstance
    var modelArrNoteList:NSMutableArray!
    var mutArrDisplaySearch:NSMutableArray!
    @IBOutlet weak var tblViewNoteList:UITableView!
    @IBOutlet weak var imgNoNoteList:UIImageView!
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mutArrDisplaySearch = NSMutableArray()
        modelArrNoteList = NSMutableArray()
        setMenu()
        // Do any additional setup after loading the view.
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.sizeToFit()
        let searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        searchBar.barTintColor = UIColor.clearColor()
        searchBar.tintColor = appDelegate.defaultColor
        if let textFieldInsideSearchBar = self.searchBar.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = UIColor.whiteColor()
            
        }
        
        let iBtnClear = searchTextField?.valueForKey("_clearButton") as! UIButton
        iBtnClear.setImage((iBtnClear.imageView?.image?.imageWithRenderingMode(.AlwaysTemplate)), forState: .Normal)
        
        iBtnClear.tintColor = UIColor.whiteColor()
        
        
        searchBarButtonItem = navigationItem.rightBarButtonItem
    }

    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_NoteListVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        self.navigationItem.title = "NOTE LIST"
        
        let mySerachbtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySerachbtn.setImage(UIImage(named: "HB_Search"), forState: UIControlState.Normal)
        mySerachbtn.frame = CGRectMake(0, 0, 24, 24)
        mySerachbtn.addTarget(self, action: #selector(HB_NoteListVC.searchButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomSearchButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySerachbtn)
        self.navigationItem.rightBarButtonItem = myCustomSearchButtonItem
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if ReachabilityCheck.isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            var userId : String = "0"
            if sharedManager.isUserLoggedIn{
                userId=sharedManager.currentUserID
            }
            if let iUrl = getUrlNoteList(Constants.eventInfo.kEventId, userID:userId) {
                SPGWebService.callGETWebService(iUrl) { (iData, iError) in
                     print(iData)
                    let dicNoteList:NSDictionary = iData as! NSDictionary
                    self.mutArrDisplaySearch = NSMutableArray()
                    self.modelArrNoteList = NSMutableArray()
                    if dicNoteList.valueForKey("data")?.count != 0
                    {
                        self.modelArrNoteList = (dicNoteList.valueForKey("data") as? NSArray)?.mutableCopy() as! NSMutableArray
                        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "time", ascending: false)
                        let sortedResults: NSArray = self.modelArrNoteList.sortedArrayUsingDescriptors([descriptor])
                        
                        self.modelArrNoteList = sortedResults.mutableCopy() as! NSMutableArray
                        
                        self.mutArrDisplaySearch = self.modelArrNoteList.mutableCopy() as! NSMutableArray
                    }
                    else
                    {
                        
                    }
                    
                    
                    
                    
                    SwiftLoader.hide()
                    
                    if  self.modelArrNoteList.count > 0 {
                        self.imgNoNoteList.hidden = true
                        self.tblViewNoteList.reloadData()
                    }else {
                        self.imgNoNoteList.hidden = false
                    }
                }
            } else {
                SPGUtility.sharedInstance().showAlert("oops, something went wrong with the URL", titleMessage: "", viewController: self)
                // oops, something went wrong with the URL
            }
            
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
            
        }
    }
  

    func getUrlNoteList(eventID: String,userID: String) -> NSURL? {
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["user_id" : userID,"event_id" : eventID], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.GET_ALL_NOTE + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    @IBAction func searchButtonTouched(sender: AnyObject){
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_NoteListVC.backButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        showSearchBar()
    }
    func showSearchBar() {
        self.searchBar.text = ""
        self.searchBar.hidden = false
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        self.searchBar.hidden = true
        navigationItem.titleView = nil
        setMenu()
        UIView.animateWithDuration(0.3, animations: {
            self.searchBar.alpha = 0
            }, completion: { finished in
        })
    }
    
    func backButtonTouched(sender: UIButton) {
        hideSearchBar()
        if self.mutArrDisplaySearch.count > 0 {
            self.mutArrDisplaySearch.removeAllObjects()
           self.mutArrDisplaySearch = self.modelArrNoteList!.mutableCopy() as! NSMutableArray
            self.tblViewNoteList.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mutArrDisplaySearch.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        

        let cell:HB_NoteListCell = tableView.dequeueReusableCellWithIdentifier("cellNoteList", forIndexPath: indexPath) as! HB_NoteListCell
        cell.selectionStyle = .None
        
        cell.lblAttendeeName.text = (mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String)! + " " + ((mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("lastname"))! as! String)
        let iStrDesignation:String = (mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("designation")) as! String
        let iStrOrginzation:String = (mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("organization")) as! String
        if  iStrDesignation.characters.count != 0 {
            cell.lbldesignation.text = iStrDesignation + " at " + iStrOrginzation
        }
        
        cell.imgVwAttandee.layer.cornerRadius = 25.0
        cell.imgVwAttandee.clipsToBounds = true
        if mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("profile_img") != nil {
            let url:String = (Constants.ImagePaths.kAttendeeImagePath + ((mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("profile_img"))! as! String))
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgVwAttandee.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else {
            let imgVw:UIImageView = UIImageView(frame: CGRectMake(27, 20, 51, 51))
            imgVw.setImageWithString(mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("firstname") as? String, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            cell.imgVwAttandee.image = imgVw.image
        }
        //note
        let iStrNote:String = (mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("note")) as! String
       
       
        
        cell.lblNote.text = iStrNote
        //cell.lblNote.text = "CFNetwork internal error (0xc01a:/BuildRoot/Library/Caches/com.apple.xbs/Sources/CFNetwork_Sim/CFNetwork-758.3.15/Loading/URLConnectionLoader.cpp:289) "
        let timeinterval:NSNumber = ((mutArrDisplaySearch?.objectAtIndex(indexPath.row).valueForKey("time")) as? NSNumber)!
                //Convert to Date
        let date = NSDate(timeIntervalSince1970: timeinterval.doubleValue)
        //Date formatting
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
        let dateString = dateFormatter.stringFromDate(date)
        let iArrDate:NSArray = dateString.componentsSeparatedByString("-")
        
        var iStrDay:String = ""
        iStrDay = iStrDay.daySuffix(date)
        
        let iStrDate:String = (iArrDate.objectAtIndex(0) as! String) + iStrDay + " " + (iArrDate.objectAtIndex(1) as! String) + ", " + (iArrDate.objectAtIndex(2) as! String)
        
        cell.lblDate.text = iStrDate
        cell.lblDate.textColor = iAppDelegate.defaultColor
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let noteString = ((modelArrNoteList?.objectAtIndex(indexPath.row).valueForKey("note")) as! String)
        
        var height : CGFloat = noteString.heightWithConstrainedWidth(260, font: UIFont.systemFontOfSize(15))
        if height < 30 {
            height = 30
        }
        return 75+height+5
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let objAttendInfoVC : HB_AttendeeInfoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("HB_AttendeeInfoVC") as! HB_AttendeeInfoVC
        objAttendInfoVC.dicAttendeeDInfo = NSMutableDictionary()
        objAttendInfoVC.dicAttendeeDInfo = mutArrDisplaySearch?.objectAtIndex(indexPath.row) as? NSMutableDictionary
        self.navigationController?.pushViewController(objAttendInfoVC, animated: true)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count == 0 {
            self.mutArrDisplaySearch = self.modelArrNoteList!.mutableCopy() as! NSMutableArray
        } else {
            self.mutArrDisplaySearch.removeAllObjects()
            self.mutArrDisplaySearch = self.modelArrNoteList!.mutableCopy() as! NSMutableArray
            let searchResults:NSArray = self.mutArrDisplaySearch!.filter{
                let industryName = $0["firstname"]!!.lowercaseString
                let industryPosition = $0["lastname"]!!.lowercaseString
                
                return industryName.rangeOfString(searchText.lowercaseString) != nil || industryPosition .rangeOfString(searchText.lowercaseString) != nil
            }
            self.mutArrDisplaySearch = searchResults.mutableCopy() as! NSMutableArray
        }
        self.tblViewNoteList.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
