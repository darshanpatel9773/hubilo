//
//  HB_AppointmentVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 28/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
class HB_AppointmentVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
 setMenu()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_AppointmentVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.navigationItem.title = "MY APPOINTMENTS"
        
    }
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
