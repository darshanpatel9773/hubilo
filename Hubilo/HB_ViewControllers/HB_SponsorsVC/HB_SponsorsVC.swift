//
//  HB_SponsorsVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 15/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import SwiftLoader
import Alamofire

class HB_SponsorsVC: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var tblSponsorsList: UITableView!
    var searchBar = UISearchBar()
    var searchBarButtonItem: UIBarButtonItem?
    var iMutArrSponserData:NSMutableArray!
    var iSearchArray:[HB_ModelSponsers]?
    var iOriginalArray:[HB_ModelSponsers]?
    var modelSPonserobj:HB_ModelSponsers?
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.sizeToFit()
        let searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        searchBar.barTintColor = UIColor.clearColor()
        searchBar.tintColor = appDelegate.defaultColor
        if let textFieldInsideSearchBar = self.searchBar.valueForKey("searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            glassIconView.tintColor = UIColor.whiteColor()
            
        }
        
        let iBtnClear = searchTextField?.valueForKey("_clearButton") as! UIButton
        iBtnClear.setImage((iBtnClear.imageView?.image?.imageWithRenderingMode(.AlwaysTemplate)), forState: .Normal)
        
        iBtnClear.tintColor = UIColor.whiteColor()
        
        
        searchBarButtonItem = navigationItem.rightBarButtonItem
        setMenu()
        iMutArrSponserData = NSMutableArray()
        
        if ReachabilityCheck.isConnectedToNetwork() {
            
            SwiftLoader.show(animated: true)
            
            
            
            iMutArrSponserData = NSMutableArray()
            
            
            
            Alamofire.request(.GET, getSponserList()!, parameters: [:])
                .responseJSON { response in
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        
                        SwiftLoader.hide()
                        let iDataDict = JSON as! NSDictionary
                        self.iMutArrSponserData = (iDataDict.valueForKey("data") as! NSArray).mutableCopy() as! NSMutableArray
                        self.iSearchArray = []
                        self.iOriginalArray = []
                        for obj in self.iMutArrSponserData {
                            self.iSearchArray?.append(HB_ModelSponsers(fromDictionary:
                                obj as! NSDictionary))
                            self.iOriginalArray?.append(HB_ModelSponsers(fromDictionary:
                                obj as! NSDictionary))
                        }
                        
                        self.tblSponsorsList.reloadData()
                    }
                    else {
                        // oops, something went wrong with the URL
                    }
                    
            }
        }
        else
        {
            SPGUtility.sharedInstance().showAlert("Please check network connection.", titleMessage: "", viewController: self)
            
            
        }
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        
        
        
        
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        guard (iSearchArray != nil) else {
            return 0
        }
        return iSearchArray!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        modelSPonserobj=iSearchArray![indexPath.row]
        
        let cell:HB_BookMarkCell=tableView.dequeueReusableCellWithIdentifier("HB_BookMarkCell", forIndexPath: indexPath) as! HB_BookMarkCell
        //cell.imgBookMarkCell.layer.cornerRadius =  cell.imgBookMarkCell.frame.size.width/2
        cell.selectionStyle = .None
        
        cell.lblCompany.text=""
        cell.lblWorkProfile.text=modelSPonserobj?.sponsorEmailid
        cell.lblName.text=modelSPonserobj?.sponsorName
        
        let url:String = (Constants.ImagePaths.kSponserImagePath + (modelSPonserobj?.sponsorLogoImg)! )
        
        cell.imgBookMarkCell.sd_setImageWithURL(NSURL(string: url), placeholderImage:UIImage(named:"HB_Sponsors"), completed:nil)
        
        return cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        modelSPonserobj=iSearchArray![indexPath.row]
        let objSponsorrDetail:HB_SponserInfoVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_SponserInfoVC") as! HB_SponserInfoVC
        
        objSponsorrDetail.modelSPonserobj = modelSPonserobj
        self.searchBar.resignFirstResponder()
        self.navigationController?.pushViewController(objSponsorrDetail, animated: true)
        //self.performSegueWithIdentifier(Constants.SegueConstant.kShowSponserInfoVC, sender: self)
        
    }
    //MARK: User Define
    func setMenu() {
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Normal)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Selected)
        myMenubtn.setImage(UIImage(named: "menu"), forState: UIControlState.Highlighted)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_SponsorsVC.menuButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        self.navigationItem.title="SPONSORS"
        
        let mySerachbtn:UIButton = UIButton(type: UIButtonType.Custom)
        mySerachbtn.setImage(UIImage(named: "HB_Search"), forState: UIControlState.Normal)
        mySerachbtn.frame = CGRectMake(0, 0, 24, 24)
        mySerachbtn.addTarget(self, action: #selector(HB_SponsorsVC.searchButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomSearchButtonItem:UIBarButtonItem = UIBarButtonItem(customView: mySerachbtn)
        self.navigationItem.rightBarButtonItem = myCustomSearchButtonItem
        
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if iOriginalArray?.count > 0 {
        if searchText.characters.count == 0 {
            self.iSearchArray?.removeAll()
            for dis in self.iOriginalArray!
            {
                self.iSearchArray?.append(dis)
            }
        } else {
            self.iSearchArray?.removeAll()
            for dis in self.iOriginalArray!
            {
                self.iSearchArray?.append(dis)
            }
            for dis in (self.iSearchArray)!
            {
                if dis.sponsorName.lowercaseString.containsString(searchText.lowercaseString) || dis.sponsorEmailid.lowercaseString.containsString(searchText.lowercaseString) || dis.sponsorTitle.lowercaseString.containsString(searchText.lowercaseString)
                {
                    
                }
                else{
                    self.iSearchArray?.removeAtIndex((self.iSearchArray?.indexOf(dis)!)!)
                }
            }
        }
        
        self.tblSponsorsList!.reloadData()
      }
        
    }
    @IBAction func searchButtonTouched(sender: AnyObject){
        let myMenubtn:UIButton = UIButton(type: UIButtonType.Custom)
        myMenubtn.setImage(UIImage(named: "back_icon"), forState: UIControlState.Normal)
        myMenubtn.frame = CGRectMake(0, 0, 24, 24)
        myMenubtn.addTarget(self, action: #selector(HB_SponsorsVC.backButtonTouched(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myMenubtn)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        showSearchBar()
    }
    func showSearchBar() {
        self.searchBar.text = ""
        self.searchBar.hidden = false
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.searchBar.alpha = 1
            }, completion: { finished in
                self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        self.searchBar.hidden = true
        navigationItem.titleView = nil
        setMenu()
        UIView.animateWithDuration(0.3, animations: {
            self.searchBar.alpha = 0
            }, completion: { finished in
        })
    }
    
    func backButtonTouched(sender: UIButton) {
        hideSearchBar()
        self.iSearchArray?.removeAll()
        if iOriginalArray?.count > 0 {
            for dis in self.iOriginalArray!
            {
                self.iSearchArray?.append(dis)
            }
            self.tblSponsorsList!.reloadData()
        }
    }
    
    @IBAction func menuButtonTouched(sender: AnyObject) {
        //        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        //        [elDrawer setDrawerState:DrawerStateOpened animated:YES];
        
        let elDrawer:KYDrawerController = self.navigationController?.parentViewController as! KYDrawerController
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Opened, animated: true)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getSponserList() -> NSURL? {
        var iUrl:NSURL = NSURL()
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(["event_id" : Constants.eventInfo.kEventId], options: NSJSONWritingOptions.init(rawValue: 0))
            let urlstr = NSString(data: jsonData, encoding: NSUTF8StringEncoding)!
            let url = (Constants.URL.SPONSOR_LIST + (urlstr as String)).stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            iUrl = NSURL(string:url as String)!
        }catch let error {
            print(error)
        }
        return iUrl
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
