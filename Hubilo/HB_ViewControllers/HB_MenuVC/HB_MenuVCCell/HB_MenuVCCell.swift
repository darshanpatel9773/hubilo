//
//  HB_MenuVCCell.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 11/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit

class HB_MenuVCCell: UITableViewCell {

    @IBOutlet weak var heightContraints: NSLayoutConstraint!
    @IBOutlet weak var imgVWMenuIcon: UIImageView!
    @IBOutlet weak var lblMenuItem: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
