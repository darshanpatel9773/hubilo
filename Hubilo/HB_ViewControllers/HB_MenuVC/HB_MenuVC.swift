//
//  HB_MenuVC.swift
//  Hubilo
//
//  Created by Pawan jaiswal on 11/06/16.
//  Copyright ©2016 nirav. All rights reserved.
//

import UIKit
import KYDrawerController
import SwiftLoader
import SDWebImage
class HB_MenuVC: UIViewController,UITableViewDelegate,UITableViewDataSource, KYDrawerControllerDelegate {

    @IBOutlet var btnLarge : UIButton?
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//    var modelEventObj:HB_ModelEvent?
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var btnEditUserProfile: UIButton!
    @IBOutlet weak var imgBackground:UIImageView!
    @IBOutlet var lblName : UILabel?
    @IBOutlet var lblEmail : UILabel?
    @IBOutlet var loginBtn : UIButton?
    var sharedManager : Globals = Globals.sharedInstance
    let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
 
    @IBOutlet var constraintleft: NSLayoutConstraint!
    
    @IBOutlet var constarintNotes: NSLayoutConstraint!
    var arravailbleMenuItem:NSMutableArray?
    let arrMenuItems:NSArray = ["Event info", "Schedule", "Speakers","Attendees","My Appointments","Poll","Discussion Forum", "Exhibitors","Venue Map","Sponsors","Settings"]
    let arrMenuImages:NSArray = ["event_info_icon", "schedule_icon", "speaker_icon","attendee_icon","my_appointments_icon","ic_poll","discussioin_icon","exhibitors_icon","venue_icon","sponsers_icon","settings_icon"]
    var isEditProfile:Bool=false
    let arrUsrMenuItems:NSArray = ["Edit Profile", "Notes List", "Bookmark List","Logout"]
    
    let arrUsrMenuImages:NSArray = ["editprofile_icon", "notelist_icon", "bookmark_list_icon","logout_icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
        
        
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HB_MenuVC.getMenuItems), name:"GetMenuItens", object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HB_MenuVC.setHeaderData), name:"SetHeaderData", object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HB_MenuVC.setUserImage), name:"setUserImage", object: nil)
       
        
        // Do any additional setup after loading the view.
    }
    
    func setHeaderData() {
        lblEmail?.text=sharedManager.currentUser.emailid
        lblName?.text=sharedManager.currentUser.firstname  + " " + sharedManager.currentUser.lastname
    }
    
    func setUserImage() {
        self.imgUserProfile.image = iAppDelegate.imageViewUser.image
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        imgUserProfile .layer.cornerRadius =  imgUserProfile.frame.size.width/2
        imgUserProfile.clipsToBounds = true
        let elDrawer:KYDrawerController=(self.navigationController?.parentViewController as? KYDrawerController)!
        elDrawer.delegate=self
        
        if !sharedManager.isUserLoggedIn {
            constraintleft.constant = (self.view.frame.size.width - imgUserProfile.frame.size.width)/2
            self.view.layoutIfNeeded()
        }else {
            constraintleft.constant = 20
            self.view.layoutIfNeeded()
        }
    }
    
    func drawerController(drawerController: KYDrawerController, stateChanged state: KYDrawerController.DrawerState) {
//        if (self.sharedManager.backgroundImg != nil){
//            imgBackground.sd_setImageWithURL(NSURL(string: sharedManager.backgroundImg))
//        }
        guard (sharedManager.currentUser != nil) else {
            loginBtn?.hidden=false
            btnLarge?.hidden=false
            lblName?.hidden=true
            lblEmail?.hidden=true
            btnEditUserProfile.hidden=true
            return
        }
        
      
        
        
        btnEditUserProfile.hidden=false
        lblName?.hidden=false
        lblEmail?.hidden=false
        loginBtn?.hidden=true
        btnLarge?.hidden=true
        lblEmail?.text=sharedManager.currentUser.emailid
        lblName?.text=sharedManager.currentUser.firstname  + " " + sharedManager.currentUser.lastname
      
        
        if (NSUserDefaults.standardUserDefaults().objectForKey("profile_pic") != nil) {
            if  let imageData = NSData(base64EncodedString: NSUserDefaults.standardUserDefaults().objectForKey("profile_pic") as! String, options: NSDataBase64DecodingOptions(rawValue: 0)){
            let decodedimage = UIImage(data: imageData)
            self.imgUserProfile.image = decodedimage
        }
        }else {
        
        if (self.sharedManager.currentUser.profileImg != nil){
            let url:String = Constants.ImagePaths.kAttendeeImagePath + self.sharedManager.currentUser.profileImg
            let imgVw:UIImageView = UIImageView(frame: self.imgUserProfile.frame)
            imgVw.setImageWithString(sharedManager.currentUser.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            self.imgUserProfile.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        }else if (self.sharedManager.fbPic != nil){
            let url:String = self.sharedManager.fbPic
            let imgVw:UIImageView = UIImageView(frame: self.imgUserProfile.frame)
            imgVw.setImageWithString(sharedManager.currentUser.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
            self.imgUserProfile.sd_setImageWithURL(NSURL(string: url), placeholderImage:imgVw.image, completed:nil)
        } else {
                let imgVw:UIImageView = UIImageView(frame: self.imgUserProfile.frame)
                imgVw.setImageWithString(sharedManager.currentUser.firstname, color: UIColor(red: 113/255.0, green: 113/255.0, blue: 113/255.0, alpha: 1.0))
                self.imgUserProfile.image = imgVw.image
            }
        
        }
    }
    
        
    
    
    override func viewDidAppear(animated: Bool) {
        
       
    }
    
    // MARK: UITableViewDelegate&DataSource methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isEditProfile {
            return arrUsrMenuItems.count
        }
        return arrMenuItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      
        let cell:HB_MenuVCCell = tableView.dequeueReusableCellWithIdentifier("HB_MenuVCCell", forIndexPath: indexPath) as! HB_MenuVCCell
        if isEditProfile
        {
            cell.lblMenuItem.text = arrUsrMenuItems.objectAtIndex(indexPath.row) as? String
            cell.imgVWMenuIcon.image = UIImage(named:arrUsrMenuImages.objectAtIndex(indexPath.row) as! String)
            cell.selectionStyle = .Gray
        }
        else
        {
            cell.lblMenuItem.text = arrMenuItems.objectAtIndex(indexPath.row) as? String
            cell.imgVWMenuIcon.image = UIImage(named:arrMenuImages.objectAtIndex(indexPath.row) as! String)
            cell.selectionStyle = .Gray
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        /*
         KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
         UINavigationController *navController;
         */
        
        
        
//        let selectedCell = tblMenu.cellForRowAtIndexPath(indexPath) as! HB_MenuVCCell
//        selectedCell.lblMenuItem.textColor = appDelegate.defaultColor
//        selectedCell.imgVWMenuIcon.image = selectedCell.imgVWMenuIcon.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
//        selectedCell.imgVWMenuIcon.tintColor = iAppDelegate.defaultColor
        
        
      let elDrawer:KYDrawerController=(self.navigationController?.parentViewController as? KYDrawerController)!
        var navController:UINavigationController?
        if isEditProfile
        {
//            iAppDelegate.selectedIndexEdit = -1
//            let selectedCell = tblMenu.cellForRowAtIndexPath(indexPath) as! HB_MenuVCCell
//            selectedCell.lblMenuItem.textColor = appDelegate.defaultColor
//            selectedCell.imgVWMenuIcon.image = selectedCell.imgVWMenuIcon.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
//            selectedCell.imgVWMenuIcon.tintColor = iAppDelegate.defaultColor
            if indexPath.row == 0
            {
                let objEditPro:HB_EditProfileVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_EditProfileVC") as! HB_EditProfileVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //return;
            //HB_EditProfileVC
            }
            if indexPath.row == 1
            {
               let  objNoteListVc:HB_NoteListVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_NoteListVC") as! HB_NoteListVC
                navController=UINavigationController.init(rootViewController: objNoteListVc)

            }
            if indexPath.row == 2
            {
                let objEditPro:HB_BookMarkVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_BookMarkVC") as! HB_BookMarkVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
//            if indexPath.row == 3 {
//                
//                let objEditPro:HB_SettingVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_SettingVC") as! HB_SettingVC
//                navController=UINavigationController.init(rootViewController: objEditPro)
//            }
            if indexPath.row == 3 {
                sharedManager.isUserLoggedIn=false
                sharedManager.currentUser=nil
                isEditProfile = !isEditProfile
                let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
                userDefaults.setBool(false, forKey: "login")
                userDefaults.setObject(nil, forKey: "userInfo")
                userDefaults.setObject(0, forKey: "userId")
                self.imgUserProfile.image = UIImage(named: "login_default")
                self.tblMenu.reloadData()
                let objEditPro:HB_EventInfoViewController=self.storyboard?.instantiateViewControllerWithIdentifier("HB_EventInfoViewController") as! HB_EventInfoViewController
                navController=UINavigationController.init(rootViewController: objEditPro)
            }
            
        }
        else
        {
           // iAppDelegate.selectedIndexMain = -1
            
            if indexPath.row == 0
            {
                let objEditPro:HB_EventInfoViewController=self.storyboard?.instantiateViewControllerWithIdentifier("HB_EventInfoViewController") as! HB_EventInfoViewController
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
            if indexPath.row == 1
            {
                let objEditPro:HB_ScheduleVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_ScheduleVC") as! HB_ScheduleVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
            if indexPath.row == 2
            {
                let objEditPro:HB_SpeakersVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_SpeakersVC") as! HB_SpeakersVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
            if indexPath.row == 3
            {
                let objEditPro:HB_AttendeeVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_AttendeeVC") as! HB_AttendeeVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
            if indexPath.row == 4
            {
                let objEditPro:HB_AppointmentVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_AppointmentVC") as! HB_AppointmentVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
            if indexPath.row == 5
            {
                let objEditPro:HB_PollListVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_PollListVC") as! HB_PollListVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
           
            if indexPath.row == 6
            {
                let objDiscussion:DiscussionVC=self.storyboard?.instantiateViewControllerWithIdentifier("DiscussionVC") as! DiscussionVC
                navController=UINavigationController.init(rootViewController: objDiscussion
                )
                //HB_EditProfileVC
            }
            if indexPath.row == 7
            {
                let objEditPro:HB_ExihibitorsVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_ExihibitorsVC") as! HB_ExihibitorsVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
            if indexPath.row == 8
            {
                let objEditPro:HB_VenueMapVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_VenueMapVC") as! HB_VenueMapVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
            if indexPath.row == 9
            {
                let objEditPro:HB_SponsorsVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_SponsorsVC") as! HB_SponsorsVC
                navController=UINavigationController.init(rootViewController: objEditPro)
                //HB_EditProfileVC
            }
            if indexPath.row == 10 {
                
                let objEditPro:HB_SettingVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_SettingVC") as! HB_SettingVC
                navController=UINavigationController.init(rootViewController: objEditPro)
            }
            
        }
        
        elDrawer.mainViewController=navController;
        elDrawer.setDrawerState(KYDrawerController.DrawerState.Closed, animated: true)
        
    }
    
//    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
//        let selectedCell = tblMenu.cellForRowAtIndexPath(indexPath) as! HB_MenuVCCell
//        
//            selectedCell.lblMenuItem.textColor = UIColor.lightGrayColor()
//            selectedCell.imgVWMenuIcon.image = selectedCell.imgVWMenuIcon.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
//            selectedCell.imgVWMenuIcon.tintColor =  UIColor(red: 121/255.0, green: 121/255.0, blue: 121/255.0, alpha: 1.0)
//       
//       
//        
//        
//        
//    }
    
    
    @IBAction func actionLogin (sender : AnyObject) {
        let elDrawer:KYDrawerController=(self.navigationController?.parentViewController as? KYDrawerController)!
        var navController:UINavigationController?
        let objEditPro:HB_LoginVC=self.storyboard?.instantiateViewControllerWithIdentifier("HB_LoginVC") as! HB_LoginVC
        let app : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        app.window?.rootViewController?.presentViewController(objEditPro, animated: true, completion: { 
            elDrawer.setDrawerState(KYDrawerController.DrawerState.Closed, animated: true)
        })
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
    
        if isEditProfile
        {
            return 50
        }
        else
        {
            

            

            return 50;
        
        
        }
        
        
    
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   //MARK: - Button Click Events
    
    @IBAction func btn_edit_click(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        if !isEditProfile {
            isEditProfile = true
            tblMenu.indexPathsForSelectedRows?.forEach {
                tblMenu.deselectRowAtIndexPath($0, animated: true)
            }
            //iAppDelegate.selectedIndexEdit = 0
            btn.setImage(UIImage(named: "ic_up"), forState: UIControlState.Normal)
        }
        else
        {
            //iAppDelegate.selectedIndexMain = 0
            tblMenu.indexPathsForSelectedRows?.forEach {
                tblMenu.deselectRowAtIndexPath($0, animated: true)
            }
            btn.setImage(UIImage(named: "ic_down"), forState: UIControlState.Normal)
            isEditProfile=false
        }
        tblMenu.reloadData()
    }
    
    func getMenuItems()
    {
        print(appDelegate.eventInfoDic)
        arravailbleMenuItem = appDelegate.eventInfoDic?.valueForKey("sideBar") as? NSMutableArray
        let iAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        imgBackground.backgroundColor = iAppDelegate.defaultColor
        //print(arravailbleMenuItem)
        tblMenu.reloadData()
    }
    
    
    
    /*
    // MARK: - Navigation
     

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
